#!/bin/bash
TIMEFOLDER="2023-05-07-03-42"
#$(date +"%Y-%m-%d-%H-%M")
DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_bg_msi_dataset_strech_contr_step_64_size_192'
DATADIR_TEXT_AUG='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_ut_dataset_50step_192x192'
DATASET='greek960_bg'
#-re 120
MODE='msi'
NOISE_LVLs=(0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)
PREV_NOISE_LVLs=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9)
#0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)
RE_EPOCHs=(100 140 180 220 260 300 340 380 420 460)
BANDS=("MB365UV_001_F" "MB625Rd_007_F" "MB455RB_002_F")
for i in ${!NOISE_LVLs[@]}
do
NOISE_LVL=${NOISE_LVLs[${i}]}
PREV_NOISE_LVL=${PREV_NOISE_LVLs[${i}]}
RE_EPOCH=${RE_EPOCHs[${i}]}
    if [ ${NOISE_LVL} == 0.0 ]
  then
    EPOCH_STEP=80
    LR=0.0004
    FREQ_SAVE=20
    RESTDIR="none"
    OLDTIMEFOLDER="none"
    PREV_NOISE_LVL=0.0
  else
    OLDTIMEFOLDER=${TIMEFOLDER}
    EPOCH_STEP=40
    FREQ_SAVE=20
    LR=0.0004
    RESTDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/background/Greek960_msi/pixelcnn-${MODE}-${OLDTIMEFOLDER}_with_text_aug/noise_${PREV_NOISE_LVL}"
  fi
echo "Noise level: ${NOISE_LVL}"
  echo "Run for ${EPOCH_STEP} steps"
  SAVEDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/background/Greek960_msi/pixelcnn-${MODE}-${TIMEFOLDER}_with_text_aug/noise_${NOISE_LVL}"
  python train_bg_text_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -aug -n 32 --nr_gpu 2 -b 8 -t ${FREQ_SAVE} -q 3 -m 3 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH} -sl ${NOISE_LVL}\
   -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_TEXT_AUG} -bands ${BANDS[@]}
done