""" serve up some ancient scroll data """

import os
import sys
import glob
import random
import matplotlib.pyplot as plt
import numpy as np
from skimage import transform,io
from skimage.transform import PiecewiseAffineTransform, warp
import copy

def wrap_img(image,quarter_idx,x_scale,y_scale,shift_scale,shift_sign):
    """
    Piecewise affine transformation of char image patch.
    The horizontal coordinate is bend according to sinusoid quarter cycle
    image - [N1,N2] numpy array
    quarter_idx - int, index of sinusoid quarter cycle starting from 0 degrees,e.g. 0-3
    coord_scale - float, by how much image coordinate should spread or shrink, inverse proportion [0.8,1.2]
    shift_scale - float, scaling of sinusoid amplitude [15,18]
    Returns:
        out - [N1,N2] numpy array
    """
    rows, cols = image.shape[0], image.shape[1]
    src_cols = np.linspace(0, cols, 20)
    src_rows = np.linspace(0, rows, 10)
    src_rows, src_cols = np.meshgrid(src_rows, src_cols)
    src = np.dstack([src_cols.flat, src_rows.flat])[0]
    # add sinusoidal oscillation to row coordinates
    dst_rows = src[:, 1] - shift_sign*np.sin(np.linspace(quarter_idx*np.pi/2+np.pi/4,
                                              (1+quarter_idx)*np.pi/2+np.pi/4, src.shape[0])) * shift_scale
    dst_cols = x_scale * src[:, 0]
    dst_rows *= y_scale
    dst_rows =dst_rows - shift_sign*int(y_scale * shift_scale)
    dst = np.vstack([dst_cols, dst_rows]).T

    tform = PiecewiseAffineTransform()
    tform.estimate(src, dst)

    out = warp(image, tform, output_shape=(rows, cols))
    #reject wraping if character is out of frame
    reject_warp = False
    if np.sum(out) < 0.3 * rows * cols:
        reject_warp = check_if_img_empty(out,rows,cols)
    if reject_warp:
        out = image
    return out

def check_if_img_empty(out,rows,cols):
    reject_warp = False
    if np.sum(out) < 0.15 * rows * cols:
        reject_warp = True
    rows_sum = np.sum(out, 1)
    if not reject_warp:
        counter = 0
        if rows_sum[0] == 0:
            for i in range(rows):
                empty_line = rows_sum[i] == 0
                if empty_line:
                    counter += 1
                    if counter > 0.5 * rows:
                        reject_warp = True
                        break
                else:
                    break

    if not reject_warp:
        counter = 0
        if rows_sum[-1] == 0:
            for i in range(rows):
                empty_line = rows_sum[-i-1] == 0
                if empty_line:
                    counter += 1
                    if counter > 0.5 * rows:
                        reject_warp = True
                        break
                else:
                    break
    return reject_warp

def im_aug(x):
    #wrap image
    quarter_idx = random.randint(0,3)
    x_scale = random.uniform(0.85,1.1)
    y_scale = random.uniform(0.85,1.1)
    shift_scale = random.uniform(0,10)
    shift_sign = random.choice([-1,1])
    for i in range(x.shape[-1]):
        x[:,:,i] = wrap_img(x[:,:,i],quarter_idx,x_scale,y_scale,shift_scale,shift_sign)
    return x

def data_aug(x):
    #distort each image in a batch
    new_x = []
    for i in range(x.shape[0]):
        new_x.append(im_aug(x[i]))
    return np.array(new_x)

def preprocess_image(im):
    im = transform.resize(im,(64,64),preserve_range=True)
    im[im>0.9] = 1
    im[im<1] = 0
    return im[:,:,np.newaxis]

def gray_2_rgb(im):
    im = np.repeat(im,3,2)
    return im

def load_data(subset_path):
    file_list = get_file_list(subset_path)
    ims = []
    for impath in file_list:
        im = io.imread(impath)
        im = preprocess_image(im)
        ims.append(im)
    return np.array(ims)

def get_file_list(subset_path):
    """path are stored as dataset/folio/line/*.png"""
    return glob.glob(os.path.join(subset_path,"*","*","*.png"))

def load(data_dir, subset='train'):
    if subset == 'train':
        return load_data(os.path.join(data_dir, 'train'))
    elif subset == 'val':
        return load_data(os.path.join(data_dir, 'val'))
    elif subset == 'test':
        return load_data(os.path.join(data_dir, 'test'))
    else:
        raise NotImplementedError('subset should be either train or test')


class DataLoader(object):
    """ an object that generates batches of CIFAR-10 data for training """

    def __init__(self, data_dir, subset, batch_size, rng=None, shuffle=False, return_labels=False,augment=False):
        """
        - data_dir is location where to store files
        - subset is train|test
        - batch_size is int, of #examples to load at once
        - rng is np.random.RandomState object for reproducibility
        - binary, if True convert data to binary values
        - augment, add augmentation to the data
        """

        self.data_dir = data_dir
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.return_labels = return_labels
        self.augment = augment

        # create temporary storage for the data, if not yet created
        if not os.path.exists(data_dir):
            print('creating folder', data_dir)
            os.makedirs(data_dir)

        # load Greek960 training data to RAM
        self.data = load(data_dir, subset=subset)


        self.p = 0  # pointer to where we are in iteration
        self.rng = np.random.RandomState(1) if rng is None else rng

    def get_observation_size(self):
        return self.data.shape[1:]


    def reset(self):
        self.p = 0

    def __iter__(self):
        return self

    def __next__(self, n=None):
        """ n is the number of examples to fetch """
        if n is None: n = self.batch_size

        # on first iteration lazily permute all data
        if self.p == 0 and self.shuffle:
            inds = self.rng.permutation(self.data.shape[0])
            self.data = self.data[inds]

        # on last iteration reset the counter and raise StopIteration
        if self.p + n > self.data.shape[0]:
            self.reset()  # reset for next time we get called
            raise StopIteration

        # on intermediate iterations fetch the next batch
        x = copy.deepcopy(self.data[self.p: self.p + n])
        if self.augment:
            x = data_aug(x)
        self.p += self.batch_size

        return x

    next = __next__  # Python 2 compatibility (https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator)

if __name__=="__main__":
    data_dir = r"c:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_dataset_50step_192x192"
    subset = "test"
    nb_iter = 2
    im_size = 64
    sigma = 0.2
    fig, ax = plt.subplots(nb_iter, 10)
    for j in range(nb_iter):
        data = DataLoader(data_dir,subset,32,rng=j,augment=True)
        obs_shape = data.get_observation_size()
        print(np.amax(data.__next__(32)))
        print("Image shape:",obs_shape)
        assert len(obs_shape) == 3, 'assumed right now'
        ims = data.__next__()
        ims += sigma*np.random.normal(0.0, 1.0, ims.shape)
        ims_new = []
        empy_list = []
        for i in range(len(ims)):
            im = ims[i]
            #im[32:,:,0] = 0
            reject_warp = False
            #if np.sum(im) < 0.3 * im_size * im_size:
            empty = check_if_img_empty(im,im_size,im_size)
            #else:
            #empty=False
            empy_list.append(empty)
            ims_new.append(im)
        ims = np.array(ims_new)
        for i in range(10):
            ax[j,i].imshow(np.squeeze(ims[i]), cmap="gray")
            ax[j,i].set_title(str(empy_list[i]))
            ax[j,i].axis('off')
    plt.show()