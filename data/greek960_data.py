""" serve up some ancient scroll data """

import os
import glob
import numpy as np
from skimage import transform,io
from greek960_data_bin import data_aug
from matplotlib import pyplot as plt


def preprocess_image(im):
    im = transform.resize(im,(64,64),preserve_range=True)
    return im[:,:,np.newaxis]

def gray_2_rgb(im):
    im = np.repeat(im,3,2)
    return im

def load_data(subset_path):
    file_list = get_file_list(subset_path)
    ims = []
    for impath in file_list:
        im = io.imread(impath)
        im = preprocess_image(im)
        ims.append(im)
    return np.array(ims)

def get_file_list(subset_path):
    """path are stored as dataset/folio/line/*.png"""
    return glob.glob(os.path.join(subset_path,"*","*","*.png"))

def load(data_dir, subset='train'):
    if subset == 'train':
        return load_data(os.path.join(data_dir, 'train'))
    elif subset == 'val':
        return load_data(os.path.join(data_dir, 'val'))
    elif subset == 'test':
        return load_data(os.path.join(data_dir, 'test'))
    else:
        raise NotImplementedError('subset should be either train or test')


class DataLoader(object):
    """ an object that generates batches of CIFAR-10 data for training """

    def __init__(self, data_dir, subset, batch_size, rng=None, shuffle=False, return_labels=False, augment=True):
        """
        - data_dir is location where to store files
        - subset is train|test
        - batch_size is int, of #examples to load at once
        - rng is np.random.RandomState object for reproducibility
        """

        self.data_dir = data_dir
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.return_labels = return_labels
        self.augment = augment

        # create temporary storage for the data, if not yet created
        if not os.path.exists(data_dir):
            print('creating folder', data_dir)
            os.makedirs(data_dir)

        # load CIFAR-10 training data to RAM
        self.data = load(data_dir, subset=subset)


        self.p = 0  # pointer to where we are in iteration
        self.rng = np.random.RandomState(1) if rng is None else rng

    def get_observation_size(self):
        return self.data.shape[1:]


    def reset(self):
        self.p = 0

    def __iter__(self):
        return self

    def __next__(self, n=None):
        """ n is the number of examples to fetch """
        if n is None: n = self.batch_size

        # on first iteration lazily permute all data
        if self.p == 0 and self.shuffle:
            inds = self.rng.permutation(self.data.shape[0])
            self.data = self.data[inds]

        # on last iteration reset the counter and raise StopIteration
        if self.p + n > self.data.shape[0]:
            self.reset()  # reset for next time we get called
            raise StopIteration

        # on intermediate iterations fetch the next batch
        x = self.data[self.p: self.p + n]
        if self.augment:
            x = data_aug(x)
        self.p += self.batch_size

        return x

    next = __next__  # Python 2 compatibility (https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator)

if __name__=="__main__":
    data_dir = r"c:\Data\PhD\text_line_segmentation\datasets\Greek_960\Greek960_dataset_50step_192x192"
    subset = "test"
    data = DataLoader(data_dir,subset,32)
    obs_shape = data.get_observation_size()
    print(np.amax(data.__next__(32)))
    print("Image shape:",obs_shape)
    assert len(obs_shape) == 3, 'assumed right now'
    fig, ax = plt.subplots(1, 10)
    for i in range(10):
        ims = data.__next__(1)
        ax[i].imshow(np.squeeze(ims), cmap="gray")
    plt.show()