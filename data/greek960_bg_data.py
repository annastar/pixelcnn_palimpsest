""" serve up some ancient scroll data """
import math
import os
import random
import sys
import glob
import numpy as np
from skimage import transform,io
from matplotlib import pyplot as plt
import copy


def preprocess_image(im):
    im = transform.resize(im,(64,64),preserve_range=True)
    return im[:,:,np.newaxis]

def gray_2_rgb(im):
    im = np.repeat(im,3,2)
    return im

def load_data(subset_path):

    file_list = get_file_list(subset_path)
    ims = []
    for impath in file_list:
        im = io.imread(impath)
        im = preprocess_image(im)
        ims.append(im)
    ims = np.array(ims)
    if np.amax(ims)<=1.0:
        raise IOError("The bg images should on the scale [0,255] not [{},{}]".format(np.amin(ims),np.amax(ims)))
    return np.array(ims)

def get_file_list(subset_path):
    """path are stored as dataset/folio/line/*.png"""
    return glob.glob(os.path.join(subset_path,"*","*","*.png"))

def load(data_dir, subset='train'):
    if subset == 'train':
        return load_data(os.path.join(data_dir, 'train'))
    elif subset == 'val':
        return load_data(os.path.join(data_dir, 'val'))
    elif subset == 'test':
        return load_data(os.path.join(data_dir, 'test'))
    else:
        raise NotImplementedError('subset should be either train or test')

def load_aug_text(data_dir, subset='train'):
    if subset == 'train':
        return load_aug_text_data(os.path.join(data_dir, 'train'))
    elif subset == 'val':
        return load_aug_text_data(os.path.join(data_dir, 'val'))
    elif subset == 'test':
        return load_aug_text_data(os.path.join(data_dir, 'test'))
    else:
        raise NotImplementedError('subset should be either train or test')


def read_aug_text_mask(mask_fpath):
    mask = io.imread(mask_fpath)
    mask = transform.resize(mask, (64, 64), preserve_range=False)
    mask[mask > 0.9] = 1
    mask[mask < 1] = 0
    return mask[:,:,np.newaxis]

def calc_aug_text_contr(x):
    """Calculate augmentation mask scalar based on background constant.
    This is done for the reason if the mask is too dark or too bright the
     relief of background would be flatten by clipping operation
     """
    nbs = x.shape[0]
    x_mean = np.mean(x,axis=(1,2,3))
    res = 255.0 - x_mean
    std = np.sqrt(1.5*res)
    sign = np.random.choice([-1,1],nbs)
    contr = sign*np.random.normal(0, std,nbs)
    return contr[:,np.newaxis,np.newaxis,np.newaxis]

def overlay_with_text(x, mask):
    contrs = calc_aug_text_contr(x)
    scaled_mask = contrs * mask
    x = x + scaled_mask
    x = np.clip(x,a_min=0.,a_max=255.)
    x = x.astype(np.uint8)
    return x

def load_aug_text_data(subset_path):
    file_list = get_file_list(subset_path)
    ims = []
    for impath in file_list:
        im = read_aug_text_mask(impath)
        ims.append(im)
    return np.array(ims)

def im_aug(x):
    flip_hor = random.choice([True,False])
    if flip_hor:
        x = x[::-1, :]
    flip_ver = random.choice([True, False])
    if flip_ver:
        x = x[:, ::-1]
    return x

def data_aug(x):
    #distort each image in a batch
    new_x = []
    for i in range(x.shape[0]):
        x_cur = im_aug(x[i])
        new_x.append(x_cur)
    return np.array(new_x)

class DataLoaderTextAug(object):
    """ an object that generates batches of CIFAR-10 data for training """

    def __init__(self, data_dir, subset, batch_size, rng=None, shuffle=False, augment=False,
                 aug_with_text=None):
        """
        - data_dir is location where to store files
        - subset is train|test
        - batch_size is int, of #examples to load at once
        - rng is np.random.RandomState object for reproducibility
        - shuffle - boolean, shuffle at each iteration
        - augment - boolean,do a simple augmentation, like random flip
        - aug_with_text - dict or None, if dict then augment bg
            images by overlaying images of text with random contrast. Dict field: "data_dir"
        """

        self.data_dir = data_dir
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.augment = augment
        self.aug_with_text = aug_with_text

        # create temporary storage for the data, if not yet created
        if not os.path.exists(data_dir):
            print('creating folder', data_dir)
            os.makedirs(data_dir)

        # load undertext mask
        self.data = load(data_dir, subset=subset)
        if aug_with_text is None:
            self.data_aug_text = None
        else:
            self.data_aug_text = load_aug_text(self.aug_with_text["data_dir"], subset)


        self.p = 0  # pointer to where we are in iteration
        self.p_aug_text = 0
        self.rng = np.random.RandomState(1) if rng is None else rng

    def get_observation_size(self):
        return self.data.shape[1:]


    def reset(self):
        self.p = 0

    def __iter__(self):
        return self

    def __next__(self, n=None):
        """ n is the number of examples to fetch """
        if n is None: n = self.batch_size

        # on first iteration lazily permute all data
        if self.p == 0 and self.shuffle:
            inds = self.rng.permutation(self.data.shape[0])
            self.data = self.data[inds]

        # on last iteration reset the counter and raise StopIteration
        if self.p + n > self.data.shape[0]:
            self.reset()  # reset for next time we get called
            raise StopIteration

        #if run out of augmentation text samples, start from the begging
        if not self.aug_with_text is None:
            if self.p_aug_text + n > self.data_aug_text.shape[0]:
                self.p_aug_text = 0
                inds = self.rng.permutation(self.data_aug_text.shape[0])
                self.data_aug_text = self.data_aug_text[inds]
        # on intermediate iterations fetch the next batch
        x = copy.deepcopy(self.data[self.p: self.p + n])
        if self.augment:
            x = data_aug(x)
        self.p += self.batch_size
        #add augmentation with text to bg samples
        if not self.aug_with_text is None:
            x_text = overlay_with_text(x,self.data_aug_text[self.p_aug_text: self.p_aug_text + n])
            self.p_aug_text += self.batch_size
            return x,x_text
        else:
            return x

    next = __next__

class DataLoader(object):
    """ an object that generates batches of CIFAR-10 data for training """

    def __init__(self, data_dir, subset, batch_size, rng=None, shuffle=False, return_labels=False,augment=False):
        """
        - data_dir is location where to store files
        - subset is train|test
        - batch_size is int, of #examples to load at once
        - rng is np.random.RandomState object for reproducibility
        - shuffle - boolean, shuffle at each iteration
        - augment - boolean,do a simple augmentation, like random flip
        - aug_with_text - dict or False, if dict then augment bg
            images by overlaying images of text with random contrast. Dict field: "data_dir"
        """

        self.data_dir = data_dir
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.return_labels = return_labels
        self.augment = augment

        # create temporary storage for the data, if not yet created
        if not os.path.exists(data_dir):
            print('creating folder', data_dir)
            os.makedirs(data_dir)

        # load undertext mask
        self.data = load(data_dir, subset=subset)


        self.p = 0  # pointer to where we are in iteration
        self.rng = np.random.RandomState(1) if rng is None else rng

    def get_observation_size(self):
        return self.data.shape[1:]


    def reset(self):
        self.p = 0

    def __iter__(self):
        return self

    def __next__(self, n=None):
        """ n is the number of examples to fetch """
        if n is None: n = self.batch_size

        # on first iteration lazily permute all data
        if self.p == 0 and self.shuffle:
            inds = self.rng.permutation(self.data.shape[0])
            self.data = self.data[inds]

        # on last iteration reset the counter and raise StopIteration
        if self.p + n > self.data.shape[0]:
            self.reset()  # reset for next time we get called
            raise StopIteration

        # on intermediate iterations fetch the next batch
        x = copy.deepcopy(self.data[self.p: self.p + n])
        if self.augment:
            x = data_aug(x)
        self.p += self.batch_size

        return x

    next = __next__  # Python 2 compatibility (https://stackoverflow.com/questions/29578469/how-to-make-an-object-both-a-python2-and-python3-iterator)


def plot_ims_vs_ims(xs1,xs2):
    from matplotlib import pyplot as plt
    fig, ax1 = plt.subplots(4, 8)
    fig, ax2 = plt.subplots(4, 8)
    for i in range(4):
        for j in range(8):
            ax1[i, j].imshow(np.squeeze(xs1[j + i * 8]), cmap="gray")
            ax1[i, j].axis("off")
            ax2[i, j].imshow(np.squeeze(xs2[j + i * 8]), cmap="gray")
            ax2[i, j].axis("off")
    plt.show()


def plot_ims(xs):
    from matplotlib import pyplot as plt
    fig,ax = plt.subplots(4,8)
    for i in range(4):
        for j in range(8):
            ax[i,j].imshow(np.squeeze(xs[j+i*8]),cmap="gray")
            ax[i,j].axis("off")
    plt.show()

def plot_aug_text_img():
    data_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_bg_dataset_strech_contr_step_64_size_192"
    data_dir_ut = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192"
    subset = "test"
    data = DataLoaderTextAug(data_dir, subset, 32, augment=True, aug_with_text={"data_dir": data_dir_ut})
    obs_shape = data.get_observation_size()
    for i in range(10):
        x_without_text, text = data.__next__(32)
        plot_ims_vs_ims(x_without_text, text)
        print("Max before scaling", np.amax(x_without_text), "Min before scaling", np.amin(x_without_text))
        x_scaled = (x_without_text - 127.5) / 127.5
        print("Max after scaling", np.amax(x_scaled), "Min after scaling", np.amin(x_scaled))
        print("Image shape:", obs_shape)

if __name__=="__main__":
    data_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\Archi_bg_dataset_strech_contr_step_57_size_172"
    subset = "train"
    aug_text_data_dir = r'C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192'
    batch_size = 8
    data = DataLoaderTextAug(data_dir, 'train', batch_size, rng=None,
                        shuffle=True, augment=True,aug_with_text= None if aug_text_data_dir is None else {"data_dir":aug_text_data_dir})
    obs_shape = data.get_observation_size()
    assert len(obs_shape) == 3, 'assumed right now'
    sum_bg = 0
    i = 0
    for batch in data:
        if type(batch) == tuple:
            batch_without, batch_with = batch
        else:
            batch_without = batch
        sum_bg+=np.mean(batch_without)
        i+=1
    print("Mean bg val",sum_bg/i)

    nb_samples = 0
    i=0
    for batch in data:
        if type(batch) == tuple:
            batch_without, batch_with = batch
            fig, ax = plt.subplots(batch_size,2)
            for i in range(batch_size):
                ax[i, 0].imshow(batch_without[i,:,:,0],cmap="gray")
                ax[i, 1].imshow(batch_with[i, :, :, 0], cmap="gray")
        else:
            batch_without = batch
            batch_without = (batch_without - 127.5) / 127.5
            fig, ax = plt.subplots(batch_size, 1)
            for i in range(batch_size):
                ax[i].imshow(batch_without[i, :, :, 0], cmap="gray")
        i+=1
        if i>2:
            break
    plt.show()
    print("Overall samples {} vs. samples {}".format(data.data.shape[0],nb_samples))

