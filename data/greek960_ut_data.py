import os
import glob
import random
import numpy as np
from skimage import transform,io,filters
from pixelcnn_palimpsest.data.greek960_data_bin import data_aug
from matplotlib import pyplot as plt
from skimage.filters import threshold_mean
import copy

def preprocess_image(im):
    im = transform.resize(im,(64,64),preserve_range=True)
    return im[:,:,np.newaxis]

def gray_2_rgb(im):
    im = np.repeat(im,3,2)
    return im

def load_data(subset_path):
    file_list = get_file_list(subset_path)
    ims = []
    for impath in file_list:
        im = io.imread(impath)
        im = preprocess_image(im)
        ims.append(im)
    return np.array(ims)

def get_file_list(subset_path):
    """path are stored as dataset/folio/line/*.png"""
    if os.path.exists(subset_path):
        return glob.glob(os.path.join(subset_path,"*","*","*.png"))
    else:
        raise Exception("Folder {} not found".format(subset_path))

def load(data_dir, subset='train'):
    if subset == 'train':
        return load_data(os.path.join(data_dir, 'train'))
    elif subset == 'val':
        return load_data(os.path.join(data_dir, 'val'))
    elif subset == 'test':
        return load_data(os.path.join(data_dir, 'test'))
    else:
        raise NotImplementedError('subset should be either train or test')

def load_aug_bg(data_dir, subset='train'):
    if subset == 'train':
        return load_aug_bg_data(os.path.join(data_dir, 'train'))
    elif subset == 'val':
        return load_aug_bg_data(os.path.join(data_dir, 'val'))
    elif subset == 'test':
        return load_aug_bg_data(os.path.join(data_dir, 'test'))
    else:
        raise NotImplementedError('subset should be either train or test')

def load_aug_bg_data(subset_path):
    file_list = get_file_list(subset_path)
    ims = []
    for impath in file_list:
        im = read_aug_bg_mask(impath)
        ims.append(im)
    return np.array(ims)

def read_aug_bg_mask(mask_fpath):
    mask = io.imread(mask_fpath)
    mask = transform.resize(mask, (64, 64), preserve_range=True)
    return mask[:,:,np.newaxis]



def bg_binarization(bg,per_thresh):
    """Threshold the image of some pattern from background
    bg - unscaled bg image
    per_thresh - persentage of pixels filled with pattern
    """
    thresh = threshold_mean(bg)
    bg_bin = bg > thresh
    frac_pos_pixel = np.sum(bg_bin)/np.prod(bg.shape)
    #per_thresh = 10 #percent threshold
    while frac_pos_pixel>per_thresh/100 and frac_pos_pixel<(100-per_thresh)/100:
        thresh+=5
        bg_bin = bg > thresh
        frac_pos_pixel = np.sum(bg_bin) / np.prod(bg.shape)
    frac_neg_pixel = 1-frac_pos_pixel
    if frac_pos_pixel<frac_neg_pixel:
        return (bg_bin)*255.0
    else:
        return (1-bg_bin)*255.0

def coord_nth_ut_pixel(im,n):
    """Return a coord of nth ut pixel"""
    """If first 10 pixels in first row"""
    thresh = n
    im_s = im / 255.0
    im_s[im<254.0]=0.0
    im_s = np.ceil(im_s)
    nb_pixs_per_row = np.cumsum(np.sum(im_s,1))
    thresh = min(np.max(nb_pixs_per_row),thresh)
    r_idx = (nb_pixs_per_row>=thresh).nonzero()[0][0]
    #how many pixels left until threshold in the row
    c_cumsum = np.cumsum(im_s[r_idx])
    if r_idx==0:
        c_idx = (c_cumsum == thresh).nonzero()[0][0]
    else:
        res = thresh - nb_pixs_per_row[r_idx-1]
        c_idx = (c_cumsum==res).nonzero()[0][0]
    return r_idx,c_idx

def erase_mask_lcorner(mask, corner_coord):
    if corner_coord[0]==0:
        mask[0,:corner_coord[1]]=0
    else:
        mask[:corner_coord[0]]=0
        mask[corner_coord[0],:corner_coord[1]]=0
    return mask

def overlay_with_bg(x,bg,per_thresh):
    sigma = 0.5
    blurred_masks = []
    for i in range(bg.shape[0]):
        mask = bg_binarization(bg[i,:,:,0],per_thresh=per_thresh)
        #corner_coord = coord_nth_ut_pixel(x[i,:,:,0],30)
        #mask = erase_mask_lcorner(mask,corner_coord)
        mask = filters.gaussian(mask, sigma=sigma)[:,:,np.newaxis]
        mask_empty = np.zeros_like(mask)
        #randomly choose btw empty mask and mask filled with pattern
        select_empty_mask = random.choice([0,1])
        if select_empty_mask==1:
            mask = mask_empty
        blurred_masks.append(mask)
    x = x + np.stack(blurred_masks,axis=0)
    x = np.clip(x, a_min=0., a_max=255.)
    x = x.astype(np.uint8)
    return x

class DataLoaderBgAug(object):
    """ an object that generates batches of CIFAR-10 data for training """

    def __init__(self, data_dir, subset, batch_size, rng=None, shuffle=False, augment=False,
                 aug_with_bg=None):
        """
        - data_dir is location where to store files
        - subset is train|test
        - batch_size is int, of #examples to load at once
        - rng is np.random.RandomState object for reproducibility
        - shuffle - boolean, shuffle at each iteration
        - augment - boolean,do a simple augmentation, like random flip
        - aug_with_text - dict or None, if dict then augment bg
            images by overlaying images of text with random contrast. Dict field: "data_dir"
        """

        self.data_dir = data_dir
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.augment = augment
        self.aug_with_bg = aug_with_bg


        # create temporary storage for the data, if not yet created
        if not os.path.exists(data_dir):
            print('creating folder', data_dir)
            os.makedirs(data_dir)

        # load undertext mask
        self.data = load(data_dir, subset=subset)
        if aug_with_bg is None:
            self.data_aug_bg = None
        else:
            self.data_aug_bg = load_aug_bg(self.aug_with_bg["data_dir"], subset)
            self.per_thresh = self.aug_with_bg["per_thresh"]


        self.p = 0  # pointer to where we are in iteration
        self.p_aug_bg = 0
        self.rng = np.random.RandomState(1) if rng is None else rng

    def get_observation_size(self):
        return self.data.shape[1:]


    def reset(self):
        self.p = 0

    def __iter__(self):
        return self

    def __next__(self, n=None):
        """ n is the number of examples to fetch """
        if n is None: n = self.batch_size

        # on first iteration lazily permute all data
        if self.p == 0 and self.shuffle:
            inds = self.rng.permutation(self.data.shape[0])
            self.data = self.data[inds]

        # on last iteration reset the counter and raise StopIteration
        if self.p + n > self.data.shape[0]:
            self.reset()  # reset for next time we get called
            raise StopIteration

        #if run out of augmentation text samples, start from the begging
        if not self.aug_with_bg is None:
            if self.p_aug_bg + n > self.data_aug_bg.shape[0]:
                self.p_aug_bg = 0
                inds = self.rng.permutation(self.data_aug_bg.shape[0])
                self.data_aug_bg = self.data_aug_bg[inds]
        # on intermediate iterations fetch the next batch
        x = copy.deepcopy(self.data[self.p: self.p + n])
        if self.augment:
            x = data_aug(x)
        self.p += self.batch_size
        #add augmentation with bg to text samples
        if not self.aug_with_bg is None:
            x_text = overlay_with_bg(copy.deepcopy(x),self.data_aug_bg[self.p_aug_bg: self.p_aug_bg + n],self.per_thresh)
            self.p_aug_bg += self.batch_size
            return x,x_text
        else:
            return x

    next = __next__

if __name__=="__main__":
    aug_bg_data_dir  = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_bg_dataset_strech_contr_step_64_size_192"
    subset = "train"
    data_dir = r'C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192'
    batch_size = 2
    data = DataLoaderBgAug(data_dir, 'train', batch_size, rng=None,
                        shuffle=True, augment=False,aug_with_bg = None if aug_bg_data_dir is None else {"data_dir":aug_bg_data_dir})
    obs_shape = data.get_observation_size()
    assert len(obs_shape) == 3, 'assumed right now'
    sum_bg = 0
    nb_samples = 0
    i=0
    for batch in data:
        print(i)
        if type(batch) == tuple:
            batch_without, batch_with = batch
            print("Value range no aug min {} max {}".format(np.amin(batch_without),np.amax(batch_without)))
            print("Value range aug min {} max {}".format(np.amin(batch_with), np.amax(batch_with)))
            fig, ax = plt.subplots(batch_size,2)
            for s_i in range(batch_size):
                ax[s_i, 0].imshow(batch_without[s_i,:,:,0],cmap="gray")
                ax[s_i, 1].imshow(batch_with[s_i, :, :, 0], cmap="gray")
        else:
            batch_without = batch
            print("Value range no aug min {} max {}".format(np.amin(batch_without), np.amax(batch_without)))
            fig, ax = plt.subplots(batch_size, 1)
            for i in range(batch_size):
                ax[i].imshow(batch_without[i, :, :, 0], cmap="gray")
        i+=1
        if i>53:
            break
    plt.show()
    print("Overall samples {} vs. samples {}".format(data.data.shape[0],nb_samples))