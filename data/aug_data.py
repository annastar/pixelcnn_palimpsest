import numpy as np
from skimage.transform import PiecewiseAffineTransform, warp
from skimage import io
from matplotlib import pyplot as plt

def wrap_img(image,quarter_idx,x_scale,y_scale,shift_scale):
    """
    Piecewise affine transformation of char image patch.
    The horizontal coordinate is bend according to sinusoid quarter cycle
    image - [N1,N2] numpy array
    quarter_idx - int, index of sinusoid quarter cycle starting from 0 degrees,e.g. 0-3
    x_scale - float, by how much image coordinate should spread or shrink, inverse proportion [0.8,1.2]
    y_scale - float, by how much image coordinate should spread or shrink, inverse proportion [0.8,1.2]
    shift_scale - float, scaling of sinusoid amplitude [15,18]
    Returns:
        out - [N1,N2] numpy array
     """
    rows, cols = image.shape[0], image.shape[1]
    src_cols = np.linspace(0, cols, 20)
    src_rows = np.linspace(0, rows, 10)
    src_rows, src_cols = np.meshgrid(src_rows, src_cols)
    src = np.dstack([src_cols.flat, src_rows.flat])[0]
    # add sinusoidal oscillation to row coordinates
    dst_rows = src[:, 1] - np.sin(np.linspace(quarter_idx*np.pi/2,(1+quarter_idx)*np.pi/2, src.shape[0])) * shift_scale
    dst_cols = x_scale * src[:, 0]
    dst_rows *= y_scale
    dst_rows -= int(y_scale * shift_scale)
    dst = np.vstack([dst_cols, dst_rows]).T

    tform = PiecewiseAffineTransform()
    tform.estimate(src, dst)

    out_rows = int(image.shape[0] - y_scale * shift_scale)
    out_cols = cols
    out = warp(image, tform, output_shape=(out_rows, out_cols))
    return out

if __name__=="__main__":
    fname = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_dataset_50step_192x192\train\0086_000085\line_9\line_9_0_24.png"
    im = io.imread(fname,as_gray=True)

    fig,ax = plt.subplots(1,5)
    ax[0].imshow(im,cmap="gray")
    ax[1].imshow(wrap_img(im,0,1.0,0.8,15),cmap="gray")
    ax[2].imshow(wrap_img(im,0,0.8,0.8,15), cmap="gray")
    ax[3].imshow(wrap_img(im,2,1.0,0.8,15), cmap="gray")
    ax[4].imshow(wrap_img(im,3,1.0,0.8,15), cmap="gray")
    plt.show()

