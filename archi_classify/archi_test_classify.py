'''
Created on Jun 13, 2019

@author: annst
'''
from __future__ import print_function
from keras.models import model_from_json
from keras.models import Model
import os
import numpy as np
from skimage import io,transform
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
from greek_transfer_svm import predict, accuracy
import csv

archi_label_dict = {'lower_alpha':0,'lower_chi':1,'lower_delta':2,'lower_epsilon':3,
                    'lower_eta':4,'lower_gamma':5,'lower_iota':6,'lower_kappa':7,'lower_lambda':8,
                    'lower_mu':9,'lower_nu':10,'lower_omega':11,'lower_omicron':12,'lower_phi':13,
                    'lower_pi':14,'lower_rho':15,'lower_sigma':16,'lower_tau':17,'lower_theta':18,'lower_upsilon':19}
test_datasetpath = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\rc\msi\final"
svm_filename = r"C:\Data\PhD\bss_gan_palimpsest\training\arche_classify\transfer_svm_archi.sav"
old_modelname = "model_lrelu"
new_modelname = "transfer_svm_archi"
invert = False
crop = False
size = 64

def read_ims(path,invert=True,crop=False,msi=False):

    # load json and create model
    json_file = open(os.path.join(r"C:\Data\PhD\bss_gan_palimpsest\training\arche_classify","{}.json".format(old_modelname)), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(os.path.join(r"C:\Data\PhD\bss_gan_palimpsest\training\arche_classify","{}.h5".format(old_modelname)))
    #print(loaded_model.summary())
    print("Loaded model from disk")
    #print(loaded_model.layers)
    layer_outputs = [loaded_model.layers[i].output for i in [14]]
    activation_model = Model(inputs=loaded_model.input, outputs=layer_outputs)
    impaths=[]
    if msi:
        for label in os.listdir(path):
            for fname in os.listdir(os.path.join(path, label)):
                impaths.append(os.path.join(path, label, fname,msi+".png"))
    else:
        for label in os.listdir(path):
            for fname in os.listdir(os.path.join(path,label)):
                impaths.append(os.path.join(path, label, fname))
    nb = len(impaths)
    labels = np.zeros((nb,))
    features = np.zeros((nb, 2304))
    for i,impath in enumerate(impaths):
        im = read_im_for_feature_extract(impath)
        features[i] = activation_model.predict(im)
        assert np.max(im)<=1, "{} has max {}".format(path,np.max(im))
        if msi:
            label = impath.split(os.sep)[-3]
        else:
            label = impath.split(os.sep)[-2]
        labels[i] = archi_label_dict[label]
    return features,labels

def read_im_for_feature_extract(impath):
    im = io.imread(impath, as_gray=True)
    if np.max(im) > 1:
        im = (im) / np.max(im)
    if crop:
        im_o = transform.resize(im, (57, 57), clip=False)
        im = np.zeros((64, 64))
        im[3:3 + 57, 3:3 + 57] = im_o
        im[0:3, 3:3 + 57] = im_o[0, :]
        im[3 + 57:64, 3:3 + 57] = im_o[56, :]
        im[:, 0:3] = np.repeat(np.reshape(im[:, 3], (64, -1)), repeats=3, axis=1)
        im[:, 60:64] = np.repeat(np.reshape(im[:, 56], (64, -1)), repeats=4, axis=1)
    else:
        im = transform.resize(im, (64, 64), clip=False)
    if invert:
        im = 1 - im
    im[im > 1] = 1
    im[im < 0] = 0
    im = im.reshape(1, 64, 64, 1)
    return im



def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    #print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return fig ,ax

def test_msi():
    homedir = r"C:\Data\PhD\bss_gan_palimpsest\training\InvNet"
    fname = "acc_{}.txt".format(test_datasetpath.split(os.sep)[-1])
    if invert:
        fname = "acc_inv_{}.txt".format(test_datasetpath.split(os.sep)[-1])
    with open(os.path.join(homedir,fname),"w") as f:
        for band in archi_bands:
            test_features,test_labels = read_ims(test_datasetpath,invert=invert,crop=crop,msi=band)
            y_test, y_pred = predict(test_features,test_labels,svm_filename)
            labels = np.array(list(archi_label_dict.keys()))
            #print('Band {} Test accuracy :{}'.format(band,accuracy(y_test.astype("uint8"),y_pred.astype("uint8"))))
            plot_confusion_matrix(y_test.astype("uint8"), y_pred.astype("uint8"),labels )
            #plt.show()
            f.write('Band {} Test accuracy :{} {}'.format(band,accuracy(y_test.astype("uint8"),y_pred.astype("uint8")),os.linesep))

def accuracy_per_char(y_true, y_pred,labels):
    pred_char_doc={}
    for i in set(y_true):
        label = [label for label,idx in archi_label_dict.items() if idx==int(i)][0]
        pred_char_doc[label]=(sum(y_pred[y_true==i]==y_true[y_true==i])/sum(y_true==i))*100
    return pred_char_doc

def save_dict_csv(log_dir,res_dict):
    f=open(os.path.join(log_dir,"results.csv"),"w",newline='')
    writer = csv.DictWriter(f, fieldnames=res_dict.keys())
    # writing headers (field names)
    writer.writeheader()
    # writing data rows
    writer.writerow(res_dict)
    f.close()

def test():
    test_features,test_labels = read_ims(test_datasetpath,invert=invert,crop=crop)
    y_test, y_pred = predict(test_features,test_labels,svm_filename)
    labels = np.array(list(archi_label_dict.keys()))
    print('Test accuracy:', accuracy(y_test.astype("uint8"),y_pred.astype("uint8")))
    fig, ax = plot_confusion_matrix(y_test.astype("uint8"), y_pred.astype("uint8"),labels )
    log_dir = (os.path.sep).join(test_datasetpath.split(os.path.sep)[:-3])
    fig.savefig(os.path.join(log_dir,"Confusion_matrix"+".png"))
    print(accuracy_per_char(y_test,y_pred,labels))
    save_dict_csv(log_dir,accuracy_per_char(y_test,y_pred,labels))
    plt.show()

def test_PCA_ICA():
    for i in range(11):
        test_features,test_labels = read_ims(test_datasetpath+str(i),invert=invert,crop=crop)
        y_test, y_pred = predict(test_features,test_labels,svm_filename)
        labels = np.array(list(archi_label_dict.keys()))
        print('Band {} accuracy:{}'.format(i,accuracy(y_test.astype("uint8"),y_pred.astype("uint8"))))
        fig, ax = plot_confusion_matrix(y_test.astype("uint8"), y_pred.astype("uint8"),labels )
        log_dir = (os.path.sep).join(test_datasetpath.split(os.path.sep)[:-3])
        fig.savefig(os.path.join(log_dir,"Confusion_matrix"+".png"))
        #print("Char acc: {}".format(accuracy_per_char(y_test,y_pred,labels)))
        save_dict_csv(log_dir,accuracy_per_char(y_test,y_pred,labels))
    plt.show()
if __name__=="__main__":
    test()