from archi_test_classify import read_im_for_feature_extract
from greek_transfer_svm import predict, accuracy
archi_bands = ("LED365_01_corr","LED445_01_raw","LED470_01_raw","LED505_01_raw","LED530_01_raw","LED570_01_raw",\
             "LED617_01_raw","LED625_01_raw","LED700_01_raw","LED735_01_raw","LED870_01_raw")
archi_label_dict = {'lower_alpha':0,'lower_chi':1,'lower_delta':2,'lower_epsilon':3,'lower_eta':4,'lower_gamma':5,'lower_iota':6,'lower_kappa':7,'lower_lambda':8,'lower_mu':9,'lower_nu':10,
              'lower_omega':11,'lower_omicron':12,'lower_phi':13,'lower_pi':14,'lower_rho':15,'lower_sigma':16,'lower_tau':17,'lower_theta':18,'lower_upsilon':19}