'''
Created on Jul 24, 2019

@author: annst
'''
from __future__ import print_function
from keras.models import model_from_json
from keras.models import Model
import os
import numpy as np
from skimage import io,transform
from sklearn.svm import SVC
from sklearn import metrics
import pickle
#load the data
train_nb = 605
val_nb = 100
test_nb = 200
batch_size = 96
size = 64
num_classes = 20
epochs = 100
train_datasetpath = r"C:\Data\PhD\palimpsest\Archimedes\thresholded_classification\train_clean_no_PsiXiBetaZeta"
val_datasetpath = r"C:\Data\PhD\palimpsest\Archimedes\thresholded_classification\validation_no_PsiXiBetaZeta"
test_datasetpath = r"C:\Data\PhD\palimpsest\Archimedes\thresholded_classification\test_clean_no_PsiXiBetaZeta"
old_modelname = "model_lrelu"
new_modelname = "transfer_svmc_archi"
LR=0.001

def read_ims(path,nb,invert=True,crop=False):
    features = np.zeros((nb,2304))
    i=0
    labels=np.zeros((nb,))
    
    # load json and create model
    json_file = open(os.path.join(r"C:\Data\PhD\bss_gan_palimpsest\training\arche_classify","{}.json".format(old_modelname)), 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(os.path.join(r"C:\Data\PhD\bss_gan_palimpsest\training\arche_classify","{}.h5".format(old_modelname)))
    print(loaded_model.summary())
    print("Loaded model from disk")
    print(loaded_model.layers)
    layer_outputs = [loaded_model.layers[i].output for i in [14]]
    activation_model = Model(inputs=loaded_model.input, outputs=layer_outputs)
    for label in os.listdir(path):
        for fname in os.listdir(os.path.join(path,label)):
            im = io.imread(os.path.join(path,label,fname), as_gray=True)
            if np.max(im)>1:
                im = (im)/np.max(im) #(im-np.min(im))/(np.max(im)-np.min(im))
            if crop:
                im_o = transform.resize(im,(57,57),clip=False)
                im = np.zeros((64,64))
                im[3:3+57,3:3+57]=im_o
                im[0:3,3:3+57]=im_o[0,:]
                im[3+57:64,3:3+57]=im_o[56,:]
                im[:,0:3]=np.repeat(np.reshape(im[:,3], (64,-1)), repeats=3, axis=1)
                im[:,60:64]=np.repeat(np.reshape(im[:,56], (64,-1)), repeats=4, axis=1)
            else:
                im = transform.resize(im,(64,64),clip=False)
            if invert:
                im=1-im
            im[im>1] = 1
            im[im<0] = 0
            im = im.reshape(1,64,64,1)
            features[i] = activation_model.predict(im)
            assert np.max(im)<=1, "{} has max {}".format(path,np.max(im))
            #print("min {} and max {}".format(np.min(im),np.max(im)))
            labels[i] = archi_label_dict[label]
            i+=1
            
    return features,labels

def train():
    train_features,train_labels = read_ims(train_datasetpath, train_nb)
    validation_features,val_labels = read_ims(val_datasetpath, val_nb)
    test_features,test_labels = read_ims(test_datasetpath, test_nb)
    
    svm_features = np.concatenate((train_features, validation_features))
    svm_labels = np.concatenate((train_labels, val_labels))
    X_train, y_train = svm_features, svm_labels

    svm = SVC(kernel='linear', C=0.01,probability=True)  # As in Tang (2013)
    svm.fit(X_train, y_train)
    pickle.dump(svm, open(r"C:\Data\PhD\bss_gan_palimpsest\training\arche_classify\{}.sav".format(new_modelname), 'wb'))
    predicted = svm.predict(test_features)
    print("Classification report for classifier %s:\n%s\n"
      % (svm, metrics.classification_report(test_labels, predicted)))
    print(sum((predicted==test_labels).astype("uint8"))*100/len(test_labels))
    print("Saved model to disk")
    
def predict(test_features,test_labels,svm_filename):
    
    loaded_svm = pickle.load(open(svm_filename, 'rb'))
    predicted = loaded_svm.predict(test_features)
    return test_labels, predicted
def accuracy(test,pred):
    return sum((pred==test).astype("uint8"))*100/len(test)

if __name__=="__main__":
    train()
    