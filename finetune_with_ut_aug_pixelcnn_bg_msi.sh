#!/bin/bash
TIMEFOLDER=$(date +"%Y-%m-%d-%H-%M")
OLDTIMEFOLDER="2023-05-07-03-42"
#DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Archimedes/Archi_bg_dataset_strech_contr_step_57_size_172'
DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Archimedes/Archi_bg_msi_dataset_strech_contr_step_57_size_172'
DATADIR_TEXT_AUG='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_ut_dataset_50step_192x192'
DATASET='greek960_bg'
#-re 120
MODE='msi'
EPOCH_STEP=40
FREQ_SAVE=20
LR=0.0004
NOISE_LVLs=(0.01)
#RE_EPOCHs=(120 160 200 240 280 320 360 400 440 480)
RE_EPOCHs=(70 100 140 180 220 260 300 340 380 420 460)
BANDS=("LED365_01_corr" "LED505_01_raw" "LED735_01_raw")
for i in ${!NOISE_LVLs[@]}
do
NOISE_LVL=${NOISE_LVLs[${i}]}
PREV_NOISE_LVL=${NOISE_LVLs[${i}]}
RE_EPOCH=${RE_EPOCHs[${i}]}
echo "Noise level: ${NOISE_LVL}"
  echo "Run for ${EPOCH_STEP} steps"
  SAVEDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/background/Archi_msi/pixelcnn-${MODE}-${TIMEFOLDER}_with_text_aug/noise_${NOISE_LVL}"
  RESTDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/background/Greek960_msi/pixelcnn-${MODE}-${OLDTIMEFOLDER}_with_text_aug/noise_${PREV_NOISE_LVL}"
  python train_bg_text_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -aug -n 32 --nr_gpu 2 -b 8 -t ${FREQ_SAVE} -q 3 -m 3 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH} -sl ${NOISE_LVL}\
   -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_TEXT_AUG} -bands ${BANDS[@]}
done