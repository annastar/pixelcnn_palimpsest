#!/bin/bash
TIMEFOLDER=$(date +"%Y-%m-%d-%H-%M")
DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_ut_dataset_50step_192x192'
DATADIR_BG_AUG='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_bg_dataset_strech_contr_step_64_size_192'
BINARY_TRUE=1
if [ ${BINARY_TRUE} == 1 ]
  then
    MODE='bin'
else
  MODE='gray'
fi
DATASET='greek960_ut'
#-re 120
EPOCH_STEP=100
FREQ_SAVE=20
LR=0.00001
NOISE_LVLs=(0.0 0.01 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)
PREV_NOISE_LVL=0.0
RE_EPOCH=0
for i in ${!NOISE_LVLs[@]}
do
NOISE_LVL=${NOISE_LVLs[${i}]}
echo "Noise level: ${NOISE_LVL}"
  if [ ${NOISE_LVL} != 0.0 ]
  then
    EPOCH_STEP=100
    LR=0.0004
    FREQ_SAVE=20
  fi
  echo "Run for ${EPOCH_STEP} steps"
  SAVEDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/undertext/pixelcnn-${MODE}-${TIMEFOLDER}/noise_${NOISE_LVL}"
  RESTDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/undertext/pixelcnn-${MODE}-${OLDTIMEFOLDER}_current/noise_${PREV_NOISE_LVL}"

  if [ ${BINARY_TRUE} == 1 ]
  then
    echo "Process binary data"
    python train_ut_bg_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -n 64 --nr_gpu 2 -b 8 -t ${FREQ_SAVE} -q 5 -m 2 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH} -sl ${NOISE_LVL} -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_BG_AUG} -bn
  else
    echo "Process grayscale data"
    python train_ut_bg_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -n 64 --nr_gpu 2 -b 8 -t ${FREQ_SAVE} -q 5 -m 2 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH} -sl ${NOISE_LVL} -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_BG_AUG}
  fi
  if [ ${NOISE_LVL} == 0.0 ]
  then
    RE_EPOCH=$((RE_EPOCH+EPOCH_STEP))
  else
    RE_EPOCH=$((EPOCH_STEP))
  fi
PREV_NOISE_LVL=$NOISE_LVL
done