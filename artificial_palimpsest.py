from skimage import io
import numpy as np
from matplotlib import pyplot as plt

def min_mixing_no_clip_no_bg(overtext,**d):
    overtext = np.reshape(overtext, (overtext.shape[0], overtext.shape[1],1))
    undertext = d["undertext"]
    undertext = np.reshape(undertext, (overtext.shape[0], overtext.shape[1], 1))
    overtext = overtext
    input = np.stack([undertext, overtext], 2)
    mix = np.amin(input, axis=2)*0.05
    return mix[:,:,0]

def min_sum_mixing(overtext,**d):
    overtext = np.reshape(overtext, (overtext.shape[0], overtext.shape[1],1))
    undertext = d["undertext"]
    undertext = np.reshape(undertext, (overtext.shape[0], overtext.shape[1], 1))
    undertext +=0.2
    overtext = overtext+0.1
    background = np.reshape(d["background"], (overtext.shape[0], overtext.shape[1], 1))
    input = np.stack([undertext, overtext,background], 2)
    mix = np.amin(input, axis=2)
    return mix[:,:,0]

def read_scale_im(fpath):
    x = io.imread(fpath,as_gray=True)
    x = x/np.amax(x)
    if x.shape[0]<192:
        x_new = np.ones(shape=[192,x.shape[1]])
        res = int((192-x.shape[0])/2)
        x_new[res:res+x.shape[0]] = x
        return x_new
    else:
        return x


##read background
bg_fpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\temp\exp_art_palimpsest\bg.tif"
undertext_fpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\temp\exp_art_palimpsest\line_27.png"
overtext_fpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\temp\exp_art_palimpsest\line_4_moved.png"


bg_scale = 1.0
ut_scale = 0.25
ot_scale = 0.1

bg = read_scale_im(bg_fpath)
ut = read_scale_im(undertext_fpath)
ot = read_scale_im(overtext_fpath)

palimp = min_sum_mixing(ot, undertext=ut,background=bg)

plt.figure()
plt.imshow(palimp,cmap="gray")
plt.show()


io.imsave(r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\temp\exp_art_palimpsest\min_sum_2e-1ut_1e-1ot_1bg_model.png",palimp)
#io.imsave(r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\temp\exp_art_palimpsest\line_4_moved.png",ot/np.max(ot))
#io.imsave(r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\temp\exp_art_palimpsest\line_27.png",ut/np.max(ut))