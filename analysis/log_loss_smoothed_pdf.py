import tensorflow as tf
import numpy as np
from pixelcnn_palimpsest.pixel_cnn_pp.nn import discretized_mix_logistic_loss_gray

def log_loss_smoothed_pdf(x,l,s,sum_all=True):
    """ log-likelihood for probability modeled with sigmoid
    s - sigma, noise variance"""
    h = 256
    smooth_log_probs = []
    disc_nll = []
    #iterate though the batch
    #skip the first and the last value
    idxs = 2. * np.arange(0, h) / (h-1) - 1
    idxs_tf = tf.constant(idxs, dtype=tf.float32)
    idxs_tf = tf.stack([tf.stack([idxs_tf]*l.shape[1],axis=1)]*l.shape[2],axis=2)
    idxs_tf = tf.expand_dims(idxs_tf,3)
    C = -tf.math.log(s * np.sqrt(2 * np.pi))
    for b_idx in range(x.shape[0]):
        l_b = tf.stack([l[b_idx]]*(h),axis=0)
        disc_log_probs = discretized_mix_logistic_loss_gray(idxs_tf,l_b,False)
        disc_log_probs = tf.expand_dims(-disc_log_probs,3)
        x_rep = tf.stack([x[b_idx]]*(h),0)
        b = tf.math.reduce_logsumexp(disc_log_probs-0.5*((x_rep-idxs_tf)/s)**2,0)
        smooth_log_probs.append(C + b)
        disc_nll.append(disc_log_probs)
    smooth_log_probs = tf.stack(smooth_log_probs,0)
    disc_log_probs = tf.stack(tf.exp(disc_nll),0)
    if sum_all:
        return tf.reduce_sum(smooth_log_probs), disc_log_probs
    else:
        return smooth_log_probs, disc_log_probs