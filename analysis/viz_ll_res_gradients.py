import tensorflow as tf
import numpy as np
import os
from skimage import io
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.pixel_cnn_pp.nn import log_loss_smoothed,sigmoid_loss_smoothed,discretized_mix_logistic_loss_gray
from pixelcnn_palimpsest.utils import plotting,files
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_prior_model,restore_model
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_1band,get_grads_fidelity_smoothed_apprx_msi
from pixelcnn_palimpsest.langevin_dynamics.simulate_mix_fun_pdf import MixingVars
from pixelcnn_palimpsest.analysis.log_loss_smoothed_pdf import log_loss_smoothed_pdf


def gradients_ll_bg(ims,batch_size,sigma,chpoint_path,add_noise=False):
    """
    Gradient and Likelihood of smooth and discrete values
    ims - ims [nb_samp,height,width,1]
    sigma - std
    chpoint_path - checkpoint_path untill "noise" folder
    Returns:
        smoothed_likelihood,grads,discrete_likelihood
        """
    tf.compat.v1.reset_default_graph()
    s = tf.constant(sigma, dtype=tf.float32)
    nr_mix = 3
    x_prior_tf, l_prior_tf = compile_prior_model(batch_size, "bg", nr_filters=32,
                                                 nr_resnet=3, nr_mix=nr_mix, model_discrete=False,
                                                 model_scope="model_bg")
    x_prior_no_noise_tf = tf.identity(x_prior_tf,name="input_no_noise")
    noise = sigma*np.random.normal(0.0,1.0,size=ims.shape)
    sess = tf.Session()
    restore_model(chpoint_path, sess, "model_bg")
    smooth_log_loss_xb_fun_tf = log_loss_smoothed(x_prior_tf, l_prior_tf, s, False)
    disc_log_loss_xb_fun_tf = discretized_mix_logistic_loss_gray(x_prior_no_noise_tf, l_prior_tf, False)
    grads_tf = tf.gradients(smooth_log_loss_xb_fun_tf, x_prior_tf)
    if add_noise:
        feed_dict = {x_prior_tf: ims+noise, x_prior_no_noise_tf:ims}
    else:
        feed_dict = {x_prior_tf: ims, x_prior_no_noise_tf:ims}
    smooth_ll,disc_ll,grads = sess.run([smooth_log_loss_xb_fun_tf,
                                        disc_log_loss_xb_fun_tf,grads_tf], feed_dict)
    sess.close()
    return np.exp(smooth_ll),grads[0],np.exp(-disc_ll)


def gradients_ll_ut(ims,batch_size,sigma,chpoint_path,model_discrete=True,grads_wrt_model_input=True):
    """    Gradient and Likelihood of smooth and discrete values
    ims - ims [nb_samp,height,width,1]
    sigma - std
    chpoint_path - checkpoint_path untill "noise" folder
    Returns:
        prob - predicted probability;
        activation - last layer activations;
        l - smooth_likelihood
        grads - grads"""
    tf.compat.v1.reset_default_graph()
    s = tf.constant(sigma, dtype=tf.float32)
    nr_mix = 2
    if grads_wrt_model_input:
        x_prior_tf, l_prior_tf = compile_prior_model(batch_size, "undertext", nr_filters=64,
                                                           nr_resnet=5, nr_mix=nr_mix, model_discrete=model_discrete,
                                                           model_scope="model_ut")
    else:
        x_prior_input_tf, l_prior_tf = compile_prior_model(batch_size, "undertext", nr_filters=64,
                                              nr_resnet=5, nr_mix=nr_mix, model_discrete=model_discrete,
                                              model_scope="model_ut")
        x_prior_tf = tf.identity(x_prior_input_tf)
    if model_discrete:
        prob_tf = tf.nn.sigmoid(l_prior_tf)
    else:
        _,prob_tf = log_loss_smoothed_pdf(x_prior_tf,l_prior_tf)
    sess = tf.Session()
    restore_model(chpoint_path, sess,  "model_ut")

    if grads_wrt_model_input:
        feed_dict = {x_prior_tf: ims}
    else:
        feed_dict = {x_prior_tf: ims, x_prior_input_tf: ims}

    if model_discrete:
        disc_l_tf = tf.nn.sigmoid_cross_entropy_with_logits(labels=x_prior_tf, logits=l_prior_tf)
    else:
        disc_l_tf = discretized_mix_logistic_loss_gray(x_prior_tf,l_prior_tf,False)
    if sigma > 0:
        smooth_loss_xu_fun_tf = sigmoid_loss_smoothed(x_prior_tf, l_prior_tf, s, False)
        grads_tf = tf.gradients(smooth_loss_xu_fun_tf, x_prior_tf)
        ll,prob,disc_l,grads = sess.run([smooth_loss_xu_fun_tf,prob_tf,disc_l_tf,grads_tf], feed_dict)
        l = np.exp(ll)
        sess.close()
        return prob,l,disc_l,grads[0]
    else:
        prob, disc_l = sess.run([prob_tf, disc_l_tf], feed_dict)
        sess.close()
        return prob,disc_l,None,None

def plot_ims_vs_ll_bg(smooth_ll,res_dir,sigma):
    fig, ax = plt.subplots(1, 1)
    pcm0 = ax.pcolormesh(smooth_ll[ ::-1, :], cmap='RdBu', vmin=np.amin(smooth_ll), vmax=np.amax(smooth_ll))
    ax.set_aspect('equal')
    fig.colorbar(pcm0, ax=ax)
    plt.savefig(os.path.join(res_dir,"Sigma_{}_LL_bg_prior".format(sigma)+".png"),dpi=300)

def plot_ims_vs_ll_grads_bg(smooth_ll,im,disc_ll,grads,res_dir,sigma):
    nb_figs = 4
    fig, ax = plt.subplots(nb_figs, 1)
    title = "Bg prior LL and gradients"
    fig.suptitle(title)
    ax[0].set_title("Smooth Likelihood")
    pcm0 = ax[0].pcolormesh(smooth_ll[ ::-1, :], cmap='RdBu', vmin=np.amin(smooth_ll), vmax=np.amax(smooth_ll))
    fig.colorbar(pcm0, ax=ax[0])
    ax[1].set_title("Discrete Likelihood")
    pcm1 = ax[1].pcolormesh(disc_ll[::-1, :], cmap='RdBu', vmin=np.amin(disc_ll), vmax=np.amax(disc_ll))
    fig.colorbar(pcm1, ax=ax[1])
    ax[2].set_title("Gradients")
    pcm2 = ax[2].pcolormesh(grads[::-1, :], cmap='RdBu', vmin=np.amin(grads), vmax=np.amax(grads))
    fig.colorbar(pcm2, ax=ax[2])
    ax[3].imshow(im,cmap="gray")
    ax[3].set_title("Image")
    for i in range(nb_figs):
        ax[i].axis('off')
        ax[i].set_aspect('equal')
    plt.savefig(os.path.join(res_dir,"Sigma_{}_LL_grad_bg_prior".format(sigma)+".png"),dpi=300)

def plot_ims_vs_ll_ut(ll,im,activation,prob,grads,res_dir,sigma):
    nb_figs = 5
    fig, ax = plt.subplots(nb_figs, 1)
    title="Ut prior LL and gradients"
    fig.suptitle(title)
    if not ll is None:
        ax[0].set_title("Likelihood")
        pcm0 = ax[0].pcolormesh(ll[ ::-1, :], cmap='RdBu', vmin=np.amin(ll), vmax=np.amax(ll))
        fig.colorbar(pcm0, ax=ax[0])
        ax[1].set_title("Gradient")
        pcm1 = ax[1].pcolormesh(grads[::-1, :], cmap='RdBu', vmin=np.amin(grads), vmax=np.amax(grads))
        fig.colorbar(pcm1, ax=ax[1])
    ax[2].imshow(im,cmap="gray")
    ax[2].set_title("Image")
    ax[3].imshow(activation,cmap="gray")
    ax[3].set_title("Activation")
    ax[4].imshow(prob,cmap="gray")
    ax[4].set_title("Probability")
    for i in range(nb_figs):
        ax[i].axis('off')
        ax[i].set_aspect('equal')
    plt.savefig(os.path.join(res_dir,"Sigma_{}_LL_grad_ut_prior".format(sigma)+".png"),dpi=300)

def plot_ims_vs_fidelity_1band(ll,im,org,title,res_dir,sigma):
    nb_figs=3
    fig, ax = plt.subplots(nb_figs, 1)
    pcm0 = ax[0].pcolormesh(ll[ ::-1, :], cmap='RdBu', vmin=np.amin(ll), vmax=np.amax(ll))
    ax[0].set_title("Likelihood")
    fig.suptitle(title)
    fig.colorbar(pcm0, ax=ax)
    ax[1].imshow(im,cmap="gray",vmin=0,vmax=1)
    ax[1].set_title("Pred. image")
    ax[2].imshow(org, cmap="gray",vmin=0,vmax=1)
    ax[2].set_title("Org image")
    for i in range(nb_figs):
        ax[i].axis('off')
        ax[i].set_aspect('equal')
    plt.savefig(os.path.join(res_dir,"Sigma_{}_Fidelity_grad".format(sigma)+".png"),dpi=300)

def plot_ims_vs_fidelity_msi(ll,im,org,title,res_dir,sigma):
    nb_ch = im.shape[-1]
    nb_figs=1+nb_ch
    fig, ax = plt.subplots(nb_figs, 2)
    pcm0 = ax[0,0].pcolormesh(ll[ ::-1, :], cmap='RdBu', vmin=np.amin(ll), vmax=np.amax(ll))
    ax[0,0].set_title("Likelihood")
    fig.suptitle(title)
    fig.colorbar(pcm0, ax=ax)
    for i in range(nb_ch):
        ax[i+1,0].imshow(im[:,:,i],cmap="gray",vmin=0,vmax=1)
    ax[1,0].set_title("Pred. image")
    for i in range(nb_ch):
        ax[i+1,1].imshow(org[:,:,i], cmap="gray",vmin=0,vmax=1)
    ax[1,1].set_title("Org image")
    for i in range(nb_figs):
        ax[i,0].axis('off')
        ax[i,0].set_aspect('equal')
        ax[i, 1].axis('off')
        ax[i, 1].set_aspect('equal')
    plt.savefig(os.path.join(res_dir,"Sigma_{}_Fidelity_grad".format(sigma)+".png"),dpi=300)

def viz_ll_reconst_im(main_dir,sigma,res_dir,pal_name):
    sigmas = {"0.01":11,"0.1":10,"0.2":9,"0.3":8,"0.4":7,"0.5":6,
              "0.6":5,"0.7":4,"0.8":3,"0.9":2,"1.0":1}
    epoch = sigmas[str(sigma)]*5000-1
    res_dir = os.path.join(main_dir, "training","langevin",pal_name,res_dir)
    d = files.json_to_dict(os.path.join(res_dir, "par.json"))
    nb_ch = d["nb_ch"]
    rest_epoch_sigma_bg = {0: 40, 0.01: 60, 0.1: 80, 0.2: 120, 0.3: 160,
                           0.4: 160, 0.5: 200, 0.6: 240, 0.7: 280,
                           0.8: 320, 0.9: 360, 1.0: 400}
    chpoint_path_bg = os.path.join(main_dir, "training", "generators", "background",
                                "pixelcnn-gray-2022-10-26-02-04", "noise_" + str(sigma),
                                "params_greek960_bg{}.ckpt".format(rest_epoch_sigma_bg[sigma]))
    rest_epoch_sigma_ut = {1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24, 0.4: 34,
                           0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
    chpoint_path_ut = os.path.join(main_dir, "training", "generators", "undertext",
                                        "pixelcnn-bin-2022-05-27-18-43","noise_" + str(sigma),
                                "params_greek960_ut{}.ckpt".format(rest_epoch_sigma_ut[sigma]))
    ut = io.imread(os.path.join(res_dir,"ut",str(epoch)+".png"),as_gray=True)/255.0
    bg = io.imread(os.path.join(res_dir, "bg", str(epoch) + ".png"),as_gray=True)/255.0
    ot = io.imread(os.path.join(res_dir, "mask.png"),as_gray=True)/255.0
    bg = 2.0*bg - 1
    if nb_ch==1:
        org = io.imread(os.path.join(res_dir, "corrupt_im" + ".png"), as_gray=True) / 255.0
    else:
        org = []
        for i in range(nb_ch):
            org.append(io.imread(os.path.join(res_dir, "corrupt_im_band_"+str(i)+".png"),as_gray=True)/255.0)
        org = np.stack(org,axis=2)
    patch_size = 64
    tile_shape = (org.shape[0] // patch_size, org.shape[1] // patch_size)
    batch_size = np.prod(tile_shape)
    uts = plotting.split_image(ut, patch_size)
    bgs = plotting.split_image(bg, patch_size)
    prob_ut,activation_ut,l_ut,grads_ut = gradients_ll_ut(uts, batch_size, sigma, chpoint_path_ut)
    smooth_l_bg,grads_bg,disc_l_bg = gradients_ll_bg(bgs,batch_size,sigma, chpoint_path_bg)
    prob_ut = plotting.stich_image(prob_ut, tile_shape)
    activation_ut = plotting.stich_image(activation_ut, tile_shape)
    l_ut = plotting.stich_image(l_ut, tile_shape)[:,:,0]
    grads_ut = plotting.stich_image(grads_ut, tile_shape)[:,:,0]
    smooth_l_bg = plotting.stich_image(smooth_l_bg,tile_shape)[:,:,0]
    grads_bg = plotting.stich_image(grads_bg, tile_shape)[:,:,0]
    disc_l_bg = plotting.stich_image(disc_l_bg, tile_shape)
    res_dir = os.path.join(res_dir,"viz_ll_grads","sigma_"+str(sigma))
    if not os.path.isdir(res_dir):
        os.makedirs(res_dir)
    plot_ims_vs_ll_ut(l_ut,ut,activation_ut,prob_ut,grads_ut,res_dir,sigma)
    plot_ims_vs_ll_grads_bg(smooth_l_bg,bg,disc_l_bg,grads_bg,res_dir,sigma)
    y_pred,ll = calc_ll(ut, ot, bg, org, sigma, d)
    if nb_ch==1:
        plot_ims_vs_fidelity_1band(ll,y_pred,org,"Likelihood of original image vs. predicted",res_dir,sigma)
    else:
        plot_ims_vs_fidelity_msi(ll, y_pred, org, "Likelihood of original image vs. predicted", res_dir, sigma)


def calc_ll(ut,ot,bg,org,sigma,d):

    du = d["Du"]
    do = d["Do"]
    dbg = d["Db"]
    org_noise_std = d["noise_level"]
    mix_fun_str = d["mix_fun_str"]
    nb_ch = d["nb_ch"]
    fid_loss = d["fid_loss"]
    with tf.Session() as sess:
        mix = MixingVars( du, do, ut.ravel(), ot.ravel(), bg.ravel(), sigma, sigma, org_noise_std,
                         False, False, True, False, np.prod(ut.shape),
                         mix_fun_str, 0.0, dbg, sess, nb_ch)

        y = np.reshape(mix.y,(ut.shape[0],ut.shape[1],nb_ch))
        org_tf = tf.constant(org, dtype=tf.float32)
        y_tf = tf.constant(y, dtype=tf.float32)
        if nb_ch==1:
            phi_list_ut_f = os.path.join(main_dir, "training",
                                         "langevin", mix_fun_str,
                                         "phi_list_nb_layers_1_org_noise_{}_xu.json".format(org_noise_std))
            phi_list_ut = files.json_to_dict(phi_list_ut_f)
            phi = phi_list_ut[str(sigma)][str(round(du, 2))]
            _,ll_tf = get_grads_fidelity_smoothed_apprx_1band(org_tf,y_tf,phi,y_tf)
            ll = sess.run(ll_tf)
        elif nb_ch>1 and fid_loss=="mvn":
            round_du = str([round(u, 3) for u in du]).replace(" ", "")
            phi_list_ut_f = os.path.join(main_dir, "training",
                                         "langevin", mix_fun_str, "mvn",
                                         "xu_{}_on_{}_Du_{}.npz".format(pal_name, org_noise_std, round_du))
            phi_list_ut = np.load(phi_list_ut_f,allow_pickle=True)
            phi = phi_list_ut[str(sigma)].astype(np.float32)
            cov_tf = tf.constant(phi,dtype=tf.float32)
            _, ll_tf = get_grads_fidelity_smoothed_apprx_msi(org_tf, y_tf, cov_tf, y_tf)
            ll = sess.run(ll_tf)
    return y,ll

if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    res_dir = "with_bg_t_2023-01-31-22-04"
    sigma = 0.01
    pal_name = "greek960"
    viz_ll_reconst_im(main_dir,sigma,res_dir,pal_name)
    plt.show()
