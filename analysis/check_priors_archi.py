"""Vizualize Disc and Smooth  likelihood for Archimedes character reconstruction"""
from pixelcnn_palimpsest.analysis.viz_ut_prior_pdf import  read_layers
import tensorflow as tf
import os
import numpy as np
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_pixelcnn_ut_model_with_par,compile_pixelcnn_bg_model_with_par
from pixelcnn_palimpsest.pixel_cnn_pp.nn import sigmoid_loss_smoothed,log_loss_smoothed,discretized_mix_logistic_loss_gray
from pixelcnn_palimpsest.langevin_dynamics.prior import restore_model
from matplotlib import pyplot as plt


def grad_ll_tf_prior(chpoint_dir,sigma, ut_model,mode,batch_size=1):

    tf.compat.v1.reset_default_graph()
    sess = tf.Session()
    if mode=="ut":

        xs, ys,_ = compile_pixelcnn_ut_model_with_par(batch_size,ut_model)

        if ut_model=="discrete":
            res_epochs = {0.0: 300, 0.01: 100, 0.1:100,0.2: 100, 1.0: 100, 0.9: 100, 1.2: 100}
            res_epoch = res_epochs[sigma]
            chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                        "params_greek960_ut{}.ckpt".format(res_epoch))
            restore_model(chpoint_path, sess, "model_ut")
            sll_tf = log_loss_smoothed(xs, ys, tf.constant(sigma, dtype=tf.float32), False)
            disc_tf = discretized_mix_logistic_loss_gray(xs,ys,False)
            l_tf = tf.exp(-disc_tf)
        elif ut_model == "binary":
            res_epochs = {1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24, 0.4: 34,
                           0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
            res_epoch = res_epochs[sigma]
            chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                        "params_greek960_ut{}.ckpt".format(res_epoch))
            restore_model(chpoint_path, sess, "model_ut")
            sll_tf = sigmoid_loss_smoothed(xs, ys, tf.constant(sigma, dtype=tf.float32), False)
            #disc_tf = tf.expand_dims(ys, axis=0, name=None)
            disc_tf = tf.nn.sigmoid_cross_entropy_with_logits(labels = xs, logits = ys)
            l_tf = tf.exp(-disc_tf)

    elif mode=="bg":
        res_epochs = {0: 40, 0.01: 60, 0.1: 80, 0.2: 120, 0.3: 160,
                               0.4: 160, 0.5: 200, 0.6: 240, 0.7: 280,
                               0.8: 320, 0.9: 360, 1.0: 400}
        res_epoch = res_epochs[sigma]
        chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                    "params_greek960_bg{}.ckpt".format(res_epoch))
        xs, ys,_ = compile_pixelcnn_bg_model_with_par(batch_size)
        restore_model(chpoint_path, sess, "model_bg")
        disc_tf = discretized_mix_logistic_loss_gray(xs, ys, False)
        l_tf = tf.exp(-disc_tf)
        sll_tf = log_loss_smoothed(xs, ys, tf.constant(sigma, dtype=tf.float32), False)
    sl_tf = tf.exp(sll_tf)

    return xs,sl_tf,l_tf,sess

def plot_ims_vs_ll_pred(im,smooth_ll,disc_ll,title):
    nb_figs = 3
    fig, ax = plt.subplots(nb_figs, 1)
    fig.suptitle(title)

    ax[1].set_title("Smooth likelihood",pad=20,fontsize=10)
    ax[1].imshow(np.squeeze(smooth_ll), cmap='RdBu', vmin=np.amin(smooth_ll), vmax=np.amax(smooth_ll))
    pcm0 = ax[1].pcolormesh(np.squeeze(smooth_ll), cmap='RdBu', vmin=np.amin(smooth_ll),
                            vmax=np.amax(smooth_ll))
    fig.colorbar(pcm0, ax=ax[1])
    ax[0].imshow(np.squeeze(im),cmap="gray",vmin=np.amin(im), vmax=np.amax(im))
    ax[0].set_title("Image",pad=20,fontsize=10)

    ax[2].imshow(np.squeeze(disc_ll), cmap='RdBu', vmin=np.amin(disc_ll),
                            vmax=np.amax(disc_ll))
    pcm0 = ax[2].pcolormesh(np.squeeze(disc_ll), cmap='RdBu', vmin=np.amin(disc_ll),
                            vmax=np.amax(disc_ll))
    fig.colorbar(pcm0, ax=ax[2])
    ax[2].set_title("Disc likelihood",pad=20,fontsize=10)
    for i in range(nb_figs):
        ax[i].axis('off')
        ax[i].set_aspect('equal')
    plt.tight_layout()



if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    ut_model = "discrete"
    if ut_model == "discrete":
        chpoint_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-gray-2023-02-08-04-13"
    elif ut_model == "binary":
        chpoint_dir = os.path.join(main_dir, "training", "generators", "undertext", "pixelcnn-bin-2022-05-27-18-43")
    res_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\1band\lower_mu\obscured_mu_lower_64x64_rgb_30"
    # r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\with_bg_t_2023-03-07-04-52"
    epoch = 109999
    batch_size = 1
    sigma = 0.01
    ot, ut, bg, org, batch_size, tile_shape = read_layers(res_dir,epoch,ut_model)
    #ut +=np.random.normal(0,scale=sigma,size=ut.shape)
    bg += np.random.normal(0, scale=sigma,size=ut.shape)
    mode = "ut"
    xs_ut,sll_ut_tf,disc_ut_tf,sess = grad_ll_tf_prior(chpoint_dir,sigma,ut_model,mode,batch_size)
    sll_ut,disc_ut = sess.run([sll_ut_tf,disc_ut_tf],{xs_ut: np.expand_dims(ut,[0,3])})
    plot_ims_vs_ll_pred(ut,sll_ut,disc_ut,"Ut")

    sess.close()
    mode = "bg"
    chpoint_dir = os.path.join(main_dir, "training", "generators", "background",
                               "pixelcnn-gray-2022-10-26-02-04")
    xs_bg,sll_bg_tf,disc_bg_tf,sess = grad_ll_tf_prior(chpoint_dir,sigma,ut_model,mode,batch_size)
    sll_bg,disc_bg = sess.run([sll_bg_tf,disc_bg_tf], {xs_bg: np.expand_dims(bg,[0,3])})
    plot_ims_vs_ll_pred(bg,sll_bg,disc_bg,"Ut")
    sess.close()
    plt.show()