import os
import xml.etree.ElementTree as ET
from skimage import io
import numpy as np
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.utils.read_images import strech_contrast
from sklearn.svm import OneClassSVM
from matplotlib import pyplot as plt
from skimage import transform


def read_bg_bbox_corners_coord(fpath):
    """Read coordinates of bounding boxes around background"""
    bs_data = ET.parse(fpath)
    root = bs_data.getroot()
    region = root.find('Region')
    geometry = region.find("GeometryDef")
    bboxs = []
    for polygon in geometry.iter("Polygon"):
        exterior = polygon.find("Exterior")
        linear_ring = exterior.find("LinearRing")
        bbox = linear_ring.find('Coordinates')
        bbox = bbox.text.split()
        bbox = {"top_left":(bbox[0],bbox[1]),"top_right":(bbox[2],bbox[3]),
             "bottom_right":(bbox[4],bbox[5]),"bottom_left":(bbox[6],bbox[7])}
        for key,value in bbox.items():
            bbox[key]={"col":int(float(value[0])),"row":int(float(value[1]))}
        bboxs.append(bbox)
    return bboxs

def read_stretch_contr_image(datapath,folioname,bandname):
    im = io.imread(os.path.join(datapath,folioname,folioname+"+"+bandname+".tif"),as_gray=True)
    if np.amax(im) > 255:
        max_val = 65535
        #im = im / 65535.0
        #im = im*255
    elif np.amax(im) > 1 and np.amax(im) < 256:
        max_val = 256
    else:
        raise IOError("Expected format is uint8 or uint16")
    im = strech_contrast(im,max_val)
    return im

def read_bg_features(datapath,bandnames,folio_idx):
    folioname = r"0086_000{}".format(folio_idx)
    bboxs_coord = read_bg_bbox_corners_coord(os.path.join(datapath, folioname, "background_roi.xml"))[1:]
    features = []

    for bbox_idx, bbox in enumerate(bboxs_coord):
        features_box = np.zeros((bbox["bottom_right"]["row"]-bbox["top_left"]["row"],
                                bbox["bottom_right"]["col"]-bbox["top_left"]["col"],len(bandnames)))
        for band_idx,bandname in enumerate(sorted(bandnames)):
            im = read_stretch_contr_image(datapath, folioname, bandname)
            im_in_box = im[bbox["top_left"]["row"]:bbox["bottom_right"]["row"],
                        bbox["top_left"]["col"]:bbox["bottom_right"]["col"]]
            features_box[:,:,band_idx] = im_in_box
        plt.figure()
        plt.imshow(features_box[:,:,0])
        features.extend(features_box.reshape(-1,len(bandnames)))
        if len(features)>10000:
            print("Break")
            break

    return features[:9999]

def read_bg_fragment(datapath,bandnames,folio_idx,coords):
    folioname = r"0086_000{}".format(folio_idx)
    im_msi = np.zeros((coords[1][1]-coords[1][0],coords[0][1]-coords[0][0],len(bandnames)))
    for band_idx,bandname in enumerate(sorted(bandnames)):
        im = read_stretch_contr_image(datapath, folioname, bandname)
        im = transform.rotate(im,-90,preserve_range=True)

        im_msi[:,:,band_idx] = im[coords[1][0]:coords[1][1],coords[0][0]:coords[0][1]]

    return im_msi

def read_bandnames(fpath):
    bands = []
    with open(fpath,"r") as f:
        for line in f:
            line = line.strip()
            if "WB" in line or "MB" in line:
                bands.append(line)
    return bands

def novelty_detection():
    datapath = r"C:\Data\PhD\palimpsest\Greek_960"
    coords = [[2600,3600],[1000,1250]]
    folio_idx_test = "084"
    bandnames = read_bandnames(os.path.join(datapath,"0086_000084","band_names.txt"))
    features = read_bg_features(datapath, bandnames, folio_idx_test)
    clf = OneClassSVM(gamma='auto',nu=0.01,degree=2).fit(features)

    fragment = read_bg_fragment(datapath,bandnames,folio_idx_test,coords)
    plt.figure()
    plt.imshow(fragment[:,:,0], cmap="gray")
    frag_shape = fragment.shape
    fragment = np.reshape(fragment,[-1,len(bandnames)])
    pred = clf.predict(fragment)
    pred =pred.reshape([frag_shape[0],frag_shape[1]])
    plt.figure()
    plt.imshow(pred,cmap="gray")
    plt.show()

if __name__=="__main__":
    novelty_detection()