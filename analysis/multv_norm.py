import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
from math import pi
from scipy.stats import multivariate_normal,norm
from matplotlib import pyplot as plt
from skimage import io
from pixelcnn_palimpsest.langevin_dynamics import posterior, data
from pixelcnn_palimpsest.utils.files import json_to_dict
from pixelcnn_palimpsest.analysis import restr_vs_contrast
import os

def multivariate_normal_anna(loc,scale,x):
    k = len(scale)
    cov = scale * np.eye(k)
    det_cov = np.linalg.det(cov)
    a = 1/( ( ( det_cov*(2*pi)**k ))**0.5 )
    inv_cov = np.linalg.inv(cov)
    b = np.exp(-0.5*np.dot(np.dot((x-loc).T,inv_cov),x-loc))
    return a*b,det_cov

def check_mvn_errors():
    loc = np.array([0.8, 0.8, 0.8])
    scale = np.array([0.1, 0.1, 0.1])
    x = np.array([1.1, 0.2, 0.8])
    prob, inv_cov = multivariate_normal_anna(loc, scale, x)
    print("Pdf from Anna",prob)
    prob = multivariate_normal.pdf(x, mean=loc, cov=scale * np.eye(len(loc)))
    print("Pdf from scipy",prob)
    x_tf = tf.constant(x, dtype=tf.float32)
    loc_tf = tf.constant(loc, dtype=tf.float32)
    phi_tf = tf.constant(scale, dtype=tf.float32)
    dist = tfp.distributions.MultivariateNormalDiag(loc_tf, scale_diag=phi_tf)
    loss_tf = dist.prob(x_tf)
    dist = tfp.distributions.MultivariateNormalFullCovariance(loc_tf,
                                                              covariance_matrix=phi_tf * tf.constant(np.eye(len(loc)),
                                                                                                     dtype=tf.float32))
    loss_tf = dist.prob(x_tf)
    with tf.Session() as sess:
        print("Pdf from tensorflow probability",sess.run(loss_tf))

def get_phi(phi_dict_ut,phi_dict_bg,sess,Du,Db):
    Du = round_to_2f(Du)
    Db = round_to_2f(Db)
    phi_bg,phi_ut = [],[]
    for ch in range(len(Du)):
        phi_ut.append(phi_dict_ut[str(Du[ch])])
        phi_bg.append(phi_dict_bg[str(Db[ch])])
    if len(Du)==1:
        return phi_ut[0],phi_bg[0]
    else:
        return np.array(phi_ut),np.array(phi_bg)

def round_to_2f(v):
    """Round a float to have 2 decimal place"""
    if type(v) is list:
        v = np.array(v)
    return np.floor(v * 100) / 100

def check_pdf_for_interm_result_art_pal(exp_dir,epoch):
    main_dir = r"/"
    mix_fun_str = "sep_layers_bg_not_fn"
    band_flist = os.listdir(os.path.join(exp_dir,r"restored"))
    band_list = []
    for band in band_flist:
        b_f = os.path.join(exp_dir,r"restored",band,str(epoch)+band+".png")
        b = io.imread(b_f,as_gray=True) / 255.0
        #b_f = os.path.join(exp_dir, r"restored", band, str(epoch) + band + ".npy")
        #b = np.load(b_f)
        band_list.append(b)
    y_pred = np.stack(band_list,axis=2)[np.newaxis,:,:,:]
    ###read the original image

    d = json_to_dict(os.path.join(main_dir, exp_dir, "par.json"))
    Do = d["Do"]
    Du = d["Du"]
    Db = d["Db"]
    noise_level = d["noise_level"]
    y_true, im_ut, im_ot = restr_vs_contrast.create_palimpsest(noise_level, Do, Du, Db)
    nb_ch = y_true.shape[-1]
    sigma = 1.0

    phi_list_ut = os.path.join(main_dir, "training",
                                   "langevin", mix_fun_str,
                                   "phi_list_nb_layers_1_org_noise_{}_xu.json".format(noise_level))
    phi_list_bg = os.path.join(main_dir, "training",
                                       "langevin", mix_fun_str,
                                       "phi_list_nb_layers_1_org_noise_{}_xb.json".format(noise_level))

    phi_list_ut = json_to_dict(phi_list_ut)

    phi_list_bg = json_to_dict(phi_list_bg)
    sess = tf.Session()
    phi_ut,phi_bg = get_phi(phi_list_ut[str(sigma)],phi_list_bg[str(sigma)],sess,Du,Db)
    y_true_tf = tf.constant(y_true,dtype=tf.float32)
    cov = (phi_ut**2)* np.eye(len(phi_ut))
    print("Covarience",cov)
    cov_tf = tf.constant(cov,dtype=tf.float32)
    y_pred_tf = tf.constant(y_pred, dtype=tf.float32)
    phi_ut_tf = tf.constant(phi_ut, dtype=tf.float32)
    _,loss_tf = posterior.get_grads_fidelity_smoothed_apprx_msi(y_true_tf, y_pred_tf, phi_ut_tf, y_true_tf)
    with tf.Session() as sess:
        print("Pdf from tensorflow probability", np.mean(sess.run(loss_tf)))
    sess.close()
    nb_samples = np.prod(y_true.shape[0:-1])
    y_pred_r = np.reshape(y_pred, [-1, nb_ch])
    y_true_r = np.reshape(y_true,[-1,nb_ch])

    prob = np.zeros((y_true_r.shape[0]))
    for i in range(nb_samples):
        prob[i] = multivariate_normal.pdf(y_pred_r[i], mean=y_true_r[i], cov=cov)
    print("Pdf from scipy", np.mean(prob))
    fig,ax = plt.subplots(nb_ch,2)
    for i in range(nb_ch):
        ax[i, 0].imshow(y_true[0,:,:,i],cmap="gray",vmin=0.0,vmax=1.0)
        ax[i, 1].imshow(y_pred[0,:,:,i], cmap="gray",vmin=0.0,vmax=1.0)


def check_pdf_for_interm_result_greek960(exp_dir,epoch):
    main_dir = r"/"
    mix_fun_str = "sep_layers_bg_not_fn"
    band_flist = os.listdir(os.path.join(exp_dir,r"restored"))
    band_list = []
    for band in band_flist:
        b_f = os.path.join(exp_dir,r"restored",band,str(epoch)+band+".png")
        b = io.imread(b_f,as_gray=True) / 255.0
        b_f = os.path.join(exp_dir, r"restored", band, str(epoch) + band + ".npy")
        b = np.load(b_f)
        band_list.append(b)
    y_pred = np.stack(band_list,axis=2)[np.newaxis,:,:,:]
    ###read the original image

    d = json_to_dict(os.path.join(main_dir, exp_dir, "par.json"))
    Do = d["Do"]
    Du = d["Du"]
    Db = d["Db"]
    nb_ch = 9
    noise_level = 1e-2
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
    row_coord = [2300, 2300 + 192]
    col_coord = [1800, 2914]
    rot_angle = -90
    convert_opt_dens = False
    palimpsest_fname = "0086_000084+WBRBG58_018_F.tif"
    bg_refl = 1403.722532 / 65535.0
    pal = data.PrepGreek960(data_dir,
                            row_coord, col_coord,
                            True, fname_ot,
                            convert_opt_dens, rot_angle,
                            palimpsest_fname, bg_refl)
    y_true = pal.im_out
    nb_ch = y_true.shape[-1]
    sigma = 1.0

    phi_list_ut = os.path.join(main_dir, "training",
                                   "langevin", mix_fun_str,
                                   "phi_list_nb_layers_1_org_noise_{}_xu.json".format(noise_level))
    phi_list_bg = os.path.join(main_dir, "training",
                                       "langevin", mix_fun_str,
                                       "phi_list_nb_layers_1_org_noise_{}_xb.json".format(noise_level))

    phi_list_ut = json_to_dict(phi_list_ut)

    phi_list_bg = json_to_dict(phi_list_bg)
    sess = tf.Session()
    phi_ut,phi_bg = get_phi(phi_list_ut[str(sigma)],phi_list_bg[str(sigma)],sess,Du,Db)
    y_true_tf = tf.constant(y_true,dtype=tf.float32)
    cov = (phi_ut**2)* np.eye(len(phi_ut))
    print("Covarience",cov)
    cov_tf = tf.constant(cov,dtype=tf.float32)
    y_pred_tf = tf.constant(y_pred, dtype=tf.float32)
    phi_ut_tf = tf.constant(phi_ut, dtype=tf.float32)
    _,loss_tf = posterior.get_grads_fidelity_smoothed_apprx_msi(y_true_tf, y_pred_tf, phi_ut_tf, y_true_tf)
    with tf.Session() as sess:
        print("Pdf from tensorflow probability", np.mean(sess.run(loss_tf)))
    sess.close()
    nb_samples = np.prod(y_true.shape[0:-1])
    y_pred_r = np.reshape(y_pred, [-1, nb_ch])
    y_true_r = np.reshape(y_true,[-1,nb_ch])

    prob = np.zeros((y_true_r.shape[0]))
    for i in range(nb_samples):
        prob[i] = multivariate_normal.pdf(y_pred_r[i], mean=y_true_r[i], cov=cov)
    print("Pdf from scipy", np.mean(prob))
    fig,ax = plt.subplots(nb_ch,2)
    for i in range(nb_ch):
        ax[i, 0].imshow(y_true[0,:,:,i],cmap="gray",vmin=0.0,vmax=1.0)
        ax[i, 1].imshow(y_pred[0,:,:,i], cmap="gray",vmin=0.0,vmax=1.0)

def simple_exp():
    true_y = np.array([0.1,0.2])
    pred_y = true_y+np.array([0.01,0.0])
    scale = 0.8
    cov = np.array([[scale**2,0],[0,0.0001**2]])
    mvn_pdf = multivariate_normal.pdf(pred_y, mean=true_y, cov=cov)
    uvn_pdf = norm.pdf(pred_y[0],loc=true_y[0],scale=scale)
    print("MVN prob",mvn_pdf)
    print("UVN prob",uvn_pdf)



if __name__=="__main__":
    exp_name = r"with_bg_t_2022-12-01-21-28"
    main_dir = r"/training/langevin/greek960"
    exp_dir = os.path.join(main_dir,exp_name)
    epoch = 0
    #check_pdf_for_interm_result_greek960(exp_dir,0)
    simple_exp()
    plt.show()