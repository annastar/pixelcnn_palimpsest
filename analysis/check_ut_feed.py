import numpy as np
import tensorflow as tf
from pixelcnn_palimpsest.data.greek960_ut_data import DataLoaderBgAug
from matplotlib import pyplot as plt


def make_feed_dict(data,argsi):

    if type(data) == tuple:
        if argsi.class_conditional:
            if len(data)>2:
                x_tr, x_aug_text, y = data
            else:
                x_tr,y = data
                x_aug_text = None
        else:
            x_tr,x_aug_text = data
            y = None
    else:
        x_tr = data
        y = None
        x_aug_text = None
    if argsi.binary_data==True or argsi.cross_entropy==True:
        x_tr = np.cast[np.float32](x_tr)/255.0
        if not x_aug_text is None:
            x_aug_text = np.cast[np.float32](x_aug_text)/255.0
    else:
        x_tr = np.cast[np.float32]((x_tr - 127.5) / 127.5) # input to pixelCNN is scaled from uint8 [0,255] to float in range [-1,1]
        if not x_aug_text is None:
            x_aug_text = np.cast[np.float32]((x_aug_text - 127.5) / 127.5)
    noise = argsi.noise_level * np.random.normal(0.0, 1.0, x_tr.shape)
    noise = np.split(noise, argsi.nr_gpu)
    x_tr = np.split(x_tr, argsi.nr_gpu)

    feed_dict = {argsi.xs_true[i] : x_tr[i] for i in range(argsi.nr_gpu)}
    feed_dict.update({argsi.ns[i]: noise[i] for i in range(argsi.nr_gpu)})
    if not x_aug_text is None:
        x_aug_text = np.split(x_aug_text, argsi.nr_gpu)
        feed_dict.update({argsi.xs_input[i]: x_aug_text[i] for i in range(argsi.nr_gpu)})
    else:
        feed_dict.update({argsi.xs_input[i]: x_tr[i] for i in range(argsi.nr_gpu)})
    if y is not None:
        y = np.split(y, argsi.nr_gpu)
        feed_dict.update({argsi.ys[i]: y[i] for i in range(argsi.nr_gpu)})
    return feed_dict


class Helper():
    def __init__(self,batch_size,nr_gpu,obs_shape):
        self.cross_entropy=False
        self.noise_level = 0.01
        self.nr_gpu=nr_gpu
        self.batch_size = batch_size
        self.class_conditional = False
        self.binary_data = True
        self.obs_shape = obs_shape
        # data place holders
        self.xs_true = [tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape, name="x_" + str(i)) for i in
                   range(self.nr_gpu)]
        # noise place holders
        self.ns = [tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape, name="noise_" + str(i)) for i in
              range(self.nr_gpu)]
        # masks place holders
        self.xs_input = [tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape,) for
                    i in range(self.nr_gpu)]
        self.ys = [tf.placeholder(tf.int32, shape=(self.batch_size,)) for i in range(self.nr_gpu)]

def check_ut_feed():
    aug_bg_data_dir = r"/datasets/Greek_960/Greek960_bg_dataset_strech_contr_step_64_size_192"
    subset = "test"
    nr_gpu = 2
    data_dir = r'/datasets/Greek_960/Greek960_ut_dataset_50step_192x192'
    batch_size = 16
    data = DataLoaderBgAug(data_dir, subset, batch_size*nr_gpu, rng=None,
                           shuffle=True, augment=False,
                           aug_with_bg=None if aug_bg_data_dir is None else {"data_dir": aug_bg_data_dir})
    obs_shape = data.get_observation_size()  # e.g. a tuple (32,32,3)
    args = Helper(batch_size,nr_gpu,obs_shape)

    sess = tf.Session()
    j = 0
    for x in data:
        d = make_feed_dict(x,args)
        print(d.keys())
        fig,ax = plt.subplots(len(d.keys()),1)
        i=0
        for key,item in d.items():
            print(key)
            ax[i].imshow(np.squeeze(item[8]),cmap="gray")
            i+=1
        j+=1
        if j>2:
            break
    sess.close()
    plt.show()

if __name__=="main":
    check_ut_feed()