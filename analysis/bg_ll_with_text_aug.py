import tensorflow as tf
import numpy as np
import os
from skimage import io
from pixelcnn_palimpsest.utils import plotting
from pixelcnn_palimpsest.langevin_dynamics import data
from pixelcnn_palimpsest.analysis.viz_ll_res_gradients import gradients_ll_bg,plot_ims_vs_ll_bg


def read_bg_Greek960(main_dir):
    data_dir_pal = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
    row_coord = [2300, 2300 + 192]
    col_coord = [1094, 1094+192]
    rot_angle = -90
    mix_fun_str = r"sep_layers_bg_not_fn"
    msi = True
    d = data.PrepGreek960(data_dir_pal, row_coord, col_coord, msi, fname_ot,
                          rot_angle, mix_fun_str=mix_fun_str, msi_partial=False)
    return d.im_out

def read_Archimedes(main_dir):
    data_dir = os.path.join(main_dir, "datasets", r"Archimedes", r"017r-016v_Arch07r_Sinar",r"normalized")
    fname_ot = r"overtext_map.png"
    row_coord = [6236, 6236 + 192]
    col_coord = [5044, 5044+192]
    rot_angle = 0
    mix_fun_str = "sep_layers_bg_not_fn"
    msi = True
    d = data.PrepArchimedes(data_dir, row_coord, col_coord, msi, fname_ot,
                            rot_angle, mix_fun_str)
    return d.im_out

def create_bg_with_text_aug(main_dir):
    """Visualize bg prior grad for a single im"""
    ###reset graph
    tf.compat.v1.reset_default_graph()
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
    data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"line_4_moved.png"
    fname_ut = r"line_27.png"
    row_coord = [0, 192]
    col_coord = [0, 192 * 4]
    pal = data.PrepArtPal(data_dir, data_dir_bg,
                          row_coord, col_coord, fname_ot,
                          fname_ut, True,True)
    patch_size = 64
    scaler = 1.0#1.3236
    bg_ut = (1.0/scaler)*pal.x_bg-(0.04/scaler)*pal.x_ut
    bg_ut = 2*bg_ut-1.0
    tile_shape = (pal.x_ut.shape[1] // patch_size, pal.x_ut.shape[2] // patch_size)
    batch_size = np.prod(tile_shape)
    ims = plotting.split_image(bg_ut[0, :, :, 0], patch_size)
    return ims,batch_size,tile_shape

def viz_bg_prior_text_aug(chpoint_dir, ims, batch_size,tile_shape):
    res_dir = os.path.join(chpoint_dir, "Reaction_to_text_aug")
    if not os.path.isdir(res_dir):
        os.makedirs(res_dir)
    im = plotting.stich_image(ims, tile_shape)
    io.imsave(os.path.join(res_dir,"org_img.png"),im)
    sigmas = [0.01,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    res_epochs = {0: 40, 0.01: 80, 0.1: 120, 0.2: 160,
                  0.3: 200, 0.4: 240, 0.5: 280, 0.6: 320,
                  0.7: 360, 0.8: 400, 0.9: 440, 1.0: 480}

    for sigma in sigmas:
        ims_new = ims+np.random.normal(0,sigma,ims.shape)
        im = plotting.stich_image(ims_new, tile_shape)
        io.imsave(os.path.join(res_dir, "org_img_noise_{}.png".format(sigma)), im)
        res_epoch = res_epochs[sigma]
        chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                    "params_greek960_bg{}.ckpt".format(res_epoch))
        smooth_l,grads,disc_l = gradients_ll_bg(ims_new,batch_size,sigma,chpoint_path)
        ll_full = plotting.stich_image(disc_l, tile_shape)
        plot_ims_vs_ll_bg(ll_full[:,:],res_dir,sigma)


if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    ch_dir_path = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\background\text_aug_exp_Greek960"
    ims, batch_size, tile_shape = create_bg_with_text_aug(main_dir)
    ch_dir= r"pixelcnn-gray-2022-12-23-19-08_with_text_aug"
    viz_bg_prior_text_aug(os.path.join(ch_dir_path,ch_dir),ims,batch_size,tile_shape)
    ch_dir = r"pixelcnn-gray-2022-12-21-20-32_without_text_aug"
    viz_bg_prior_text_aug(os.path.join(ch_dir_path, ch_dir), ims, batch_size, tile_shape)
