import tensorflow as tf
import matplotlib as mpl
from matplotlib.colors import colorConverter
import os
from pixelcnn_palimpsest.langevin_dynamics.data import PrepGreek960,PrepArchimedes
import numpy as np
import math
from scipy.stats import multivariate_normal
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.utils.files import json_to_dict
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_msi, get_grads_fidelity_smoothed_apprx_1band
from skimage import io

def read_msi_greek_960(main_dir):
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084","normalized_WB")
    fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
    row_coord = [2300, 2300 + 192]
    col_coord = [1800, 2914]
    rot_angle = -90
    mix_fun_str = r"sep_layers_bg_ind_from_ot_not_fn"
    msi = True
    data = PrepGreek960(data_dir, row_coord, col_coord, msi, fname_ot,
                         rot_angle, mix_fun_str=mix_fun_str,msi_partial=False)
    return data, data.ut_refl,data.bg_mean

def calc_angle(du,db):
    nom = np.sum(du*db,axis=3)
    den = np.sqrt(np.sum(du ** 2, axis=3) * np.sum(db ** 2, axis=3))
    den[den==0]=1e-9
    angle = np.arccos(nom/den)
    return angle*180/math.pi

def calc_spect_angle_btw_vec_image(data,vec):
    im = data.im_out
    vec = np.array(vec)[np.newaxis, np.newaxis, np.newaxis, :]
    angle = calc_angle(im, vec)[0,:,:]
    angle = angle/np.amax(angle)
    return angle

def save_spect_angle_rgb_image(data,vec_1,vec_2,vec_3,pal_dir):
    angle_r = calc_spect_angle_btw_vec_image(data,vec_1)
    angle_g = calc_spect_angle_btw_vec_image(data, vec_2)
    angle_b = calc_spect_angle_btw_vec_image(data, vec_3)
    angle = np.stack([angle_r,angle_g,angle_b],axis=2)
    io.imsave(os.path.join(pal_dir,"sangle_im_ot_rgb.png"),angle)

def plot_the_angle(data,vec,title):
    im = data.im_out
    vec = np.array(vec)[np.newaxis,np.newaxis,np.newaxis,:]
    vec = np.zeros_like(im)+vec
    angle = calc_angle(im,vec)
    fig,ax = plt.subplots(1,1)
    ax.set_title(title)
    ax_obj = ax.imshow(np.squeeze(angle),cmap='RdBu')
    fig.colorbar(ax_obj, ax=ax,
                 orientation='vertical')

def plot_the_angle_with_mask(data,vec,title):
    im = data.im_out
    mask = data.x_ot
    thresh = 0.3
    mask[mask > thresh] = 1.0
    mask[mask < thresh] = 0.0
    vec = np.array(vec)[np.newaxis,np.newaxis,np.newaxis,:]
    vec = np.zeros_like(im)+vec
    angle = calc_angle(im,vec)

    # generate the colors for your colormap
    color1 = colorConverter.to_rgba('black')
    color2 = colorConverter.to_rgba('green')

    # make the colormaps
    cmap2 = mpl.colors.LinearSegmentedColormap.from_list('my_cmap2', [color1, color2], 2)

    cmap2._init()  # create the _lut array, with rgba values

    # create your alpha array and fill the colormap with them.
    # here it is progressive, but you can create whathever you want
    alphas = np.linspace(0.0, 0.8, cmap2.N)
    cmap2._lut[:cmap2.N, -1] = alphas
    fig,ax = plt.subplots(1,1)
    ax.set_title(title)

    ax.imshow(np.squeeze(angle),cmap='RdBu')
    ax.imshow(np.squeeze(mask),cmap=cmap2)



def plot_likelihood_mvn(main_dir,data,vec,d,title,mode,mix_fun_str,pal_name):

    im = data.im_out
    fig,ax = plt.subplots(11,1)
    round_du = str([round(u, 3) for u in d]).replace(" ", "")

    #vec = np.array(vec)[np.newaxis, np.newaxis, np.newaxis, :]
    #vec = np.zeros_like(im) + vec
    vec_tf = tf.constant(vec,dtype=tf.float32)
    im_tf = tf.constant(im,dtype=tf.float32)
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    sess = tf.Session()
    for i,s in enumerate(sigma_list):
        phi_list_ut_f = os.path.join(main_dir, "training",
                                     "langevin", mix_fun_str, "mvn",
                                     "{}_{}_on_{}_{}_{}.npz".format(mode,pal_name, 1e-2,"Du" if mode=="xu" else "Db", round_du))
        cov = np.load(phi_list_ut_f, allow_pickle=True)[str(s)]
        det = np.linalg.det(cov)
        print("Determinant",det)
        cov_tf = tf.constant(cov,dtype=tf.float32)
        grads_tf,loss_tf = get_grads_fidelity_smoothed_apprx_msi(im_tf, vec_tf, cov_tf, vec_tf)
        prob = multivariate_normal.pdf(im, mean=np.array(vec), cov=cov)
        #prob = sess.run(loss_tf)
        print("MVN likelihood",np.mean(prob))
        a = ax[i].imshow(np.squeeze(prob),cmap='RdBu',vmin=0,vmax=1e2)
        ax[i].set_title("sigma=" + str(s))
    #fig.colorbar(ax[i - 1].imshow(np.squeeze(prob), cmap=plt.cm.RdBu, vmin=0, vmax=np.amax(cum_prob)), ax=ax[i - 1],orientation='vertical')
    axcb = fig.colorbar(a, ax=ax.ravel().tolist(), pad=0.04, aspect=30)
    plt.suptitle(title)
    sess.close()

def plot_likelihood_uvn(main_dir,data,vec,d,title,mode,mix_fun_str,pal_name):

    idx=5
    im = data.im_out[:,:,:,idx][:,:,:,np.newaxis]
    fig,ax = plt.subplots(11,1)
    du = str(round(d[idx], 2))

    #vec = np.array(vec)[np.newaxis, np.newaxis, np.newaxis, :]
    #vec = np.zeros_like(im) + vec
    vec_tf = tf.constant(vec[idx],dtype=tf.float32)
    im_tf = tf.constant(im,dtype=tf.float32)
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    sess = tf.Session()
    for i,s in enumerate(sigma_list):
        phi_list_ut_f = os.path.join(main_dir, "training",
                                     "langevin", mix_fun_str,"phi_list_nb_layers_1_org_noise_{}_xb.json".format(1e-2))
        std = json_to_dict(phi_list_ut_f)[str(s)][du]
        std_tf = tf.constant(std,dtype=tf.float32)
        grads_tf,loss_tf = get_grads_fidelity_smoothed_apprx_1band(im_tf, vec_tf, std_tf, vec_tf)
        #prob = np.log(multivariate_normal.pdf(im, mean=np.array(vec), cov=cov))
        prob = sess.run(loss_tf)
        a = ax[i].imshow(np.squeeze(prob),cmap='RdBu',vmin=0,vmax=1e1)
        ax[i].set_title("sigma=" + str(s))
    #fig.colorbar(ax[i - 1].imshow(np.squeeze(prob), cmap=plt.cm.RdBu, vmin=0, vmax=np.amax(cum_prob)), ax=ax[i - 1],orientation='vertical')
    axcb = fig.colorbar(a, ax=ax.ravel().tolist(), pad=0.04, aspect=30)
    plt.suptitle(title)
    sess.close()

if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    mix_fun_str = "sep_layers_bg_ind_from_ot_not_fn"
    pal_name = "archi"

    if "archi" in pal_name:
        msi = True
        data_dir = os.path.join(main_dir, "datasets", r"Archimedes", r"017r-016v_Arch07r_Sinar", r"normalized")
        fname_ot = r"overtext_map.png"
        d = PrepArchimedes(data_dir,[0, 10880],[0, 8160], msi, fname_ot,
                                   0, mix_fun_str,False,False)
    elif "greek" in pal_name:
        data_dir = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084", "normalized_WB")
        fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
        msi = True
        d = PrepGreek960(data_dir, [2300, 2300 + 192],[1800, 2914], msi, fname_ot,
                                 -90, mix_fun_str=mix_fun_str, msi_partial=True)

    do, du, db = d.D_o.tolist(), d.D_u.tolist(), d.D_bg.tolist()
    save_spect_angle_rgb_image(d,d.ot_refl,d.ut_refl,d.bg_mean,data_dir)
    # plot_the_angle_with_mask(data, ut_refl,"Angle btw mean ut refl and im pixels, "+chr(176))
    # plot_the_angle_with_mask(data, bg_mean, "Angle btw  mean bg refl and im pixels, "+chr(176))
    #plot_likelihood_mvn(main_dir,d,d.ut_refl,du,"MVN Likelihood, mean=ut_refl","xu",mix_fun_str,"greek960")
    #plot_likelihood_mvn(main_dir,d,d.bg_mean, db, "MVN Likelihood, mean=bg_refl","xb",mix_fun_str,"greek960")
    plt.show()

