import tensorflow as tf
import numpy as np
import os
from skimage import io
from pixelcnn_palimpsest.pixel_cnn_pp.nn import log_loss_smoothed,sigmoid_loss_smoothed
from pixelcnn_palimpsest.utils import files
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_prior_model,restore_model
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_1band,get_grads_fidelity_smoothed_apprx_msi
from pixelcnn_palimpsest.exp_langevin.simulate_mix_fun_pdf import MixingVars


def viz_ll_reconst_im(main_dir,sigma,res_dir,pal_name):
    sigmas = {"0.01":11,"0.1":10,"0.2":9,"0.3":8,"0.4":7,"0.5":6,
              "0.6":5,"0.7":4,"0.8":3,"0.9":2,"1.0":1}
    epoch = sigmas[str(sigma)]*5000-1
    res_dir = os.path.join(main_dir, "training","langevin",pal_name,res_dir)
    ut = io.imread(os.path.join(res_dir,"ut",str(epoch)+".png"),as_gray=True)/255.0
    bg = io.imread(os.path.join(res_dir, "bg", str(epoch) + ".png"),as_gray=True)/255.0
    ot = io.imread(os.path.join(res_dir, "mask.png"),as_gray=True)/255.0
    return ot,ut,bg

def get_prior_l(main_dir,res_dir,sigma,ims,pixel_idx):
    d = files.json_to_dict(os.path.join(res_dir, "par.json"))
    rest_epoch_sigma_bg = {0: 40, 0.01: 60, 0.1: 80, 0.2: 120, 0.3: 160,
                           0.4: 160, 0.5: 200, 0.6: 240, 0.7: 280,
                           0.8: 320, 0.9: 360, 1.0: 400}
    chpoint_path_bg = os.path.join(main_dir, "training", "generators", "background",
                                   "pixelcnn-gray-2022-10-26-02-04", "noise_" + str(sigma),
                                   "params_greek960_bg{}.ckpt".format(rest_epoch_sigma_bg[sigma]))
    rest_epoch_sigma_ut = {1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24, 0.4: 34,
                           0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
    chpoint_path_ut = os.path.join(main_dir, "training", "generators", "undertext",
                                   "pixelcnn-bin-2022-05-27-18-43", "noise_" + str(sigma),
                                   "params_greek960_ut{}.ckpt".format(rest_epoch_sigma_ut[sigma]))
    lbg = l_bg(ims, pixel_idx, sigma, chpoint_path_bg)
    lut = l_ut(ims,pixel_idx,sigma,chpoint_path_ut)
    return lbg,lut

def calc_ll(main_dir,res_dir,ut,ot,bg,org,sigma,pix_c):
    d = files.json_to_dict(os.path.join(main_dir,res_dir, "par.json"))
    nb_ch = d["nb_ch"]
    du = d["Du"]
    do = d["Do"]
    dbg = d["Db"]
    org_noise_std = d["noise_level"]
    mix_fun_str = d["mix_fun_str"]
    nb_ch = d["nb_ch"]
    fid_loss = d["fid_loss"]
    with tf.Session() as sess:
        mix = MixingVars( du, do, ut.ravel(), ot.ravel(), bg.ravel(), sigma, sigma, org_noise_std,
                         False, False, True, False, np.prod(ut.shape),
                         mix_fun_str, 0.0, dbg, sess, nb_ch)

        y = np.reshape(mix.y,(ut.shape[0],ut.shape[1],nb_ch))
        y_pix = y[pix_c[0],pix_c[1],:]
        org_tf = tf.constant(org[pix_c[0],pix_c[1],:], dtype=tf.float32)
        y_tf = tf.constant(y, dtype=tf.float32)
        if nb_ch==1:
            phi_list_ut_f = os.path.join(main_dir, "training",
                                         "langevin", mix_fun_str,
                                         "phi_list_nb_layers_1_org_noise_{}_xu.json".format(org_noise_std))
            phi_list_ut = files.json_to_dict(phi_list_ut_f)
            phi = phi_list_ut[str(sigma)][str(round(du, 2))]
            _,ll_tf = get_grads_fidelity_smoothed_apprx_1band(org_tf,y_tf,phi,y_tf)
            ll = sess.run(ll_tf)
        elif nb_ch>1 and fid_loss=="mvn":
            round_du = str([round(u, 3) for u in du]).replace(" ", "")
            phi_list_ut_f = os.path.join(main_dir, "training",
                                         "langevin", mix_fun_str, "mvn",
                                         "xu_{}_on_{}_Du_{}.npz".format(pal_name, org_noise_std, round_du))
            phi_list_ut = np.load(phi_list_ut_f,allow_pickle=True)
            phi = phi_list_ut[str(sigma)].astype(np.float32)
            cov_tf = tf.constant(phi,dtype=tf.float32)
            _, ll_tf = get_grads_fidelity_smoothed_apprx_msi(org_tf, y_tf, cov_tf, y_tf)
            ll = sess.run(ll_tf)
    return y,ll

def l_bg(ims, batch_size, sigma, chpoint_path):
    """
    Likelihood of smooth values bg
    ims - ims [nb_samp,height,width,1]
    sigma - std
    chpoint_path - checkpoint_path untill "noise" folder
    Returns:
        smoothed_likelihood,grads,discrete_likelihood
        """
    tf.compat.v1.reset_default_graph()
    s = tf.constant(sigma, dtype=tf.float32)
    nr_mix = 3
    x_prior_tf, l_prior_tf = compile_prior_model(batch_size, "bg", nr_filters=32,
                                                 nr_resnet=3, nr_mix=nr_mix, model_discrete=False,
                                                 model_scope="model_bg")
    sess = tf.Session()
    restore_model(chpoint_path, sess, "bg", "model_bg")
    smooth_log_loss_xb_fun_tf = log_loss_smoothed(x_prior_tf, l_prior_tf, s, False)
    smooth_ll = sess.run([smooth_log_loss_xb_fun_tf], {x_prior_tf: ims})
    sess.close()
    return np.exp(smooth_ll)

def l_ut(ims,batch_size,sigma,chpoint_path):
    """    Likelihood of smooth undertext
    ims - ims [nb_samp,height,width,1]
    sigma - std
    chpoint_path - checkpoint_path untill "noise" folder
    Returns:
        prob - predicted probability;
        activation - last layer activations;
        l - smooth_likelihood
        grads - grads"""
    tf.compat.v1.reset_default_graph()
    s = tf.constant(sigma, dtype=tf.float32)
    nr_mix = 2
    x_prior_tf, l_prior_tf = compile_prior_model(batch_size, "undertext", nr_filters=64,
                                          nr_resnet=5, nr_mix=nr_mix, model_discrete=True,
                                          model_scope="model_ut")
    sess = tf.Session()
    restore_model(chpoint_path, sess, "ut", "model_ut")

    smooth_loss_xu_fun_tf = sigmoid_loss_smoothed(x_prior_tf, l_prior_tf, s, False)
    ll,prob,activation,grads = sess.run([smooth_loss_xu_fun_tf], {x_prior_tf: ims})
    l = np.exp(ll)
    sess.close()
    return l