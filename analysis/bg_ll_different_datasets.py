import numpy as np
import os
from pixelcnn_palimpsest.utils import plotting
from pixelcnn_palimpsest.langevin_dynamics import data
from pixelcnn_palimpsest.analysis.viz_ll_res_gradients import gradients_ll_bg
from matplotlib import pyplot as plt


def read_bg_Greek960(main_dir):
    data_dir_pal = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
    row_coord = [2300, 2300 + 192]
    col_coord = [1094, 1094+192]
    rot_angle = -90
    mix_fun_str = r"sep_layers_bg_not_fn"
    msi = True
    d = data.PrepGreek960(data_dir_pal, row_coord, col_coord, msi, fname_ot,
                          rot_angle, mix_fun_str=mix_fun_str, msi_partial=True, modalities=None)
    band_names, band_idxs = d.band_idx_fname, d.band_idxs
    D_bg = d.D_bg
    return d.im_out,band_names,band_idxs,D_bg

def read_Archimedes(main_dir):
    data_dir = os.path.join(main_dir, "datasets", r"Archimedes", r"017r-016v_Arch07r_Sinar",r"normalized")
    fname_ot = r"overtext_map.png"
    row_coord = [6236, 6236 + 192]
    col_coord = [5044, 5044+192]
    rot_angle = 0
    mix_fun_str = "sep_layers_bg_not_fn"
    msi = True
    msi_partial = True
    d = data.PrepArchimedes(data_dir, row_coord, col_coord, msi, fname_ot,
                            rot_angle, mix_fun_str, msi_partial=msi_partial)
    band_names,band_idxs = d.band_idx_fname, d.band_idxs
    D_bg = d.D_bg
    return d.im_out,band_names,band_idxs,D_bg


def viz_bg_prior_dif_dataset(chpoint_dir, im, dataset,band_names,band_idxs,D_bg,sigma):
    res_dir = os.path.join(chpoint_dir, "Reaction_to_"+dataset)
    if not os.path.isdir(res_dir):
        os.makedirs(res_dir)
    res_epochs = {0: 40, 0.01: 80, 0.1: 120, 0.2: 160,
                  0.3: 200, 0.4: 240, 0.5: 280, 0.6: 320,
                  0.7: 360, 0.8: 400, 0.9: 440, 1.0: 480}

    patch_size = 64
    tile_shape = (im.shape[1] // patch_size, im.shape[2] // patch_size)
    batch_size = np.prod(tile_shape)
    band_idx_list = sorted(band_names)
    for i in range(len(band_idxs)):
        ims = plotting.split_image(im[0, :, :,i], patch_size)
        ims = ims/D_bg[i]
        ims = 2*ims-1
        print("{}, D_bg={}".format(band_names[band_idx_list[band_idxs[i]]],D_bg[i]))
        res_epoch = res_epochs[sigma]
        chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                "params_greek960_bg{}.ckpt".format(res_epoch))
        smooth_l,grads,disc_l = gradients_ll_bg(ims,batch_size,sigma,chpoint_path,add_noise=True)
        ll_full = plotting.stich_image(disc_l, tile_shape)
        ims_resh = plotting.stich_image(ims[:,:,:,0],tile_shape)
        plot_ims_vs_ll_bg(ll_full[:,:],ims_resh,res_dir,sigma,band_names[band_idx_list[band_idxs[i]]],dataset)

def plot_ims_vs_ll_bg(l,im,res_dir,sigma,band_name,title):
    fig, ax = plt.subplots(2, 1)
    fig.suptitle(title)
    ax[0].imshow(im,cmap="gray")
    ax[0].set_title("Org image")
    pcm0 = ax[1].pcolormesh(l[ ::-1, :], cmap='RdBu', vmin=np.amin(l), vmax=np.amax(l))
    ax[1].set_title("Discrete likelihood")
    ax[1].set_aspect('equal')
    fig.colorbar(pcm0, ax=ax.ravel().tolist())
    plt.savefig(os.path.join(res_dir,"Sigma_{}_{}".format(sigma,band_name)+".png"),dpi=300)

if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    ch_dir_path = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\background\text_aug_exp"
    ch_dir = r"pixelcnn-gray-2022-12-23-19-08_with_text_aug"
    sigma = 0.2
    im,band_names,band_idxs,D_bg = read_bg_Greek960(main_dir)
    viz_bg_prior_dif_dataset(os.path.join(ch_dir_path,ch_dir), im,"Greek960",band_names,band_idxs,D_bg,sigma)
    im,band_names,band_idxs,D_bg = read_Archimedes(main_dir)
    viz_bg_prior_dif_dataset(os.path.join(ch_dir_path,ch_dir), im,"archi",band_names,band_idxs,D_bg,sigma)
    plt.show()