from skimage import io
import os
from pixelcnn_palimpsest.langevin_dynamics.data import PrepGreek960
from matplotlib import pyplot as plt
import numpy as np


def calc_entropy(im,mask):
    im = im.ravel()
    mask = mask.ravel()
    text_pxls = []
    for i in range(len(im)):
        if mask[i]>0:
            text_pxls.append(im[i])
    text_pxls = np.array(text_pxls)
    hist, edges = np.histogram(text_pxls,20)
    hist = hist/len(im)
    ent = -(hist * np.ma.log(np.abs(hist))).sum()
    return ent

maindir = r"C:\Data\PhD\bss_autoreg_palimpsest"
resdir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\_t_2023-02-05-03-15"
pal = io.imread(os.path.join(resdir,"corrupt_im.png"),as_gray=True)/255
bg_dir = os.path.join(resdir,"continue_rec_from_159998","bg")
epoch = sorted([int(s[:-4]) for s in os.listdir(bg_dir)])[-1]
bg = io.imread(os.path.join(bg_dir,str(epoch)+".png"),as_gray=True)/255
ot = io.imread(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\with_bg_t_2023-02-03-20-45\mask.png",as_gray=True)/255


#ot[ot>0]=255.0
data_dir_pal = os.path.join(maindir, "datasets", r"Greek_960", r"0086_000084")
fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
row_coord = [2300, 2300 + 192]
col_coord = [1800, 2914]
rot_angle = -90
mix_fun_str = r"sep_layers_bg_not_fn"
msi = False
data = PrepGreek960(data_dir_pal, row_coord, col_coord, msi, fname_ot,
                    rot_angle, mix_fun_str=mix_fun_str, msi_partial=False)
nb_pixels = np.prod(bg.shape)
Do = data.D_o[0]
Db = data.D_bg[0]
Du = data.D_u[0]
pal_pred_add = (bg*Db+ot*Do)
diff_add = pal - pal_pred_add
xu_add = diff_add/(Du)
ot_without_bg = (pal - bg*Db)/Do

ent_ot_after = calc_entropy(ot_without_bg ,ot)
print("Entropy of ot if subtract bg",ent_ot_after)
ent_ot_before = calc_entropy(pal,ot)
print("Entropy of ot if do not subtract bg",ent_ot_before)


mix_fun_str = r"sep_layers_bg_ind_from_ot_not_fn"
data = PrepGreek960(data_dir_pal, row_coord, col_coord, msi, fname_ot,
                    rot_angle, mix_fun_str=mix_fun_str, msi_partial=False)
Do = data.D_o[0]
Db = data.D_bg[0]
Du = data.D_u[0]
pal_pred_sub = (bg*Db*(1-ot)+ot*Do)
ut_sub = (pal - pal_pred_sub)/(Du)
plt.figure()
plt.hist(ut_sub.ravel(),density=True,bins=50)
plt.title("Histogram of xu=y-(bg*Db*(1-ot)+ot*Do)/Du")

plt.figure()
ut = ut_sub>0.9
plt.imshow(ut_sub>0.5, cmap="gray")
plt.title("Thresholded undertext")
plt.figure()
plt.hist(pal[ut].ravel(),density=True,bins=50)
plt.title("Histogram of Duxu")


plt.figure()
ut_pixels = [pal.ravel()[i]-Db*bg.ravel()[i]-Du for i in range(nb_pixels) if ut.ravel()[i]]
plt.hist(ut_pixels,density=True,bins=50)
plt.title("Histogram of undertext variation without bg")
ot_pixels = [pal.ravel()[i]-Do for i in range(nb_pixels) if ot.ravel()[i]]
plt.figure()
plt.hist(ot_pixels,density=True,bins=50)
plt.title("Histogram of overtext variation")


bg_without_variation = pal - bg*Db
ot = ot>0.0
ut_ot = np.clip(ut+ot,a_min=0.0,a_max=1.0).astype(np.uint8)
bg_pixels = [pal.ravel()[i]-Db*bg.ravel()[i] for i in range(nb_pixels) if ut_ot.ravel()[i]!=1]
plt.figure()
plt.hist(bg_pixels,density=True,bins=50)
plt.title("Histogram of background variation")

mix_fun_str = r"sep_layers_all_ind_bg_not_fn"
data = PrepGreek960(data_dir_pal, row_coord, col_coord, msi, fname_ot,
                    rot_angle, mix_fun_str=mix_fun_str, msi_partial=False)
Do = data.D_o[0]
Db = data.D_bg[0]
Du = data.D_u[0]
plt.figure()
ut_pixels = [pal.ravel()[i]-Du for i in range(nb_pixels) if ut.ravel()[i]]
plt.hist(ut_pixels,density=True,bins=50)
plt.title("Histogram of undertext variation with bg")


fig,ax = plt.subplots(3,1)
ax[0].imshow(pal,cmap="gray",vmax=1.0,vmin=0.0)
ax[0].set_title("Org pal")
ax[1].imshow(pal_pred_add,cmap="gray",vmax=1.0,vmin=0.0)
ax[1].set_title("xu=(pal - (bg*Db+ot*Do))/(Du*(1-ot))")
ax[2].imshow(xu_add,cmap="gray",vmax=1.0)
pcm0 = ax[2].pcolormesh(xu_add[:, :], cmap='RdBu', vmin=-1.0,
                            vmax=1.0)
fig.colorbar(pcm0, ax=ax[:])
ax[2].set_title("Difference Dbxb+Doxo")

fig,ax = plt.subplots(3,1)
ax[0].imshow(pal,cmap="gray",vmax=1.0,vmin=0.0)
ax[0].set_title("Org pal")
ax[1].imshow(pal_pred_sub,cmap="gray",vmax=1.0,vmin=0.0)
ax[1].set_title("xu=(pal - (bg*Db*(1-ot)+ot*Do))/(Du*(1-ot))")
ax[2].imshow(ut_sub,cmap="gray",vmax=1.0)
pcm0 = ax[2].pcolormesh(ut_sub[:, :], cmap='RdBu', vmin=-1.0,
                            vmax=1.0)
fig.colorbar(pcm0, ax=ax[:])
ax[2].set_title("Difference Dbxb(1-xo)+Doxo")
plt.show()



