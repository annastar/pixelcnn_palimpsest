import tensorflow as tf
import numpy as np
from pixelcnn_palimpsest.pixel_cnn_pp import nn
from pixelcnn_palimpsest.langevin_dynamics import data
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_prior_model,restore_model
from pixelcnn_palimpsest.pixel_cnn_pp.nn import log_loss_smoothed
import os
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 18})

def compile_restore_bg_model(sess,nb_samples,res_epoch,nb_mixtures,nb_res_blocks,checkpoint_dir):
    x_prior, l_prior = compile_prior_model(nb_samples, "bg", nr_filters=32,
                                               nr_resnet=nb_res_blocks, nr_mix=nb_mixtures, model_discrete=False,
                                               model_scope="model_bg")
    sess.run(tf.initialize_all_variables())
    restore_model(os.path.join(checkpoint_dir,
                               "params_greek960_bg{}.ckpt".format(res_epoch)), sess, "model_bg")
    return x_prior,l_prior


def create_bg_ut_pair(data_dir,main_dir):
    main_dir = r"/"
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
    data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"line_4_moved.png"
    fname_ut = r"line_27.png"
    row_coord = [0, 192]
    col_coord = [0, 192 * 4]
    pal = data.PrepArtPal(data_dir, data_dir_bg, row_coord, col_coord, "undertext", fname_ot, fname_ut, True)
    return pal.im_bg,pal.im_ut

def faded_text_on_bg_samples(step):
    """Create pairs of bg image without text and with added text with different range of fading"""
    data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    ###creat bg image with ut
    bg, ut = create_bg_ut_pair(data_dir_bg, main_dir)

    res_p = 1 - np.mean(bg[ut == 1])
    res_m = np.mean(bg[ut == 1]) + 0

    nb_steps_p = int(res_p // step)
    nb_steps_m = max(int(res_m // step), 1)
    # level of ut intensity
    alpha_p = (step * np.arange(1, nb_steps_p + 1)).reshape([-1, 1, 1, 1])
    alpha_m = -(step * np.arange(1, nb_steps_m + 1)[::-1]).reshape([-1, 1, 1, 1])
    # bg images with black and white text
    bg_ut_white = np.repeat(bg, nb_steps_p, 0) + alpha_p * np.repeat(ut, nb_steps_p, 0)
    bg_ut_black = np.repeat(bg, nb_steps_m, 0) + alpha_m * np.repeat(ut, nb_steps_m, 0)
    # clip values higher then 254/255 and lower then 1/255
    bg_ut_black = np.clip(bg_ut_black, a_min=0 / 255., a_max=255 / 255.)
    bg_ut_white = np.clip(bg_ut_white, a_min=0 / 255., a_max=255 / 255.)
    bg = np.clip(bg, a_min=0 / 255., a_max=255 / 255.)
    # rescale im from -1;1
    bg_ut_black = 2.0 * bg_ut_black - 1.0
    bg_ut_white = 2.0 * bg_ut_white - 1.0
    bg = 2.0 * bg - 1.0
    return bg,ut,bg_ut_black,bg_ut_white,alpha_m,alpha_p


def calc_ll_on_samples(sess,nb_steps_p,nb_steps_m,rows,cols,tile_shape,
                        p_size,smooth_loss_tf,nll_tf,x_prior_tf,bg,ut,bg_ut_white,
                        bg_ut_black,s_tf,sigma,mode):
    """Calculate LL for the range of contrast steps"""
    if "disc" in mode:
        loss_tf = nll_tf
        prob_conv = nll_to_prob
    else:
        loss_tf = smooth_loss_tf
        prob_conv = smoothll_to_prob

    loss_p = np.zeros((nb_steps_p,rows, cols))
    loss_m = np.zeros((nb_steps_m,rows, cols))
    loss_bg = np.zeros((rows,cols))
    d = {s_tf:sigma}
    for i in range(tile_shape[0]):
        for j in range(tile_shape[1]):
            i0,i1 = i*p_size,(i+1)*p_size
            j0, j1 = j * p_size, (j + 1) * p_size
            for p_idx in range(nb_steps_p):
                d.update({x_prior_tf:bg_ut_white[p_idx,i0:i1,j0:j1][np.newaxis,:,:,:]})
                xx = np.squeeze(sess.run(loss_tf,feed_dict=d))
                loss_p[p_idx,i0:i1,j0:j1] = xx
            for m_idx in range(nb_steps_m):
                d.update({x_prior_tf: bg_ut_black[m_idx,i0:i1,j0:j1][np.newaxis,:,:,:]})
                xx = np.squeeze(sess.run(loss_tf,feed_dict=d))
                loss_m[m_idx,i0:i1,j0:j1] = xx
            d.update({x_prior_tf: bg[:,i0:i1,j0:j1]})
            loss_bg[i0:i1,j0:j1] = np.squeeze(sess.run(loss_tf, feed_dict=d))
    l_bg_ut_p = []
    l_bg_ut_m = []
    for i in range(nb_steps_p):
        l_bg_ut_p.append(np.sum(prob_conv(loss_p[i])[ut[0,:,:,0]==1],axis=0)/np.sum(ut==1))
    for i in range(nb_steps_m):
        l_bg_ut_m.append(np.sum(prob_conv(loss_m[i])[ut[0,:,:,0]==1],axis=0)/np.sum(ut==1))
    l_bg_mean = np.mean(prob_conv(loss_bg))
    l_ut_to_bg = np.concatenate([l_bg_ut_m/l_bg_mean,l_bg_ut_p/l_bg_mean],0)
    return  l_ut_to_bg

def nll_to_prob(x):
    return np.exp(-x)

def smoothll_to_prob(x):
    return np.exp(x)

def ll_vs_ut_contr_on_bg(main_dir,noises,nb_mixtures,nb_res_blocks,checkpoint_dir):
    """Plot likelihood vs constrast of added text to background"""
    tf.compat.v1.reset_default_graph()
    p_size = 64
    step_inc = 0.01
    bg,ut, bg_ut_black, bg_ut_white,alpha_m,alpha_p = faded_text_on_bg_samples(step_inc)

    nb_steps_p = len(bg_ut_white)
    nb_steps_m = len(bg_ut_black)
    rows,cols = bg[0,:,:,0].shape
    tile_shape = rows // p_size, cols // p_size
    ut_mask = ut[0] == 1
    bg_mean = np.mean((bg+1)/2)
    ut_contr_m = [-np.sqrt(np.mean(((bg_ut_black[i][ut_mask]+1)/2-bg_mean)**2)) for i in range(nb_steps_m)]
    ut_contr_p = [np.sqrt(np.mean(((bg_ut_white[i][ut_mask]+1)/2-bg_mean)**2)) for i in range(nb_steps_p)]
    # ut intensities range
    contrast = np.concatenate([ut_contr_m,ut_contr_p],0)
    x_prior_tf, l_prior_tf = compile_prior_model(1, "bg", nr_filters=32,
                                           nr_resnet=nb_res_blocks, nr_mix=nb_mixtures, model_discrete=False,
                                           model_scope="model_bg")
    s_tf = tf.placeholder(shape=(), dtype=tf.float32)
    smooth_loss_tf = log_loss_smoothed(x_prior_tf, l_prior_tf, s_tf, False)
    nll_tf = nn.discretized_mix_logistic_loss_gray(x_prior_tf, l_prior_tf, False)

    plt.figure(figsize=(8, 6))
    #plt.title("Relative likelihood of bg. model vs. undertext contrast")

    for noise, res_epoch in noises.items():
        sess = tf.Session()
        chpoint_bg_path = os.path.join(main_dir, "training", "generators", "background", checkpoint_dir,
                                       "noise_{}".format(noise))
        restore_model(os.path.join(chpoint_bg_path,
                                   "params_greek960_bg{}.ckpt".format(res_epoch)), sess, "bg", "model_bg")
        bg_ut_black1 = bg_ut_black + noise * np.random.uniform(0, 1.0, bg.shape)
        bg_ut_white1 = bg_ut_white + noise * np.random.uniform(0, 1.0, bg.shape)
        bg1 = bg + noise * np.random.uniform(0, 1.0, bg.shape)
        if noise == 0:
            l_ut_to_bg = calc_ll_on_samples(sess, nb_steps_p, nb_steps_m, rows, cols, tile_shape,
                                             p_size, smooth_loss_tf, nll_tf, x_prior_tf, bg1, ut, bg_ut_white1,
                                             bg_ut_black1, s_tf, noise, "discrete")
        else:
            l_ut_to_bg = calc_ll_on_samples(sess, nb_steps_p, nb_steps_m, rows, cols, tile_shape,
                                             p_size, smooth_loss_tf, nll_tf, x_prior_tf, bg1, ut, bg_ut_white1,
                                             bg_ut_black1, s_tf, noise, "smooth")
        sess.close()
        plt.plot(contrast,l_ut_to_bg,label="\u03C3={}".format(noise))
    plt.xlabel('undertext contrast')
    plt.ylabel('Relative Likelihood')
    plt.grid(True)
    plt.legend(loc='upper left')
    plt.savefig( os.path.join(main_dir, "training", "generators", "background", checkpoint_dir, "rel_ll_vs_contrast.png"), dpi=300)


def viz_disc_vs_smooth_ll(s,im,disc_ll,smooth_ll,title):
    fig, ax = plt.subplots(1, 3)
    fig.suptitle(title)
    ax[0].imshow(im, cmap="gray", vmax=1, vmin=-1.0)
    ax[0].set_title("Im.")
    ax[1].imshow(disc_ll, cmap="gray", vmax=0.2, vmin=0.0)
    ax[1].set_title("Disc. LL")
    ax[2].imshow(smooth_ll, cmap="gray", vmax=0.2, vmin=0.0)
    ax[2].set_title("Smooth LL,s={}".format(s))
    for i in range(len(ax)):
        ax[i].axis(False)

def viz_pixels_pdf(main_dir,res_epoch,nb_mix,nb_res_blocks,checkpoint_dir,noise):
    tf.compat.v1.reset_default_graph()
    sess = tf.Session()
    s_tf = tf.constant(noise, dtype=tf.float32)
    checkpoint_dir = os.path.join(main_dir,checkpoint_dir,"noise_{}".format(noise))
    x_prior_tf, l_prior_tf = compile_restore_bg_model( sess, 1, res_epoch, nb_mix,nb_res_blocks,checkpoint_dir)
    smooth_loss_tf, idxs, disc_prob_tf, _ = log_loss_smoothed(x_prior_tf, l_prior_tf, s_tf, False)
    nll_tf = nn.discretized_mix_logistic_loss_gray(x_prior_tf, l_prior_tf, False)
    ### read ims
    data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    bg,ut = create_bg_ut_pair(data_dir_bg,main_dir)
    contr = -0.2
    bg = bg + contr*ut
    bg = np.clip(bg,a_min=1/255.,a_max=254./255.)
    bg = 2.0 * bg - 1.0
    if noise>0:
        bg = bg + noise * np.random.uniform(0,1.0,bg.shape)
    coords = [[31, 10],[36,10]]#[[22,4],[24,6],[31, 10]]
    color = ["r","b","y"]
    stem_stile = ["-","--",":"]
    stem_marker = ["x",".","o"]
    disc_prob,nll,smooth_loss= sess.run([disc_prob_tf,nll_tf,smooth_loss_tf], feed_dict={
        x_prior_tf: bg[:, 0:64, 0:64]})
    print("Sum disc prob for [21,1]={}".format(sum(disc_prob[:,21,1])))
    fig,ax = plt.subplots(1,1)
    fig.suptitle("Pdf of 3 pixels")
    for i in range(len(coords)):
        plot_pixel_pdf(idxs,coords[i],disc_prob,bg[0],ax,color[i],"*",stem_stile[i],stem_marker[i])
    ax.legend()
    ax.set_xlabel('val bg')
    ax.set_ylabel('LL_bg')
    plt.show()
    plt.figure()
    plt.imshow(bg[0, 0:64, 0:64],cmap="gray")
    for i in range(len(coords)):
        plt.plot(coords[i][1],coords[i][0], marker='v', color=color[i])

    plt.figure()
    plt.imshow(nll_to_prob(nll[0,:64,:64]), cmap="gray", vmax=np.max(nll_to_prob(nll[0,:64,:64])), vmin=0.0)
    plt.title("Discrete LL, ut={}".format(2.*contr-1))
    for i in range(len(coords)):
        plt.plot(coords[i][1], coords[i][0], marker='v', color=color[i])
    sess.close()

    plt.figure()
    plt.imshow(smoothll_to_prob(smooth_loss[0,:64,:64]), cmap="gray", vmax=np.max(smoothll_to_prob(smooth_loss[0,:64,:64])), vmin=0.0)
    plt.title("Smooth LL, ut={}".format(2.*contr-1))
    for i in range(len(coords)):
        plt.plot(coords[i][1], coords[i][0], marker='v', color=color[i])
    sess.close()

def plot_pixel_pdf(idxs,coord,pdf,im,ax,color,val_marker,stem_stile,stem_marker):
    ax.stem(np.squeeze(idxs), pdf[:, coord[0], coord[1]], label="[21,1]", linefmt='{}{}'.format(color,stem_stile), markerfmt="{}{}".format(color,stem_marker))
    ax.plot(im[coord[0], coord[1]], 0, marker=val_marker, color=color)





def sampled_im_ll(main_dir,res_epoch,nb_mixtures,nb_res_blocks,checkpoint_dir):
    tf.compat.v1.reset_default_graph()
    sess = tf.Session()
    s = 0.5
    x_prior_tf, l_prior_tf = compile_restore_bg_model(main_dir, sess, 1, res_epoch, nb_mixtures,nb_res_blocks,checkpoint_dir)
    ###read image generated by prior model during training
    bg_generated = np.load(os.path.join(main_dir, "training", "generators", "background",
                                        checkpoint_dir,
                                        "greek960_bg_sample{}.npz".format(res_epoch)))
    s_tf = tf.constant(s, dtype=tf.float32)
    smooth_loss_tf, idxs,disc_prob_tf , _= log_loss_smoothed(x_prior_tf, l_prior_tf, s_tf, False)
    nll_tf = nn.discretized_mix_logistic_loss_gray(x_prior_tf, l_prior_tf, False)
    bg_gen_im = bg_generated.f.arr_0[2]
    nll_bg_gen = sess.run(nll_tf, feed_dict={x_prior_tf: bg_gen_im[np.newaxis, :, :, :]})
    smooth_bg_gen = sess.run(smooth_loss_tf,
                             feed_dict={x_prior_tf: bg_gen_im[np.newaxis, :, :, :]})
    #prior generated bg
    fig,ax = plt.subplots(1,3)
    ax[2].imshow(np.exp(smooth_bg_gen[0,:,:,0]), cmap="gray", vmax=0.6,vmin=0.0)
    ax[2].set_title("Smooth LL bg")
    ax[1].imshow(np.exp(-nll_bg_gen[0]), cmap="gray", vmax=0.2, vmin=0.0)
    ax[1].set_title("Disc LL bg gen")
    ax[0].imshow(bg_gen_im[:,:,0], cmap="gray", vmax=1, vmin=-1.0)
    ax[0].set_title("bg gen")
    for i in range(len(ax)):
        ax[i].axis(False)

if __name__=="__main__":
    main_dir =  r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\background\text_aug_exp_Greek960"
    checkpoint_dir = os.path.join("pixelcnn-gray-2022-12-23-19-08_with_text_aug")
    noises = {0:40, 0.01:60, 0.1:80,0.2:120,0.3:120,0.4:160,0.5:200,0.6:240,0.7:280,0.8:320,0.9:360,1.0:400}
    {0: 40, 0.01: 80, 0.1: 120, 0.2: 160,
     0.3: 200, 0.4: 240, 0.5: 280, 0.6: 320,
     0.7: 360, 0.8: 400, 0.9: 440, 1.0: 480, 1.2: 520, 1.4: 560, 1.6: 600}
    nb_mixtures=3
    nb_res_blocks=3
    #ll_vs_ut_contr_on_bg(main_dir, noises, nb_mixtures, nb_res_blocks, checkpoint_dir)
    viz_pixels_pdf(main_dir,80,nb_mixtures,nb_res_blocks,checkpoint_dir,0.1)
    checkpoint_dir = os.path.join("pixelcnn-gray-2022-04-06-02-55")
    noises = {0:40,1.0: 1180, 0.01: 60, 0.1: 60, 0.2: 110, 0.3: 180, 0.4: 270,
                           0.5: 380, 0.6: 500, 0.7: 640, 0.8: 800, 0.9: 980}
    nb_mixtures=5
    nb_res_blocks=2

    plt.show()