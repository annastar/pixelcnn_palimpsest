import numpy as np
import tensorflow as tf
from pixelcnn_palimpsest.data.greek960_ut_data import DataLoaderBgAug
from pixelcnn_palimpsest.pixel_cnn_pp import nn
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_prior_model,restore_model
from pixelcnn_palimpsest.pixel_cnn_pp.model import model_spec_gray_log_mix,model_spec_discrete
import os
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.data.greek960_ut_data import bg_binarization
from skimage import io,transform


class Helper():
    def __init__(self,batch_size,obs_shape,chpoint_path,sess):
        self.cross_entropy=False
        self.noise_level = 0.00
        self.batch_size = batch_size
        self.class_conditional = False
        self.binary_data = True
        self.obs_shape = obs_shape
        # data place holders
        self.xs_true = tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape, name="x_")
        # noise place holders
        self.ns = tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape, name="noise_")
        # masks place holders
        #self.xs_input = tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape,)
        self.ys = tf.placeholder(tf.int32, shape=(self.batch_size,))
        self.xs_input = tf.placeholder(tf.float32, shape=(batch_size, 64, 64, 1), name="x_ut")
        model_opt = {'nr_resnet': 5, 'nr_filters': 64, 'nr_logistic_mix': 2,
                     'resnet_nonlinearity': 'concat_elu', 'q_levels': 1}
        model = tf.make_template("model_ut",model_spec_discrete)
        self.xs_aug = tf.placeholder(tf.float32, shape=(self.batch_size,) + self.obs_shape, name="aug_")
        xs = tf.clip_by_value(self.xs_input + self.xs_aug, 0.0, 1.0)
        l_prior_tf = model(xs, None, init=False, ema=None, dropout_p=0., **model_opt)


        self.probs = tf.nn.sigmoid(l_prior_tf)
        restore_model(chpoint_path, sess, "model_ut")
        self.new_x_gen = nn.sample_from_sigmoid(l_prior_tf)

def plot_pred(im,x,y,title):
    plt.figure()
    plt.title("{}, Pixel x={},y={}".format(title,x,y))
    plt.imshow(np.squeeze(im),cmap="gray",vmin=0.0,vmax=1.0)

def generate_some_pattern(impath):
    im = io.imread(impath,as_gray=True)
    im = bg_binarization(im,30)
    im = transform.resize(im, (64, 64), preserve_range=True)
    im = im/255.0
    return im[np.newaxis,:,:,np.newaxis]
def generate_org_im(impath,scaler):
    im = io.imread(impath, as_gray=True)
    im = transform.resize(im, (64, 64), preserve_range=True)
    im = im / 255.0
    im = im*scaler
    return im[np.newaxis, :, :, np.newaxis]

def sample_from_model(sess,args,mask):
    x_gen = np.zeros((args.batch_size,) + obs_shape, dtype=np.float32)
    fd = {args.ns: args.noise_level * np.random.normal(0.0, 1.0, x_gen.shape)}
    fd.update({args.xs_aug:mask})#np.random.choice([0.0,1.0],size =x_gen.shape,p=[0.95,0.05] )})
    plot_pred(fd[args.xs_aug],0,0,"org_im")
    fd.update({args.xs_input: x_gen})
    probs = sess.run(args.probs,fd)
    plot_pred(probs, 0, 0, "prob")
    for yi in range(obs_shape[0]):
        for xi in range(obs_shape[1]):
            fd.update({args.xs_input:x_gen})
            new_x_gen_np,probs = sess.run([args.new_x_gen,args.probs], fd)
            x_gen[:,yi,xi,:] = new_x_gen_np[:,yi,xi,:]

        plot_pred(x_gen, xi, yi, "generated")
        plt.show()
    return x_gen

aug_bg_data_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_bg_dataset_strech_contr_step_64_size_192"
chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2023-04-01-18-41"
chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2022-05-27-18-43_current"
bg_aug_im_path = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_bg_dataset_strech_contr_step_64_size_192\train\0086_000083\0\patch419.png"
ut_im_path = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192\test\0086_000084\line_27\line_27_0_23.png"
noise = 0.0
epoch = 12#90#12
np.random.seed(123)
#os.path.join(chpoint_dir,"noise_"+str(noise),r"params_greek960_ut{}.ckpt".format(epoch))#
chpoint_dir = os.path.join(chpoint_dir,r"params_greek960_ut{}.ckpt".format(epoch))#os.path.join(chpoint_dir,"noise_"+str(noise),r"params_greek960_ut{}.ckpt".format(epoch))
subset = "test"
data_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192"
batch_size = 1
data = DataLoaderBgAug(data_dir, subset, batch_size, rng=None,
                           shuffle=True, augment=False,
                           aug_with_bg=None if aug_bg_data_dir is None else {"data_dir": aug_bg_data_dir})

obs_shape = data.get_observation_size()  # e.g. a tuple (32,32,3)
sess = tf.Session()
args = Helper(batch_size, obs_shape,chpoint_dir, sess)
mask = generate_some_pattern(bg_aug_im_path)
mask = generate_org_im(ut_im_path,0.3)
x_gen = sample_from_model(sess,args,mask)
sess.close()


