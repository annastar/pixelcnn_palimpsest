"""Vizualize Disc and Smooth  likelihood, perdiction and  Grads for an image of character or background"""
import numpy as np
import os
from skimage import io,transform
from pixelcnn_palimpsest.utils import plotting
from pixelcnn_palimpsest.analysis.viz_ll_res_gradients import gradients_ll_bg, gradients_ll_ut
from matplotlib import pyplot as plt

def plot_pred_im(org_im,model_out,mode,binary):
    fig,ax = plt.subplots(2,1)
    ax[0].imshow(org_im,cmap="gray")
    ax[0].set_title("Org. image")
    if mode=="ut" and binary:

        ax[1].imshow(np.squeeze(model_out),cmap="gray")
        ax[1].set_title("Probability of pixel==1")
    else:
        ax[1].imshow(np.squeeze(model_out), cmap="gray")
        ax[1].set_title("Predicted pixels")

def plot_ims_vs_ll_pred(im,disc_ll,smooth_ll,grads,title,binary):
    nb_figs = 4
    fig, ax = plt.subplots(nb_figs//2, 2)
    fig.suptitle(title)
    if not binary:
        ax[0,1].set_title("Disc Likelihood",pad=20,fontsize=10)
    elif binary:
        ax[0, 1].set_title("Binary crossentropy", pad=20, fontsize=10)
    pcm0 = ax[0,1].pcolormesh(disc_ll[::-1, :], cmap='RdBu', vmin=np.amin(disc_ll),
                                vmax=np.amax(disc_ll))
    fig.colorbar(pcm0, ax=ax[0,1])
    ax[0,0].imshow(im,cmap="gray",vmin=np.amin(im), vmax=np.amax(im))
    ax[0,0].set_title("Image",pad=20,fontsize=10)
    if not smooth_ll is None:
        ax[1,0].set_title("Smooth Likelihood",pad=20,fontsize=10)
        pcm1 = ax[1,0].pcolormesh(smooth_ll[::-1, :], cmap='RdBu', vmin=np.amin(smooth_ll),
                                vmax=np.amax(smooth_ll))
        fig.colorbar(pcm1, ax=ax[1,0])
    if not grads is None:
        ax[1,1].set_title("Grads",pad=20,fontsize=10)
        pcm2 = ax[1,1].pcolormesh(grads[::-1, :], cmap='RdBu', vmin=np.amin(grads),
                                vmax=np.amax(grads))
        fig.colorbar(pcm2, ax=ax[1,1])
    for i in range(nb_figs//2):
        for j in range(2):
            ax[j,i].axis('off')
            ax[j,i].set_aspect('equal')
    plt.tight_layout()

def read_im_from_logdir(main_dir,log_dir,epoch,mode,binary):
    data_dir_pal = os.path.join(main_dir, log_dir)
    fname = os.path.join(data_dir_pal,mode,str(epoch)+".png")
    #fname_bg = os.path.join(data_dir_pal, "corrupt_im.png")
    im = io.imread(fname,as_gray=True)[np.newaxis,:,:,np.newaxis]
    im = im /255.0
    if mode=="bg":
        im = 2*im - 1
    else:
        if not binary:
            im = 2 * im - 1
    return im

def read_im_from_datadir(impath,mode,binary):
    data_dir_pal = os.path.join(main_dir, log_dir)
    fname = os.path.join(data_dir_pal,impath)
    #fname_bg = os.path.join(data_dir_pal, "corrupt_im.png")
    im = io.imread(fname,as_gray=True)
    im = transform.resize(im,(64,64),preserve_range=True)[np.newaxis,:,:,np.newaxis]
    im = im /255.0
    if mode=="bg":
        im = 2*im - 1
    else:
        if not binary:
            im = 2 * im - 1
    return im
def prior_likelihood_grads(chpoint_dir, im, sigma,mode,binary):
    """
    Get gradients and discrete and continuous likelihood of prior model for an image
    chpoint_dir -
    im - scaled im
    sigma - sigma,
    mode - ut or bg
    binary - binary loss or log loss
    """

    if mode=="bg":
        res_epochs = {0: 40, 0.01: 80, 0.1: 120, 0.2: 160,
                      0.3: 200, 0.4: 240, 0.5: 280, 0.6: 320,
                      0.7: 360, 0.8: 400, 0.9: 440, 1.0: 480,1.2:520,1.4:540,1.6:580}
    elif mode=="ut":
        if binary:
            res_epochs = {0.0:12,1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24,
                      0.4: 34,0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
        else:
            res_epochs = {0.0: 300, 0.01: 100, 0.1: 100,
                          0.2: 100, 0.3: 100, 0.4: 100, 0.5: 100,
                          0.6: 100, 0.7: 100, 0.8: 100, 0.9: 100, 1.0: 100, 1.2: 100, 1.4:100,1.6:80}

    patch_size = 64
    tile_shape = (im.shape[1] // patch_size, im.shape[2] // patch_size)
    batch_size = np.prod(tile_shape)
    ims = plotting.split_image(im[0, :, :,0], patch_size)
    ims = ims
    res_epoch = res_epochs[sigma]
    chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                            "params_greek960_{}{}.ckpt".format(mode,res_epoch))
    #if mode=="ut" and sigma==0.0:
    #    chpoint_path = os.path.join(chpoint_dir,
    #                                "params_greek960_{}{}.ckpt".format(mode, res_epoch))
    if mode=="bg":
        smooth_l,grads,disc_l = gradients_ll_bg(ims,batch_size,sigma,chpoint_path)
        pred = None
    elif mode=="ut":
        pred,disc_l,smooth_l,grads = gradients_ll_ut(ims, batch_size, sigma, chpoint_path,model_discrete=binary)
        if binary:
            disc_l = disc_l[:,:,:,0]
        else:
            pred = np.argmax(pred, 1)
            pred = plotting.stich_image(pred, tile_shape)
    disc_l = plotting.stich_image(disc_l, tile_shape)
    if not smooth_l is None:
        smooth_l = plotting.stich_image(smooth_l[:,:,:,0], tile_shape)
    else:
        smooth_l = None
    im = plotting.stich_image(ims[:,:,:,0],tile_shape)
    if not smooth_l is None:
        grads = plotting.stich_image(grads[:, :, :,0], tile_shape)
    else:
        grads = None
    return im,disc_l,smooth_l,grads,pred


if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    log_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\1band\lower_eta\obscured_eta_lower_148x148_rgb_28"
    impath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192\train\0086_000001\line_12\line_12_0_23.png"
    mode = "ut"
    read_from_logdir = False
    binary = True
    if mode=="bg":
        chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\background\text_aug_exp\pixelcnn-gray-2022-12-23-19-08_with_text_aug"
    elif mode=="ut":
        if binary:
            chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2022-05-27-18-43_current"
        else:
            chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-gray-2023-02-08-04-13"

    epoch = 60000
    sigma = 0.0
    if read_from_logdir:
        im = read_im_from_logdir(main_dir,log_dir,epoch,mode,binary)
    else:
        im = read_im_from_datadir(impath,mode,binary)

    im,disc_l,smooth_l,grads,pred = prior_likelihood_grads(chpoint_dir,im,sigma,mode,binary)
    plot_ims_vs_ll_pred(im, disc_l, smooth_l, grads, mode + ", sigma=" + str(sigma) + ", epoch=" + str(epoch), binary)
    plot_pred_im(np.squeeze(im), np.squeeze(pred), "ut", binary)
    plt.show()