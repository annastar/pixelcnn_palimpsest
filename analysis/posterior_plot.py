import tensorflow as tf
from pixelcnn_palimpsest.pixel_cnn_pp.nn import sigmoid_loss_smoothed
from pixelcnn_palimpsest.exp_langevin.simulate_mix_fun_pdf import MixingVars
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_1band, \
    get_grads_fidelity_smoothed_apprx_msi
import numpy as np
import math
from matplotlib import pyplot as plt

def plot_posterior(du,do,dbg,org_noise,add_noise_bg):
    nb_samples =10000
    xu = np.linspace(-2.0,2.0,nb_samples,dtype=np.float32)
    xo = np.zeros_like(xu,dtype=np.float32)

    if hasattr(du, "__len__"):
        nb_bands = len(du)
    else:
        nb_bands = 1
    mix_fun_name = "sep_layers_bg_not_fn"
    sigma = 0.7
    #org_noise = 1e-10
    l=-1.0
    y_true = du+0.5
    sess = tf.Session()
    xu1 = np.ones_like(xu)
    xb = 0.5*np.ones_like(xu)
    d1 = MixingVars(du, do, xu1, xo, xb, sigma,sigma, org_noise,
                       add_noise_du=False, add_noise_do=False,
                       add_noise_xu=True,add_noise_xb=add_noise_bg, nb_samples=nb_samples,
                       mix_fun_name=mix_fun_name,bg_bias=0.0,bg_scaler=dbg,sess = sess,nb_bands=nb_bands)
    phi = math.sqrt(np.mean((d1.y - d1.y_smooth_noise) ** 2))
    print("Phi",phi)
    xu_tf = tf.constant(xu,dtype=tf.float32)
    pdf_0_tf = tf.exp(-(xu_tf ** 2) / (2 * sigma ** 2))
    pdf_1_tf = tf.exp(-((xu_tf - 1) ** 2) / (2 * sigma ** 2))
    C_tf= tf.log(sigma * tf.math.sqrt(2 * tf.constant(math.pi)))
    log_prior_tf = sigmoid_loss_smoothed(xu_tf,l,sigma,False)
    grad_prior_tf = tf.gradients(log_prior_tf,xu_tf)
    y_org_tf = tf.constant(y_true*np.ones((nb_samples,1,1,1)),dtype=tf.float32)
    if nb_bands==1:
        grad_fid_tf,log_fid_tf = get_grads_fidelity_smoothed_apprx_1band(y_org_tf, d1.y_pred, phi,d1.xs_ut)
    else:
        grad_fid_tf,log_fid_tf = get_grads_fidelity_smoothed_apprx_msi(y_org_tf, d1.y_pred, phi,d1.xs_ut)
    with sess as sess:
        prior,grad_prior,pdf_1,pdf_0,C = sess.run([log_prior_tf,grad_prior_tf,pdf_1_tf,pdf_0_tf,C_tf])
        grad_fid,ll = sess.run([grad_fid_tf,log_fid_tf],{d1.xs_ut:xu[:,np.newaxis,np.newaxis,np.newaxis],
                                                         d1.xs_ot:xo[:,np.newaxis,np.newaxis,np.newaxis],
                                                         d1.xs_bg:xb[:,np.newaxis,np.newaxis,np.newaxis]})

    grad_fid = [g/nb_bands for g in grad_fid]
    #ll = np.log(normal_pdf(d.y,y_true,phi))
    posterior = np.mean(ll,axis=(1,2,3))+prior
    fig,ax = plt.subplots(1,2)
    fig.suptitle("Smooting noise std={}, original noise std={:.3f},\n"
              "Prior model predicts xu='1' with confidence {},\n"
              "true values y={}, Du={}, xo=0, y=xo*Do+Du*xu(1-xo)".format(sigma,org_noise,l,y_true,du))
    ax[0].plot(xu,prior,label="log_prior",color="orange")
    ax[0].plot(xu,np.mean(ll,axis=(1,2,3)),label="log_likelihood",color="blue")
    ax[0].plot(xu,posterior, label="log_posterior",color="green")
    ax[0].grid()
    ax[0].legend()

    ax[1].plot(xu,np.squeeze(grad_prior[0]),label="grad-prior",color="orange")
    ax[1].plot(xu,np.squeeze(grad_fid[0])+grad_prior[0],label="grad-posterior",color="green")
    ax[1].plot(xu,np.squeeze(grad_fid[0]),label="grad_fid",color="blue")
    ax[1].legend()
    ax[1].grid()

if __name__=="__main__":
    #plot_posterior(0.1, 1e-10)
    do=np.array([0.6])
    dbg = np.array([1.0])
    du = np.array([0.1])
    plot_posterior(du,do,dbg, 1e-10, False)
    do=np.array([0.6])
    dbg = np.array([1.0])
    du = np.array([0.1])
    plot_posterior(du,do,dbg, 1e-10, True)
    plt.show()