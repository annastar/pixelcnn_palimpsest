import tensorflow as tf
import numpy as np
import os
from skimage import io
from pixelcnn_palimpsest.utils import plotting
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_pixelcnn_ut_model_with_par,compile_pixelcnn_bg_model_with_par
from pixelcnn_palimpsest.langevin_dynamics.prior import restore_model
from pixelcnn_palimpsest.analysis.viz_ut_prior_image import log_loss_smoothed
from pixelcnn_palimpsest.pixel_cnn_pp.nn import sigmoid_loss_smoothed
from pixelcnn_palimpsest.utils import files
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_1band,get_grads_fidelity_smoothed_apprx_msi


def read_layers(res_dir,epoch,ut_model):
    d = files.json_to_dict(os.path.join(res_dir, "par.json"))
    nb_ch = d["nb_ch"]
    ut = io.imread(os.path.join(res_dir, "ut", str(epoch) + ".png"), as_gray=True) / 255.0
    if ut_model=="discrete":
        ut = ut*2.0 - 1
    bg = io.imread(os.path.join(res_dir, "bg", str(epoch) + ".png"), as_gray=True) / 255.0
    ot = io.imread(os.path.join(res_dir, "mask.png"), as_gray=True) / 255.0
    bg = 2.0 * bg - 1
    if nb_ch == 1:
        org = io.imread(os.path.join(res_dir, "corrupt_im" + ".png"), as_gray=True) / 255.0
    else:
        org = []
        for i in range(nb_ch):
            org.append(io.imread(os.path.join(res_dir, "corrupt_im_band_" + str(i) + ".png"), as_gray=True) / 255.0)
        org = np.stack(org, axis=2)
    patch_size = 64
    tile_shape = (org.shape[0] // patch_size, org.shape[1] // patch_size)
    batch_size = np.prod(tile_shape)
    plot_layers(ot,ut,bg,org)
    return ot,ut,bg,org,batch_size,tile_shape

def plot_layers(ot,ut,bg,org):
    fig,ax = plt.subplots(4,1)
    ax[0].imshow(np.squeeze(ot),cmap="gray")
    ax[0].set_title("Ot")
    ax[1].imshow(np.squeeze(ut),cmap="gray")
    ax[1].set_title("Ut")
    ax[2].imshow(np.squeeze(bg),cmap="gray")
    ax[2].set_title("Bg")
    ax[3].imshow(np.squeeze(org),cmap="gray")
    ax[3].set_title("Org")



def coord_of_max_grad(chpoint_dir,ims,tile_shape, ut_model,mode):
    tf.compat.v1.reset_default_graph()
    sigma = 1.2
    xs, sll_tf, disc_pdf_tf, grad_tf,sess = grad_ll_tf_prior(chpoint_dir, sigma, ut_model,mode)
    grad = sess.run(grad_tf,{xs: ims})
    grad = np.squeeze(plotting.stich_image(grad[0], tile_shape))
    x,y = np.unravel_index(grad.argmax(), grad.shape)

    full_y = (y // 64) * 64 + (y % 64)
    full_x = (x // 64) * 64 + (x % 64)
    print("Max Grad val:", grad[x, y])
    print("Grad val check:",grad[full_x, full_y])
    return x,y

def calc_prior_grad_dL_dx_manually(ut, sigma,pix_coords, patch_size, val_range, ut_model,mode):
    """Calculate dL/dx, where L is prior likelihood loss, x is loss argument(not input to the network)
    im - original image
    val_range - an array with ordered and equally distributed values on the ut range of values
                for the purpose of calculated pdf at each point
    """
    # for delta of cur val, on val_range what is the index of this value
    cur_val = round(ut[pix_coords[0],pix_coords[0]],2)
    print(cur_val)
    a = np.round(val_range,2)
    cur_val_idx = np.where(a==cur_val)[0]
    batch_idx, new_coord = coord_full_im_2_batch(pix_coords,patch_size)
    uts = plotting.split_image(ut, patch_size)
    xs, sll_tf, disc_pdf_tf, grad_tf,sess = grad_ll_tf_prior(chpoint_dir, sigma, ut_model, mode)
    sll_pdf = sess.run(sll_tf,
                                 {xs: uts[batch_idx, :, :, :][np.newaxis, :, :, :]})
    next_val = sll_pdf[cur_val_idx+1,new_coord[0],new_coord[1]]
    val = sll_pdf[cur_val_idx,new_coord[0],new_coord[1]]
    grad = (next_val-val)/(val_range[1]-val_range[0])
    print("Grad val check:", grad)

def coord_full_im_2_batch(pix_coords,patch_size):
    x,y = pix_coords
    new_coord = (x % patch_size), (y % patch_size)
    batch_idx = (x // patch_size) * (y // patch_size)
    return batch_idx,new_coord

def grad_ll_tf_prior(chpoint_dir,sigma, ut_model,mode,batch_size=1):

    tf.compat.v1.reset_default_graph()
    sess = tf.Session()
    if mode=="ut":

        xs, ys,_ = compile_pixelcnn_ut_model_with_par(batch_size,ut_model)

        if ut_model=="discrete":
            res_epochs = {0.0: 300, 0.01: 100, 0.2: 100, 1.0: 100, 0.9: 100, 1.2: 100}
            res_epoch = res_epochs[sigma]
            chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                        "params_greek960_ut{}.ckpt".format(res_epoch))
            restore_model(chpoint_path, sess, "model_ut")
            sll_tf, disc_pdf_tf = log_loss_smoothed(xs, ys, tf.constant(sigma, dtype=tf.float32), False)
        elif ut_model == "binary":
            res_epochs = {1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24, 0.4: 34,
                           0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
            res_epoch = res_epochs[sigma]
            chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                        "params_greek960_ut{}.ckpt".format(res_epoch))
            restore_model(chpoint_path, sess, "model_ut")
            sll_tf = sigmoid_loss_smoothed(xs, ys, tf.constant(sigma, dtype=tf.float32), False)
            disc_pdf_tf = tf.expand_dims(ys, axis=0, name=None)
    elif mode=="bg":
        res_epochs = {0: 40, 0.01: 60, 0.1: 80, 0.2: 120, 0.3: 160,
                               0.4: 160, 0.5: 200, 0.6: 240, 0.7: 280,
                               0.8: 320, 0.9: 360, 1.0: 400}
        res_epoch = res_epochs[sigma]
        chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                    "params_greek960_bg{}.ckpt".format(res_epoch))
        xs, ys,_ = compile_pixelcnn_bg_model_with_par(batch_size)
        restore_model(chpoint_path, sess, "model_bg")
        sll_tf, disc_pdf_tf = log_loss_smoothed(xs, ys, tf.constant(sigma, dtype=tf.float32), False)
    grad_tf = tf.gradients(sll_tf, xs)
    return xs,sll_tf,disc_pdf_tf,grad_tf,sess

def prior_prediction(chpoint_dir,res_dir, sigma, epoch,ut_model,mode):

    ot, ut, bg, org, batch_size, tile_shape = read_layers(res_dir, epoch, ut_model)
    if mode=="ut":
        im = ut
    elif mode=="bg":
        im = bg
    ims = plotting.split_image(im,64)
    xs, sll_tf, disc_pdf_tf, grad_tf, sess = grad_ll_tf_prior(chpoint_dir, sigma, ut_model, mode,batch_size=len(ims))

    disc_pdf = sess.run(disc_pdf_tf,{xs:ims})
    pred = np.argmax(disc_pdf,1)
    pred = plotting.stich_image(pred,tile_shape)
    plot_ims_vs_network_prediction(im,pred,r"",sigma)

def plot_ims_vs_network_prediction(im,pred,res_dir,sigma):
    fig, ax = plt.subplots(2, 1)
    ax[0].imshow(im,cmap="gray")
    ax[1].imshow(pred,cmap="gray")
    #plt.savefig(os.path.join(res_dir,"Ims_vs_Prior_output".format(sigma)+".png"),dpi=300)

def calc_prior_pdf(chpoint_dir,res_dir,pix_coords, sigma, epoch,ut_model,val_range,mode):

    patch_size = 64
    batch_idx, new_coord = coord_full_im_2_batch(pix_coords,patch_size)
    ot, ut, bg, org, batch_size, tile_shape = read_layers(res_dir,epoch,ut_model)
    if mode=="ut":
        im = ut
        print("Ut value:", ut[pix_coords[0], pix_coords[1]])
    elif mode=="bg":
        im = bg
        print("Bg value:", bg[pix_coords[0], pix_coords[1]])

    ims = plotting.split_image(im,patch_size)

    xs,sll_tf,disc_pdf_tf,grad_tf,sess = grad_ll_tf_prior(chpoint_dir, sigma, ut_model,mode)
    sll_pdf_l = []
    grad_l = []
    for val in val_range:
        ims = ims[batch_idx, :, :, :][np.newaxis, :, :, :]
        ims[:,new_coord[0],new_coord[1],:] = val
        disc_pdf, sll_pdf,grads = sess.run([disc_pdf_tf, sll_tf, grad_tf],
                                       {xs: ims})

        sll_pdf = np.squeeze(np.array(sll_pdf)[:,new_coord[0],new_coord[1],:])
        grads = np.squeeze(grads[0][:,new_coord[0],new_coord[1],:])
        grad_l.append(grads)
        sll_pdf_l.append(sll_pdf)
    disc_pdf = np.squeeze(disc_pdf[:,:, new_coord[0], new_coord[1], :])
    sess.close()
    return np.array(sll_pdf_l),disc_pdf,np.array(grad_l)


def calc_fid_pdf(main_dir,sigma,pal_name,ut_model,res_dir,epoch,pix_coords,xs,mode):
    tf.compat.v1.reset_default_graph()
    x, y = pix_coords
    d = files.json_to_dict(os.path.join(res_dir, "par.json"))
    du = d["Du"][0]
    do = d["Do"][0]
    dbg = d["Db"][0]
    org_noise_std = d["noise_level"]
    print("Org noise std={}, Du={}".format(org_noise_std,du))
    mix_fun_str = d["mix_fun_str"]
    nb_ch = d["nb_ch"]
    fid_loss = d["fid_loss"]
    ot, ut, bg, org, batch_size, tile_shape = read_layers(res_dir,epoch,ut_model)

    nb_smp = len(xs)
    ut = np.array([ut[x,y]]*nb_smp)
    ot = np.array([ot[x,y]]*nb_smp)
    bg = np.array([bg[x,y]]*nb_smp)
    org = np.array([org[x,y]]*nb_smp)
    print("Palimpsest org value", org[0])
    org_tf = tf.constant(np.expand_dims(org,[1,2,3]), dtype=tf.float32)
    mix_fun = eval("mix_model." + mix_fun_str)
    xs_ut_tf = tf.placeholder(shape=(None, 1, 1, 1), dtype=tf.float32,name="ut")
    xs_ot_tf = tf.placeholder(shape=(None, 1, 1, 1), dtype=tf.float32,name="ot")
    xs_bg_tf = tf.placeholder(shape=(None, 1, 1, 1), dtype=tf.float32,name="bg")
    if mode == "ut":
        d = du
        mode = "xu"
        xs_tf = xs_ut_tf
        if ut_model:
            xs = xs
    elif mode == "bg":
        d = dbg
        mode = "xb"
        xs_tf = xs_bg_tf
        xs = xs

    xs_bg_tf_scaled = (xs_bg_tf+1)/2
    if ut_model=="discrete":
        xs_ut_tf_scaled = (xs_ut_tf+1)/2
    elif ut_model=="binary":
        xs_ut_tf_scaled = xs_ut_tf
    y_pred_tf, _= mix_fun(undertext=xs_ut_tf_scaled, background=xs_bg_tf_scaled,
                               overtext=xs_ot_tf, nb_ch=nb_ch, bg_bias=0.0,
                               bg_scaler=dbg, ut_refl=du, ot_refl=do)

    with tf.Session() as sess:

        if nb_ch == 1:
            phi_list_f = os.path.join(main_dir, "training",
                                         "langevin", mix_fun_str,"xu_"+ut_model,"uvn",
                                         "phi_list_nb_layers_1_org_noise_{}_{}.json".format(org_noise_std,mode))
            phi_list = files.json_to_dict(phi_list_f)
            phi = phi_list[str(sigma)][str(round(d, 2))]
            print("Sigma={} ,Phi={}".format(sigma,phi))
            grad_tf,ll_tf = get_grads_fidelity_smoothed_apprx_1band(org_tf,y_pred_tf,phi,xs_tf)
        elif nb_ch>1 and fid_loss=="mvn":
            round_d = str([round(u, 3) for u in d]).replace(" ", "")
            phi_list_f = os.path.join(main_dir, "training",
                                         "langevin", mix_fun_str,"xu_"+ut_model, "mvn",
                                         "{}_{}_on_{}_{}_{}.npz".format(mode,pal_name, org_noise_std,"Du" if mode=="xu" else "Db" ,round_d))
            phi_list = np.load(phi_list_f,allow_pickle=True)
            phi = phi_list[str(sigma)].astype(np.float32)
            print("Sigma={} ,Phi={}".format(sigma, phi))
            cov_tf = tf.constant(phi,dtype=tf.float32)
            grad_tf, ll_tf = get_grads_fidelity_smoothed_apprx_msi(org_tf, y_pred_tf, cov_tf, xs_tf)
        sess.run(tf.initialize_all_variables())
        if mode=="xu":
            grad,ll,y_pred = sess.run([grad_tf,tf.log(ll_tf),y_pred_tf],{xs_ut_tf:np.expand_dims(xs,[1,2,3]),
                                                        xs_ot_tf:np.expand_dims(ot.ravel(),[1,2,3]),
                                                        xs_bg_tf:np.expand_dims(bg.ravel(),[1,2,3])})
        if mode == "xb":
            grad, ll,y_pred = sess.run([grad_tf, tf.log(ll_tf),y_pred_tf], {xs_ut_tf: np.expand_dims(ut.ravel(), [1, 2, 3]),
                                                           xs_ot_tf: np.expand_dims(ot.ravel(), [1, 2, 3]),
                                                           xs_bg_tf: np.expand_dims(xs, [1, 2, 3])})
    return np.squeeze(ll),np.squeeze(grad[0])

def viz_pdf_of_1_pix(disc_log_probs, pix_coord, idxs,title):
    """Plot pdf for 1 pixel"""
    plt.figure()
    plt.title("{}, pixel coord {}".format(title,pix_coord))
    plt.stem(idxs,np.squeeze(disc_log_probs))


def viz_pdf_ll_fid_1_pix(ll1,ll2, pix_coord, idxs,title):
    """Plot pdf for 1 pixel"""
    plt.figure()
    plt.title("{}, pixel coord {}".format(title,pix_coord))
    plt.stem(idxs,np.squeeze(ll1),label="prior",markerfmt="*")
    plt.stem(idxs, np.squeeze(ll2),label="fid",markerfmt=".")
    plt.legend()


def plot_ims_vs_ll_bg(smooth_ll,res_dir,sigma):
    fig, ax = plt.subplots(1, 1)
    pcm0 = ax.pcolormesh(smooth_ll[ ::-1, :], cmap='RdBu', vmin=np.amin(smooth_ll), vmax=np.amax(smooth_ll))
    ax.set_aspect('equal')
    fig.colorbar(pcm0, ax=ax)
    plt.savefig(os.path.join(res_dir,"Sigma_{}_LL_bg_prior".format(sigma)+".png"),dpi=300)



def viz_pdf_prior_ll(main_dir,chpoint_dir,res_dir,mode,ut_model):
    sigma = 1.0
    pal_name = "greek960"

    epoch =499
    pix_coords = [52,170]
    val_range1 = np.arange(-1.5,1.5,0.1)
    #prior_pred(chpoint_dir, res_dir, sigma, epoch, ut_model, mode)
    sll_pdf,disc_pdf,grad_prior = calc_prior_pdf(chpoint_dir,res_dir,pix_coords, sigma, epoch,ut_model,val_range1,mode)
    val_range2 = np.arange(-1.5,1.5,0.1)
    ll_pdf,grad_fid = calc_fid_pdf(main_dir, sigma, pal_name, ut_model, res_dir, epoch, pix_coords, val_range2,mode)
    #viz_pdf_of_1_pix(ll_pdf,[pix_coords[0],pix_coords[1]],val_range2,"{} Fidelity".format(mode))
    #viz_pdf_of_1_pix(np.exp(ll_pdf), [pix_coords[0], pix_coords[1]], val_range2, "{} exp of Fidelity".format(mode))
    h = 256
    #idxs =2. * np.arange(0, h) / (h - 1.) - 1
    #viz_pdf_of_1_pix(disc_pdf,[pix_coords[0],pix_coords[1]],idxs, "{} Discrete prior ll".format(mode))
    #viz_pdf_of_1_pix(sll_pdf,[pix_coords[0],pix_coords[1]],val_range1,"{} Smooth prior ll".format(mode))
    #viz_pdf_of_1_pix(np.exp(sll_pdf), [pix_coords[0], pix_coords[1]], val_range1, "{} exp Smooth prior ll".format(mode))
    viz_pdf_of_1_pix(grad_fid, [pix_coords[0], pix_coords[1]], val_range2, "{} Grads fidelity".format(mode))
    viz_pdf_of_1_pix(grad_prior, [pix_coords[0], pix_coords[1]], val_range1, "{} Grads prior".format(mode))
    viz_pdf_ll_fid_1_pix(np.exp(sll_pdf),np.exp(ll_pdf),[pix_coords[0], pix_coords[1]],val_range1,"{} Prior vs. Fidelity likelihood".format(mode))



if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    ut_model = "discrete"
    if ut_model=="discrete":
        chpoint_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-gray-2023-02-08-04-13"
    elif ut_model=="binary":
        chpoint_dir = os.path.join(main_dir, "training", "generators", "undertext","pixelcnn-bin-2022-05-27-18-43")
    res_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\with_bg_t_2023-02-21-16-18_grads_through_prior"
    #r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\with_bg_t_2023-03-07-04-52"
    mode = "ut"
    viz_pdf_prior_ll(main_dir, chpoint_dir, res_dir,mode,ut_model)
    mode = "bg"
    chpoint_dir = os.path.join(main_dir, "training", "generators", "background",
                     "pixelcnn-gray-2022-10-26-02-04")
    viz_pdf_prior_ll(main_dir, chpoint_dir, res_dir,mode,ut_model)
    plt.show()