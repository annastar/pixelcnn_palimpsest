import tensorflow as tf
import numpy as np
import os
from skimage import io,transform
from pixelcnn_palimpsest.utils import plotting
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.pixel_cnn_pp.model import model_spec_gray_log_mix,model_spec_discrete
from pixelcnn_palimpsest.langevin_dynamics.prior import restore_model
from pixelcnn_palimpsest.pixel_cnn_pp.nn import sigmoid_loss_smoothed,discretized_mix_logistic_loss_gray
from pixelcnn_palimpsest.analysis.log_loss_smoothed_pdf import log_loss_smoothed_pdf



def create_ut(im_mode, distor_mode,ut_model):
    """Visualize bg prior grad for a single im"""
    ###reset graph
    if im_mode=="white":
        im= np.ones((64,64))
    elif im_mode=="test":
        path = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192\test\0086_000084\line_24\line_242_0_20.png"
        im = io.imread(path,as_gray=True)/255.0
        im = transform.resize(im,(64,64))
    elif im_mode=="train":
        path = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192\train\0086_000083\line_24\line_242_0_28.png"
        im = io.imread(path,as_gray=True)/255.0
        im = transform.resize(im, (64, 64))
    elif im_mode=="generated":
        path = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\with_bg_t_2023-02-22-23-54\ut\15499.png"
        im = io.imread(path,as_gray=True)/255.0
    if distor_mode=="vline":
        im[:,30:34]=1.0
    elif distor_mode=="hline":
        im[30:34,:]=1.0
    elif distor_mode=="lh_half":
        im[30:63,:]=1.0
    size_y, size_x = im.shape[0], im.shape[1]
    im = plotting.split_image(im,64)
    if ut_model=="discrete":
        im = im*2 - 1
    elif ut_model=="binary":
        im=im
    else:
        raise ValueError("No such model")
    batch_size = im.shape[0]
    tile_shape = (size_y // 64, size_x // 64)
    return im,batch_size,tile_shape

def compile_ut_prior_model(batch_size,model_scope,ut_model):
    """
    Restore pixelCNN++ model
    Arg:
        checkpoint_path - checkpoint path
    Returns:
        outputs:
            ys [Tensor] - model output node
            xs [Tensor] - model input placeholder
            sess [obj] - session with restored parameters
    """
    if ut_model=="discrete":
        model = model_spec_gray_log_mix
    elif ut_model == "binary":
        model = model_spec_discrete
    nr_filters=64
    nr_resnet = 5
    nr_mix = 2
    xs = tf.placeholder(tf.float32, shape=(batch_size, 64, 64, 1), name="x_ut")
    model_opt = {'nr_resnet': nr_resnet, 'nr_filters': nr_filters, 'nr_logistic_mix': nr_mix,
                     'resnet_nonlinearity': 'concat_elu','q_levels':1}
    model = tf.make_template(model_scope, model)
    ys = model(xs, None, init=False, ema=None, dropout_p=0., **model_opt)
    return xs,ys



def viz_ut_prior_text_aug(chpoint_dir, ims, batch_size,tile_shape,mode_ut,mode_dest,ut_model):

    dir_name = mode_ut
    dir_name += mode_dest if mode_dest is not None else ""
    res_dir = os.path.join(chpoint_dir, "Reaction_distortion",dir_name)
    if not os.path.isdir(res_dir):
        os.makedirs(res_dir)
    im = plotting.stich_image(ims[:,:,:,0], tile_shape)
    io.imsave(os.path.join(res_dir,"org_img.png"),(im+1)/2)
    sigmas = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.2,1.4,1.6]
    if ut_model=="binary":
        res_epochs = {0.0:190,1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24,
                      0.4: 34,0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
    elif ut_model=="discrete":
        res_epochs = {0.0:300,0.01:100,0.1:100,
                  0.2:100,0.3:100,0.4:100,0.5:100,
                  0.6:100,0.7:100,0.8:100, 0.9:100,1.0:100,1.2:100}
    tf.compat.v1.reset_default_graph()
    xs, ys = compile_ut_prior_model(batch_size, "model_ut",ut_model)
    all_params = tf.trainable_variables()
    ema = tf.train.ExponentialMovingAverage(decay=0.9995)
    maintain_averages_op = tf.group(ema.apply(all_params))

    sess = tf.Session()
    for sigma in sigmas:
        ims = ims + np.random.normal(0.0,sigma,ims.shape)
        im = plotting.stich_image(ims[:, :, :, 0], tile_shape)
        res_epoch = res_epochs[sigma]
        chpoint_path = os.path.join(chpoint_dir, "noise_" + str(sigma),
                                       "params_greek960_ut{}.ckpt".format(res_epoch))

        restore_model(chpoint_path, sess, "model_ut")
        for p in all_params:
            p.assign(ema.average(p))
        if ut_model == "discrete":
            nll = discretized_mix_logistic_loss_gray(xs,ys,False)
            l_tf = tf.exp(-nll)
            sll, dics_pdf_tf = log_loss_smoothed_pdf(xs, ys, tf.constant(sigma, dtype=tf.float32), False)

        elif ut_model == "binary":
            l_tf = tf.nn.sigmoid(ys)
            #l_tf = tf.nn.sigmoid_cross_entropy_with_logits(labels = xs, logits =ys)#sigmoid_loss_binary(xs,ys,False)
            if sigma>0.0:
                sll = sigmoid_loss_smoothed(xs,ys,tf.constant(sigma, dtype=tf.float32), False)

        grad_tf = tf.gradients(sll,xs)

        sl_tf = tf.exp(sll)
        sl,l,grad = sess.run([sl_tf,l_tf,grad_tf],{xs:ims})
        l = plotting.stich_image(l,tile_shape)
        sl = plotting.stich_image(sl, tile_shape)
        grad = plotting.stich_image(grad[0],tile_shape)
        plot_ims_vs_ll_pred(np.squeeze(l),np.squeeze((im+1)/2),None, res_dir, sigma,"Disc ut prior LL and gradients")
        plot_ims_vs_ll_pred(np.squeeze(sl), np.squeeze((im + 1) / 2), None, res_dir, sigma,"Smooth ut prior LL and gradients")
        plot_ims_vs_ll_bg(np.squeeze(grad), res_dir,sigma)
    sess.close()



def plot_ims_vs_ll_pred(ll,im,prob,res_dir,sigma,title):
    nb_figs = 3
    if prob==None:
        nb_figs-=1
    fig, ax = plt.subplots(nb_figs, 1)
    fig.suptitle(title)
    if not ll is None:
        ax[1].set_title("Prior Likelihood",pad=20,fontsize=10)
        ax[1].imshow(ll[ :, :], cmap='RdBu', vmin=np.amin(ll), vmax=np.amax(ll))
        pcm0 = ax[1].pcolormesh(ll[:, :], cmap='RdBu', vmin=np.amin(ll),
                                vmax=np.amax(ll))
        fig.colorbar(pcm0, ax=ax[1])
    ax[0].imshow(im,cmap="gray",vmin=np.amin(im), vmax=np.amax(im))
    ax[0].set_title("Imagen",pad=20,fontsize=10)
    if not prob is None:
        ax[2].imshow(prob,cmap="gray")
        ax[2].set_title("Probability of x=1",pad=20,fontsize=10)
    for i in range(nb_figs):
        ax[i].axis('off')
        ax[i].set_aspect('equal')
    plt.tight_layout()
    plt.savefig(os.path.join(res_dir,"Sigma_{}_LL_grad_ut_prior".format(sigma)+".png"),dpi=300)

def plot_ims_vs_ll_bg(smooth_ll,res_dir,sigma):
    fig, ax = plt.subplots(1, 1)
    pcm0 = ax.pcolormesh(smooth_ll[ ::-1, :], cmap='RdBu', vmin=np.amin(smooth_ll), vmax=np.amax(smooth_ll))
    ax.set_aspect('equal')
    fig.colorbar(pcm0, ax=ax)
    plt.savefig(os.path.join(res_dir,"Sigma_{}_LL_bg_prior".format(sigma)+".png"),dpi=300)

if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    ch_dir_path = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext"

    ch_dir= r"pixelcnn-gray-2023-02-08-04-13"
    modes_ut = ["train","test","white", "generated"]
    modes_dest = [None,"lh_half","vline","hline"]
    ut_model = "discrete"

    ims, batch_size, tile_shape = create_ut(modes_ut[1],modes_dest[0],ut_model)
    viz_ut_prior_text_aug(os.path.join(ch_dir_path, ch_dir), ims, batch_size, tile_shape,modes_ut[1],modes_dest[0],ut_model)
    plt.show()