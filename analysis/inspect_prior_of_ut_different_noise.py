import tensorflow as tf
import numpy as np
import os
from skimage import io,transform

from pixelcnn_palimpsest.analysis.inspect_prior_of_reconst import plot_ims_vs_ll_pred,prior_likelihood_grads, plot_pred_im
from matplotlib import pyplot as plt




def plot_noise_vs_grads(noise,grads):
    fig,ax = plt.subplots(2,1)
    ax[0].imshow(noise,cmap="gray")
    ax[0].set_title("Noise")
    ax[1].imshow(np.squeeze(grads),cmap="gray")
    ax[1].set_title("Grads")

def read_im_with_diff_noise(main_dir,data_dir,file_name,mode,binary,noise_type,noise_scale):
    fpath = os.path.join(main_dir, data_dir,file_name)
    #fname_bg = os.path.join(data_dir_pal, "corrupt_im.png")
    im = io.imread(fpath,as_gray=True)
    im = transform.resize(im,output_shape=(64,64),preserve_range=True)
    im = im /255.0
    if mode=="bg":
        im = 2*im - 1
    else:
        if not binary:
            im = 2 * im - 1
    if noise_type=="gaussian":
        noise = np.random.normal(0,noise_scale,size=im.shape)
    elif noise_type=="cross":
        noise = np.zeros_like(im)
        noise[30:34] = 1
        noise[:,30:34] = 1
    else:
        noise = np.random.uniform(-noise_scale,+noise_scale,size=im.shape)
    return im[np.newaxis,:,:,np.newaxis], noise[np.newaxis,:,:,np.newaxis]

if __name__=="__main__":
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    data_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192\train\0086_000081\line_3"
    fname = r"line_3_0_50.png"
    mode = "ut"
    binary = True
    if mode=="bg":
        chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\background\text_aug_exp\pixelcnn-gray-2022-12-23-19-08_with_text_aug"
    elif mode=="ut":
        if binary:
            chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2022-05-27-18-43_current"
        else:
            chpoint_dir= r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-gray-2023-02-08-04-13"


    sigma = 0.9
    noise_type = "gaussian"
    noise_scale = 0.9

    im,noise = read_im_with_diff_noise(main_dir,data_dir,fname,mode,binary,noise_type,noise_scale)
    im_noise = im + noise
    #im_noise = np.clip(im_noise, a_min=0.0 - sigma, a_max=1.0 + sigma)
    im_,disc_l,smooth_l,grads,pred = prior_likelihood_grads(chpoint_dir,im_noise,sigma,mode,binary)
    plot_ims_vs_ll_pred(np.squeeze(im), disc_l, smooth_l, grads,
                        "{}, sigma={}, noise {} with noise scale={}".format(mode, sigma, noise_type, noise_scale),
                        binary)

    plot_pred_im(np.squeeze(im_noise), np.squeeze(pred), "ut", binary)
    plot_noise_vs_grads(np.squeeze(noise),grads)
    plt.show()