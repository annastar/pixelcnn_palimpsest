
import tensorflow as tf
from pixelcnn_palimpsest.pixel_cnn_pp.nn import sigmoid_loss_smoothed
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_1band
import numpy as np
from matplotlib import pyplot as plt
import math


def grad_anna_binary(x,f):
    """Grad calculated by hand from binary smooth loss
    f - probability of one"""
    y = -(x / (s ** 2)) + (f* np.exp((2 * x - 1) / (2 * s ** 2))) /((s ** 2) * (f * np.exp((2 * x - 1) / (2 * s ** 2)) + 1 - f))
    return y

def log_loss_smoothed(x,l,s,batch_size,sum_all=True):
    """ log-likelihood for probability modeled with sigmoid
    s - sigma, noise variance"""
    h = 2
    #iterate though the batch
    #skip the first and the last value
    idxs = 2. * np.arange(0, h) / (h - 1.) - 1
    idxs_tf = tf.constant(idxs, dtype=tf.float32)
    idxs_tf = tf.expand_dims(tf.stack([idxs_tf]*batch_size,axis=0),axis=2)
    C = tf.cast(-tf.math.log(s * np.sqrt(2 * np.pi)),dtype=tf.float32)
    disc_log_probs = tf.concat([tf.concat([1-l,l],axis=1)]*batch_size,axis=0)
    x = tf.concat([x,x],axis=1)
    b = tf.math.reduce_logsumexp(tf.log(disc_log_probs)-0.5*((x-idxs_tf)/s)**2,1)
    smooth_log_probs = C + b
    if sum_all:
        return tf.reduce_sum(smooth_log_probs)
    else:
        return smooth_log_probs

def grads_fid(x,y_org,phi,Du,wrt_var):
    y_pred = x*Du
    grads,loss = get_grads_fidelity_smoothed_apprx_1band(y_org,y_pred,phi,wrt_var)
    return grads,loss

batch_size = 200
mode = "discrete"
phi_org = 0.01
Du = -0.07
x = np.linspace(-5,5,batch_size)[:,np.newaxis,np.newaxis]
l = np.array([-0.5])[:,np.newaxis,np.newaxis]
x_tf = tf.constant(x, dtype=tf.float32)
y_org_tf = tf.constant(np.array([0.0]*batch_size)[:,np.newaxis,np.newaxis], dtype=tf.float32)
l = tf.constant(l, dtype=tf.float32)
prob_tf = tf.nn.sigmoid(l)
nb_plots = 3
fig1,ax1 = plt.subplots(1,nb_plots)
fig2,ax2 = plt.subplots(1,nb_plots)
for s in [1.4,1.2,1.0,0.8]:#[1.3,1.2,1.1,1.0,0.9,0.8]:
    #np.random.normal(0,Du*s,1000)+np.random.normal(0,phi_org,1000)
    #np.random.normal(0, (Du/2) * s, 1000) + np.random.normal(0, phi_org, 1000)
    phi_g = math.sqrt((Du * s / 2) ** 2 + phi_org**2)
    phi_b = math.sqrt((Du * s ) ** 2 + phi_org**2)
    grads_fid_b_tf,fid_b_tf = grads_fid(x_tf, y_org_tf, phi_b,Du,x_tf)
    grads_fid_g_tf, fid_g_tf = grads_fid((x_tf+1)/2, y_org_tf, phi_g,Du,x_tf)
    s_tf = tf.constant(s,dtype=tf.float32)
    y_binary = sigmoid_loss_smoothed(x_tf,l,s,False)
    y_grayscale =log_loss_smoothed(x_tf,prob_tf,s,batch_size,False)
    grads_b_tf = tf.gradients(y_binary,x_tf)
    grads_g_tf = tf.gradients(y_grayscale,x_tf)
    with tf.Session() as sess:
        mode = "binary"
        grads_b, prob, ll_binary,grad_fid_b = sess.run([grads_b_tf, prob_tf, y_binary,grads_fid_b_tf])
        grad_b_anna = grad_anna_binary(x, prob)
        fig1.suptitle("x from [0,1], prior {}".format(mode))
        ax1[1].plot(np.squeeze(x), np.squeeze(np.exp(ll_binary)), label="s={}".format(s))
        ax1[1].set_title("Prior Liklihood")
        ax1[0].plot(np.squeeze(x),np.squeeze(grads_b[0]),label="s={}".format(s))
        ax1[0].set_title("Grad prior")
        ax1[2].plot(np.squeeze(x), np.squeeze(grad_fid_b), label="s={}".format(s))
        ax1[2].set_title("Grad fid")
        mode = "discrete"
        grads_g, prob, ll_grayscale,grad_fid_g = sess.run([grads_g_tf, prob_tf, y_grayscale,grads_fid_g_tf])
        fig2.suptitle("x from [-1,1], prior {}".format(mode))
        ax2[0].plot(np.squeeze(x),np.squeeze(grads_g[0]),label="s={}".format(s))
        ax2[1].plot(np.squeeze(x), np.squeeze(np.exp(ll_grayscale)), label="s={}".format(s))
        ax2[2].plot(np.squeeze(x), np.squeeze(grad_fid_g), label="s={}".format(s))
        ax2[2].set_title("Grad fid")
        ax2[1].set_title("Prior Likelihood")
        ax2[0].set_title("Grad prior")
        print("Sigma={},Phi_b={}, Phi_g={}".format(s,phi_b,phi_g))
print("Probability of x being 1=", prob)
for i in range(nb_plots):
    ax1[i].legend()
    ax1[i].grid()
    ax2[i].legend()
    ax2[i].grid()
plt.show()