import math

import numpy as np
import tensorflow as tf
from pixelcnn_palimpsest.langevin_dynamics import data
from pixelcnn_palimpsest.utils.files import json_to_dict
import os
from skimage import io
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 18})

def create_palimpsest(noise_level,Do,Du,Db):
    mix_fun_str = "sep_layers_bg_not_fn"
    main_dir = r"/"
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
    data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"line_4_moved.png"
    fname_ut = r"line_27.png"
    row_coord = [0, 192]
    col_coord = [0, 192 * 4]
    pal = data.PrepArtPal(data_dir, data_dir_bg, row_coord, col_coord, "undertext", fname_ot, fname_ut, True)
    sess = tf.Session()
    y_true = pal.create_art_palimpsest(sess,noise_level,mix_fun_str,Db,Do,Du,True)
    im_ut = pal.im_ut
    im_ot = pal.im_ot
    sess.close()
    return y_true,im_ut,im_ot

def ext_contast(y_ut,im_ut,im_ot):

    ut_map = im_ut>0.1
    ut_map = np.logical_and(ut_map,(1-im_ot)>0.5)
    bg_map = ut_map==False

    bg_mean = np.mean(y_ut[bg_map])
    contrast = np.sqrt(np.mean((y_ut[ut_map] - bg_mean) ** 2))
    return contrast

def mse(ut_true,ut_pred):
    return np.mean((ut_true-ut_pred)**2)

def rest_vs_contrast(main_dir):
    list_exp_dir = os.listdir(main_dir)
    conts=[]
    accs=[]
    for exp_dir in list_exp_dir:
        tf.compat.v1.reset_default_graph()
        d = json_to_dict(os.path.join(main_dir,exp_dir, "par.json"))
        Du = d["Du"]
        Db = d["Db"]
        n_level = d["noise_level"]
        y_true_ut, im_ut,im_ot = create_palimpsest(n_level,0.0,Du,Db)
        contrast = ext_contast(y_true_ut, im_ut,im_ot)
        conts.append(contrast)
        epoch = d["nb_epoch"] * 11 - 1
        im_pred = io.imread(os.path.join(main_dir,exp_dir,"ut",str(epoch)+".png"),as_gray=True)/255.0
        acc = mse(np.squeeze(im_ut),im_pred)
        print("Du={}, contrast={}".format(Du,contrast))
        accs.append(acc)
    plt.figure(figsize=(8, 6))
    plt.plot(conts,accs,marker="x")
    plt.xlabel('undertext contrast')
    plt.ylabel('MSE')
    plt.grid(True)
    plt.savefig(os.path.join(main_dir,"restr_vs_contrast.png"),dpi=300)
    plt.show()

def calc_angle(du,db):
    db = 0.521321576748332*np.array(db)
    du = np.array(du)+db
    angle = math.acos(np.sum(du*db)/math.sqrt(np.sum(du**2)*np.sum(db**2)))
    return angle*180/math.pi
def rest_vs_spec_angle(main_dir):
    list_exp_dir = os.listdir(main_dir)
    angles=[]
    accs=[]
    for exp_dir in list_exp_dir:
        tf.compat.v1.reset_default_graph()
        d = json_to_dict(os.path.join(main_dir,exp_dir, "par.json"))
        Du = d["Du"]
        Db = d["Db"]
        n_level = d["noise_level"]
        y_true_ut, im_ut,im_ot = create_palimpsest(n_level,0.0,Du,Db)
        angle = calc_angle(Du,Db)
        angles.append(angle)
        epoch = d["nb_epoch"] * 11 - 1
        im_pred = io.imread(os.path.join(main_dir,exp_dir,"ut",str(epoch)+".png"),as_gray=True)/255.0
        acc = mse(np.squeeze(im_ut),im_pred)
        print("Du={}, angle={}".format(Du,angle))
        accs.append(acc)
    plt.figure(figsize=(8, 6))
    plt.title("Spec. angle vs.restoration accuracy")
    plt.plot(angles,accs,marker="x")
    plt.xlabel('Deg,{}'.format(chr(176)))
    plt.ylabel('MSE')
    plt.grid(True)
    plt.savefig(os.path.join(main_dir,"restr_vs_spac_angle.png"),dpi=300)
    plt.show()
if __name__=="__main__":
    main_dir = r"/training/langevin/art_pal/2band/mvn"
    #rest_vs_contrast(main_dir)
    rest_vs_spec_angle(main_dir)