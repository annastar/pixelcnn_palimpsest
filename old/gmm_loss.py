import tensorflow as tf
import tensorflow_probability as tfp

from matplotlib import pyplot as plt

import numpy as np
import os
from pixelcnn_palimpsest.langevin_dynamics import data


def gmm_loss(x_all,nb_obs,mean_g,cov_g=None,sum_all=True):
    """
    The loss function that calculate energy of pixels of being from the same pdf
    all_x - pixels under considerations
    x_mask - undertext
    x_over_mask - mask over overtext, to decrease loss over overtext area
    """
    #Gaussian mean:
    mean_g = tf.constant(mean_g,dtype=tf.float32)
    if cov_g is None:
        cov_g = 0.01*tf.eye(nb_obs,nb_obs)
    else:
        cov_g = tf.constant(cov_g,dtype=tf.float32)
    pdf = tfp.distributions.MultivariateNormalFullCovariance(
            mean_g, cov_g, validate_args=False, allow_nan_stats=True, name='Normal')
    neg_log_prob = -pdf.log_prob(tf.reshape(x_all,[-1,nb_obs]))
    if sum_all:
        neg_log_prob = tf.reduce_sum(neg_log_prob)
    return neg_log_prob

def simple_gaussian_loss(x_all,x_mask,x_over_mask, graph, nb_obs, with_par=False,std_fixed=False):
    """
    The loss function that calculate energy of pixels of being from the same pdf
    all_x - pixels under considerations
    x_mask - undertext
    x_over_mask - mask over overtext, to decrease loss over overtext area
    """
    #Gaussian mean:
    with graph.as_default():
        with tf.variable_scope("gmm_undertext", reuse=tf.AUTO_REUSE):
            x_mask = tf.cond(tf.math.reduce_min(x_mask)<0,lambda: 0.5*x_mask+0.5,lambda :x_mask) #scale if values from -1 to 1
            x_mask = tf.cast(tf.math.greater(x_mask,0.8),tf.float32)#leave only center
            if x_over_mask!=None:
                x_mask = tf.multiply(x_mask,x_over_mask)
            x_all_masked = tf.reduce_sum(tf.multiply(x_all, x_mask), axis=[0,1,2],
                          name="normal_mean")
            mean_g = tf.divide(x_all_masked,tf.reduce_sum(x_mask),name="mean_g")

            sub = tf.reshape(x_all,[-1,nb_obs])-mean_g
            centered_x = tf.multiply(tf.square(sub),tf.reshape(x_mask,[-1,1]))
            var_g = tf.divide(tf.reduce_sum(centered_x,axis=[0]),tf.reduce_sum(x_mask))
            if std_fixed:
                std_g = 0.0001*tf.eye(nb_obs,nb_obs)
            else:
                std_g = tf.diag(tf.sqrt(var_g))
            pdf = tfp.distributions.MultivariateNormalFullCovariance(
                mean_g, std_g, validate_args=False, allow_nan_stats=True, name='Normal')
            neg_log_prob = -tf.multiply(pdf.log_prob(tf.reshape(x_all,[-1,nb_obs])),tf.reshape(x_mask,[-1,1]))
            neg_log_prob = tf.reduce_mean(neg_log_prob)
    if with_par:
        return neg_log_prob, mean_g,x_mask
    else:
        return neg_log_prob



def gmm_loss_plot():
    ###experiment parameters
    patch_size_ut = 64
    patch_size_bg = 16
    assign_scale_ou_after = 10000
    data_dir = r"/datasets"
    data_dir = os.path.join(data_dir, r"Greek_960", r"0086_000084")
    ot_fname = r"0086_000084+MB625Rd_007_F_thresh.png"
    coord_x = [2300, 2300+192]
    coord_y = [1800, 2914]
    msi = True
    conv_opt_dens = False
    rot_angle = -90
    palimpsest_fname = None
    bg_refl = None
    ys_out,x_ot,data_range,bg_mean,bg_std = data.prep_im_data(data_dir, coord_x, coord_y, msi, ot_fname,
                                                              conv_opt_dens, rot_angle, palimpsest_fname, bg_refl)
    ys_out_tf = tf.constant(ys_out,dtype=tf.float32)
    graph = tf.get_default_graph()
    sess = tf.Session()
    loss_tf = gmm_loss(ys_out_tf,ys_out.shape[-1],bg_mean,std_g=bg_std/2,sum_all=False)
    loss = sess.run(loss_tf)
    loss = np.reshape(loss,[ys_out.shape[1],ys_out.shape[2]])

    fig,ax = plt.subplots(3,1)
    ax[0].imshow(ys_out[0,:,:,0],cmap="gray")
    ax[0].set_title("Original")
    ax[1].imshow(loss, cmap="gray")
    ax[1].set_title("Neg log prob")
    ax[2].imshow((loss>0).astype(np.uint8)-(loss>100).astype(np.uint8), cmap="gray")
    ax[2].set_title("Thresholded Neg. log prob")
    plt.show()

if __name__=="__main__":
    gmm_loss_plot()