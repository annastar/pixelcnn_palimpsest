import tensorflow as tf
from pixelcnn_palimpsest.langevin_dynamics import prior
from pixelcnn_palimpsest.utils import plotting, losses
import numpy as np
from copy import deepcopy as dc
from pixelcnn_palimpsest.old.gmm_loss import gmm_loss

class ReconExpPar():
    def __init__(self,nb_ch, y_org, x_ot, log_dir, sess,
                 patch_size_ut, patch_size_bg, msi, with_bg,
                 mixing_fun_str, mean_bg,epochs,
                 momentum, nb_scope_aver_ut,nb_scope_aver_bg,
                 scale_rec_ut,scale_rec_bg, scale_prior_ut, scale_prior_bg,
                 scale_cor_ut,scale_cor_bg,step_mix,main_dir,
                 size_y,size_x,data_range,gmm_scaler,grad_order,cov_bg,
                 scale_ut_grad_by_max,scale_bg_grad_by_max,ut_refl,ot_refl,**kwargs):
        if "nb_repeats" in kwargs:
            self.nb_repeats = kwargs["nb_repeats"]
        else:
            self.nb_repeats = 1
        if "save_prior_grad" in kwargs:
            self.save_prior_grad = kwargs["save_prior_grad"]
        if "save_rec_grad" in kwargs:
            self.save_rec_grad = kwargs["save_rec_grad"]
        self.nb_ch = nb_ch
        self.patch_size_ut = patch_size_ut
        self.patch_size_bg = patch_size_bg
        self.assign_scale_ou_after = 10000
        self.mixing_fun_str = mixing_fun_str
        self.msi = msi
        self.with_bg = with_bg
        self.step_prior_ut = 0  # 1e-04
        self.step_cor_ut = 0  #
        self.step_rec_ut = 0
        self.step_prior_bg = 0  # 1e-04
        self.step_cor_bg = 0  #
        self.step_rec_bg = 0
        self.step_mix = step_mix
        self.epochs = epochs
        self.momentum = momentum
        self.main_dir = main_dir
        self.grad_order = grad_order
        self.nb_scope_aver_ut = nb_scope_aver_ut  # if 1 then no averaging
        self.nb_scope_aver_bg = nb_scope_aver_bg
        self.scale_rec_ut = scale_rec_ut  # if less then 0 do not do gradient step adjustment
        self.scale_rec_bg = scale_rec_bg
        self.scale_prior_ut = scale_prior_ut
        self.scale_prior_bg = scale_prior_bg
        self.scale_cor_ut = scale_cor_ut
        self.scale_cor_bg = scale_cor_bg
        self.data_range = data_range
        self.y_org = y_org
        self.xs_ot = x_ot
        self.log_dir = log_dir
        self.sess = sess
        self.kwargs = kwargs
        self.size_y = size_y
        self.size_x = size_x
        self.cur_iter = 0
        self.scale_ut_grad_by_max = scale_ut_grad_by_max
        self.scale_bg_grad_by_max = scale_bg_grad_by_max
        self.gmm_scaler = gmm_scaler
        self.gmm_scaler_pl = tf.placeholder(shape=(), dtype=tf.float32)
        self.tile_shape_ut = (self.size_y // self.patch_size_ut, self.size_x // self.patch_size_ut)
        self.batch_size_ut = np.prod(self.tile_shape_ut)
        ###compile mixing model
        self.mm = MixModel(self.size_y,self.size_x,self.nb_ch,self.mixing_fun_str,self.with_bg,
                            self.step_mix,self.data_range,self.gmm_scaler_pl,mean_bg,cov_bg,ut_refl,ot_refl)
        ###initialize mixing model
        sess.run(tf.initialize_all_variables())
        ###restore generator
        self.xs_prior_ut, self.ys_prior_ut, \
        self.grads_prior_ut_tf, self.loss_prior_ut_tf = self.comp_restore_gen(self.batch_size_ut,self.nb_repeats,"undertext")
        ###compile gradients
        self.grads_rec_ut_tf, self.loss_rec_ut_tf = self.grad_rec_loss(self.mm.xs_mix_ut)
        self.grads_cor_ut_tf, self.loss_cor_ut_tf = self.grad_cor_loss_ut()
        ###init image ut
        init = np.random.uniform(low=0.1, high=0.9, size=[self.batch_size_ut*self.nb_repeats,
                                                                        self.patch_size_ut, self.patch_size_ut, 1])
        self.__init_ims_ut = self.mask_overtext_with_rep(init,self.tile_shape_ut,self.patch_size_ut)

        self.__ut_shadow = dc(self.__init_ims_ut)
        ###init image bg
        # create empy image for storing image momemtum
        self.__prev_ims_ut_prior = np.zeros_like(self.__init_ims_ut)
        self.__prev_im_ut_rec = np.zeros([self.nb_repeats,self.size_y,self.size_x,1])
        self.__prev_im_ut_cor = np.zeros([self.nb_repeats,self.size_y,self.size_x,1])
        self.__prev_ut_prior_shadow = dc(self.__prev_ims_ut_prior)
        self.__prev_ut_rec_shadow = dc(self.__prev_im_ut_rec)
        self.__prev_ut_cor_shadow = dc(self.__prev_im_ut_cor)

        if with_bg:
            ###all the same for bg
            self.tile_shape_bg = (self.size_y // self.patch_size_bg, self.size_x // self.patch_size_bg)
            self.batch_size_bg = np.prod(self.tile_shape_bg)
            self.xs_prior_bg, self.ys_prior_bg, \
            self.grads_prior_bg_tf, self.loss_prior_bg_tf = self.comp_restore_gen(self.batch_size_bg,self.nb_repeats, "bg")
            self.grads_rec_bg_tf, self.loss_rec_bg_tf = self.grad_rec_loss(self.mm.xs_mix_bg)
            self.grads_cor_bg_tf, self.loss_cor_bg_tf = self.grad_cor_loss_bg()
            self.__init_ims_bg = np.random.uniform(low=-1.0, high=0.9,
                                                    size=[self.batch_size_bg * self.nb_repeats, self.patch_size_bg,
                                                          self.patch_size_bg, 1])
            self.__bg_shadow = dc(self.__init_ims_bg)
            self.__prev_im_bg_rec = np.zeros([self.nb_repeats,self.size_y,self.size_x,1])
            self.__prev_ims_bg_prior = np.zeros_like(self.__init_ims_bg)
            self.__prev_im_bg_cor = np.zeros([self.nb_repeats,self.size_y,self.size_x,1])
            self.__prev_bg_prior_shadow = dc(self.__prev_ims_bg_prior)
            self.__prev_bg_rec_shadow = dc(self.__prev_im_bg_rec)
            self.__prev_bg_cor_shadow = dc(self.__prev_im_bg_cor)

    def update_ut_after_prior(self,ims_ut):
        self.__prev_ims_ut_prior = dc(self.__init_ims_ut)
        self.__init_ims_ut = ims_ut

    def update_just_ut_ims(self,ims):
        self.__init_ims_ut = ims

    def update_just_bg_ims(self,ims):
        self.__init_ims_bg = ims

    def mask_overtext_with_rep(self,x,tile_shape,patch_size):
        x = self.stich_im_with_repeats(x, self.nb_repeats, self.size_y, self.size_x, tile_shape)
        for i in range(self.nb_repeats):
            x[i][self.xs_ot[0]!=0]=0
        x = self.split_im_with_repeats(x,self.nb_repeats,np.prod(tile_shape),patch_size)
        return x


    def split_im_with_repeats(self,im,nb_repeats,batch_size,patch_size):
        ims = np.zeros([nb_repeats * batch_size, patch_size, patch_size, 1])
        for nr in range(nb_repeats):
            ims[nr * batch_size:(1 + nr) * batch_size] = plotting.split_image(
                im[nr, :, :, 0], patch_size).reshape(-1, patch_size, patch_size, 1)
        return ims

    def update_just_ut_im(self,im):
        im = self.split_im_with_repeats(dc(im),self.nb_repeats, self.batch_size_ut, self.patch_size_ut)
        self.__init_ims_ut = im

    def update_just_bg_im(self,im):
        im = self.split_im_with_repeats(dc(im), self.nb_repeats, self.batch_size_bg, self.patch_size_bg)
        self.__init_ims_bg = im

    def update_bg_after_prior(self, ims_bg):
        self.__prev_ims_bg_prior = dc(self.__init_ims_bg)
        self.__init_ims_bg = ims_bg

    def update_ut_after_rec(self,im_ut,loss,step):
        self.__prev_im_ut_rec = self.stich_im_with_repeats(dc(self.__init_ims_ut),
                                                           self.nb_repeats,self.size_y,self.size_x,self.tile_shape_ut)
        self.__init_ims_ut = self.split_im_with_repeats(im_ut,self.nb_repeats, self.batch_size_ut,
                                                         self.patch_size_ut)
        self.loss_rec_ut = loss
        self.step_rec_ut = step

    def check_if_ut_updated(self):
        if np.sum(self.__ut_shadow-self.__init_ims_ut)!=0:
            print("UT im updated")
        self.__ut_shadow = self.get_ut_ims()

    def check_if_bg_updated(self):
        if np.sum(self.__bg_shadow-self.__init_ims_bg)!=0:
            print("BG im updated")
        self.__bg_shadow = self.get_bg_ims()

    def check_if_prev_ut_rec_updated(self):
        if np.sum(self.__prev_ut_rec_shadow-self.__prev_im_ut_rec)!=0:
            print("UT prev rec im updated")
        self.__prev_ut_rec_shadow = self.__prev_im_ut_rec

    def check_if_prev_ut_prior_updated(self):
        if np.sum(self.__prev_ut_prior_shadow-self.__prev_ims_ut_prior)!=0:
            print("UT prev prior im updated")
        self.__prev_ut_prior_shadow = self.__prev_ims_ut_prior

    def check_if_prev_ut_cor_updated(self):
        if np.sum(self.__prev_ut_cor_shadow-self.__prev_im_ut_cor)!=0:
            print("UT prev cor im updated")
        self.__prev_ut_cor_shadow = self.__prev_im_ut_cor

    def check_if_bg_updated(self):
        if np.sum(self.__bg_shadow-self.__init_ims_bg)!=0:
            print("BG im updated")
        self.__bg_shadow = self.get_bg_ims()

    def check_if_prev_bg_rec_updated(self):
        if np.sum(self.__prev_bg_rec_shadow - self.__prev_im_bg_rec) != 0:
            print("BG prev rec im updated")
        self.__prev_bg_rec_shadow = self.__prev_im_bg_rec

    def check_if_prev_bg_prior_updated(self):
        if np.sum(self.__prev_bg_prior_shadow - self.__prev_ims_bg_prior) != 0:
            print("BG prev prior im updated")
        self.__prev_bg_prior_shadow = self.__prev_ims_bg_prior

    def check_if_prev_bg_cor_updated(self):
        if np.sum(self.__prev_bg_cor_shadow - self.__prev_im_bg_cor) != 0:
            print("BG prev cor im updated")
        self.__prev_bg_cor_shadow = self.__prev_im_bg_cor

    def update_ut_after_cor(self,im_ut,loss,step):
        self.__prev_im_ut_cor = self.stich_im_with_repeats(dc(self.__init_ims_ut),
                                                           self.nb_repeats,self.size_y,self.size_x,self.tile_shape_ut)
        self.__init_ims_ut = self.split_im_with_repeats(im_ut,self.nb_repeats, self.batch_size_ut, self.patch_size_ut)
        self.loss_cor_ut = loss
        self.step_cor_ut = step

    def stich_im_with_repeats(self,ims,nb_repeats,size_y,size_x,tile_shape):
        im = np.zeros([nb_repeats, size_y, size_x, 1])
        batch_size = np.prod(tile_shape)
        for nr in range(self.nb_repeats):
            one_im = ims[nr * batch_size:(nr + 1) * batch_size]
            im[nr] = plotting.img_tile(one_im, border=0, stretch=False,
                                                       tile_shape=tile_shape)
        return im

    def update_bg_after_cor(self,im_bg,loss,step):
        self.__prev_im_bg_cor = self.stich_im_with_repeats(dc(self.__init_ims_bg),self.nb_repeats,self.size_y,self.size_x,self.tile_shape_bg)
        self.__init_ims_bg = self.split_im_with_repeats(im_bg,
                                    self.nb_repeats,self.batch_size_bg,self.patch_size_bg)
        self.loss_cor_bg = loss
        self.step_cor_bg = step


    def update_bg_after_rec(self,im_bg, loss,step):
        self.__prev_im_bg_rec = self.stich_im_with_repeats(dc(self.__init_ims_bg),
                                    self.nb_repeats,self.size_y,self.size_x,self.tile_shape_bg)
        self.__init_ims_bg = self.split_im_with_repeats(im_bg,
                                    self.nb_repeats,self.batch_size_bg,self.patch_size_bg)
        self.loss_rec_bg = loss
        self.step_rec_bg = step

    def get_ut_ims(self):
        return dc(self.__init_ims_ut)

    def get_prev_ut_ims_prior(self):
        return dc(self.__prev_ims_ut_prior)

    def get_prev_ut_ims_rec(self):
        return dc(self.__prev_im_ut_rec)

    def get_prev_ut_ims_cor(self):
        return dc(self.__prev_im_ut_cor)

    def get_prev_bg_ims_cor(self):
        return dc(self.__prev_im_bg_cor)

    def get_prev_bg_ims_prior(self):
        return dc(self.__prev_ims_bg_prior)

    def get_prev_bg_ims_rec(self):
        return dc(self.__prev_im_bg_rec)

    def get_ut_im(self):
        img_ut = dc(self.stich_im_with_repeats(self.__init_ims_ut,
                                            self.nb_repeats, self.size_y, self.size_x,
                                            self.tile_shape_ut))
        return img_ut

    def get_bg_ims(self):
        return dc(self.__init_ims_bg)

    def get_bg_im(self):
        img_bg = dc(self.stich_im_with_repeats(self.__init_ims_bg,
                                   self.nb_repeats,self.size_y,self.size_x,
                                            self.tile_shape_bg))
        return img_bg

    def grad_cor_loss_ut(self):
        grads, loss = prior.get_gradients_cor_2val_loss(self.mm.xs_mix_ut, self.mm.xs_mix_ot)
        return grads, loss

    def grad_cor_loss_bg(self):
        grads, loss = prior.get_gradients_cor_3val_loss(self.mm.xs_mix_bg, self.mm.xs_mix_ot, self.mm.xs_mix_ut)
        return grads, loss

    def comp_restore_gen(self,batch_size,nb_repeats,mode):
        xs, ys, grads, loss = prior.compile_restore_gen(self.main_dir, batch_size * nb_repeats, mode,
                                                        self.sess)
        return xs,ys,grads,loss

    def grad_rec_loss(self,xs):
        grads_rec, loss_rec = prior.get_gradients_rec_loss(self.mm.ys_mix, self.mm.ys_org_tf,
                                                           xs, self.data_range)
        return grads_rec, loss_rec

class MixModel():
    def __init__(self,nb_rows,nb_cols,nb_ch,mixing_fun_str,with_bg,step_mix,data_range,gmm_scaler_pl,mean_bg,cov_bg,ut_refl,ot_refl,**kwargs):
        shape = [None, nb_rows, nb_cols, 1]
        self.xs_mix_ut = tf.placeholder(shape=shape, dtype=tf.float32)
        self.xs_mix_ot = tf.placeholder(shape=shape, dtype=tf.float32)
        if with_bg:
            self.xs_mix_bg = tf.placeholder(shape=shape,dtype=tf.float32)
        else:
            self.xs_mix_bg = None
        shape = [None, nb_rows, nb_cols, nb_ch]
        self.ys_org_tf = tf.placeholder(shape=shape, dtype=tf.float32)
        self.nb_ch = nb_ch
        self.mixing_fun_str = mixing_fun_str
        self.step_mix = step_mix
        self.data_range = data_range
        self.gmm_scaler_pl = gmm_scaler_pl
        self.mean_bg = mean_bg
        self.cov_bg = cov_bg
        self.ot_refl = ot_refl
        self.ut_refl = ut_refl
        self.compile_mixing_model()


    def compile_mixing_model(self):

        mixing_fun = eval("mix_model."+self.mixing_fun_str)
        y_pred,D_b = mixing_fun(undertext=self.xs_mix_ut,background=self.xs_mix_bg,
                                overtext=self.xs_mix_ot,nb_ch=self.nb_ch,bg_refl=self.mean_bg,
                                ut_refl=self.ut_refl,ot_refl=self.ot_refl)

        if int(self.ys_org_tf.get_shape()[-1])>1:
            loss = losses.weighted_l2_loss(self.ys_org_tf, y_pred, self.data_range, True)
            if not D_b is None:
                self.gmm_loss_val = gmm_loss(D_b,self.nb_ch,self.mean_bg,self.cov_bg,True)
                loss = loss + self.gmm_scaler_pl*self.gmm_loss_val
        else:
            loss = losses.l2_loss(self.ys_org_tf, y_pred, True)
        if "fixed" in self.mixing_fun_str:
            optim = None
        else:
            optim = tf.train.AdamOptimizer(self.step_mix).minimize(loss, var_list=tf.get_collection(
                tf.GraphKeys.GLOBAL_VARIABLES, scope="MixingNet"))

        self.D_b = D_b
        self.opt_mixnet = optim
        self.loss_mixnet = loss
        self.ys_mix = y_pred