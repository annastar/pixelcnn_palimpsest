#!/bin/bash
TIMEFOLDER=$(date +"%Y-%m-%d-%H-%M")
OLDTIMEFOLDER="2022-05-27-18-43"
DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_ut_dataset_50step_192x192'
DATADIR_BG_AUG='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_bg_dataset_strech_contr_step_64_size_192'
BINARY_TRUE=1
if [ ${BINARY_TRUE} == 1 ]
  then
    MODE='bin'
else
  MODE='gray'
fi
DATASET='greek960_ut'
#-re 120
EPOCH_STEP=100
FREQ_SAVE=10
LR=0.0004
NOISE_LVLs=(0.0)
BG_AUG_PER_AREA=5.0
PREV_NOISE_LVL=0.0
RE_EPOCH=12
for i in ${!NOISE_LVLs[@]}
do
NOISE_LVL=${NOISE_LVLs[${i}]}
echo "Noise level: ${NOISE_LVL}"
  if [ ${NOISE_LVL} != 0.0 ]
  then
    EPOCH_STEP=100
    LR=0.0004
    FREQ_SAVE=20
  fi
  echo "Run for ${EPOCH_STEP} steps"
  SAVEDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/undertext/pixelcnn-${MODE}-${TIMEFOLDER}/noise_${NOISE_LVL}"
  RESTDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/undertext/pixelcnn-${MODE}-${OLDTIMEFOLDER}_current/noise_${PREV_NOISE_LVL}"

  if [ ${BINARY_TRUE} == 1 ]
  then
    echo "Process binary data"
    python train_ut_bg_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -n 64 --nr_gpu 1 -b 8 -t ${FREQ_SAVE} -q 5 -m 2 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH}\
    -sl ${NOISE_LVL} -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_BG_AUG} -bn -perth ${BG_AUG_PER_AREA} -pw 15.0
  else
    echo "Process grayscale data"
    python train_ut_bg_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -n 64 --nr_gpu 1 -b 8 -t ${FREQ_SAVE} -q 5 -m 2 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH}\
    -sl ${NOISE_LVL} -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_BG_AUG} -perth ${BG_AUG_PER_AREA}
  fi
  if [ ${NOISE_LVL} == 0.0 ]
  then
    RE_EPOCH=$((RE_EPOCH+EPOCH_STEP))
  else
    RE_EPOCH=$((EPOCH_STEP))
    OLDTIMEFOLDER=TIMEFOLDER
  fi
PREV_NOISE_LVL=$NOISE_LVL
done