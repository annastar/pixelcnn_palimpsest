#!/bin/bash
TIMEFOLDER="2022-12-23-19-08"
#$(date +"%Y-%m-%d-%H-%M")
DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_bg_dataset_strech_contr_step_64_size_192'
DATADIR_TEXT_AUG='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_ut_dataset_50step_192x192'
DATASET='greek960_bg'
EPOCH_STEP=40
LR=0.001
NOISE_LVLs=(1.2 1.4 1.6)
PREV_NOISE_LVL=1.0
RE_EPOCH=480
for i in ${!NOISE_LVLs[@]}
do
NOISE_LVL=${NOISE_LVLs[${i}]}
echo "Noise level: ${NOISE_LVL}"
  if [ ${NOISE_LVL} != 0 ]
  then
    EPOCH_STEP=40
    LR=0.0004
  fi
echo "Run for ${EPOCH_STEP} steps"
SAVEDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/background/pixelcnn-gray-${TIMEFOLDER}/noise_${NOISE_LVL}"
RESTDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/background/pixelcnn-gray-${TIMEFOLDER}/noise_${PREV_NOISE_LVL}"
python train_bg_text_aug.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -aug -n 32 --nr_gpu 2 -b 8 -t 5 -q 3 -m 3 -dv "gpu" -x ${EPOCH_STEP} -re ${RE_EPOCH} -sl ${NOISE_LVL} -l ${LR} -ro ${RESTDIR} -i_at ${DATADIR_TEXT_AUG}
RE_EPOCH=$((RE_EPOCH+EPOCH_STEP))
PREV_NOISE_LVL=$NOISE_LVL
done