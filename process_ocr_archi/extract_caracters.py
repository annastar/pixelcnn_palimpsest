import os
import shutil

main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\1band"
new_dir = os.path.join(main_dir,"final")
for _char in os.listdir(main_dir):
    if not "lower" in _char:
        continue
    for char_idx in os.listdir(os.path.join(main_dir,_char)):
        try:
            src = os.path.join(main_dir,_char,char_idx,"ut","109999.png")
            dst = os.path.join(new_dir,_char,char_idx+".png")
            if not os.path.exists(os.path.join(new_dir,_char)):
                os.makedirs(os.path.join(new_dir,_char))
            shutil.copyfile(src, dst)
        except:
            print("No such file")
