#!/bin/bash -l
# Declaring Slurm Configuration Options
#SBATCH --comment="the bg prior trained on Greek960"
#SBATCH --job-name=char
#SBATCH --account=palim
#SBATCH --partition=debug
#SBATCH --time=12:30:00
#SBATCH --output=logs/%x_%j.out
#SBATCH --error=logs/%x_%j.err
#SBATCH --nodes=1
#SBATCH --gres=gpu:a100:1
#SBATCH --cpus-per-task=36
#SBATCH --mem=10g

# Loading Software/Libraries
spack env activate tf114-23040301

# Running Code
export PYTHONPATH="/home/as3297/bss_autoreg_palimpsest"
python3 -u process_one_char.py "rc" "lower_alpha/obscured_alpha_lower_148x148_rgb_29"