#!/bin/bash -l
# Declaring Slurm Configuration Options
#SBATCH --comment="the bg prior trained on Greek960"
#SBATCH --job-name=char
#SBATCH --account=palim
#SBATCH --partition=tier3
#SBATCH --time=12:00:00
#SBATCH --output=logs/%x_%j.out
#SBATCH --error=logs/%x_%j.err
#SBATCH --nodes=1
#SBATCH --gres=gpu:a100:1
#SBATCH --cpus-per-task=18
#SBATCH --mem=10g
#SBATCH --array=0-160
FILEPATH="/home/as3297/bss_autoreg_palimpsest/training/langevin/archi_chars/archi_chars.txt"
CHARNAMEs=(`cat "$FILEPATH"`)
# Loading Software/Libraries
spack env activate tf114-23040301
# Running Code
export PYTHONPATH="/home/as3297/bss_autoreg_palimpsest"
python3 -u process_one_char.py "rc" ${CHARNAMEs[$SLURM_ARRAY_TASK_ID]} "bg_witouthaug"