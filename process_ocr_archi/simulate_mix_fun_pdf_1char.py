import os
from pixelcnn_palimpsest.langevin_dynamics import simulate_mix_fun_pdf as smf
from pixelcnn_palimpsest.langevin_dynamics.data import PrepArchiChar

main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
dirpath = os.path.join(main_dir,"datasets","Archimedes","test_obscured_MSI")
mix_fun_str = "sep_layers_bg_not_fn"
msi = True
msi_partial = True
pal_name = "archi_chars"
ut_model = "binary"
org_noise = 1e-2
for letter in os.listdir(dirpath):
    if ".txt" in letter:
        continue
    for sample in os.listdir(os.path.join(dirpath,letter)):
        char_dir = os.path.join(dirpath,letter,sample)
        data = PrepArchiChar(char_dir, mix_fun_str, msi, msi_partial)
        do, du, db = data.D_o.tolist(), data.D_u.tolist(), data.D_bg.tolist()
        smf.calc_save_std_mvn(pal_name,du,do,db,"xu",mix_fun_str,org_noise = org_noise,ut_model=ut_model)
        smf.calc_save_std_mvn(pal_name, du,do,db,"xb",mix_fun_str,org_noise = org_noise,ut_model=ut_model)