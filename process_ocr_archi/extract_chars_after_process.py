import os
import shutil
from pixelcnn_palimpsest.process_ocr_archi.process_one_char import erase_char_from_list

def extract_final_char_im():
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\rc\1band"
    new_dir = os.path.join(main_dir,"final")
    for _char in os.listdir(main_dir):
        if not "lower" in _char:
            continue
        for char_idx in os.listdir(os.path.join(main_dir,_char)):
            try:
                src = os.path.join(main_dir,_char,char_idx,"ut","109999.png")
                dst = os.path.join(new_dir,_char,char_idx+".png")
                if not os.path.exists(os.path.join(new_dir,_char)):
                    os.makedirs(os.path.join(new_dir,_char))
                shutil.copyfile(src, dst)
            except:
                print("No such file")

def delete_finished_chars_from_list():
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\rc"
    list_path = os.path.join(main_dir,"archi_chars.txt")
    main_dir = os.path.join(main_dir,"1band")
    for _char in os.listdir(main_dir):
        if not "lower" in _char:
            continue
        for char_idx in os.listdir(os.path.join(main_dir,_char)):
            try:
                src = os.path.join(main_dir,_char,char_idx,"ut","109999.png")
                if os.path.exists(src):
                    erase_char_from_list(list_path,_char+"/"+char_idx,list_path)

            except:
                print("No such file")


if __name__=="__main__":
    extract_final_char_im()