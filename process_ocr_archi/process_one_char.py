import platform
import os
import time
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
import json
import sys
from pixelcnn_palimpsest.langevin_dynamics.tf_vars import Model2Var
from pixelcnn_palimpsest.langevin_dynamics import restoration_2layers as rpost
from pixelcnn_palimpsest.langevin_dynamics.io_var import InOutVals
from pixelcnn_palimpsest.process_ocr_archi.exp_par import LogPar
from pixelcnn_palimpsest.langevin_dynamics import continue_restoration


def restore_im_2layers(main_dir,device,nb_epoch,with_bg,
               exp_name,opt_mixpar,pal_name,fid_loss,
              mix_fun_str,noise_org,msi, msi_partial,
                       restore_dir,restore_epoch,char_dir,main_log_dir,bg_chpoint_dir,**kwargs):
    # set experiment parameters
    with_ut = True
    restore_only_bg = False
    if restore_epoch > 0:
        par = continue_restoration.LogPar(main_dir,device,restore_dir, restore_epoch)
    else:
        par = LogPar(main_dir,nb_epoch,with_ut,with_bg,
                  opt_mixpar,pal_name,None,None,None,
                    device,fid_loss,
                 mix_fun_str,noise_org,msi, msi_partial,restore_only_bg,data_dir=char_dir,bg_chpoint_dir=bg_chpoint_dir)
    print("Created log parameters")
    # create log dirs and exp parameters
    log_dirs = rpost.LogDirs(main_dir,main_log_dir,exp_name,par,False)
    print("Created log dirs")
    ###reset graph
    tf.compat.v1.reset_default_graph()
    print("Reset graph")
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=True))
    print("Created session")
    tf.set_random_seed(1234)

    ###create image layers
    ior = InOutVals(main_dir,par.mix_fun_str,par.noise_level,fid_loss,with_bg,pal_name) # inverse model inputs/outputs
    print("Read files with phi lists")
    rpost.create_im_layers(par,ior)
    print("Create image initializations")
    ior.read_phi(par.Du,par.Db,par)
    print("Read phi")
    # store experiment parameters into json file
    with open(os.path.join(log_dirs.expdir, 'par.json'), 'w') as f:
        json.dump(par.__dict__, f)
    print("store json experiment parameters file")

    #create wrapper class with model variables and placeholders prior models
    m = Model2Var(par,sess,ior.batch_size)
    print("compile model")
    # create original palimpsest if it is artificial
    if par.pal_name == "art_pal":
        rpost.create_art_palimpsest(m, par, ior, sess)
    if restore_epoch>0:
        continue_restoration.restore_res_as_initial(main_dir,par, ior, restore_dir, restore_epoch)
    else:
        ### save init and org images before reconstruction
        rpost.save_init_img(log_dirs.expdir, par, ior)
    print("starting optimization")
    rpost.ld_optimization(sess,par,"posterior",m,ior,log_dirs)
    sess.close()


def read_list(fpath):
    lines = []
    with open(fpath,"r") as f:
        for line in f:
            lines.append(line.rstrip())
    return lines
def write_list(fpath,lines):
    with open(fpath,"w") as f:
        for line in lines:
            f.write(line+"\n")


def erase_char_from_list(fpath,finished_char):
    lines = read_list(fpath)
    try:
        lines.remove(finished_char)
        write_list(fpath,lines)
    except:
        print("The element {} not in the list".format(finished_char))

def is_char_in_list(fpath,new_char):
    lines = read_list(fpath)
    present = False
    for line in lines:
        if new_char==line:
            present = True
        if present:
            break
    return present


def process_1_char(main_dir,char_dir,device,bg_chpoint_dir):
    st_time = time.time()
    msi = False
    restore_epoch = 0
    restore_dir = ""
    nb_epoch = 10000
    noise_org = 1e-2
    opt_mixpar = False
    with_bg = True
    if msi:
        msi_partial = True
    else:
        msi_partial = False
    fid_loss = "uvn"
    pal_name = "archi_chars"  # "art_pal"#"art_pal"#"archi"#
    char_name = char_dir.split(os.sep)[-2]
    char_name_idx = char_dir.split(os.sep)[-1]
    mix_fun_str = "sep_layers_bg_not_fn"
    #exp_name = os.path.join(pal_name,"msi" if msi else "1band",char_name,char_name_idx)
    exp_name = os.path.join(pal_name, "msi" if msi else "1band"+"_no_bg_aug", char_name, char_name_idx)
    main_log_dir = os.path.join(main_dir,"training","langevin")
    restore_im_2layers(main_dir, device, nb_epoch=nb_epoch,
                                       with_bg=with_bg,
                                       exp_name=exp_name,
                                       opt_mixpar=opt_mixpar,
                                       pal_name=pal_name,
                                       Du=None, Db=None, Do=None,
                                       fid_loss=fid_loss,
                                       mix_fun_str=mix_fun_str,
                                       noise_org=noise_org,
                                       msi=msi, msi_partial=msi_partial,
                                       restore_epoch=restore_epoch, restore_dir=restore_dir,
                                       char_dir=char_dir,main_log_dir=main_log_dir,bg_chpoint_dir=bg_chpoint_dir)
    print("Processing time",time.time()-st_time)



if __name__=="__main__":
    if len(sys.argv)>1:
        if sys.argv[1]=="cis":
            main_dir = r"/cis/phd/as3297/projects/bss_autoreg_palimpsest"
            device="gpu:0"
            char_dir = os.path.join(main_dir, r"datasets", r"Archimedes", r"test_obscured_MSI", r"lower_mu",
                                    r"obscured_mu_lower_64x64_rgb_28")
        elif sys.argv[1]=="rc":
            device = "gpu:0"
            main_dir = r"/home/as3297/bss_autoreg_palimpsest"
            print("sys.argv[2]",sys.argv[2])
            char_dir = os.path.join(main_dir, r"datasets", r"Archimedes", r"test_obscured_MSI",sys.argv[2])
        if sys.argv[3]=="bg_withaug":
            bg_dir = "pixelcnn-gray-2023-04-04-13-00_with_text_aug"
        elif sys.argv[3]=="bg_witouthaug":
            bg_dir = r"pixelcnn-gray-2023-05-31-01-51_without_text_aug"
        else:
            raise IOError("No such option for bg prior model checkpoint as {}".format(sys.argv[3]))
        bg_chpoint_dir = os.path.join(main_dir, "training", "generators", "background",
                                            "text_aug_exp_Archi",
                                            bg_dir)
    else:
        main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
        device = "cpu:0"
        char_dir = os.path.join(main_dir, r"datasets", r"Archimedes", r"test_obscured_MSI", r"lower_mu",
                                r"obscured_mu_lower_64x64_rgb_28")
        bg_dir = "pixelcnn-gray-2023-04-04-13-00_with_text_aug"
        bg_ckpoint_dir = os.path.join(main_dir, "training", "generators", "background",
                                      "text_aug_exp_Archi",
                                      bg_dir)
    process_1_char(main_dir, char_dir,device,bg_chpoint_dir)


