import os
from skimage import io,transform

def dict_band_name_sufx():
    full_band_names_fpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\017r-016v_Arch07r_Sinar\normalized\band_names.txt"
    d = {}
    with open(full_band_names_fpath,"r") as f:
        lines = f.readlines()
        # Strips the newline character
        for line in lines:
            fname = line.strip()[:-6]
            sufx = line.strip()[-6:]
            d[fname+".tif"] = sufx
            print("Line {}: {}".format(fname,sufx))
    return d

def resave_ut_files(d):
    dirpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\test_obscured_MSI"
    new_dirpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\test_obscured_MSI_new"
    for letter in os.listdir(dirpath):
        if ".txt" in letter:
            continue
        for sample in os.listdir(os.path.join(dirpath, letter)):
            old_data_dir = os.path.join(dirpath, letter, sample)
            new_data_dir = os.path.join(new_dirpath, letter, sample)
            if not os.path.exists(new_data_dir):
                os.makedirs(new_data_dir)
            for imf in os.listdir(old_data_dir):
                im = io.imread(os.path.join(old_data_dir,imf), as_gray=True)

                io.imsave(os.path.join(new_data_dir,imf[:-4]+d[imf]+".tif"),im)

def rescale_files():
    dirpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\test_obscured_MSI_new"
    for letter in os.listdir(dirpath):
        if ".txt" in letter:
            continue
        for sample in os.listdir(os.path.join(dirpath, letter)):
            old_data_dir = os.path.join(dirpath, letter, sample)
            for imf in os.listdir(old_data_dir):
                im = io.imread(os.path.join(old_data_dir,imf), as_gray=True)
                im = transform.resize(im, (192, 192), preserve_range=True)
                io.imsave(os.path.join(old_data_dir,imf),im)

def move_ot_files():
    dirpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\test_obscured_MSI_new"
    ot_dirpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\test_obscured_MSI_thresholded"
    for letter in os.listdir(dirpath):
        for sample in os.listdir(os.path.join(dirpath,letter)):
            new_data_dir = os.path.join(dirpath,letter,sample)
            old_data_fpath = os.path.join(ot_dirpath,letter,sample+".bmp")
            im = io.imread(old_data_fpath,as_gray=True)
            io.imsave(os.path.join(new_data_dir,"overtext_map.png"),im)

def create_charnames_filelist():
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    dirpath = os.path.join(main_dir, "datasets", "Archimedes", "test_obscured_MSI")
    fpath = os.path.join(main_dir,"training","langevin","archi_chars","rc","archi_chars.txt")
    with open(fpath, "w") as f:
        for letter in os.listdir(dirpath):
            if ".txt" in letter:
                continue
            for sample in os.listdir(os.path.join(dirpath, letter)):
                f.write(letter +"/"+ sample + "\n")

if __name__=="__main__":
    #d = dict_band_name_sufx()
    #resave_ut_files(d)
    #move_ot_files()
    #rescale_files()
    create_charnames_filelist()




