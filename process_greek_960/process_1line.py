import os
import platform
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
    from tf_slim import add_arg_scope
else:
    import tensorflow as tf
    from tensorflow.contrib.framework.python.ops import add_arg_scope
import json
import sys
from pixelcnn_palimpsest.langevin_dynamics.tf_vars import Model2Var
from pixelcnn_palimpsest.langevin_dynamics import restoration_2layers as rpost
from pixelcnn_palimpsest.langevin_dynamics.io_var import InOutVals
from pixelcnn_palimpsest.process_greek_960.exp_par import LogPar
from pixelcnn_palimpsest.langevin_dynamics import  continue_restoration

def restore_im_2layers(main_dir,device,nb_epoch,with_bg,
               exp_name,opt_mixpar,pal_name,Du,Db,Do,fid_loss,
               chpoint_bg_path,mix_fun_str,noise_org,msi, msi_partial,restore_dir,restore_epoch,main_log_dir,**kwargs):
    # set experiment parameters
    with_ut = True
    restore_only_bg = False
    if restore_epoch > 0:
        par = continue_restoration.LogPar(main_dir,device,restore_dir, restore_epoch)
    else:
        par = LogPar(main_dir,nb_epoch,with_ut,with_bg,
                  opt_mixpar,pal_name,
                  Du,Db,Do,device,fid_loss,chpoint_bg_path,
                 mix_fun_str,noise_org,msi, msi_partial,restore_only_bg)
    # create log dirs and exp parameters
    log_dirs = rpost.LogDirs(main_dir,main_log_dir,exp_name,par)

    ###reset graph
    tf.compat.v1.reset_default_graph()
    sess = tf.Session()
    tf.set_random_seed(1234)

    ###create image layers
    ior = InOutVals(main_dir,par.mix_fun_str,par.noise_level,fid_loss,with_bg,pal_name) # inverse model inputs/outputs
    rpost.create_im_layers(par,ior)
    ior.read_phi(par.Du,par.Db,par)
    # store experiment parameters into json file
    with open(os.path.join(log_dirs.expdir, 'par.json'), 'w') as f:
        json.dump(par.__dict__, f)

    #create wrapper class with model variables and placeholders prior models
    m = Model2Var(par,sess,ior.batch_size)
    # create original palimpsest if it is artificial
    if par.pal_name == "art_pal":
        rpost.create_art_palimpsest(m, par, ior, sess)

    if restore_epoch>0:
        continue_restoration.restore_res_as_initial(main_dir,par, ior, restore_dir, restore_epoch)
    else:
        ### save init and org images before reconstruction
        rpost.save_init_img(log_dirs.expdir, par, ior)
    rpost.ld_optimization(sess,par,"posterior",m,ior,log_dirs)
    sess.close()

if __name__=="__main__":
    if len(sys.argv)>1:
        if sys.argv[1]=="cis":
            main_dir = r"/cis/phd/as3297/projects/bss_autoreg_palimpsest"
        elif sys.argv[1]=="rc":
            main_dir = r"/home/as3297/bss_autoreg_palimpsest"
        device="gpu:0"
        print("sys.argv[2]", sys.argv[2])
        print("sys.argv[3]", sys.argv[3])
        if sys.argv[2] == "msi":
            msi = True
        elif sys.argv[2] == "1band":
            msi = False
        else:
            print("No such option as {}".format(sys.argv[2]))
        expname = sys.argv[3]
    else:
        main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
        device = "cpu:0"

    main_log_dir = os.path.join(main_dir,"training","langevin")
    restore_epoch = 0
    restore_dir = None
    nb_epoch = 10000
    noise_org = 1e-2
    opt_mixpar = False
    with_bg = True
    fid_loss = "uvn"
    chpoint_bg_path = "pixelcnn-gray-2022-12-23-19-08_with_text_aug"
    if msi==False:
        fid_loss = "uvn"
        msi_partial = False
    else:
        msi_partial = True
    pal_name = "greek960"#"archi_testchar"#"art_pal"#"art_pal"#"archi"#
    mix_fun_str = "sep_layers_bg_not_fn"
    exp_name = os.path.join(pal_name,expname)
    restore_im_2layers(main_dir, device, nb_epoch=nb_epoch,
                   with_bg=with_bg,
                   exp_name=exp_name,
                   opt_mixpar=opt_mixpar,
                   pal_name=pal_name,
                   Du=None, Db=None, Do=None,
                   fid_loss=fid_loss,
                   chpoint_bg_path=chpoint_bg_path,
                   mix_fun_str=mix_fun_str,
                   noise_org = noise_org,
                             msi = msi, msi_partial = msi_partial,
                       restore_epoch=restore_epoch, restore_dir=restore_dir,main_log_dir=main_log_dir)
