import os

class LogPar():
    def __init__(self,main_dir,nb_epoch,with_ut,with_bg,
                opt_mixpar,pal_name,Du,Db,Do,device,fid_loss,
                 chpoint_bg_path,mix_fun_str,org_noise,msi,msi_partial,restore_only_bg,data_dir=None):
        self.device = device
        self.ut_model = "binary"
        self.restore_epoch = None
        self.sigma_idx = 0
        self.pal_name = pal_name #use artificial palimpsest
        self.mix_fun_str = mix_fun_str#"sep_layers_bg_ind_from_ot_not_fn"#"sep_layers_bg_fun_sigmoid"
        self.change_sigma = False
        self.optimize_mix_parameters = opt_mixpar
        self.nb_epoch = 10000
        self.with_bg = with_bg
        self.with_ut = with_ut
        self.start_epoch = 0
        if with_bg and with_ut:
            self.warm_up = True
        else:
            self.warm_up = False
        self.record_loss_freq = nb_epoch
        self.record_imgs_freq = nb_epoch
        self.im_size = 64
        if self.ut_model == "binary":
            self.bg_repeats = 4
        elif self.ut_model == "discrete":
            self.bg_repeats = 1
            self.warm_up = False
        else:
            raise ValueError("No such ut model mode as {}".format(self.ut_model))
        self.fid_loss = fid_loss
        self.delta_ut = 1.0*1e-6#2*1e-05 #parameters accordind to Jayaram, Vivek, and John Thickstun.
                        # Source separation with deep generative priors. PMLR, 2020.
        self.delta_bg = 1.0 * 1e-6
        if self.pal_name=="art_pal":
            self.Do = Do#0.6#
            self.Du = Du
            self.Db = Db
            if type(Du) is list or type(Db) is list:
                self.nb_ch = len(Du)
                if len(Du) != len(Db):
                    raise IOError("Du and Db should have the same length")
                if len(Du) != len(self.Do):
                    raise IOError("Du and Do should have the same length")
            else:
                self.nb_ch = 1
            self.noise_level = org_noise
            self.data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
            self.data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
            self.fname_ot = r"line_4_moved.png"
            self.fname_ut = r"line_27.png"
            self.row_coord = [0, 192]
            self.col_coord = [0, 192 * 4]
        elif self.pal_name=="greek960":
            self.msi = msi
            self.msi_partial = msi_partial
            if self.msi:
                self.nb_ch = 21
                if self.msi_partial:
                    self.nb_ch = 3
            else:
                self.nb_ch = 1
            self.noise_level = org_noise
            self.data_dir = os.path.join(main_dir,"datasets",r"Greek_960",r"0086_000084")
            self.fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
            if restore_only_bg:
                self.fname_ot = r"0086_000084+MB625Rd_007_F_thresh_erode_both_ut_ot.png"
            self.row_coord = [2300, 2300+192]
            self.col_coord = [1800, 2914]
            self.rot_angle = -90
            self.convert_opt_dens = False
            self.palimpsest_fname = "0086_000084+WBRBG58_018_F.tif"
        else:
            raise IOError("No such option as",self.pal_name)
        if self.ut_model=="discrete":
            self.chpoint_ut_path = os.path.join(main_dir,"training","generators","undertext",
                                                "pixelcnn-gray-2023-02-08-04-13")
        elif self.ut_model=="binary":
            self.chpoint_ut_path = os.path.join(main_dir, "training", "generators", "undertext",
                                                "pixelcnn-bin-2022-05-27-18-43_current")
        else:
            raise ValueError("No such ut model mode as {}".format(self.ut_model))
        if self.ut_model == "binary":
            self.rest_epoch_sigma_ut = {1.0: 30, 0.01: 8, 0.1: 15, 0.2: 18, 0.3: 24, 0.4: 34,
                                   0.5: 48, 0.6: 64, 0.7: 80, 0.8: 100, 0.9: 120}
            self.sigma_list_ut = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        elif self.ut_model == "discrete":
            self.rest_epoch_sigma_ut = {1.6: 80, 1.4: 100, 1.2: 100, 1.0: 100, 0.01: 100, 0.1: 100, 0.2: 100, 0.3: 100,
                                   0.4: 100,
                                   0.5: 100, 0.6: 100, 0.7: 100, 0.8: 100, 0.9: 100}
            self.sigma_list_ut = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,1.2,1.4,1.6]
        if with_bg:
            self.chpoint_bg_path = os.path.join(main_dir, "training", "generators", "background", "text_aug_exp_Greek960",
                                                chpoint_bg_path)  #
            if self.ut_model == "binary":
                self.sigma_list_bg = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,1.0]
            elif self.ut_model == "discrete":
                self.sigma_list_ut = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6]
            self.rest_epoch_sigma_bg = {0: 40, 0.01: 80, 0.1: 120, 0.2: 160,
                                   0.3: 200, 0.4: 240, 0.5: 280, 0.6: 320,
                                   0.7: 360, 0.8: 400, 0.9: 440, 1.0: 480, 1.2: 520, 1.4: 560, 1.6: 600}