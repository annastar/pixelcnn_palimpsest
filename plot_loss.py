from matplotlib import pyplot as plt
import numpy as np
import os

logdir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2023-04-11-17-45\noise_0.0"
mode = "ut"


test_loss = np.load(os.path.join(logdir,"test_bpd_greek960_{}.npz".format(mode)))
train_loss = np.load(os.path.join(logdir,"train_bpd_greek960_{}.npz".format(mode)))

for idx,loss in enumerate(test_loss['test_bpd']):
    print("epoch {},train loss {},test loss {}".format(idx,train_loss["train_bpd"][idx],loss))

plt.figure()
a=test_loss['test_bpd']
plt.plot(test_loss['test_bpd'],label="test")
plt.plot(train_loss["train_bpd"],label="train")
plt.legend()
plt.savefig(os.path.join(logdir,"loss_plot.png"))
plt.grid()
plt.show()