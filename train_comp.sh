#!/bin/bash
TIMEFOLDER=$(date +"%Y-%m-%d-%H-%M")
DATADIR="c:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_dataset_50step_192x192"
SAVEDIR="c:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-${TIMEFOLDER}"
DATASET='greek960'
python train.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -m 2 --nr_gpu 1 -b 2 -bn -aug -dv "cpu"