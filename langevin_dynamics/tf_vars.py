import platform
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
import numpy as np
from pixelcnn_palimpsest.langevin_dynamics import mix_model
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_pixelcnn_ut_model_with_par,\
                                                            compile_pixelcnn_bg_model_with_par
###do not delete line below
from pixelcnn_palimpsest.langevin_dynamics.posterior import get_grads_fidelity_smoothed_apprx_1band,\
    get_grads_fidelity_smoothed_apprx_msi, get_grads_prior_log_loss_smoothed,get_grads_prior_sigmoid_loss_smoothed


class Model2Var():
    """TF model and variables for two layers"""
    def __init__(self,par,sess,batch_size):
        ###compile prior models
        self.batch_size = batch_size
        self.ut_model = par.ut_model
        if self.ut_model!="discrete" and self.ut_model!="binary":
            raise ValueError("No such ut model mode as {}".format(par.ut_model))
        if par.with_ut:
            self.xs_prior_ut_tf, self.l_prior_ut_tf, _ = compile_pixelcnn_ut_model_with_par(self.batch_size,par.ut_model)
            if self.ut_model=="discrete":
                self.xs_ut_tf = (self.xs_prior_ut_tf+1)/2
            elif self.ut_model=="binary":
                self.xs_ut_tf = self.xs_prior_ut_tf
        else:
            self.xs_prior_ut_tf = None
            self.xs_ut_tf = None

        if par.with_bg:
            self.xs_prior_bg_tf, self.l_prior_bg_tf, _ = compile_pixelcnn_bg_model_with_par(self.batch_size)
            self.xs_bg_tf = (self.xs_prior_bg_tf + 1) / 2
        else:
            self.xs_prior_bg_tf = None
            self.xs_bg_tf = None

        self.nb_ch = par.nb_ch
        self.fid_loss = par.fid_loss
        self.mix_fun_str = par.mix_fun_str
        self.im_size = par.im_size
        #compile mixing model
        self.xs_ot_tf, self.ys_org_tf, self.ys_pred_tf = mix_fun_wrapper(sess, par.Do, par.Du, par.Db,
                                                                         par.mix_fun_str,
                                                                         self.im_size, self.nb_ch,
                                                                         self.fid_loss, self.xs_ut_tf
                                                                         ,self.xs_bg_tf)
        self.sigma_tf = tf.placeholder(shape=(), dtype=tf.float32, name="sigma")
        self.phi_tf = create_phi_tf(self.fid_loss,self.nb_ch)
        ###extract pointers to model variables just in case
        with tf.variable_scope("MixingNet", reuse=True):
            self.Du_tf = tf.get_variable("D_u")
            self.Do_tf = tf.get_variable("D_o")
            self.Db_tf = tf.get_variable("D_b")
            self.Db_bias_tf = tf.get_variable("D_b_bias")
        ###assign the random values to the mixing model parametrs
        if par.optimize_mix_parameters:
            with tf.device('/{}'.format(par.device)):
                self.assign_rand_init_mix_model(sess)
        # compile gradients and losses
        with tf.device('/{}'.format(par.device)):
            if par.with_ut:
                self.get_grads_prior_ut()
                self.grad_ll_ut_tf,self.loss_ll_ut_tf = self.get_grads_fid(self.phi_tf,self.xs_prior_ut_tf)
            if par.with_bg:
                self.get_grads_prior_bg()
                self.grad_ll_bg_tf,self.loss_ll_bg_tf = self.get_grads_fid(self.phi_tf, self.xs_prior_bg_tf)

            if par.optimize_mix_parameters:
                self.create_grads_mixing_par(par)
            else:
                if self.fid_loss == "uvn" and self.nb_ch > 1:
                    self.dict_mixpar = {str(i): None for i in range(self.nb_ch)}
                else:
                    self.dict_mixpar = None


    def get_grads_prior_ut(self):
        if self.ut_model=="discrete":
            self.grad_prior_ut_tf, self.loss_prior_ut_tf = get_grads_prior_log_loss_smoothed(self.xs_prior_ut_tf,
                                                                                                  self.l_prior_ut_tf,
                                                                                                  self.sigma_tf,
                                                                                                 False)
        elif self.ut_model=="binary":
            self.grad_prior_ut_tf, self.loss_prior_ut_tf = get_grads_prior_sigmoid_loss_smoothed(self.xs_prior_ut_tf,
                                                                                             self.l_prior_ut_tf,
                                                                                             self.sigma_tf,
                                                                                             False)
    def get_grads_prior_bg(self):
        self.grad_prior_bg_tf, self.loss_prior_bg_tf = get_grads_prior_log_loss_smoothed(self.xs_prior_bg_tf,
                                                                                         self.l_prior_bg_tf,
                                                                                         self.sigma_tf, False)
    def get_grads_fid(self,phi_tf,xs_tf):
        if self.fid_loss=="uvn" and self.nb_ch > 1:
            grad_ll_tf = {}
            loss_ll_tf = {}
            for ch in range(self.nb_ch):
                grad_tf, loss_tf = get_grads_fidelity_smoothed_apprx_1band(
                                                            self.ys_org_tf, self.ys_pred_tf[str(ch)],
                                                                        phi_tf, xs_tf)
                grad_ll_tf[str(ch)] = grad_tf
                loss_ll_tf[str(ch)] = loss_tf
        elif self.nb_ch==1:
            grad_ll_tf, loss_ll_tf = get_grads_fidelity_smoothed_apprx_1band(
                self.ys_org_tf, self.ys_pred_tf,phi_tf, xs_tf)
        elif self.fid_loss == "mvn":
            grad_ll_tf, loss_ll_tf = get_grads_fidelity_smoothed_apprx_msi(self.ys_org_tf, self.ys_pred_tf,
                                                                 phi_tf, xs_tf)
        return grad_ll_tf,loss_ll_tf



    def assign_rand_init_mix_model(self,sess):
        with tf.variable_scope("MixingNet", reuse=True):
            update_ops = []
            update_do = tf.assign(self.Du_tf, np.random.normal(0.2, 0.1, (1,)))
            update_ops.extend([update_do])
            if self.with_ut:
                update_du = tf.assign(self.Do_tf, np.random.normal(0.2, 0.1, (1,)))
                update_ops.extend([update_du])
            if self.with_bg:
                update_db = tf.assign(self.Db_tf, np.random.normal(0.2, 0.1, (1,)))
                update_db_bias = tf.assign(self.Db_bias_tf, np.random.normal(0.2, 0.1, (1,)))
                update_ops.extend([update_db, update_db_bias])
            ops_grouped = tf.group(update_ops)
            sess.run(ops_grouped)

    def create_grads_mixing_par(self):
        grad_post_Do, _ = self.grad_loss_fid(self.phi_Do_tf,self.Do_tf)
        self.dict_mixpar = {self.Do_tf: grad_post_Do[0]}
        if self.with_ut:
            grad_post_Du, _ = self.grad_loss_fid(self.phi_Du_tf,self.Du_tf)
        self.dict_mixpar[self.Du_tf]= grad_post_Du[0]
        if self.with_bg:
            grad_post_Db, _ = self.grad_loss_fid(self.phi_Db_tf,self.Db_tf)
            grad_post_Db_bias, _ = self.grad_loss_fid(self.phi_Db_bias_tf,self.Db_bias_tf)
            self.dict_mixpar.update({self.Db_tf: grad_post_Db[0], self.Db_bias_tf: grad_post_Db_bias[0]})

def mix_fun_wrapper(sess,Do,Du,Db,mix_fun_str,im_size,nb_ch,fid_loss,xs_ut_tf,xs_bg_tf):
    mix_fun = eval("mix_model." + mix_fun_str)
    ###create original artificial palimpsest image
    xs_ot_tf = tf.placeholder(shape=(None,im_size,im_size,1), dtype=tf.float32, name="x_ot")
    ys_pred_tf = set_mix_model_par(mix_fun,Do,Du,Db,xs_ot_tf,xs_ut_tf,xs_bg_tf,fid_loss,nb_ch)
    sess.run(tf.initialize_all_variables())
    nb_ch = nb_ch if nb_ch == 1 or fid_loss == "mvn" else 1
    ys_org_tf = tf.placeholder(shape=(None, im_size, im_size, nb_ch), dtype=tf.float32,
                                 name="y_org")
    return xs_ot_tf,ys_org_tf,ys_pred_tf





def create_phi_tf(fid_loss,nb_ch):
    nb_ch = nb_ch if nb_ch == 1 or fid_loss == "mvn" else 1
    if fid_loss == "uvn":
        phi_tf = tf.placeholder(shape=(), dtype=tf.float32, name="phi")
    else:
        phi_tf = tf.placeholder(shape=(nb_ch, nb_ch), dtype=tf.float32, name="phi")
    return phi_tf

def set_mix_model_par(mix_fun,Do,Du,Db,xs_ot_tf,xs_ut_tf,xs_bg_tf,fid_loss,nb_ch):
    y_pred, _ = mix_fun(undertext=xs_ut_tf, background=xs_bg_tf,
                        overtext=xs_ot_tf, nb_ch=nb_ch, bg_bias=[0.0] * nb_ch,
                        bg_scaler=Db,
                        ut_refl=Du, ot_refl=Do)
    if nb_ch > 1 and fid_loss == "uvn":
        y_pred_split = tf.split(value=y_pred, num_or_size_splits=nb_ch, axis=-1)
        y_pred = {}
        for i in range(nb_ch):
            y_pred[str(i)] = y_pred_split[i]
    return y_pred