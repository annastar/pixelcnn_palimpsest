import platform
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
from pixelcnn_palimpsest.pixel_cnn_pp.model import model_spec_gray_log_mix,model_spec_discrete
from pixelcnn_palimpsest.pixel_cnn_pp import nn
import numpy as np
import os



def restore_model_at_noise_level(par,sess,noise_level,epoch,mode):
    if mode=="ut":
        restore_model(os.path.join(par.chpoint_ut_path,"noise_"+str(noise_level),
                                   "params_greek960_ut{}.ckpt".format(epoch)), sess, "model_ut")
    elif mode=="bg":
        restore_model(os.path.join(par.chpoint_bg_path,"noise_"+str(noise_level),
                                   "params_greek960_bg{}.ckpt".format(epoch)), sess, "model_bg")
    else:
        raise ValueError("Mode should be ut or bg, not {}".format(mode))

def compile_pixelcnn_bg_model_with_par(batch_size):
    """
    Restore the prior model
    Args:
        batch_size - number of tiles
        mode - str, "ut" or "bg"
    Returns:
        xprior - tf placeholder input to prior model
        l_prior - tf placeholder output of the model
        y_prior - tf placeholder, separate placeholder for input image
        if we want input to model and output to be disconnected
    """
    ###restore model
    nr_mix = 3
    xprior, l_prior = compile_prior_model(batch_size, "bg", nr_filters=32,
                                          nr_resnet=3, nr_mix=nr_mix, model_discrete=False,
                                          model_scope="model_bg")
    with tf.variable_scope("model_bg", reuse=tf.AUTO_REUSE):
        yprior = tf.placeholder(tf.float32, shape=(batch_size, 64, 64, 1))
    return xprior, l_prior, yprior

def compile_pixelcnn_ut_model_with_par(batch_size, mode):
    """
    Restore the prior model
    Args:
        batch_size - number of tiles
        mode - str, "ut" or "bg"
    Returns:
        xprior - tf placeholder input to prior model
        l_prior - tf placeholder output of the model
        y_prior - tf placeholder, separate placeholder for input image
        if we want input to model and output to be disconnected
    """
    ###restore model
    if mode == "discrete":
        nr_mix = 2
        xprior, l_prior = compile_prior_model(batch_size, "undertext", nr_filters=64,
                                              nr_resnet=5, nr_mix=nr_mix, model_discrete=False,
                                              model_scope="model_ut")

    elif mode == "binary":
        nr_mix = 2
        xprior, l_prior = compile_prior_model(batch_size, "undertext", nr_filters=64,
                                              nr_resnet=5, nr_mix=nr_mix, model_discrete=True,
                                              model_scope="model_ut")
    else:
        raise Warning("No such ut model mode as {}".format(mode))
    with tf.variable_scope("model", reuse=tf.AUTO_REUSE):
        yprior = tf.placeholder(tf.float32, shape=(batch_size, 64, 64, 1))
    return xprior, l_prior, yprior

def compile_prior_model(batch_size,mode,nr_filters,nr_resnet,nr_mix,model_discrete,model_scope):
    """
    Restore pixelCNN++ model
    Arg:
        checkpoint_path - checkpoint path
    Returns:
        outputs:
            ys [Tensor] - model output node
            xs [Tensor] - model input placeholder
            sess [obj] - session with restored parameters
    """
    if model_discrete:
        model = model_spec_discrete
    else:
        model = model_spec_gray_log_mix
    if mode=="undertext":
        xs = tf.placeholder(tf.float32, shape=(batch_size, 64, 64, 1), name="x_ut")
        model_opt = {'nr_resnet': nr_resnet, 'nr_filters': nr_filters, 'nr_logistic_mix': nr_mix,
                     'resnet_nonlinearity': 'concat_elu','q_levels':1}
        model = tf.make_template(model_scope, model)
    elif mode=="bg":
        xs = tf.placeholder(tf.float32, shape=(batch_size, 64, 64, 1), name="x_bg")
        model_opt = {'nr_resnet': nr_resnet, 'nr_filters': nr_filters, 'nr_logistic_mix': nr_mix,
                     'resnet_nonlinearity': 'concat_elu'}
        model = tf.make_template(model_scope, model)

    ys = model(xs, None, init=False, ema=None, dropout_p=0., **model_opt)
    return xs,ys


def restore_model(checkpoint_path, sess, model_scope):

    vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=model_scope)
    saver = tf.train.Saver(vars)
    print('Restoring weights from ', checkpoint_path)
    saver.restore(sess, checkpoint_path)

def get_gradients_prior(x,y,l,mode,mean_loss,binary,s=None):
    """
    Get gradient tensor for prior
    y - true image
    l - predicted disribution
    x - input image
    mode - "bg"
    """
    if mode=="undertext":
        if binary:
            loss = nn.sigmoid_loss_binary(y, l, False,s)
        else:
            loss = nn.discretized_mix_logistic_loss_gray(y, l, False)
    elif mode=="bg":
        loss = nn.discretized_mix_logistic_loss_gray(y, l, False)
    grads = tf.gradients(loss, x)
    if mean_loss:
        loss = tf.reduce_mean(loss)/(np.log(2.)*int(y.get_shape()[1])*int(y.get_shape()[2]))#change log base
    else:
        loss = loss/np.log(2.)
    return grads,loss




