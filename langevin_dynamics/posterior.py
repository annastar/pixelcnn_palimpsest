import platform
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
from math import pi,sqrt
from pixelcnn_palimpsest.pixel_cnn_pp import nn
import tensorflow_probability as tfp
import numpy as np

def get_grads_fidelity_smoothed_calc(y_org_tf,xs_ut_tf,xs_ot_tf,xs_bg_tf,s_i_tf,s_org_tf,grad_wrt_var):
    """
    Get a fidelity gradient tensor approximated with Gaussian
    Args:
        y_org - org palimp image;
        y_pred_tf - reconstructed image tensor;
        phi - std of fidelity term likelihood if y=g(x~)+g(-epsilon)
        grad_wrt_var - variable with respect to what take a gradient
    """
    with tf.variable_scope("MixingNet", reuse=True):
        Du_tf = tf.get_variable("D_u")
        Do_tf = tf.get_variable("D_o")
    if not xs_bg_tf is None:
        loss_tf = likelihood_exclusion_model_1_var(y_org_tf,Do_tf,
                                                   Du_tf,xs_ot_tf,
                                                   xs_ut_tf,s_org_tf,s_i_tf)
    else:
        with tf.variable_scope("MixingNet", reuse=True):
            Db_tf = tf.get_variable("D_b")
            Db_bias_tf = tf.get_variable("D_b_bias")
        loss_tf = likelihood_exclusion_model_2_var(y_org_tf, Do_tf, Du_tf,
                                                   Db_tf, Db_bias_tf, xs_ot_tf,
                                                   xs_ut_tf,xs_bg_tf, s_org_tf, s_i_tf)
    if grad_wrt_var.shape.as_list() != loss_tf.shape.as_list():
        loss_tf = loss_tf/tf.cast(tf.size(xs_ut_tf),tf.float32)
    grads_tf = tf.gradients(loss_tf, grad_wrt_var)

    return grads_tf,loss_tf

def get_grads_fidelity_smoothed_apprx_1band(y_org_tf,y_pred_tf,phi,grad_wrt_var):
    """
    Get a fidelity gradient tensor approximated with Gaussian
    Args:
        y_org - org palimp image;
        y_pred_tf - reconstructed image tensor;
        phi - std of fidelity term likelihood if y=g(x~)+g(-epsilon)
        grad_wrt_var - variable with respect to what take a gradient
    """
    cmult = tf.math.log(1/(phi*sqrt(2*pi)))
    loss_tf = cmult+(-1/(2*phi**2))*((y_org_tf-y_pred_tf)**2)
    grads_tf = tf.gradients(loss_tf, grad_wrt_var)
    return grads_tf,tf.exp(loss_tf)

def get_grads_fidelity_smoothed_apprx_msi(y_org_tf,y_pred_tf,cov_tf,grad_wrt_var):
    if platform.release() == "5.10.153-1.el7.x86_64":
        scale_tf = tf.linalg.cholesky(cov_tf)
        dist = tfp.distributions.MultivariateNormalTriL(loc=y_org_tf, scale_tril=scale_tf)
    else:
        dist = tfp.distributions.MultivariateNormalFullCovariance(y_org_tf,covariance_matrix=cov_tf)
    loss_tf = dist.log_prob(y_pred_tf)
    grads_tf = tf.gradients(loss_tf, grad_wrt_var)
    return grads_tf, tf.exp(loss_tf)


def likelihood_exclusion_model_1_var(y,Do,Du,xo,xus,s_org,s_i):
    """
    y - org palimpsest
    Do -
    Du
    xo -
    xus - smoothed xu
    s_org - original noise level

    """
    a = (y*(s_i**2)*Du*(1-xo)-(s_i**2)*Do*xo*Du*(1-xo)+(s_org**2)*xus)/((s_i*s_org)**2)
    c = (-((s_i*y)**2)+2*(s_i**2)*y*Do*xo-((s_i*Do*xo)**2)-((s_org*xus)**2))/(2*(s_i*s_org)**2)
    b = (2*((s_i*Du*(1-xo))**2)+(2*(s_org**2)))/(4*(s_i*s_org)**2)
    ll = (tf.math.exp(c+(a**2)/(4*b))/(s_i*s_org*2*tf.math.sqrt(pi*b)))
    return ll

def get_grads_prior_sigmoid_loss_smoothed(x,l,s,sum_all):
    """
    Get gradients of smoothed Binary Cross Entropy loss
    Args:
        x - tf tensor for input;
        l - tf tensor for logits;
        s - sigmoid;
        sum_all - boolean, to sum or not to sum the output
    """
    loss = nn.sigmoid_loss_smoothed(x,l,s,sum_all)
    grads = tf.gradients(loss, x)
    loss = tf.reduce_mean(loss)#/(np.log(2.)*int(x.get_shape()[1])*int(x.get_shape()[2]))#change log base
    return grads, loss

def get_grads_prior_log_loss_smoothed(x,l,s,sum_all):
    """
    Get gradients of smoothed Binary Cross Entropy loss
    Args:
        x - tf tensor for input;
        l - tf tensor for logits;
        s - sigmoid;
        sum_all - boolean, to sum or not to sum the output
    """
    loss = nn.log_loss_smoothed(x,l,s,sum_all)
    grads = tf.gradients(loss, x)
    loss = tf.reduce_mean(loss)#/(np.log(2.)*int(x.get_shape()[1])*int(x.get_shape()[2]))#change log base
    return grads, loss



def lagevin_dynamics_eq_1_var_posterior(sess,xs_ut_tf,x_ut,xs_bg_tf,x_bg,sigma_tf,
                                        sigma,phi_tf,phi,grad_prior_tf,grad_ll_tf,
                                        eta,ys_org_tf,y_org,xs_ot_tf,xs_ot,noise_shape,updated_var):
    """
    Langevin dynamics equation
    args:
    sess - tf Session
    xtf - input image tensor
    sigmatf - smoothing noise std tf placeholder
    grad_post_tf - posterior gradient placeholder
    x - input image
    sigma - smoothing noise std value
    phi_tf - phi tf placeholder
    phi - std value of smoothed likelihood p(y,\tilde(x))
    eta - noise scale
    Returns:
        x - nd array [1xnxmx1] updated image
    """

    noise = np.random.normal(0, 1.0, noise_shape)
    feed_dict = {ys_org_tf: y_org, xs_ot_tf:xs_ot}
    if not xs_bg_tf is None:
        feed_dict[xs_ut_tf] = x_ut
    if not sigma_tf is None:
        feed_dict[sigma_tf] = sigma
    if not phi_tf is None:
        feed_dict[phi_tf] = phi
    if not xs_bg_tf is None:
        feed_dict[xs_bg_tf] = x_bg
    grads_prior,grad_ll = sess.run([grad_prior_tf,grad_ll_tf],feed_dict)
    grads = grad_ll[0]+grads_prior[0]
    res = eta * grads
    scaled_noise = np.sqrt(2 * eta) * noise
    updated_var = updated_var + res + scaled_noise
    return updated_var

def ld_1_var_grad_posterior(sess,xs_ut_tf,x_ut,xs_bg_tf,x_bg,sigma_tf,
                            sigma,phi_tf,phi,grad_prior_tf,grad_ll_tf,
                            ys_org_tf,y_org,xs_ot_tf,xs_ot):
    """
     Getting Gradients of posterior according to Langevin dynamics
    args:
    sess - tf Session
    xtf - input image tensor
    sigmatf - smoothing noise std tf placeholder
    grad_post_tf - posterior gradient placeholder
    x - input image
    sigma - smoothing noise std value
    phi_tf - phi tf placeholder
    phi - std value of smoothed likelihood p(y,\tilde(x))
    eta - noise scale
    Returns:
        x - nd array [1xnxmx1] updated image
    """
    feed_dict = {ys_org_tf: y_org, xs_ot_tf:xs_ot}
    if not xs_ut_tf is None:
        feed_dict[xs_ut_tf] = x_ut
    if not sigma_tf is None:
        feed_dict[sigma_tf] = sigma
    if not phi_tf is None:
        feed_dict[phi_tf] = phi
    if not xs_bg_tf is None:
        feed_dict[xs_bg_tf] = x_bg
    grads_prior,grad_ll = sess.run([grad_prior_tf,grad_ll_tf],feed_dict)
    grads = grad_ll[0]+grads_prior[0]
    return grads

def ld_add_noise_to_grads(grads,eta,updated_var,noise_shape):
    noise = np.random.normal(0, 1.0, noise_shape)
    res = eta * grads
    scaled_noise = np.sqrt(2 * eta) * noise
    updated_var = updated_var + res + scaled_noise
    return updated_var


if __name__=="__main__":
    pass