import platform
import os
if platform.release()=="5.10.153-1.el7.x86_64":
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import math
from copy import deepcopy
from pixelcnn_palimpsest.utils import files
from pixelcnn_palimpsest.langevin_dynamics import data,mix_model
import matplotlib as mpl
import sys

class MixingVars():
    def __init__(self,du,do,xu,xo,xb,sigma_ut,sigma_bg,org_noise_sigma,
                 add_noise_du,add_noise_do,add_noise_xu,add_noise_xb,nb_samples,
                 mix_fun_name,bg_bias,bg_scaler,sess,nb_bands,ut_model):
        """
        Creates the palimpsest image from layers with Gaussian noise
        du,do - 1D array [nb_channels], optical densities of under-, over- text;
        xu,xo,xb - vectorized layer of over- and under- text, background all in range [0;1];
        add_noise_du, add_noise_do, add_noise_xu, add_noise_xb - boolean, a flag that
            shows whether to add Gaussian noise to a variable;
        sigma - std of smoothing noise;
        org_noise_sigma - std of original noise
        nb_samples - nb of pixels
        mix_fun_name - name of mixing function method
        bg_bias - background opt. dens. bias
        controlled noise
        """

        self.sess = sess
        org_noise = np.random.normal(0,org_noise_sigma,(nb_samples,1,1,nb_bands))
        self.du,self.du_noise = du,deepcopy(du)
        self.do,self.do_noise = do,deepcopy(do)
        if ut_model == "binary":
            self.xu, self.xu_noise = xu, deepcopy(xu)
        elif ut_model == "discrete":
            self.xu,self.xu_noise = self.rescale_bg(deepcopy(xu)),self.rescale_bg(deepcopy(xu))
        else:
            raise ValueError("No such ut model as".format(ut_model))
        self.xo,self.xo_noise = xo,deepcopy(xo)
        self.nb_bands = nb_bands
        self.bg_bias = bg_bias
        self.bg_scaler = bg_scaler
        self.compile_model(self.du,self.do,xb,self.bg_bias,self.bg_scaler,mix_fun_name)
        if not xb is None:
            self.xb, self.xb_noise = self.rescale_bg(deepcopy(xb)),self.rescale_bg(deepcopy(xb))
            if add_noise_xb:
                noise = np.random.normal(0, sigma_bg, (nb_samples,))
                self.xb_noise =self.rescale_bg(xb + noise)
        else:
            self.xb, self.xb_noise = None,None
            self.bg_bias = None
        if add_noise_du:
            noise = np.random.normal(0, sigma_ut, (nb_samples,))
            self.du_noise = du + noise
        if add_noise_do:
            noise = np.random.normal(0, sigma_ut, (nb_samples,))
            self.do_noise = do + noise
        if add_noise_xu:
            noise = np.random.normal(0, sigma_ut, (nb_samples,))
            if ut_model == "binary":
                self.xu_noise = xu + noise
            elif ut_model == "discrete":
                self.xu_noise = self.rescale_bg(xu + noise)
        self.y = self.get_output(self.xu,self.xo,self.xb)
        self.y_smooth = self.get_output(self.xu_noise,self.xo_noise,self.xb_noise)
        self.y_smooth_noise = self.y_smooth + org_noise

    def reset_graph(self):
        tf.compat.v1.reset_default_graph()

    def rescale_bg(self,x):
        """Rescale x from interval [-1;1] to [0,1]"""
        x = (x+1)/2
        return x

    def compile_model(self,du,do,x_bg,bg_bias,bg_scaler,mix_fun_name):
        mix_fun = eval("mix_model." + mix_fun_name)
        self.xs_ut = tf.placeholder(shape=(None, 1, 1, 1), dtype=tf.float32)
        self.xs_ot = tf.placeholder(shape=(None, 1, 1, 1), dtype=tf.float32)
        if not x_bg is None:
            self.xs_bg = tf.placeholder(shape=(None, 1, 1, 1), dtype=tf.float32)
        else:
            self.xs_bg = None
        self.y_pred, D_b = mix_fun(undertext=self.xs_ut, background=self.xs_bg,
                                   overtext=self.xs_ot, nb_ch=self.nb_bands, bg_bias=bg_bias,
                                   bg_scaler=bg_scaler, ut_refl=du, ot_refl=do)

    def get_output(self,xu,xo,x_bg):
        d = {self.xs_ut:np.expand_dims(xu,[1,2,3]), self.xs_ot:np.expand_dims(xo,[1,2,3])}
        if not self.xs_bg is None:
            d.update({self.xs_bg:np.expand_dims(x_bg,[1,2,3])})
        self.sess.run(tf.initialize_all_variables())
        out = self.sess.run(self.y_pred,d)
        return out

def normal_pdf(x,mu,sigma):
    scaler = 1/(sigma*math.sqrt(2*math.pi))
    args = (-(x-mu)**2)/(2*sigma**2)
    pdf = scaler*np.exp(args)
    return pdf

def calc_save_std_uvn(mix_fun_name,org_noise,ut_model):
    if ut_model=="discrete":
        dirpath = os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin", mix_fun_name,"xu_discrete","uvn")
    elif ut_model=="binary":
        dirpath = os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin", mix_fun_name, "xu_binary",
                               "uvn")
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,1.0,1.2,1.4,1.6]
    du_list = np.arange(-100,100)/100 # values from -5 to 10 with step 0.01
    fname = r"phi_list_nb_layers_1_org_noise_{}_xu.json".format(org_noise)
    file_path = os.path.join(dirpath, fname)
    dict_sigmas_ut = {}
    for sigma in sigma_list:
        dict_du = {}
        for du in du_list:
            print("Sigma {} Du {}".format(sigma,du))
            std_du = calc_ll_std_ut(du,sigma,org_noise,mix_fun_name,ut_model)
            dict_du[du]=std_du
        dict_sigmas_ut[sigma]=dict_du
    files.dict_to_json(file_path, dict_sigmas_ut, False)
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,1.2,1.4,1.6]
    db_list = np.arange(-250, 250) / 100  # values from -5 to 10 with step 0.01
    fname_bg = r"phi_list_nb_layers_1_org_noise_{}_xb.json".format(org_noise)
    dict_sigmas_bg = {}
    for sigma in sigma_list:
        dict_db = {}
        for db in db_list:
            print("Sigma {} Db {}".format(sigma, db))
            std_db = calc_ll_std_bg(db,sigma,org_noise,mix_fun_name,ut_model)
            dict_db[db]=std_db
        dict_sigmas_bg[sigma]=dict_db
    file_path = os.path.join(dirpath, fname_bg)
    files.dict_to_json(file_path, dict_sigmas_bg, False)



def calc_save_cov_mat(pal_name,org_noise,dirpath,du,do,db,bg_bias,
                      sigma_list,mix_fun_name,nb_samples,add_noise_to,ut_model):

    nb_bands = len(du)
    if add_noise_to== "xu":
        add_noise_xu = True
        add_noise_xb = False
        xu = np.ones((nb_samples))
        xb = 0.0 * np.ones((nb_samples))
        xo = np.zeros_like(xu)
        fname = add_noise_to + r"_{}_on_{}".format(pal_name, org_noise)
        fname = fname + "_Du_" + str([round(du[u], 3) for u in range(nb_bands)]).replace(" ","")
    elif add_noise_to== "xb":
        xu = 0.0*np.ones((nb_samples))
        xb = 0.5 * np.ones((nb_samples))
        xo = np.zeros_like(xu)
        add_noise_xu = False
        add_noise_xb = True
        fname = add_noise_to + r"_{}_on_{}".format(pal_name, org_noise)
        fname = fname + "_Db_" + str([round(db[u], 3) for u in range(nb_bands)]).replace(" ","")

    file_path = os.path.join(dirpath, fname)
    dict_ut = {"du": du, "do": do, "db": db, "noise_org": org_noise, "sigma_list": sigma_list}
    dict_cov_mat = {}
    for sigma in sigma_list:
        print("Sigma {} {}".format(sigma,add_noise_to))
        sess = tf.Session()
        if add_noise_to == "xu":
            d = MixingVars(du, do, xu, xo, xb, sigma, 0.0, org_noise,
                           add_noise_du=False, add_noise_do=False,
                           add_noise_xu=add_noise_xu, add_noise_xb=add_noise_xb, nb_samples=nb_samples,
                           mix_fun_name=mix_fun_name, bg_bias=bg_bias, bg_scaler=db, sess=sess, nb_bands=nb_bands, ut_model=ut_model)
        elif add_noise_to == "xb":
            d = MixingVars(du, do, xu, xo, xb, 0.0, sigma, org_noise,
                           add_noise_du=False, add_noise_do=False,
                           add_noise_xu=add_noise_xu, add_noise_xb=add_noise_xb, nb_samples=nb_samples,
                           mix_fun_name=mix_fun_name, bg_bias=bg_bias, bg_scaler=db, sess=sess, nb_bands=nb_bands, ut_model=ut_model)
        sess.close()
        d.reset_graph()
        cov_mat = calc_cov_mat(np.squeeze(d.y),np.squeeze(d.y_smooth_noise))
        print(cov_mat)
        dict_cov_mat[str(sigma)] = cov_mat
    files.dict_to_json(file_path + ".json", dict_ut, False)
    np.savez(file_path + ".npz", **dict_cov_mat)

def calc_save_std_mvn(pal_name,du,do,db,add_noise_to,mix_fun_name, org_noise,ut_model):
    if ut_model=="discrete":
        dirpath = os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin", mix_fun_name,"xu_discrete","mvn")
    elif ut_model=="binary":
        dirpath = os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin", mix_fun_name, "xu_binary",
                               "mvn")
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,1.0]

    bg_bias = 0.0
    nb_samples = 10000


    calc_save_cov_mat(pal_name, org_noise, dirpath, du,
                      do, db, bg_bias, sigma_list,
                      mix_fun_name, nb_samples,add_noise_to,ut_model)


def calc_ll_std_ut(du,sigma_ut,org_noise,mix_fun_name,ut_model):
    """Calculate x_ut std for each sigma level for particular du"""
    do = 0.6
    bg_bias = 0.0
    bg_scaler = 1.0

    nb_samples = 10000
    xu = np.ones((nb_samples))
    xb = 0.5*np.ones((nb_samples))
    xo = np.zeros_like(xu)
    # variables with noise
    sess = tf.Session()
    d = MixingVars(du, do, xu, xo, xb, sigma_ut,sigma_ut, org_noise,
                       add_noise_du=False, add_noise_do=False,
                       add_noise_xu=True,add_noise_xb=False, nb_samples=nb_samples,
                       mix_fun_name=mix_fun_name,bg_bias=bg_bias,bg_scaler=bg_scaler,sess=sess,nb_bands=1,ut_model=ut_model)
    sess.close()
    d.reset_graph()
    var = calc_var(d.y,d.y_smooth_noise)
    std = math.sqrt(var)
    return std

def calc_ll_std_ut_bg(du, dbg,sigma_ut,sigma_bg,org_noise,mix_fun_name,ut_model):
    """Calculate x_ut std for each sigma level for particular du"""

    nb_samples = 10000
    xu = np.ones((nb_samples))
    xb = 0.5*np.ones((nb_samples))
    xo = np.zeros_like(xu)
    do = 0.4533091568449682
    # variables with noise
    sess = tf.Session()
    d = MixingVars(du, do, xu, xo, xb, sigma_ut,sigma_bg, org_noise,
                       add_noise_du=False, add_noise_do=False,
                       add_noise_xu=True,add_noise_xb=True, nb_samples=nb_samples,
                       mix_fun_name=mix_fun_name,bg_bias=0.0,
                       bg_scaler=dbg,sess=sess,nb_bands=1,ut_model=ut_model)
    sess.close()
    d.reset_graph()
    var = calc_var(d.y,d.y_smooth_noise)
    std = math.sqrt(var)
    return std

def calc_save_std_uvn_ut_bg(mix_fun_name,org_noise):
    if ut_model=="discrete":
        dirpath = os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin", mix_fun_name,"xu_discrete","smoothing_xu_xb")
    elif ut_model=="binary":
        dirpath = os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin", mix_fun_name, "xu_binary",
                               "smoothing_xu_xb")
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    du_list = np.arange(9,14)/100 # values from -5 to 10 with step 0.01
    dbg_list = np.arange(75,77) / 100  # values from -5 to 10 with step 0.01
    fname = r"phi_list_nb_layers_2_org_noise_{}.json".format(org_noise)
    file_path = os.path.join(dirpath, fname)
    dict_sigmas_ut = {"sigma_ut":{}}
    for sigma_ut in sigma_list:
        dict_sigmas_bg = {"sigma_bg":{}}
        for sigma_bg in sigma_list:
            dict_du = {"du":{}}
            for du in du_list:
                dict_dbg = {"dbg": {}}
                for dbg in dbg_list:
                    print("Sigma_ut:{},sigma_bg:{}, Du {}, Dbg {}".format(sigma_ut,sigma_bg,du,dbg))
                    std_du_dbg = calc_ll_std_ut_bg(du,dbg,sigma_ut,sigma_bg,org_noise,mix_fun_name,ut_model)
                    dict_dbg["dbg"][dbg]=std_du_dbg
                dict_du["du"][du] = dict_dbg
            dict_sigmas_bg["sigma_bg"][sigma_bg]=dict_du
        dict_sigmas_ut["sigma_ut"][sigma_ut]=dict_sigmas_bg
    files.dict_to_json(file_path, dict_sigmas_ut, False)

def calc_ll_std_bg(bg_scaler,sigma_bg, org_noise,mix_fun_name,ut_model):
    do = 0.6
    du = 0.5
    bg_bias = 1.0
    nb_samples = 10000
    xu = 0.0*np.ones((nb_samples))
    xb = 0.5 * np.ones((nb_samples))
    xo = np.zeros_like(xu)
    # variables with noise
    sess = tf.Session()
    d = MixingVars(du, do, xu, xo, xb, sigma_bg, sigma_bg, org_noise,
                   add_noise_du=False, add_noise_do=False,
                   add_noise_xu=False, add_noise_xb=True, nb_samples=nb_samples,
                   mix_fun_name=mix_fun_name, bg_bias=bg_bias,bg_scaler=bg_scaler,
                   sess=sess,nb_bands=1,ut_model=ut_model)
    sess.close()
    d.reset_graph()
    var = np.mean((d.y - d.y_smooth_noise) ** 2)
    std = math.sqrt(var)
    return std

def calc_var(x,x_noisy):
    return np.mean((x - x_noisy) ** 2)

def calc_cov_mat(x,x_noise):
    nb = x.shape[-1]
    cov_mat = np.zeros((nb,nb))
    for i in range(nb):
        for j in range(nb):
            if i==j:
                cov_mat[i,j] = calc_var(x[:,i],x_noise[:,i])
            else:
                cov_mat[i,j] = calc_cov(x[:,i],x_noise[:,i],x[:,j],x_noise[:,j])
    return cov_mat

def calc_cov(x,x_noise,y,y_noise):
    return np.mean((x-x_noise)*(y-y_noise))

def plot_hist_vs_calc_ll_uvn(du,add_xu_noise,add_bg_noise):
    # noise level
    mix_fun_name = "sep_layers_bg_not_fn"  # "sep_layers_bg_fun_sigmoid"#"sep_layers_bg_not_fn"
    sigma_list = [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]  # np.linspace(0.01, 1, 10)
    # variables without noise
    nb_samples = 100000
    do = 0.6
    org_noise = 1e-10
    xu = np.ones((nb_samples))
    xo = np.zeros_like(xu)
    xb = 0.5*np.ones_like(xu)
    # variables with noise
    dict = {}
    for idx, sigma in enumerate(sigma_list):
        sess = tf.Session()
        d = MixingVars(du, do, xu, xo,xb, sigma,sigma, org_noise,
                       add_noise_du=False, add_noise_do=False,
                       add_noise_xu=add_xu_noise,add_noise_xb=add_bg_noise,
                       nb_samples=nb_samples,mix_fun_name=mix_fun_name,bg_bias=1,bg_scaler=1,sess=sess)
        d.reset_graph()
        sess.close()
        mpl.style.use("default")
        fig, ax = plt.subplots(1, 2)
        fig.suptitle("Du {}, Pdf vs. hist of y_p, with sigma:{}".format(du,sigma), color='C0')
        var = np.mean((d.y - d.y_smooth_noise) ** 2)
        dict[sigma] = math.sqrt(var)
        ax[0].scatter(d.y_smooth_noise, normal_pdf(d.y_smooth_noise, d.y, dict[sigma]), marker=".", color="red")
        ax[0].hist(d.y_smooth_noise, bins=22,label="histogram",density=True)

        ax[1].hist(np.random.normal(d.y, dict[sigma]), bins=22, label="approx_normal", density=True,stacked=True)

        ax[0].legend(loc='upper right')
        ax[1].legend(loc='upper right')
        print("Sigma {}, std cur {}".format(sigma, math.sqrt(var)))
        print("Sigma {}, std bg {}".format(sigma, np.sqrt(np.mean((d.xb-d.xb_noise)**2))))



if __name__=="__main__":

    if len(sys.argv) > 1:
        if sys.argv[1] == "cis":
            main_dir = r"/cis/phd/as3297/projects/bss_autoreg_palimpsest"
    else:
        main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    mix_fun_str = "sep_layers_bg_not_fn"
    msi = True
    msi_partial = True
    org_noise = 1e-2
    pal_name = "greek960"
    joint_smoothing = False
    ut_model = "binary"
    if msi:
        if pal_name == "art_pal":
            nb_bands = len(db)
            sc = 1.3236
            do = [do[a] / sc for a in range(nb_bands)]
            db = [db[a] / sc for a in range(nb_bands)]
            du = [du[a] / sc for a in range(nb_bands)]
        if "archi" in pal_name:
            data_dir = os.path.join(main_dir, "datasets", r"Archimedes", r"017r-016v_Arch07r_Sinar")
            fname_ot = r"overtext_map.png"
            data = data.PrepArchimedes(data_dir, [8468, 8468 + 192], [1082, 1082 + 4 * 192], msi, fname_ot,
                                       0, mix_fun_str, msi_partial=msi_partial)
        elif "greek" in pal_name:
            data_dir = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
            fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
            msi = True
            data = data.PrepGreek960(data_dir, [2300, 2300 + 192], [1800, 2914], msi, fname_ot,
                                     -90, mix_fun_str=mix_fun_str, msi_partial=msi_partial)
        do, du, db = data.D_o.tolist(), data.D_u.tolist(), data.D_bg.tolist()
        calc_save_std_mvn(pal_name,du,do,db,"xu",mix_fun_str,org_noise = org_noise,ut_model=ut_model)
        calc_save_std_mvn(pal_name, du,do,db,"xb",mix_fun_str,org_noise = org_noise,ut_model=ut_model)
    else:
        if joint_smoothing:
            calc_save_std_uvn_ut_bg(mix_fun_str,org_noise=org_noise,ut_model=ut_model)
        else:
            calc_save_std_uvn(mix_fun_str,org_noise=org_noise,ut_model=ut_model)
    plt.show()