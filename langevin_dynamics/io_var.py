import numpy as np
import os
from pixelcnn_palimpsest.utils.files import json_to_dict

class InOutVals():
    def __init__(self,main_dir,mix_fun_str,noise_level,fid_loss,with_bg,pal_name):
        self.main_dir = main_dir
        self.mix_fun_str = mix_fun_str
        self.noise_level = noise_level
        self.fid_loss = fid_loss
        self.with_bg = with_bg
        self.pal_name = pal_name

    def read_phi(self,Du,Db,par):
        if par.ut_model=="binary":
            self.read_phi_ut_binary(Du,Db)
        elif par.ut_model=="discrete":
            self.read_phi_ut_discrete(Du, Db)
        else:
            raise Warning("No such ut model regime as {}".format(par.ut_model))

    def read_phi_ut_discrete(self,Du,Db):
        ###read list of stds for Gaussian likelihood function for different level of smoothing
        if self.fid_loss=="uvn":
            phi_list_ut_f = os.path.join(self.main_dir, "training",
                                            "langevin", self.mix_fun_str,"xu_discrete","uvn",
                                            "phi_list_nb_layers_1_org_noise_{}_xu.json".format(self.noise_level))
            self.phi_list_ut = json_to_dict(phi_list_ut_f)
            if self.with_bg:
                phi_list_bg_f = os.path.join(self.main_dir, "training",
                                                "langevin", self.mix_fun_str,"xu_discrete","uvn",
                                                "phi_list_nb_layers_1_org_noise_{}_xb.json".format(self.noise_level))
                self.phi_list_bg = json_to_dict(phi_list_bg_f)
        else:
            round_du = str([round(u, 3) for u in Du]).replace(" ", "")

            phi_list_ut_f = os.path.join(self.main_dir, "training",
                                            "langevin", self.mix_fun_str,"xu_discrete", "mvn",
                                            "xu_{}_on_{}_Du_{}.npz".format(self.pal_name, self.noise_level, round_du))
            self.phi_list_ut = np.load(phi_list_ut_f,allow_pickle=True)
            if self.with_bg:
                round_db = str([round(u, 3) for u in Db]).replace(" ", "")
                phi_list_bg_f = os.path.join(self.main_dir, "training",
                                                "langevin", self.mix_fun_str,"xu_discrete", "mvn",
                                                "xb_{}_on_{}_Db_{}.npz".format(self.pal_name, self.noise_level,
                                                                               round_db))
                self.phi_list_bg = np.load(phi_list_bg_f,allow_pickle=True)

    def read_phi_ut_binary(self, Du, Db):
        ###read list of stds for Gaussian likelihood function for different level of smoothing
        if self.fid_loss == "uvn":
            phi_list_ut_f = os.path.join(self.main_dir, "training",
                                         "langevin", self.mix_fun_str, "xu_binary","uvn",
                                         "phi_list_nb_layers_1_org_noise_{}_xu.json".format(self.noise_level))
            self.phi_list_ut = json_to_dict(phi_list_ut_f)
            if self.with_bg:
                phi_list_bg_f = os.path.join(self.main_dir, "training",
                                             "langevin", self.mix_fun_str, "xu_binary","uvn",
                                             "phi_list_nb_layers_1_org_noise_{}_xb.json".format(self.noise_level))
                self.phi_list_bg = json_to_dict(phi_list_bg_f)
        else:
            round_du = str([round(u, 3) for u in Du]).replace(" ", "")

            phi_list_ut_f = os.path.join(self.main_dir, "training",
                                         "langevin", self.mix_fun_str, "xu_binary", "mvn",
                                         "xu_{}_on_{}_Du_{}.npz".format(self.pal_name, self.noise_level, round_du))
            self.phi_list_ut = np.load(phi_list_ut_f, allow_pickle=True)
            if self.with_bg:
                round_db = str([round(u, 3) for u in Db]).replace(" ", "")
                phi_list_bg_f = os.path.join(self.main_dir, "training",
                                             "langevin", self.mix_fun_str, "xu_binary", "mvn",
                                             "xb_{}_on_{}_Db_{}.npz".format(self.pal_name, self.noise_level,
                                                                            round_db))
                self.phi_list_bg = np.load(phi_list_bg_f, allow_pickle=True)
