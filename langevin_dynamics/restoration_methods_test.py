import copy
from pixelcnn_palimpsest.process_ocr_archi.exp_par import LogPar
from pixelcnn_palimpsest.langevin_dynamics.tf_vars import Model2Var
from pixelcnn_palimpsest.langevin_dynamics.io_var import InOutVals
from pixelcnn_palimpsest.langevin_dynamics import restoration_2layers as rpost
import tensorflow as tf
from matplotlib import pyplot as plt
import numpy as np

def create_im_layers_test(ior,par):
    im = ior.mask
    if len(im.shape)!=4:
        raise Exception("The shape of mask should be 4 dims but is {}".format(len(im.shape)))
    plt.figure()
    plt.imshow(np.squeeze(im),cmap="gray")
    plt.title("mask")
    if par.pal_name=="art_pal":
        if par.with_ut:
            im = ior.org_img_ut
            if len(im.shape)!=4:
                raise Exception("The shape of org_img_ut should be 4 dims but is {}".format(len(im.shape)))
            plt.figure()
            plt.imshow(np.squeeze(im), cmap="gray")
            plt.title("org_ut")
        if par.with_bg:
            im = ior.org_img_bg
            if len(im.shape) != 4:
                raise Exception("The shape of org_img_bg should be 4 dims but is {}".format(len(im.shape)))
            plt.figure()
            plt.imshow(np.squeeze(im), cmap="gray")
            plt.title("org_bg")
    if par.with_ut:
        im = ior.init_img_ut
        if len(im.shape) != 4:
            raise Exception("The shape of init_img_ut should be 4 dims but is {}".format(len(im.shape)))
        plt.figure()
        plt.imshow(np.squeeze(im), cmap="gray")
        plt.title("init_ut")
    if par.with_bg:
        im = ior.init_img_bg
        if len(im.shape)!=4:
            raise Exception("The shape of init_img_bg should be 4 dims but is {}".format(len(im.shape)))
        plt.figure()
        plt.imshow(np.squeeze(im), cmap="gray")
        plt.title("init_bg")



def y_org_test(ior,par):
    y_org = ior.y_org
    print("Max val Yorg band{}={}".format(ch, np.amax(ior.y_org)))
    if len(y_org.shape)!=4:
        raise Exception("The shape of pal should be 4 dims but is {}".format(len(y_org.shape)))
    if par.nb_ch>1:
        if y_org.shape[-1]==1:
            raise Exception("Number of channels should be {} but is 1".format(par.nb_ch))
    fig,ax = plt.subplots(par.nb_ch,1)
    if par.nb_ch>1:
        for i in range(par.nb_ch):
            ax[i].imshow(np.squeeze(y_org[:,:,:,i]),cmap="gray")
    else:
        ax.imshow(np.squeeze(y_org), cmap="gray")


#ior.init_imgs_bg = plotting.split_image(init_img_bg[0, :, :, 0], par.im_size)
def ld_opt_par_test(par,ior,m,sess):
    idx = 8
    if par.with_ut:
        ior.sigma_ut, ior.eta_ut = rpost.calc_ld_opt_par(par.sigma_list_ut, idx)
        rpost.restore_prior_models_at_noise_step(par, ior.sigma_ut, sess, "ut")
        ior.phi_ut = rpost.calc_likel_std(par, ior.phi_list_ut, ior.sigma_ut, m, sess, "ut")
        old_phi = copy.deepcopy(ior.phi_ut)
        old_sigma = copy.deepcopy(ior.)
    if par.with_bg:
        ior.sigma_bg, ior.eta_bg = rpost.calc_ld_opt_par(par.sigma_list_bg, idx)
        rpost.restore_prior_models_at_noise_step(par, ior.sigma_bg, sess, "bg")
        ior.phi_bg = rpost.calc_likel_std(par, ior.phi_list_bg, ior.sigma_bg, m, sess, "bg")
    idx = 7
    if par.with_ut:
        ior.sigma_ut, ior.eta_ut = rpost.calc_ld_opt_par(par.sigma_list_ut, idx)
        rpost.restore_prior_models_at_noise_step(par, ior.sigma_ut, sess, "ut")
        ior.phi_ut = rpost.calc_likel_std(par, ior.phi_list_ut, ior.sigma_ut, m, sess, "ut")
        if ior.phi_ut == ior.phi_bg:
            raise Exception("Something wrong with phi")
    if par.with_bg:
        ior.sigma_bg, ior.eta_bg = rpost.calc_ld_opt_par(par.sigma_list_bg, idx)
        rpost.restore_prior_models_at_noise_step(par, ior.sigma_bg, sess, "bg")
        ior.phi_bg = rpost.calc_likel_std(par, ior.phi_list_bg, ior.sigma_bg, m, sess, "bg")
    if ior.phi_ut==ior.phi_bg:
        raise Exception("Something wrong with phi")


def create_pal_test():
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    device = "cpu"
    nb_epoch = 10000
    noise_org = 1e-2
    opt_mixpar = False
    with_bg = True
    with_ut = True
    msi = False
    msi_partial = False
    fid_loss = "uvn"
    pal_name = "art_pal"#"art_pal"#"archi"#
    mix_fun_str = "sep_layers_bg_not_fn"
    chpoint_bg_path = "pixelcnn-gray-2022-10-26-02-04"
    exp_name = "temp"
    if msi:
        Du = [0.08, 0.14]
        Db = [1.0, 1.0]
        Do = [0.6, 0.5]

    else:
        Du= -0.25243022538467225
        Db = 1.976708049660265
        Do = -0.58078732181440
    par = LogPar(main_dir,nb_epoch,with_ut,with_bg,
                  "prior",opt_mixpar,pal_name,
                  Du,Db,Do,device,fid_loss,chpoint_bg_path,
                 mix_fun_str,noise_org,msi, msi_partial)
    sess = tf.Session()
    ior = InOutVals(main_dir, par.mix_fun_str, par.noise_level, fid_loss, with_bg,
                    pal_name)  # inverse model inputs/outputs
    rpost.create_im_layers(par, ior)
    create_im_layers_test(ior,par)
    ior.read_phi(par.Du, par.Db)
    # store experiment parameters into json file
    # create wrapper class with model variables and placeholders prior models
    m = Model2Var(par, sess, ior.batch_size)
    # create original palimpsest if it is artificial
    if par.pal_name == "art_pal":
        rpost.create_art_palimpsest(m, par, ior, sess)
    y_org_test(ior,par)
    rpost.save_init_img(expdir, par, ior)
    plt.show()


if __name__=="__main__":
    create_pal_test()