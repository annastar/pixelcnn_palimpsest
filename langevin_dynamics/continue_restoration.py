import math
import os
import numpy as np
from pixelcnn_palimpsest.utils.files import json_to_dict
from skimage import io


class LogPar():
    def __init__(self,main_dir,device,restore_dir,restore_epoch):
        self.restore_dir = os.path.join(main_dir,restore_dir)
        d = json_to_dict(os.path.join(self.restore_dir,"par.json"))
        for key in d:
            setattr(self, key, d[key])
        self.nb_epoch = 10000
        self.device = device
        sigma_idx = math.floor(restore_epoch / self.nb_epoch)
        if (restore_epoch+1)%self.nb_epoch!=0:
            self.sigma_idx=sigma_idx
            self.start_epoch = restore_epoch%self.nb_epoch+1
        else:
            self.start_epoch = 0
            self.sigma_idx = sigma_idx+1

        self.restore_epoch = restore_epoch

        if self.pal_name=="art_pal":
            self.data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
            self.data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
        elif self.pal_name=="greek960":
            self.data_dir = os.path.join(main_dir,"datasets",r"Greek_960",r"0086_000084")
        elif self.pal_name=="archi":
            self.data_dir = os.path.join(main_dir, "datasets", r"Archimedes",
                                         r"017r-016v_Arch07r_Sinar",r"nonnormalized")
        if self.ut_model == "discrete":
            self.chpoint_ut_path = os.path.join(main_dir, "training", "generators", "undertext",
                                                "pixelcnn-gray-2023-02-08-04-13")
        elif self.ut_model == "binary":
            self.chpoint_ut_path = os.path.join(main_dir, "training", "generators", "undertext",
                                                "pixelcnn-bin-2022-05-27-18-43")
        self.chpoint_bg_path = os.path.split(self.chpoint_bg_path)[-1]
        self.chpoint_bg_path = os.path.join(main_dir, "training", "generators", "background",
                     self.chpoint_bg_path)#



def restore_res_as_initial(main_dir,par,ior,expdir,restore_epoch):
    if par.with_ut:
        im_ut = io.imread(os.path.join(main_dir,expdir,"ut",str(restore_epoch) + ".png"),as_gray=True)/255.
        ior.init_img_ut = im_ut[np.newaxis, :, :, np.newaxis]
        if par.ut_model=="discrete":
            ior.init_img_ut = ior.init_img_ut*2-1

    if par.with_bg:
        im_bg = io.imread(os.path.join(main_dir,expdir,"bg",str(restore_epoch) + ".png"),as_gray=True)/255.
        ior.init_img_bg = im_bg[np.newaxis, :, :, np.newaxis]*2-1


if __name__=="__main__":
    pass


