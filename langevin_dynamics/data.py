import platform
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf


from pixelcnn_palimpsest.utils.read_images import DataUtility,find_max
import numpy as np
import os
from pixelcnn_palimpsest.utils import read_images
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.utils.files import dict_to_json


class PrepRealPal():
    def __init__(self,data_dir,row_coord,col_coord,ot_fname,
                 rot_angle,mix_fun_str,modalities,bit_depth, band_idxs,
                 max_val=None,adjust_im_scope=True,scale_true=True, calc_Ds=True):
        ###load image
        loader = DataUtility(data_dir, True,modalities)
        self.band_idxs = band_idxs
        self.mix_fun_str = mix_fun_str
        self.max_val = max_val
        self.bit_depth = bit_depth
        self.ordered_fnames = loader.ordered_fnames
        self.band_idx_fname = loader.band_idx_fname
        self.band_idxs_full = [i for i in range(loader.nb_bands)]
        if adjust_im_scope:
            row_coord = loader.adjust_im_scope(row_coord, 192)
            col_coord = loader.adjust_im_scope(col_coord, 192)
        im_out = loader.read_scale_ims_msi_fragment(col_coord=col_coord, row_coord=row_coord,
                                                        scale_true=scale_true, rot_angle=rot_angle)
        if self.max_val is None:
            max_val = 0
            max_value_depth = 1.0
            for ch in range(im_out.shape[-1]):
                max_value_ch = find_max(im_out[:,:,:,ch],max_value_depth)
                if max_value_ch>max_val:
                    max_val=max_value_ch
            self.max_val = max_val


        self.im_unscaled = im_out
        self.im_out = im_out/max_val
        self.im_out = np.clip(self.im_out, 0.0, 1.0)
        self.x_ot = loader.read_scale_overtext_fragment(ot_fname, col_coord=col_coord,
                                                   row_coord=row_coord, scale_true=True)
        if calc_Ds:
            self.calc_Ds(loader)

    def calc_Ds(self,loader):
        self.bg_mean = loader.read_json_mean_refl_val("bg",self.bit_depth)/self.max_val
        self.ut_refl = loader.read_json_mean_refl_val("ut",self.bit_depth)/self.max_val
        self.ot_refl = loader.read_json_mean_refl_val("ot",self.bit_depth)/self.max_val
        self.D_o, self.D_u, self.D_bg = calc_Ds_from_real_pal(self.bg_mean,self.ut_refl,
                                                              self.ot_refl,self.mix_fun_str)
        # pick only relevant bands
        if not self.band_idxs is None:
            if not type(self.band_idxs) is int:
                self.im_out = self.im_out[:, :, :, self.band_idxs]
                self.D_bg = np.array(self.D_bg)[self.band_idxs]
                self.D_u = np.array(self.D_u)[self.band_idxs]
                self.D_o = np.array(self.D_o)[self.band_idxs]
            else:
                self.im_out = self.im_out[:, :, :, self.band_idxs][:,:,:,np.newaxis]
                self.D_bg = [np.array(self.D_bg)[self.band_idxs]]
                self.D_u = [np.array(self.D_u)[self.band_idxs]]
                self.D_o = [np.array(self.D_o)[self.band_idxs]]
        else:
            self.D_bg = np.array(self.D_bg)
            self.D_u = np.array(self.D_u)
            self.D_o = np.array(self.D_o)

        print("Du", self.D_u)
        print("Do", self.D_o)
        print("Db", self.D_bg)

class PrepArchimedes(PrepRealPal):
    def __init__(self,data_dir,row_coord,col_coord,msi,ot_fname,
                 rot_angle,mix_fun_str,msi_partial,adjust_im_scope=True,scale_true=True,modalities=None):

        max_val = 0.875
        self.bit_depth = 8
        self.band_idxs = None
        if msi_partial and msi:
            self.band_idxs = [0,3,10]
        if not msi:
            self.band_idxs = 0
        super().__init__(data_dir, row_coord, col_coord, ot_fname,
                         rot_angle, mix_fun_str,  modalities,
                         bit_depth=8, band_idxs=self.band_idxs, max_val=max_val,
                         adjust_im_scope=adjust_im_scope,scale_true=scale_true)


class PrepGreek960(PrepRealPal):
    def __init__(self, data_dir, row_coord, col_coord, msi, ot_fname,
                 rot_angle, mix_fun_str, msi_partial, modalities=["MB", "WB"]):

        self.band_idxs = None
        if msi_partial and msi:
            self.band_idxs = [0,1,6]#[0,1,2,3,12,18]
        if not msi:
            self.band_idxs = 0
        super().__init__( data_dir, row_coord, col_coord,  ot_fname,
                 rot_angle, mix_fun_str, modalities,bit_depth = 16,
                          band_idxs=self.band_idxs)

class PrepArchiChar(PrepRealPal):
    def __init__(self,data_dir,mix_fun_str,msi,msi_partial):
        self.bit_depth = 16
        self.band_idxs = None
        if not msi:
            self.band_idxs = 0
        if msi_partial:
            self.band_idxs = [0,3,9]
        self.rot_angle = 0
        self.mix_fun_str = mix_fun_str
        self.ot_fname = "overtext_map.png"
        self.modalities = None
        self.adjust_im_scope = False
        super().__init__(data_dir, [0,192],[0,192], self.ot_fname,
                         self.rot_angle, self.mix_fun_str, self.modalities,
                         bit_depth=self.bit_depth, band_idxs=self.band_idxs,
                         adjust_im_scope=self.adjust_im_scope,scale_true=True,calc_Ds=True)

class PrepArtPal():
    def __init__(self,data_dir,data_dir_bg,row_coord,col_coord,fname_ot,
                 fname_ut,with_ut,with_bg):
        """Create art palimpsest"""
        ###data specific parameters
        loader = DataUtility(data_dir, False)

        self.x_ot = loader.read_scale_overtext_fragment(fname_ot, col_coord=col_coord,
                                                   row_coord=row_coord, scale_true=True)
        nrows, ncols = self.x_ot.shape[1],self.x_ot.shape[2]
        if with_ut:
            x_ut = loader.read_scale_ims_fragment(fname_ut, col_coord=col_coord,
                                                   row_coord=row_coord, scale_true=True)
            self.x_ut = 1 - x_ut
        if with_bg:
            self.x_bg = read_images.build_im_background_scaled(data_dir_bg)[:, :nrows, :ncols, :]


    def create_art_palimpsest(self,sess,noise_level,mix_fun_str,bg_scaler,ot_refl,ut_refl,
                              with_bg):
        """Create palimpsest artificially
            sess = tf Session;
            noise_level - noise level;
            with_bg - with background.
         """
        ###reset graph

        nb_ch = len(ut_refl) if type(ut_refl) is list else 1
        mix_fun = eval("mix_model." + mix_fun_str)
        xs_ot_tf = tf.constant(self.im_ot, dtype=tf.float32)
        xs_ut_tf = tf.placeholder(shape=self.im_ut.shape,dtype=tf.float32)
        xs_bg_tf = tf.placeholder(shape=self.im_ut.shape,dtype=tf.float32)

        ys_pred_tf, D_b = mix_fun(undertext=xs_ut_tf, background=xs_bg_tf,
                              overtext=xs_ot_tf, nb_ch=nb_ch, bg_bias=0.0, bg_scaler=bg_scaler,
                              ut_refl=ut_refl, ot_refl=ot_refl)
        sess.run(tf.initialize_all_variables())
        noise_org_tf = tf.constant(np.random.normal(0, noise_level, self.im_ot.shape), dtype=tf.float32,
                                     name="org_noise")
        ymix_tf = ys_pred_tf + noise_org_tf
        if not with_bg:
            ys_org = sess.run(ymix_tf, feed_dict={xs_ut_tf: self.im_ut})
        else:
            ys_org, _ = sess.run([ymix_tf, noise_org_tf],
                                        feed_dict={xs_ut_tf: self.im_ut,
                                                   xs_bg_tf: self.im_bg})
        return ys_org




def calc_Ds_from_real_pal(bg_mean,ut_refl,ot_refl,mix_fun_str):
    bg_mean_dataset = 89.29467879175377 / 255.0  #
    D_bg = [bg_mean[rb] / bg_mean_dataset for rb in range(len(bg_mean))]
    if mix_fun_str == "sep_layers_bg_not_fn":
        D_u = [ut_refl[rt] - bg_mean[rt] for rt in range(len(ut_refl))]
        D_o = [ot_refl[rt] - bg_mean[rt] for rt in range(len(ot_refl))]
    elif mix_fun_str == "sep_layers_bg_ind_from_ot_not_fn":
        D_u = [ut_refl[rt] - bg_mean[rt] for rt in range(len(ut_refl))]
        D_o = [ot_refl[rt] for rt in range(len(ot_refl))]
    elif mix_fun_str == "sep_layers_all_ind_bg_not_fn":
        D_u = [ut_refl[rt] for rt in range(len(ut_refl))]
        D_o = [ot_refl[rt] for rt in range(len(ot_refl))]
    else:
        raise IOError("Does know {} mixing function".format(mix_fun_str))
    return np.array(D_o),np.array(D_u),np.array(D_bg)

def plot_msi(ims):
    fig,ax = plt.subplots(1,ims.shape[-1])
    for i in range(ims.shape[-1]):
        ax[i].imshow(ims[0,:,:,i],cmap="gray")
    plt.show()

def extract_mean_ref(ims,coord,name,data_dir,ordered_fnames,bit_depth):
    """
    Calc mean relf of mater based on pixel coord
    ims - multispectral image
    bg_coord,ut_coord,ot_coord - multiple pixels [[start_row,end_row],[start_col,end_col]],
                                or one pixel [row,col]
    """
    if type(coord[0]) == list and type(coord[1]) == list:
        mean = np.mean(ims[:,coord[0][0]:coord[0][1],coord[1][0]:coord[1][1],:],axis=(0,1,2))
    elif type(coord[0]) == list and type(coord[1]) != list:
        mean = np.mean(ims[:, coord[0][0]:coord[0][1], coord[1], :], axis=(0,1,2))
    elif type(coord[0]) != list and type(coord[1]) == list:
        mean = np.mean(ims[:, coord[0], coord[1][0]:coord[1][1], :], axis=(0,1,2))
    else:
        mean = ims[0, coord[0], coord[1], :]
    print(name,mean)
    if "ut" in name:
        fname = "undertext_mean.json"
    elif "bg" in name:
        fname = "background_mean.json"
    elif "ot" in name:
        fname = "overtext_mean.json"
    if bit_depth==16:
        mean = mean*65535.0
    elif bit_depth==8:
        mean = mean*255.0
    d={}
    for i in range(len(mean)):
        for key,value in ordered_fnames.items():
            if value==i:
                band_fname = key
        d[band_fname]=mean[i]
    dict_to_json(os.path.join(data_dir,fname),d,False)
    return mean

def create_art_pal(main_dir,mix_fun_str,D_o,D_u,D_bg):
    data_dir_pal = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
    fname_ot = r"line_4_moved.png"
    fname_ut = r"line_27.png"
    loader = DataUtility(data_dir, False)
    mask = loader.read_scale_overtext_fragment(fname_ot, col_coord=[0, 3 * 192],
                                               row_coord=[0, 192], scale_true=True)
    bg = read_images.build_im_background_scaled(data_dir_pal)[:, :mask.shape[1], :mask.shape[2], :]
    print("Mean bg", np.mean(bg))
    im_ut = loader.read_scale_ims_fragment(fname_ut, col_coord=[0, 3 * 192],
                                           row_coord=[0, 192], scale_true=True)
    im_ut = 1 - im_ut
    if mix_fun_str=="sep_layers_bg_not_fn":
        art_pal = mask * D_o + im_ut * D_u + D_bg * bg - mask * im_ut * D_u
    elif mix_fun_str == "sep_layers_bg_ind_from_ot_not_fn":
        art_pal = mask * D_o + im_ut * D_u + D_bg * bg*(1-mask) - mask * im_ut * D_u
    elif mix_fun_str == "sep_layers_all_ind_bg_not_fn":
        art_pal = mask * D_o + im_ut * D_u + D_bg * bg * (1 - mask)*(1 - im_ut) - mask * im_ut * D_u
    noise = np.random.normal(0, 1e-1,art_pal.shape)
    art_pal = art_pal+noise
    return art_pal

def plot_art_pal_vs_real(art_pal,real_pal):
    nb_bands = real_pal.shape[-1]
    fig, ax = plt.subplots(nb_bands, 2)
    if nb_bands == 1:
        ax[0].imshow(np.squeeze(real_pal[:, :, :, 0]), cmap="gray", vmin=0.0, vmax=1.0)
        ax[1].imshow(np.squeeze(art_pal[:, :, :, 0]), cmap="gray", vmin=0.0, vmax=1.0)
    else:
        for i in range(nb_bands):
            print("Max val band{}={}".format(i, np.amax(real_pal[:, :, :, i])))
            ax[i, 0].imshow(np.squeeze(real_pal[:, :, :, i]), cmap="gray", vmin=0.0, vmax=1.0)
            ax[i, 1].imshow(np.squeeze(art_pal[:, :, :, i]), cmap="gray", vmin=0.0, vmax=1.0)
    plt.show()


def read_Greek960():
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    data_dir_pal = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"0086_000084+MB625Rd_007_F_thresh.png"
    row_coord = [2300, 2300 + 192]
    col_coord = [1800, 2914]
    rot_angle = -90
    mix_fun_str = r"sep_layers_bg_not_fn"
    msi = True
    data = PrepGreek960(data_dir_pal, row_coord, col_coord, msi, fname_ot,
                        rot_angle, mix_fun_str=mix_fun_str,msi_partial=True,modalities=["MB","WB"])
    generate_rgb_combinations(data.im_out)
    art_pal = create_art_pal(main_dir,mix_fun_str,data.D_o,data.D_u,data.D_bg)
    plot_art_pal_vs_real(art_pal,data.im_out)

def read_Archimedes(main_dir):
    main_dir = r"C:\Data\PhD\bss_autoreg_palimpsest"
    data_dir = os.path.join(main_dir, "datasets", r"Archimedes", r"017r-016v_Arch07r_Sinar",r"nonnormalized")
    fname_ot = r"overtext_map.png"
    row_coord = [8468, 8468 + 192]
    col_coord = [1082, 1082+4*192]
    rot_angle = 0
    mix_fun_str = "sep_layers_bg_not_fn"
    msi = True
    data = PrepArchimedes(data_dir, row_coord, col_coord, msi, fname_ot,
                         rot_angle,mix_fun_str,msi_partial=True)
    art_pal = create_art_pal(main_dir,mix_fun_str,data.D_o,data.D_u,data.D_bg)
    plot_art_pal_vs_real(art_pal,data.im_out)


def plot_vec(D_o,D_u,D_bg):
    from math import factorial as fc
    #fig,ax = plt.subplots(int(fc(len(D_o))/(2*fc(len(D_o)-2))))
    count=0
    for i in range(len(D_o)):
        for j in range(i+1,len(D_o)):
            fig, ax = plt.subplots(1)
            ax.plot([0,D_o[i]],[0,D_o[j]],label ="D_o_band_{}_{}".format(i,j))
            ax.plot([0,D_u[i]],[0,D_u[j]], label="D_u_band_{}_{}".format(i,j))
            ax.plot([0,D_bg[i]],[0,D_bg[j]], label="D_bg_band_{}_{}".format(i,j))
            ax.legend()


def generate_rgb_combinations(pal):
    from math import factorial as fc
    nb_bands = pal.shape[-1]
    nb_combinations = int(fc(nb_bands) / (fc(3) * fc(nb_bands - 3)))
    count = 0
    for i in range(nb_bands):
        for j in range(i + 1,nb_bands):
            for q in range(j + 1, nb_bands):
                fig, ax = plt.subplots(1)
                im = np.stack([pal[0, :, :, i], pal[0, :, :, j], pal[0, :, :, q]], axis=2)
                ax.imshow(im, vmin=0.0,vmax=1.0)
                ax.set_title("Bands are R_{},G_{},B_{}".format(i,j,q))
                count+=1
                if count==nb_combinations:
                    break
                plt.show()

def read_Archi_1char():
    dirpath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Archimedes\test_obscured_MSI"
    mix_fun_str = "sep_layers_bg_not_fn"
    main_dir =  r"C:\Data\PhD\bss_autoreg_palimpsest"
    msi = True
    msi_partial = True
    nb_char = 0
    for letter in os.listdir(dirpath):
        if ".txt" in letter:
            continue
        nb_char +=len(os.listdir(os.path.join(dirpath, letter)))
        for sample in os.listdir(os.path.join(dirpath,letter)):
            datadir = os.path.join(dirpath,letter,sample)
            data = PrepArchiChar(datadir,mix_fun_str,msi,msi_partial)
            #plot_vec(data.D_o, data.D_u, data.D_bg)
            art_pal = create_art_pal(main_dir,data.mix_fun_str, data.D_o, data.D_u, data.D_bg)
            generate_rgb_combinations(data.im_out)

            plot_art_pal_vs_real(art_pal, data.im_out)
    print("Nb of characters:",nb_char )

if __name__=="__main__":
    #read_Archimedes()
    read_Greek960()
    #read_Archi_1char()
