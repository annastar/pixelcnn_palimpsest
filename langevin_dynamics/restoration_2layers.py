import platform
import os
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
import imageio
import matplotlib.pyplot as plt
from skimage.io import imsave,imread
import numpy as np
from pixelcnn_palimpsest.utils import files,plotting
import time
from pixelcnn_palimpsest.langevin_dynamics.prior import restore_model_at_noise_level
from pixelcnn_palimpsest.utils.save_results import record_loss,record_result_ims
from pixelcnn_palimpsest.langevin_dynamics.posterior import ld_1_var_grad_posterior,ld_add_noise_to_grads
from pixelcnn_palimpsest.langevin_dynamics import data
from pixelcnn_palimpsest.utils.read_phi import get_phi
from time import strftime, gmtime


class LogDirs():
    def __init__(self,main_dir,main_log_dir,exp_name,par,test_time=True):
        if not par.restore_epoch is None:
            self.expdir = os.path.join(par.restore_dir,"restore_"+str(par.restore_epoch))
        else:
            if test_time:
                logdir = os.path.join(main_dir, r"training")
                expdir = os.path.join(logdir,"langevin",exp_name)
                if par.with_bg:
                    expdir += "with_bg"
                else:
                    expdir += "without_bg"
                timestamp = strftime("%Y-%m-%d-%H-%M", gmtime())
                expdir += "_t_" + timestamp

            else:
                expdir = os.path.join(main_log_dir,exp_name)
        self.expdir = expdir
        files.create_dir(self.expdir)
        self.savedir_restored = os.path.join(self.expdir, "restored")
        files.create_dir(self.savedir_restored)
        if par.nb_ch>1:
            for i in range(par.nb_ch):
                savedir_restored_band = os.path.join(self.expdir, "restored","band"+str(i))
                files.create_dir(savedir_restored_band)
        if par.with_ut:
            self.savedir_ut = os.path.join(self.expdir, "ut")
            files.create_dir(self.savedir_ut)
            self.savedir_grads_ut = os.path.join(self.expdir, "grads_ut")
            files.create_dir(self.savedir_grads_ut)
        if par.with_bg:
            self.savedir_bg = os.path.join(self.expdir, "bg")
            files.create_dir(self.savedir_bg)
            self.savedir_grads_bg = os.path.join(self.expdir, "grads_bg")
            files.create_dir(self.savedir_grads_bg)
        if par.warm_up:
            self.logdir_warmup = os.path.join(self.expdir,"warm_up")
            files.create_dir(self.logdir_warmup)


def create_art_palimpsest(m,par,ior,sess):
    """Create palimpsest artificially"""
    nb_ch = par.nb_ch if par.fid_loss=="mvn" else 1
    org_imgs_ot = plotting.split_image(ior.mask[0, :, :, 0], par.im_size)
    batch_size = org_imgs_ot.shape[0]
    feed_dict = {m.xs_ot_tf:org_imgs_ot}
    if par.with_ut:
        org_imgs_ut = plotting.split_image(ior.org_img_ut[0, :, :, 0], par.im_size)
        feed_dict[m.xs_ut_tf] = org_imgs_ut
    if par.with_bg:
        org_imgs_bg = plotting.split_image(ior.org_img_bg[0, :, :, 0], par.im_size)
        feed_dict[m.xs_prior_bg_tf] = 2 * org_imgs_bg - 1
    noise_org_tf = tf.constant(np.random.normal(0, par.noise_level,
                                                [batch_size,
                                                 par.im_size, par.im_size, nb_ch]),
                               dtype=tf.float32,
                               name="org_noise")
    if par.fid_loss=="uvn" and par.nb_ch>1:
        ys_org = []
        for i in range(par.nb_ch):
            ymix_tf = m.ys_pred_tf[str(i)] + noise_org_tf
            y = sess.run(ymix_tf, feed_dict)
            y = plotting.stich_image(y[:, :, :, 0], ior.tile_shape)
            ys_org.append(y[np.newaxis,:,:,np.newaxis])
        ys_org = np.concatenate(ys_org,axis=3)
    else:
        ymix_tf = m.ys_pred_tf + noise_org_tf
        ys_org = sess.run(ymix_tf, feed_dict)
    ys_org = np.clip(ys_org,a_min=0.0,a_max=1.0)
    if par.nb_ch>1:
        y_org_msi = []
        for ch in range(ys_org.shape[-1]):
            y_org_ch = plotting.stich_image(ys_org[:, :, :, ch], ior.tile_shape)
            y_org_msi.append(y_org_ch)
        ior.y_org = np.concatenate(y_org_msi,axis=2)[np.newaxis,:,:,:]
    else:
        ior.y_org = plotting.stich_image(ys_org[:, :, :, 0], ior.tile_shape)[np.newaxis,:,:,np.newaxis]

def save_init_img(expdir,par,ior):
    if par.nb_ch>1:
        for ch in range(ior.y_org.shape[-1]):
            imsave(os.path.join(expdir, "corrupt_im_band_{}.png".format(ch)),
                   np.squeeze((np.squeeze(ior.y_org[:,:,:,ch])*255).astype(np.uint8)))
    else:
        imsave(os.path.join(expdir, "corrupt_im.png"), np.squeeze(ior.y_org))
    if par.pal_name=="art_pal":
        imsave(os.path.join(expdir, "org_im.png"), np.squeeze(ior.org_im))
    imsave(os.path.join(expdir, "mask.png"), np.squeeze(ior.mask))
    ###create init image
    if par.with_ut:
        if par.ut_model=="discrete":
            imsave(os.path.join(expdir, "init_img.png"), np.squeeze(ior.init_img_ut+1)/2)
        elif par.ut_model=="binary":
            imsave(os.path.join(expdir, "init_img.png"), np.squeeze(ior.init_img_ut))
    if par.with_bg:
        imsave(os.path.join(expdir, "init_img_bg.png"), np.squeeze((ior.init_img_bg+1)/2))


def create_im_layers(par,ior):
    if par.pal_name == "art_pal":
        pal = data.PrepArtPal(par.data_dir, par.data_dir_bg,
                              par.row_coord, par.col_coord, par.fname_ot,
                              par.fname_ut, par.with_ut, par.with_bg)
        ### add original images to io
        if par.with_ut:
            ior.org_img_ut = pal.x_ut
        if par.with_bg:
            ior.org_img_bg = pal.x_bg
            ior.init_img_bg = 0.5*np.ones_like(pal.x_bg)
    else:
        if par.pal_name == "greek960":
            pal = data.PrepGreek960(par.data_dir,
                                    par.row_coord, par.col_coord,
                                    par.msi, par.fname_ot,
                                    par.rot_angle, par.mix_fun_str,
                                    par.msi_partial)
        elif par.pal_name == "archi":
            pal = data.PrepArchimedes(par.data_dir,
                                      par.row_coord, par.col_coord,
                                      par.msi, par.fname_ot,
                                      par.rot_angle, par.mix_fun_str, msi_partial=par.msi_partial)
        elif par.pal_name == "archi_chars":
            pal = data.PrepArchiChar(par.data_dir, par.mix_fun_str, par.msi, par.msi_partial)
        ior.y_org = pal.im_out
        par.Du = pal.D_u.tolist() if type(pal.D_u) is np.ndarray else pal.D_u
        par.Do = pal.D_o.tolist() if type(pal.D_o) is np.ndarray else pal.D_o
        par.Db = pal.D_bg.tolist() if type(pal.D_bg) is np.ndarray else pal.D_bg

        if par.with_bg:
            init_img_bg = (pal.im_out[:, :, :, 0] - par.Do[0]*pal.x_ot[:,:,:,0]) / par.Db[0]
            init_img_bg = init_img_bg[:, :, :, np.newaxis] * 2 - 1
            ior.init_img_bg = init_img_bg
    ior.mask = pal.x_ot
    ### create init images
    if par.with_ut:
        #init_img_ut = (pal.im_out[:, :, :, 0] - par.Do[0] * pal.x_ot[:, :, :, 0])
        #init_img_ut = 1-init_img_ut[:, :, :, np.newaxis]/np.amax(init_img_ut)
        init_img_ut = np.random.choice([0.0,1.0],size=ior.mask.shape,p=[0.9,0.1])
        #init_img_ut = np.zeros_like(ior.mask)
        if par.ut_model=="discrete":
            ior.init_img_ut = 2*init_img_ut - 1
        elif par.ut_model=="binary":
            ior.init_img_ut = init_img_ut
    ior.size_y, ior.size_x = ior.mask.shape[1], ior.mask.shape[2]
    ior.tile_shape = (ior.size_y // par.im_size, ior.size_x // par.im_size)
    ior.left_edges = create_edges(ior.size_x, par.im_size)
    ior.batch_size = len(ior.left_edges)


def reduce_sigma(max_ll,counter,m,ior,par,sigma_ut,sigma_bg,sess):
    """Reduce sigma if likelihood is not growing for 100 epoch"""
    feed_dict = {m.xs_ut_tf: ior.x_ut, m.sigma_i_ut_tf: sigma_ut, m.ys_org_tf: ior.ys_org}
    if par.apprx:
        feed_dict[m.phi_ut_tf] = ior.phi_ut
    if par.with_bg:
        feed_dict.update({m.xs_prior_bg_tf: ior.x_bg, m.sigma_i_bg_tf: sigma_bg})
        feed_dict[m.phi_bg_tf] = ior.phi_bg
    cur_ll_list = sess.run(m.loss_ll_ut_tf, feed_dict)
    cur_ll = np.mean(cur_ll_list)
    change_sigma=False
    if cur_ll>max_ll:
        max_ll=cur_ll
        counter=0
    else:
        counter+=1
    if counter>300:
        change_sigma=True
        counter=0
        max_ll=cur_ll
    return change_sigma,max_ll,counter

def save_im(im,title,epoch,logdir):
    imageio.imsave(os.path.join(logdir,title+"_"+str(epoch)+".png"),np.squeeze(im/np.amax(im)))

def langevin_dynamics_1step(m,par,ior,sess,ys_org,
                            phi_ut,phi_bg,mode,
                            grad_ll_ut_tf,grad_ll_bg_tf):
    nb_patchs_in_im = ior.size_x//par.im_size
    if mode == "posterior":
        if par.with_ut:
            grads_ut = ld_1_var_grad_posterior(sess, m.xs_prior_ut_tf,ior.xs_ut,m.xs_prior_bg_tf,
                                                            ior.xs_bg,m.sigma_tf,
                                                            ior.sigma_ut,m.phi_tf, phi_ut,
                                                            m.grad_prior_ut_tf, grad_ll_ut_tf,
                                            m.ys_org_tf, ys_org,
                                            m.xs_ot_tf, ior.xs_ot)
            grads_ut = aver_grads(ior,par,grads_ut)
            xs_ut = plotting.stich_image(ior.xs_ut[:nb_patchs_in_im,:,:,0],[1,nb_patchs_in_im])[np.newaxis,:,:,np.newaxis]
            xs_ut = ld_add_noise_to_grads(grads_ut, ior.eta_ut, xs_ut, grads_ut.shape)
            ior.xs_ut = split_image(xs_ut,ior.batch_size,ior.left_edges,par.im_size)
            if par.ut_model=="discrete":
                ior.xs_ut = np.clip(ior.xs_ut,a_min=-(1.0+ior.sigma_ut),a_max=(1.0+ior.sigma_ut))
            elif par.ut_model=="binary":
                ior.xs_ut = np.clip(ior.xs_ut, a_min=-(0.0 + ior.sigma_ut), a_max=(1.0 + ior.sigma_ut))
            else:
                print("No such model")
        if par.with_bg:
            for _ in range(par.bg_repeats):
                grads_bg = ld_1_var_grad_posterior(sess, m.xs_prior_ut_tf, ior.xs_ut, m.xs_prior_bg_tf,
                                                   ior.xs_bg, m.sigma_tf,
                                                   ior.sigma_bg, m.phi_tf, phi_bg,
                                                   m.grad_prior_bg_tf, grad_ll_bg_tf,
                                                   m.ys_org_tf, ys_org,
                                                   m.xs_ot_tf, ior.xs_ot)
                grads_bg = aver_grads(ior,par,grads_bg)
                xs_bg = plotting.stich_image(ior.xs_bg[:nb_patchs_in_im, :, :, 0], [1, nb_patchs_in_im])[np.newaxis, :, :, np.newaxis]
                xs_bg = ld_add_noise_to_grads(grads_bg, ior.eta_bg, xs_bg, grads_bg.shape)
                ior.xs_bg = split_image(xs_bg, ior.batch_size, ior.left_edges, par.im_size)
            ior.xs_bg = np.clip(ior.xs_bg, a_min=-(1.0+ior.sigma_bg), a_max=(1.0+ior.sigma_bg))

    elif mode == "bg_warm_up":
        grads_bg = ld_1_var_grad_posterior(sess, m.xs_prior_ut_tf, ior.xs_ut, m.xs_prior_bg_tf,
                                           ior.xs_bg, m.sigma_tf,
                                           ior.sigma_bg, m.phi_tf, phi_bg,
                                           m.grad_prior_bg_tf, grad_ll_bg_tf,
                                           m.ys_org_tf, ys_org,
                                           m.xs_ot_tf, ior.xs_ot)
        grads_bg = aver_grads(ior,par,grads_bg)
        xs_bg = plotting.stich_image(ior.xs_bg[:nb_patchs_in_im, :, :, 0], [1, nb_patchs_in_im])[np.newaxis, :, :,
                np.newaxis]
        xs_bg = ld_add_noise_to_grads(grads_bg, ior.eta_bg, xs_bg, grads_bg.shape)
        ior.xs_bg = split_image(xs_bg, ior.batch_size, ior.left_edges, par.im_size)
        ior.xs_bg = np.clip(ior.xs_bg, a_min=-(1.0 + ior.sigma_bg), a_max=(1.0 + ior.sigma_bg))


def restore_prior_models_at_noise_step(par,sigma,sess,mode):

    if mode=="ut":
        restore_model_at_noise_level(par, sess, sigma, par.rest_epoch_sigma_ut[sigma], "ut")
    elif mode=="bg":
        restore_model_at_noise_level(par, sess, sigma, par.rest_epoch_sigma_bg[sigma], "bg")
    else:
        raise Exception("No such mode as {}, model is not restored".format(mode))


def calc_ld_opt_par(sigma_list,delta,idx):
    """Calc Langevin Danemics optimization parameters
    such as, eta, sigma, and likelihood std"""
    sigma = sigma_list[-(idx + 1)]
    eta = delta * ((sigma ** 2) / (sigma_list[0] ** 2))
    return sigma,eta

def calc_likel_std(par, phi_list,sigma, m, sess,mode):
    """Calc phi parameter, which represents likelihoods std"""
    if par.fid_loss == "uvn":
        if mode == "ut":
            D = sess.run(m.Du_tf)
        elif mode == "bg":
            D = sess.run(m.Db_tf)
        else:
            raise Exception("No such mode as {}, "
                            "could not get Phi for this variable".format(mode))
        phi = get_phi(phi_list[str(sigma)], D)
    else:
        phi = phi_list[str(sigma)]
    return phi

def check_sigmas_list_length(par):
    """Check if bg and ut are ordered and are of the same length"""
    if len(par.sigma_list_ut)!=len(par.sigma_list_bg):
        raise Exception("Sigma ut and bg should be the same length")
    par.sigma_list_ut = sorted(par.sigma_list_ut)
    par.sigma_list_bg = sorted(par.sigma_list_bg)

def ld_optimization(sess,par,mode,m,ior,log_dirs):
    """
    Langevin dynamics optimization algorithm
    Args:
        sess - tf Session
        shape - tuple, image shape
        xtf - input image tensor
        sigmatf - smoothing noise std tf placeholder
        grad_post_tf - posterior gradient placeholder
        phi_tf - phi tf placeholder
        phi - list of std values of smoothed likelihood p(y,\tilde(x))
        """

    #split image
    check_sigmas_list_length(par)
    split_images(par,ior)
    ior.start_time = time.time()
    for idx in range(par.sigma_idx,len(par.sigma_list_ut)):
        # set all needed LD parameters
        if par.with_ut:
            ior.sigma_ut,ior.eta_ut = calc_ld_opt_par(par.sigma_list_ut,par.delta_ut,idx)
            restore_prior_models_at_noise_step(par, ior.sigma_ut, sess, "ut")
            ior.phi_ut = calc_likel_std(par,ior.phi_list_ut,ior.sigma_ut,m,sess,"ut")
        if par.with_bg:
            ior.sigma_bg, ior.eta_bg = calc_ld_opt_par(par.sigma_list_bg,par.delta_bg, idx)
            restore_prior_models_at_noise_step(par,ior.sigma_bg,sess,"bg")
            ior.phi_bg = calc_likel_std(par,ior.phi_list_bg,ior.sigma_bg, m, sess, "bg")
        # to do bg warm-up before starting optimization at new sigma level
        for t in range(par.start_epoch,par.nb_epoch):

            ior.cur_epoch = idx*par.nb_epoch+t
            if idx>0 and t == 0:
                if par.warm_up:
                    bg_warmup(m, par, ior, log_dirs, sess, idx)
            if par.nb_ch>1 and par.fid_loss == "uvn":
                xs_ut_sum = 0
                xs_bg_sum = 0
                for ch in range(par.nb_ch):
                    langevin_dynamics_1step(m,par,ior,sess,
                                                    ior.ys_org[:,:,:,ch][:,:,:,np.newaxis],ior.phi_ut[ch],
                                                    ior.phi_bg[ch],mode,
                                                    m.grad_ll_ut_tf[str(ch)],m.grad_ll_bg_tf[str(ch)])
                    xs_ut_sum += ior.xs_ut
                    xs_bg_sum += ior.xs_bg
                ior.xs_ut=xs_ut_sum/par.nb_ch
                ior.xs_bg=xs_bg_sum/par.nb_ch
            else:
                langevin_dynamics_1step(m, par, ior,sess,
                                       ior.ys_org, ior.phi_ut,
                                       ior.phi_bg, mode,
                                       m.grad_ll_ut_tf,
                                       m.grad_ll_bg_tf)
            if (t + 1) % par.record_imgs_freq == 0:
                org_tile = [1,ior.size_x//par.im_size]
                ior.init_img_ut = plotting.stich_image(ior.xs_ut[:org_tile[1],:,:,0],org_tile)[np.newaxis,:,:,np.newaxis]
                ior.init_img_bg = plotting.stich_image(ior.xs_bg[:org_tile[1],:,:,0],org_tile)[np.newaxis,:,:,np.newaxis]
                record_result_ims(idx*par.nb_epoch+t, sess, log_dirs,par,m,ior)
                record_loss(idx*par.nb_epoch+t, sess, log_dirs.expdir,par,m,ior,True)
                # restart the timer
                ior.start_time = time.time()
        par.start_epoch = 0
    record_result_ims(idx * par.nb_epoch + t, sess, log_dirs, par, m, ior)

def bg_warmup(m,par,ior,log_dirs,sess,idx):
    nb_iter = 1000
    for t in range(nb_iter):
        if par.nb_ch > 1 and par.fid_loss == "uvn":
            xs_bg_sum = 0

            for ch in range(par.nb_ch):
                langevin_dynamics_1step(m, par, ior, sess,
                                               ior.ys_org[:, :, :, ch][:, :, :, np.newaxis], ior.phi_ut[ch],
                                               ior.phi_bg[ch], "bg_warm_up",
                                               m.grad_ll_ut_tf[str(ch)], m.grad_ll_bg_tf[str(ch)])

                xs_bg_sum += ior.xs_bg
            ior.xs_bg = xs_bg_sum / par.nb_ch
        else:
            langevin_dynamics_1step(m, par, ior, sess,
                                               ior.ys_org, ior.phi_ut,
                                               ior.phi_bg, "bg_warm_up",
                                               m.grad_ll_ut_tf, m.grad_ll_bg_tf)
        #if t%100==0:
        #    record_loss(idx * nb_iter + t, sess, log_dirs.logdir_warmup, par, m, ior)



def create_edges(nb_cols,im_size):
    left_edges = np.arange(0,nb_cols,im_size)
    if len(left_edges)>1:
        left_edges_a = np.arange(im_size//2,nb_cols-im_size//2,im_size)
        left_edges = np.concatenate([left_edges,left_edges_a])
    return left_edges

def split_images(par,ior):
    if par.with_ut:
        ior.xs_ut = split_image(ior.init_img_ut,ior.batch_size,ior.left_edges,par.im_size)
    else:
        ior.xs_ut = [None]*ior.batch_size
    if par.with_bg:
        ior.xs_bg = split_image(ior.init_img_bg,ior.batch_size,ior.left_edges,par.im_size)
    else:
        ior.xs_bg = [None]*ior.batch_size
    ior.xs_ot = split_image(ior.mask,ior.batch_size,ior.left_edges,par.im_size)
    ior.ys_org = split_image(ior.y_org,ior.batch_size,ior.left_edges,par.im_size)


def split_image(im,batch_size,left_edges,im_size):
    im_rep = [im[0]] * batch_size
    right_edges = (np.array(left_edges) + im_size).tolist()
    ims = list(map(cut_im, im_rep, left_edges, right_edges))
    return np.array(ims)

def cut_im(im,l_edge,r_edge):
    im_cut = im[ :, l_edge:r_edge, :]
    return im_cut

def aver_grads(ior,par,grads):
    first_batch = ior.size_x//par.im_size
    grads_n = plotting.stich_image(grads[:first_batch,:,:,0],[1,first_batch])
    if first_batch>1 and len(grads)>first_batch:
        grads_a = plotting.stich_image(grads[first_batch:,:,:,0],[1,len(grads)-first_batch])
        grads_n[:,par.im_size//2:-par.im_size//2]+=grads_a
        grads_n[:,par.im_size//2:-par.im_size//2]/=2
    return grads_n[np.newaxis,:,:,np.newaxis]


class Inline(object):
    pass

def check_if_grad_aver_works_corr():
    par = Inline
    par.im_size = 64
    ior = Inline
    ior.mask = imread(r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\with_bg_t_2023-01-13-02-31\mask.png",as_gray=True)/255.0
    ior.size_x = ior.mask.shape[1]
    ior.mask = ior.mask[np.newaxis,:,:,np.newaxis]
    left_edges = create_edges(ior.size_x,par.im_size)
    grads = split_image(ior.mask,len(left_edges),left_edges,par.im_size)
    grads = aver_grads(ior,par,grads)
    plt.figure()
    plt.imshow(np.squeeze(grads),cmap="gray")
    plt.figure()
    plt.title("Difference btw org. and restored version")
    plt.imshow(np.squeeze(ior.mask - grads), cmap="gray")
    if np.sum(ior.mask - grads)!=0:
        raise ValueError("The restored image do not correspond to original")
    else:
        print("The averaging works correctly")
    plt.show()


