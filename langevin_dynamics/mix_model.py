import platform
if platform.release()=='5.10.176-1.el7.x86_64':
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
import numpy as np



def mixing_opt_dens(**kwargs):
    with tf.variable_scope("MixingNet", reuse=tf.AUTO_REUSE):
        undertext = kwargs["undertext"]
        overtext = kwargs["overtext"]
        shape = (1,undertext.get_shape()[1], undertext.get_shape()[2],1)
        undertext = tf.reshape(undertext, shape=[-1,kwargs["nb_ch"]])
        overtext = tf.reshape(overtext, shape=[-1,kwargs["nb_ch"]])
        overlap = tf.multiply(overtext, undertext)
        D_u = tf.get_variable("D_u", [kwargs["nb_ch"]], initializer=tf.constant_initializer(8.477121254719663))
        D_o = tf.get_variable("D_o", [kwargs["nb_ch"]], initializer=tf.constant_initializer(8.477121254719663))
        D_obs = tf.multiply(D_u, undertext) + tf.multiply(D_o,overtext)
        D_obs = tf.reshape(D_obs, shape=shape)
        return D_obs



def mixing_opt_dens_olap(**kwargs):
    with tf.variable_scope("MixingNet", reuse=tf.AUTO_REUSE):
        undertext = kwargs["undertext"]
        overtext = kwargs["overtext"]
        #undertext = tf.cond(tf.reduce_min(kwargs["undertext"]) < 0, lambda: (kwargs["undertext"] + 1) / 2, lambda: kwargs["undertext"])
        #overtext = tf.cond(tf.reduce_min(kwargs["overtext"]) < 0, lambda: (kwargs["overtext"] + 1) / 2, lambda: kwargs["overtext"])
        shape = (1,undertext.get_shape()[1], undertext.get_shape()[2],1)
        undertext = tf.reshape(undertext, shape=[-1,kwargs["nb_ch"]])
        overtext = tf.reshape(overtext, shape=[-1,kwargs["nb_ch"]])
        overlap = tf.multiply(overtext, undertext)
        D_u = tf.get_variable("D_u", [kwargs["nb_ch"]], initializer=tf.constant_initializer(8.477121254719663))
        D_o = tf.get_variable("D_o", [kwargs["nb_ch"]], initializer=tf.constant_initializer(8.477121254719663))
        scale_ou_on = tf.stop_gradient(tf.get_variable("scale_ou_on", [], initializer=tf.constant_initializer(0)))
        scale_ou = tf.get_variable("scale_overlap", [kwargs["nb_ch"]],
                                        initializer=tf.constant_initializer(0.0))
        scale_ou = tf.cond(tf.math.equal(scale_ou_on,1),lambda: scale_ou, lambda: 0.0)
        D_obs = tf.multiply(D_u, undertext) + tf.multiply(D_o,overtext) + tf.multiply(scale_ou,overlap)
        D_obs = tf.reshape(D_obs, shape=shape)
        return D_obs

def dense(x_,num_units,name, nonlinearity=None,**kwargs):
    ''' fully connected layer '''
    with tf.variable_scope(name):
        if kwargs["w_pos"]:
            init_v = tf.keras.initializers.RandomUniform(minval=0.0, maxval=0.1)
        else:
            init_v = tf.keras.initializers.RandomUniform(minval=-0.3, maxval=0.0)
        init_b = tf.keras.initializers.RandomUniform(minval=0.3, maxval=1.)#tf.constant_initializer(1.)
        V = tf.get_variable("w", shape=[int(x_.get_shape()[1]),num_units], dtype=tf.float32, initializer=init_v, regularizer=None)
        b = tf.get_variable('b', shape=[num_units], dtype=tf.float32, initializer=init_b, trainable=True)
        # use weight normalization (Salimans & Kingma, 2016)
        x = tf.matmul(x_, V) + tf.reshape(b, [1, num_units])
        # apply nonlinearity
        if nonlinearity is not None:
            x = nonlinearity(x)
    return x

def mixing_nn_conv_1d(**kwargs):
    with tf.variable_scope("MixingNet", reuse=tf.AUTO_REUSE):
        if kwargs["overtext"] is None:
            source_images = tf.concat([kwargs["undertext"], kwargs["background"]], 3)
        else:
            source_images = tf.concat([kwargs["undertext"], kwargs["background"],kwargs["overtext"]], 3)

        source_images = tf.reshape(source_images, shape=[-1,3,1])

        inv1 = tf.layers.conv1d(source_images,36, 3,padding='same',name="mix_h1",activation='relu',
                                kernel_initializer=tf.truncated_normal_initializer(stddev=0.2))
        inv2 = tf.layers.conv1d(inv1, kwargs["nb_ch"], 3,padding='valid',name="mix_h2",activation=None,
                                kernel_initializer=tf.truncated_normal_initializer(stddev=0.2))
        if kwargs["reshape"]:
            inv2 = tf.reshape(inv2, shape=[-1, kwargs["image_size"],kwargs["image_size"], kwargs["nb_ch"]])
    return inv2

def mixing_nn_1d(**k):
    """1D mixing model"""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        if k["background"] is None and k["overtext"] is None:
            input = k["undertext"]
            nb_layers = 1
        elif k["background"] is None:
            input = tf.concat([k["undertext"], k["overtext"]], 3)
            nb_layers = 2
        elif k["overtext"] is None:
            input = tf.concat([k["undertext"], k["background"]], 3)
            nb_layers = 2
        else:
            input = tf.concat([k["undertext"], k["background"], k["overtext"]], 3)
            nb_layers = 3
        input = tf.reshape(input, shape=[-1, nb_layers])
        mix1 = tf.layers.dense(input, 32, activation="relu",kernel_initializer=tf.constant_initializer(1))#dense(input,32,"mix1",tf.nn.relu,w_pos=False)
        mix1 = tf.layers.dense(mix1, 32, activation="relu",
                               kernel_initializer=tf.truncated_normal_initializer(stddev=0.2))
        mix2 = tf.layers.dense(mix1, k["nb_ch"], activation=None,kernel_initializer=tf.constant_initializer(1))#dense(mix1,k["nb_ch"],"mix2",None,w_pos=True)#
        mix2 = tf.reshape(mix2, shape=[-1, k["undertext"].get_shape()[1], k["undertext"].get_shape()[2], k["nb_ch"]])
    return mix2



def sep_layers_Db_fun(**k):
    """1D mixing model for completely independent layers where Db is a function"""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        undertext = tf.reshape(k["undertext"], shape=[-1, 1])
        overtext = tf.reshape(k["overtext"], shape=[-1, 1])
        D_u = tf.get_variable("D_u", [k["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        D_u = tf.nn.sigmoid(D_u)
        D_o = tf.get_variable("D_o", [k["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        D_o = tf.nn.sigmoid(D_o)
        if k["background"] is None:
            D_b = None
            y = tf.multiply(overtext, D_o) + tf.multiply(tf.multiply(undertext, D_u), (1 - overtext))
        else:
            background = tf.reshape(k["background"], shape=[-1, 1])
            D_b = d_bg_nbands(background, k["nb_ch"], "bg_net")
            y = tf.multiply(overtext, D_o) + tf.multiply(tf.multiply(undertext, D_u), (1 - overtext))+\
                tf.multiply(tf.multiply(D_b,(1-undertext)),(1-overtext))
        y = tf.reshape(y, shape=[-1, k["undertext"].get_shape()[1], k["undertext"].get_shape()[2], k["nb_ch"]])
        return y,D_b


def sep_ut_ot_sum_bg_fn(**k):
    """1D mixing model, ot is independent from ut, bg is a NN network. The Db is just a vector"""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        undertext = tf.reshape(k["undertext"], shape=[-1, 1])
        overtext = tf.reshape(k["overtext"], shape=[-1, 1])
        D_u = tf.get_variable("D_u", [k["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        D_o = tf.get_variable("D_o", [k["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        if k["background"] is None:
            D_b = None
            y = tf.multiply(overtext, D_o) + tf.multiply(tf.multiply(undertext, D_u), (1 - overtext))
        else:
            background = tf.reshape(k["background"], shape=[-1, 1])
            D_b = d_bg_nbands(background, k["nb_ch"], "bg_net")
            y = tf.multiply(overtext, D_o) + tf.multiply(tf.multiply(undertext, D_u), (1 - overtext))+\
                tf.multiply(D_b,(1-overtext))
        y = tf.reshape(y, shape=[-1, k["undertext"].get_shape()[1], k["undertext"].get_shape()[2], k["nb_ch"]])
        return y,D_b


def sep_layers_bg_not_fn(**k):
    """1D mixing model, ot is independent from ut, bg is not a function. The Db is just a vector."""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        ut = tf.reshape(k["undertext"], shape=[-1, 1])
        ot = tf.reshape(k["overtext"], shape=[-1, 1])
        D_u = tf.get_variable("D_u", [k["nb_ch"]], initializer=tf.constant_initializer(k["ut_refl"]))
        D_o = tf.get_variable("D_o", [k["nb_ch"]], initializer=tf.constant_initializer(k["ot_refl"]))
        if k["background"] is None:
            D_b_bg = None
            y = tf.multiply(ot, D_o) + tf.multiply(tf.multiply(ut, D_u), (1 - ot))
        else:
            bg = tf.reshape(k["background"], shape=[-1, 1])
            D_b = tf.get_variable("D_b", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_scaler"]))
            D_b_bias = tf.get_variable("D_b_bias", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_bias"]))
            D_b_bg = tf.multiply(bg, D_b)+D_b_bias
            y = tf.multiply(ot, D_o) + tf.multiply(ut, D_u)+ D_b_bg - tf.multiply(tf.multiply(ut,ot),D_u)
        y = tf.reshape(y, shape=[-1, k["undertext"].get_shape()[1], k["undertext"].get_shape()[2], k["nb_ch"]])
        return y,D_b_bg

def sep_layers_bg_ind_from_ot_not_fn(**k):
    """1D mixing model, ot is independent from bg and ut, bg is not a function. The Db is just a vector."""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        ot = tf.reshape(k["overtext"], shape=[-1, 1])
        D_o = tf.get_variable("D_o", [k["nb_ch"]], initializer=tf.constant_initializer(k["ot_refl"]))
        if not k["undertext"] is None:
            ut = tf.reshape(k["undertext"], shape=[-1, 1])
            D_u = tf.get_variable("D_u", [k["nb_ch"]], initializer=tf.constant_initializer(k["ut_refl"]))
        if k["background"] is None:
            D_b_bg = None
            y = tf.multiply(ot, D_o) + tf.multiply(tf.multiply(ut, D_u), (1 - ot))
        elif k["undertext"] is None:
            bg = tf.reshape(k["background"], shape=[-1, 1])
            D_b = tf.get_variable("D_b", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_scaler"]))
            D_b_bias = tf.get_variable("D_b_bias", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_bias"]))
            D_b_bg = tf.multiply(bg, D_b) + D_b_bias
            y = tf.multiply(ot, D_o)  + \
                tf.multiply(D_b_bg, (1 - ot))
        else:
            bg = tf.reshape(k["background"], shape=[-1, 1])
            D_b = tf.get_variable("D_b", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_scaler"]))
            D_b_bias = tf.get_variable("D_b_bias", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_bias"]))
            D_b_bg = tf.multiply(bg, D_b) + D_b_bias
            y = tf.multiply(ot, D_o) + tf.multiply(ut, D_u)+ D_b_bg*(1-ot) - tf.multiply(tf.multiply(ut,ot),D_u)
        y = tf.reshape(y, shape=[-1, k["overtext"].get_shape()[1], k["overtext"].get_shape()[2], k["nb_ch"]])
        return y,D_b_bg

def sep_layers_all_ind_bg_not_fn(**k):
    """1D mixing model, all layers indepedent, bg is not a function. The Db is just a vector."""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        ut = tf.reshape(k["undertext"], shape=[-1, 1])
        ot = tf.reshape(k["overtext"], shape=[-1, 1])
        D_u = tf.get_variable("D_u", [k["nb_ch"]], initializer=tf.constant_initializer(k["ut_refl"]))
        D_o = tf.get_variable("D_o", [k["nb_ch"]], initializer=tf.constant_initializer(k["ot_refl"]))
        if k["background"] is None:
            D_b_bg = None
            y = tf.multiply(ot, D_o) + tf.multiply(tf.multiply(ut, D_u), (1 - ot))
        else:
            bg = tf.reshape(k["background"], shape=[-1, 1])
            D_b = tf.get_variable("D_b", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_scaler"]))
            D_b_bias = tf.get_variable("D_b_bias", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_bias"]))
            D_b_bg = tf.multiply(bg, D_b) + D_b_bias
            y = tf.multiply(ot, D_o) + tf.multiply(ut, D_u)+ D_b_bg*(1-ot)*(1-ut) - tf.multiply(tf.multiply(ut,ot),D_u)
        y = tf.reshape(y, shape=[-1, k["undertext"].get_shape()[1], k["undertext"].get_shape()[2], k["nb_ch"]])
        return y,D_b_bg

def sep_layers_bg_fun_sigmoid(**k):
    """1D mixing model, all layers indepedent. The Db is just a vector."""
    with tf.variable_scope("MixingNet",reuse=tf.AUTO_REUSE):
        ut = tf.reshape(k["undertext"], shape=[-1, 1])
        ot = tf.reshape(k["overtext"], shape=[-1, 1])
        D_u = tf.get_variable("D_u", [k["nb_ch"]], initializer=tf.constant_initializer(k["ut_refl"]))
        D_o = tf.get_variable("D_o", [k["nb_ch"]], initializer=tf.constant_initializer(k["ot_refl"]))
        if k["background"] is None:
            D_b_bg = None
            y = tf.nn.sigmoid(10*(tf.multiply(ot, D_o)+tf.multiply(ut, D_u))/D_o-5.0)
        else:
            bg = tf.reshape(k["background"], shape=[-1, 1])
            D_b = tf.get_variable("D_b", [k["nb_ch"]], initializer=tf.constant_initializer(np.ones(k["nb_ch"])))
            D_b_bias = tf.get_variable("D_b_bias", [k["nb_ch"]], initializer=tf.constant_initializer(k["bg_refl"]))
            D_b_bg = tf.multiply(bg, D_b)+D_b_bias
            y = tf.nn.sigmoid(10*(tf.multiply(ot, D_o) + tf.multiply(ut, D_u)+ D_b_bg)/D_o-5.0)
        y = tf.reshape(y, shape=[-1, k["undertext"].get_shape()[1], k["undertext"].get_shape()[2], k["nb_ch"]])
        return y,D_b_bg

def d_bg_nbands(x,nb_ch,scope_name):
    with tf.variable_scope(scope_name, reuse=tf.AUTO_REUSE):

        x = tf.layers.dense(x, 32, activation="relu",
                               kernel_initializer=tf.truncated_normal_initializer(stddev=0.2))
        x = tf.layers.dense(x, nb_ch, activation=None,
                               kernel_initializer=tf.truncated_normal_initializer(stddev=0.2))#tf.keras.activations.tanh
    return x

def min_mixing_no_clip(**kwargs):
    with tf.variable_scope("MixingNet", reuse=tf.AUTO_REUSE):
        undertext = 1-kwargs["undertext"]
        overtext = 1-kwargs["overtext"]
        input = tf.concat([undertext, overtext], 3)*0.3
        mix = tf.math.reduce_min(input, axis=3, keepdims=True, name=None)
    return mix

def min_sum_mixing_no_var(**kwargs):
    with tf.variable_scope("MixingNet", reuse=tf.AUTO_REUSE):
        undertext = 1-kwargs["undertext"]+0.2
        overtext = 1-kwargs["overtext"]+0.1
        background = kwargs["background"]
        input = tf.concat([undertext, overtext, background], 3)
        mix = tf.math.reduce_min(input, axis=3, keepdims=True, name=None)
    return mix

def min_sum_mixing_no_var(**kwargs):
    with tf.variable_scope("MixingNet", reuse=tf.AUTO_REUSE):
        D_u = tf.get_variable("D_u", [kwargs["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        D_o = tf.get_variable("D_o", [kwargs["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        D_bg = tf.get_variable("D_bg", [kwargs["nb_ch"]], initializer=tf.truncated_normal_initializer(stddev=0.2))
        undertext = 1-kwargs["undertext"] + D_u
        overtext = 1-kwargs["overtext"] + D_o
        background = D_bg*kwargs["background"]
        input = tf.concat([undertext, overtext, background], 3)
        mix = tf.math.reduce_min(input, axis=3, keepdims=True, name=None)
    return mix