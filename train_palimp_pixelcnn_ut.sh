#!/bin/bash
TIMEFOLDER=$(date +"%Y-%m-%d-%H-%M")
DATADIR='/cis/phd/as3297/projects/bss_autoreg_palimpsest/datasets/Greek_960/Greek960_dataset_50step_192x192'
SAVEDIR="/cis/phd/as3297/projects/bss_autoreg_palimpsest/training/generators/undertext/pixelcnn-bin-${TIMEFOLDER}"
DATASET='greek960_ut'
#-re 120
python train.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -aug -bn -m 2 -n 64 --nr_gpu 2 -b 8 -t 6 -q 5 -dv "gpu" -x 100