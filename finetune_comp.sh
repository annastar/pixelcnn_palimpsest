#!/bin/bash
TIMEFOLDER="2022-05-27-18-43"
DATADIR="c:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_dataset_50step_192x192"
SAVEDIR="c:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-${TIMEFOLDER}"
DATASET='greek960_ut'
python finetune.py -i ${DATADIR} -o ${SAVEDIR} -d ${DATASET} -m 2 --nr_gpu 1 -b 2 -bn -aug -dv "cpu" -q 5 -re 6