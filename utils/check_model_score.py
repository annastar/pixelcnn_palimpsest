import os.path
import platform
if platform.release()=="5.10.153-1.el7.x86_64":
    import tensorflow.compat.v1 as tf
    tf.disable_v2_behavior()
else:
    import tensorflow as tf
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_pixelcnn_ut_model_with_par,\
    compile_pixelcnn_bg_model_with_par
from pixelcnn_palimpsest.pixel_cnn_pp.nn import sigmoid_loss_binary,discretized_mix_logistic_loss_gray
from pixelcnn_palimpsest.langevin_dynamics.prior import restore_model
import numpy as np

scope = "model_ut"
batch_size = 16
ut_model = "binary"
data_dir = r'C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192'
checkpoint_path = r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2022-05-27-18-43_current\params_greek960_ut12.ckpt"
def scale_input(x,model_type):
    """Scale input
    x - input image array
    model_type - str, indicator of how to scale data, "discrete" - interval [-1,1], binary interval [0,1]
    Returns:
        dict with scaled x
    """
    if model_type=="binary":
        x = np.cast[np.float32](x)/255.0
    else:
        x = np.cast[np.float32]((x - 127.5) / 127.5)
    return x


if scope=="model_ut":
    model_type = ut_model
    from pixelcnn_palimpsest.data.greek960_ut_data import DataLoaderBgAug as DataLoader
    test_data = DataLoader(data_dir, 'test', batch_size,
                           shuffle=False, aug_with_bg=None)
    obs_shape = test_data.get_observation_size()  # e.g. a tuple (32,32,3)
    xs_prior_tf, l_prior_tf, _ = compile_pixelcnn_ut_model_with_par(batch_size, ut_model)
    if ut_model=="binary":
        loss_tf = sigmoid_loss_binary(xs_prior_tf,l_prior_tf)
        loss_tf = loss_tf/(np.prod(obs_shape) * batch_size)
    elif ut_model=="dicrete":
        loss_tf = discretized_mix_logistic_loss_gray(xs_prior_tf,l_prior_tf)
        loss_tf = loss_tf / (np.log(2.) * np.prod(obs_shape) * batch_size)
if scope=="model_bg":
    model_type = "discrete"
    from pixelcnn_palimpsest.data.greek960_bg_data import DataLoaderTextAug as DataLoader
    test_data = DataLoader(data_dir, 'test', batch_size,
                           shuffle=False, aug_with_text=None)
    xs_prior_tf, l_prior_tf, _ = compile_pixelcnn_bg_model_with_par(batch_size)
    loss_tf = discretized_mix_logistic_loss_gray(xs_prior_tf,l_prior_tf)
    loss_tf = loss_tf/(np.log(2.) * np.prod(obs_shape) * batch_size)

all_params = tf.trainable_variables()
ema = tf.train.ExponentialMovingAverage(decay=0.9995)
maintain_averages_op = tf.group(ema.apply(all_params))
ema_params = [ema.average(p) for p in all_params]
losses = []
with tf.Session() as sess:
    initializer = tf.global_variables_initializer()
    sess.run(initializer)
    restore_model(checkpoint_path,sess=sess,model_scope=scope)
    for d in test_data:
        if type(d) == tuple:
            print("Do not add augmentation during this test!")
        else:
            d = scale_input(d,model_type)
        loss = sess.run(loss_tf,{xs_prior_tf:d})
        losses.append(loss)
    losses = np.mean(losses)

with open(os.path.join(os.path.split(checkpoint_path)[0],"check_score.txt"),"a") as f:
    f.write("ON platform {}, release {}, the model {} test score is={}\n".format(platform.platform(),
                                                                                  platform.release(),
                                                                                  os.path.split(checkpoint_path)[-1],
                                                                                  losses))
