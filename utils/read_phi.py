import numpy as np

def get_phi(phi_dict,D):
    D = round_to_2f(D)
    phi = []
    for ch in range(len(D)):
        phi.append(phi_dict[str(D[ch])])
    if len(D)==1:
        return phi[0]
    else:
        return np.array(phi)


def round_to_2f(v):
    """Round a float to have 2 decimal place"""
    return np.floor(v * 100) / 100