import numpy as np
import random
from pixelcnn_palimpsest.utils import plotting


def find_peak_scale_value(x, double_search=False):
    #get histogram
    x = x.ravel()
    x_hist, x_edges = np.histogram(x,density=True,bins=10)
    max_idx = np.argmax(x_hist)
    if double_search:
        x = [x[i] for i in range(len(x)) if x[i]>x_edges[max_idx] and x[i]<x_edges[max_idx+1]]
        x_hist, x_edges = np.histogram(np.array(x), density=True, bins=10)
        max_idx = np.argmax(x_hist)
    biggest_edge = abs(x_edges[max_idx]) if abs(x_edges[max_idx])>abs(x_edges[max_idx+1]) else abs(x_edges[max_idx+1])
    return biggest_edge

def prior_grad_aver(imgs, patch_size,tile_shape, sess, grads_n, loss_n, xs, nb_scopes,nb_repeats):
    shift = [[0,0]]
    for i in range(nb_scopes-1):
        shift.append([random.choice([-1,1])*int(random.uniform(1,int(patch_size/2))),
                      random.choice([-1,1])*int(random.uniform(1,int(patch_size/2)))])
    nb_batch = np.prod(tile_shape)
    img = np.zeros([nb_repeats,tile_shape[0]*patch_size,tile_shape[1]*patch_size,1])
    for nr in range(nb_repeats):
        img[nr] = plotting.img_tile(imgs[nr*nb_batch:(1+nr)*nb_batch], border=0, stretch=False, tile_shape=tile_shape)
    _,height,width,_ = img.shape
    acc_count = np.zeros((nb_repeats,height,width))
    acc_grads = np.zeros((nb_repeats,height,width))
    acc_loss = np.zeros(list(map(int, loss_n.get_shape())))
    for shift_x, shift_y in shift:
        grads,loss,count = img_grad_from_scope(img, shift_x, shift_y, patch_size, sess, grads_n, loss_n, xs, tile_shape)
        acc_count += count
        acc_grads += grads
        acc_loss += loss
    acc_grads = np.mean(np.divide(acc_grads,acc_count),axis=0)
    acc_grads = plotting.split_image(acc_grads, patch_size)
    acc_grads = np.concatenate(nb_repeats*[acc_grads],0)
    acc_loss = acc_loss/nb_scopes
    return [acc_grads],acc_loss

def img_grad_from_scope(img,sh_x,sh_y,patch_size,sess,grads_n,loss_n,xs,tile_shape):
    #pad the image
    img = img[:,:,:,0]
    nb_repeats,height,width = img.shape
    batch_size = np.prod(tile_shape)
    new_image = np.zeros((nb_repeats,height,width))
    old_im_croped = img[:,max(0, sh_x):min(height + sh_x, height), max(0, sh_y):min(width + sh_y, width)]
    new_image[:,max(0, -sh_x):min(height - sh_x, height), max(0, -sh_y):min(width - sh_y, width)]=old_im_croped
    ###store the scope placement
    count = np.zeros((nb_repeats,height,width))
    count[:,max(0, sh_x):min(height + sh_x, height), max(0, sh_y):min(width + sh_y, width)] = 1
    #split the image
    init_imgs = np.zeros([nb_repeats*batch_size,patch_size,patch_size,1])
    for nr in range(nb_repeats):
        init_imgs[nr*batch_size:(nr+1)*batch_size] = plotting.split_image(new_image[nr],patch_size)
    #calculate the gradients
    grads_ims,loss = sess.run([grads_n,loss_n], {xs: init_imgs})
    grads_im = np.zeros([nb_repeats, height, width])
    for nr in range(nb_repeats):
        grads_im[nr] = plotting.stich_image(grads_ims[0][nr*batch_size:(nr+1)*batch_size,:,:,0], tile_shape)
    #loss = stich_image(loss[:,:,:,0],tile_shape)
    grads_shifted = np.zeros((nb_repeats,height,width))
    grads_shifted[:,max(0, sh_x):min(height + sh_x, height), max(0, sh_y):min(width + sh_y, width)]=\
        grads_im[:,max(0, -sh_x):min(height - sh_x, height), max(0, -sh_y):min(width - sh_y, width)]
    return grads_shifted,loss,count

if __name__=="__main__":
    mu = np.array([[-1.0],[0.8]])
    sigma = np.array([[0.1],[0.5]])
    x = sigma * np.random.randn(1,1000) + mu
    cov = np.cov(x)
    print("cov",cov)
    edge = find_peak_scale_value(x, True)
    print("Edge",edge)
    import tensorflow as tf
    import tensorflow_probability as tfp
    mean_g = tf.constant(np.squeeze(mu), dtype=tf.float32)
    cov_g = tf.constant(cov, dtype=tf.float32)
    pdf = tfp.distributions.MultivariateNormalFullCovariance(
        mean_g, cov_g, validate_args=False, allow_nan_stats=True, name='Normal')
    batch_size = 5
    x_batch = np.zeros((batch_size,2))
    for i in range(batch_size):
        x_batch[i,:] = x[:,i]
    x_tf = tf.cast(x_batch,tf.float32)
    neg_log_prob = -pdf.log_prob(x_tf)
    neg_log_prob = tf.reduce_sum(neg_log_prob)
    with tf.Session() as sess:
        pp=sess.run(neg_log_prob)
    print("X:",x_batch)
    print("Neg LogLikelihood",pp)
    from matplotlib import pyplot as plt
    plt.figure()
    plt.hist(x,bins=10,density=True)
    plt.plot(np.array([edge]*10),np.array([0,1,2,3,4,5,6,7,8,9])/10.0,"x")
    plt.show()