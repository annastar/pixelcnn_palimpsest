import matplotlib.pyplot as plt
import numpy as np
from pixelcnn_palimpsest.utils import opt_dens_util,extract_roi
from pixelcnn_palimpsest.utils.read_images import DataUtility
from skimage.draw import polygon
import os

def extract_pixels_from_polygons(data_dir,poly_verts):
    d = DataUtility(data_dir,True)
    ims = d.read_ims_msi(0)
    pixels = []
    for poly_vert in poly_verts:
        rr,cc = polygon(poly_vert[0],poly_vert[1])
        for i in range(len(rr)):
            pixels.append(ims[rr[i],cc[i]])
    return np.array(pixels).T

def calc_cov_opt_dens(x,data_dir):
    """Calculate covariance matrix for the set of pixels"""
    bg_val = np.loadtxt(os.path.join(data_dir,"background_mean.txt"))
    if np.amax(bg_val) > 255:
        max_val = 65535.0
    elif np.amax(bg_val) > 1:
        max_val = 255.0
    x = opt_dens_util.refl2opt_dens(x.T,bg_val/max_val)
    print(np.mean(x,0))
    cov = np.cov(x.T)
    return cov

def calc_cov(x,data_dir):
    """Calculate covariance matrix for the set of pixels"""
    cov = np.cov(x)
    return cov

if __name__=="__main__":
    roipath = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\0086_000084\background_roi.xml"
    datadir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\0086_000084"
    coords = extract_roi.read_roi_xml(roipath)
    #rot coord
    #extract_roi.show_roi(datadir,coords)
    pixels = extract_pixels_from_polygons(datadir,coords)
    plt.figure()
    plt.hist(pixels[:,0],bins="auto")
    plt.show()
    cov = calc_cov_opt_dens(pixels,datadir)
    np.savetxt(os.path.join(datadir,"cov_opt_dens_bg.npy"),cov)
    print(np.loadtxt(os.path.join(datadir,"std_bg.npy")))
