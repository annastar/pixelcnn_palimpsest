import os
import numpy as np


def save_optical_density(log_dir,sess,R,mixing_model_name,is_bg=False):
    import tensorflow as tf
    """Save parameters of optical density"""
    with tf.variable_scope("MixingNet", reuse=True):
        D_u_tf = tf.get_variable("D_u")
        D_o_tf = tf.get_variable("D_o")
        if is_bg:
            var_bb_tf = tf.get_variable("var_bb")
    D_u, D_o = sess.run((D_u_tf, D_o_tf))
    s_u_str = [str(x) for x in D_u]
    s_o_str = [str(x) for x in D_o]
    with open(os.path.join(log_dir, "D_u_pred.txt"), "a") as f:
        f.write(','.join(s_u_str))
        f.write('\n')
    with open(os.path.join(log_dir, "D_o_pred.txt"), "a") as f:
        f.write(','.join(s_o_str))
        f.write('\n')
    if is_bg:
        var_bb = sess.run(var_bb_tf)
        var_bb_str = [str(x) for x in var_bb]
        with open(os.path.join(log_dir, "var_bb_pred.txt"), "a") as f:
            f.write(','.join(var_bb_str))
            f.write('\n')
    if "corr" in mixing_model_name:
        with tf.variable_scope("MixingNet", reuse=True):
            scale_bu_var = tf.get_variable("scale_bu")
            scale_bo_var = tf.get_variable("scale_bo")
        scale_bu, scale_bo = sess.run((scale_bu_var, scale_bo_var))
        scale_bu_str = [str(x) for x in scale_bu]
        scale_bo_str = [str(x) for x in scale_bo]
        with open(os.path.join(log_dir, "scale_bo.txt"), "a") as f:
            f.write(','.join(scale_bo_str))
            f.write('\n')
        with open(os.path.join(log_dir, "scale_bu.txt"), "a") as f:
            f.write(','.join(scale_bu_str))
            f.write('\n')
    if "olap" in mixing_model_name:
        with tf.variable_scope("MixingNet", reuse=True):
            scale_ou_var = tf.get_variable("scale_overlap")
        scale_ou = sess.run(scale_ou_var)
        scale_ou_str = [str(x) for x in scale_ou]
        with open(os.path.join(log_dir, "scale_ou.txt"), "a") as f:
            f.write(','.join(scale_ou_str))
            f.write('\n')

def opt_den2refl(opt_dens,background_refl):
    """convert optical density to reflectance"""
    return np.multiply(np.power(10, -opt_dens), background_refl)

def refl2opt_dens(refl, background_refl):
    """convert reflectance to optical density"""
    refl_data_range = np.max(refl)
    backgr_data_range = np.max(background_refl)
    if not (refl_data_range<=1)==(backgr_data_range<=1):
        raise ValueError('Background reflectance and Palimpsest reflectance are from'
                         'different data range. Scale palimpsest images values to be less or equal to 1.')
    if np.amin(refl)<0:
        refl=(refl+1)/2
    return -np.log10(np.maximum(refl,1e-9)/background_refl)

def read_mean_signature(data_dir,mode):
    import csv
    org_mix = []
    org_path = r"C:\Data\PhD\palimpsest\Archimedes\Archi_MSI\034v-029r_Arch52v_Sinar"
    with open(os.path.join(org_path,mode+".csv"), mode='r') as csv_file:
        csv_reader = csv.reader(csv_file,delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count > 10:
                org_mix.append([float(row[i]) for i in range(2,13)])
            line_count=line_count+1
    org_mix = np.array(org_mix)
    org_mix_mean = np.squeeze(np.mean(org_mix,0))
    np.savetxt(os.path.join(data_dir,mode+"_refl_values.txt"),org_mix_mean, delimiter=",")

def plot_summary_msi():
    from skimage import io
    from matplotlib import pyplot as plt

    data_dir = r"C:\Data\PhD\bss_gan_palimpsest\datasets\mnist_palimpsest_with_bg_1_char_5_obs\test\classify_1"
    impath = "0\original\im10"
    R = np.loadtxt(os.path.join(data_dir, "background_refl_values.txt"), delimiter=",")
    im = np.zeros((28, 28, 5))
    for idx, imname in enumerate(os.listdir(os.path.join(data_dir, impath))):
        ch = io.imread(os.path.join(data_dir, impath, imname), as_gray=True)
        im[:, :, idx] = ch / 255.0
    im_den = refl2opt_dens(im, R)
    upper_thresh = refl2opt_dens(5 * [1], R)
    im_den_tf = tf.constant(im_den, tf.float32, name="org_lat")
    im_den_tf = tf.clip_by_value(im_den_tf, clip_value_min=upper_thresh,
                                 clip_value_max=im_den_tf, name="threshold")
    mix_write = tf.summary.FileWriter(os.path.join(data_dir, 'logs'), tf.get_default_graph())
    summ = tf.summary.image("im_den", tf.expand_dims(tf.slice(im_den_tf, [0, 0, 0], [28, 28, 3]), 0))
    with tf.Session() as sess:
        im_den_eval, summ_buf = sess.run([im_den_tf, summ])
        mix_write.add_summary(summ_buf, 0)
        mix_write.close()
    im_max = np.max(im_den_eval[:, :, :3])
    im_den_eval = im_den_eval[:, :, :3] / im_max
    plt.figure()
    plt.imshow(im_den_eval)
    plt.show()

if __name__=="__main__":
    data_dir = r"C:\Data\PhD\bss_gan_palimpsest\datasets\Archimedes\test_obscured_MSI"
    read_mean_signature(data_dir,"overtext")





