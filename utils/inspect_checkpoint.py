import tensorflow as tf

ckpt_dir_or_file=r"C:\Data\PhD\bss_autoreg_palimpsest\training\generators\undertext\pixelcnn-bin-2023-02-24-21-15\noise_0.0\params_greek960_ut20.ckpt"
ars_list = tf.train.list_variables(ckpt_dir_or_file)
for v in ars_list:
    print(v)

ckpt_reader = tf.train.load_checkpoint(ckpt_dir_or_file)
value = ckpt_reader.get_tensor('model_ut/conv2d_0/g/ExponentialMovingAverage')
print(value)