import platform
import os
if platform.release()=="5.10.153-1.el7.x86_64":
    import tensorflow.compat.v1 as tf
    #tf.disable_v2_behavior()
else:
    import tensorflow as tf
from skimage.io import imsave
import numpy as np
from pixelcnn_palimpsest.utils import files,plotting
import time
import copy
from pixelcnn_palimpsest.utils.losses import l2_loss
from pixelcnn_palimpsest.utils.plotting import stich_image


def record_layer(im,savedir,epoch,rescale,tile_shape=None):
    if rescale:
        im = copy.deepcopy((im+1)/2)
    if not tile_shape is None:
        im = stich_image(im, tile_shape)
    imsave(os.path.join(savedir, str(epoch) + ".png"),
           np.clip(np.squeeze(im), 0.0, 1.0))


def save_grad_fid_vs_prior(log_dirs,tile_shape,epoch,ll_test,prior_test,mode):
    fpath = os.path.join(log_dirs, "grads_{}.png".format(epoch))
    ll_test = plotting.stich_image(ll_test[:, :, :, 0], tile_shape)
    prior_test = plotting.stich_image(prior_test[:, :, :, 0], tile_shape)
    plotting.ll_vs_prior_grad_map(ll_test, prior_test, fpath,mode)


def record_result_ims(epoch, sess, log_dirs, par, m, ior):
    # Saving images
    #save ut and bg layers

    dict_var_input_pair={}
    if par.with_ut:
        if par.ut_model=="discrete":
            record_layer(ior.init_img_ut[0, :, :, 0], log_dirs.savedir_ut, epoch,rescale=True)
        elif par.ut_model=="binary":
            record_layer(ior.init_img_ut[0, :, :, 0], log_dirs.savedir_ut, epoch, rescale=False)
        dict_var_input_pair[m.xs_prior_ut_tf] = ior.init_img_ut
    if par.with_bg:
        record_layer(ior.init_img_bg[0, :, :, 0], log_dirs.savedir_bg, epoch, rescale=True)
        dict_var_input_pair[m.xs_prior_bg_tf] = ior.init_img_bg
    ###generate and save y_pred
    dict_var_input_pair[m.xs_ot_tf] = ior.mask
    im_batch_size = len(plotting.split_image(ior.mask[0,:,:,0],par.im_size))
    get_save_y_pred(sess, log_dirs.savedir_restored,m.ys_pred_tf,
                    dict_var_input_pair, par.nb_ch, ior.tile_shape,
                    par.im_size, epoch, par.fid_loss,par,ior.batch_size,im_batch_size)
    ###generate and save gradients
    if par.fid_loss == "mvn" or par.nb_ch == 1:
        ys_org = plotting.split_image_msi(ior.y_org, (1,par.im_size,par.im_size,par.nb_ch))
        ys_org = np.concatenate([ys_org,ys_org[:ior.batch_size - im_batch_size]],0)
        if par.with_ut:
            get_save_grads_fid_vs_prior(
                sess, log_dirs.savedir_grads_ut, m.ys_org_tf,
                ys_org, dict_var_input_pair,  m.grad_ll_ut_tf,
                m.grad_prior_ut_tf,m.phi_tf, ior.phi_ut, m.sigma_tf,
                ior.sigma_ut,"ut",ior.tile_shape, par.im_size, epoch,
                ior.batch_size,im_batch_size)
        if par.with_bg:
            get_save_grads_fid_vs_prior(
                sess, log_dirs.savedir_grads_bg, m.ys_org_tf,
                ys_org, dict_var_input_pair, m.grad_ll_bg_tf,
                m.grad_prior_bg_tf, m.phi_tf, ior.phi_bg, m.sigma_tf,
                ior.sigma_bg, "bg", ior.tile_shape, par.im_size, epoch,
                ior.batch_size,im_batch_size)

    else:
        for ch in range(par.nb_ch):
            ys_org = plotting.split_image(ior.y_org[0, :, :, ch], par.im_size)
            ys_org = np.concatenate([ys_org, ys_org[:ior.batch_size-im_batch_size]], 0)
            if par.with_ut:
                get_save_grads_fid_vs_prior(
                    sess, log_dirs.savedir_grads_ut,m.ys_org_tf,
                    ys_org, dict_var_input_pair, m.grad_ll_ut_tf[str(ch)],
                    m.grad_prior_ut_tf, m.phi_tf, ior.phi_ut[ch], m.sigma_tf,
                    ior.sigma_ut, "ut", ior.tile_shape, par.im_size, epoch,
                    par.batch_size,im_batch_size)
            else:
                get_save_grads_fid_vs_prior(
                    sess, log_dirs.savedir_grads_bg, m.ys_org_tf,
                    ys_org, dict_var_input_pair, m.grad_ll_bg_tf[str(ch)],
                    m.grad_prior_bg_tf, m.phi_tf, ior.phi_bg[ch], m.sigma_tf,
                    ior.sigma_bg, "bg", ior.tile_shape, par.im_size, epoch,
                    par.batch_size, im_batch_size)
    plotting.plt.close('all')


def get_save_grads_fid_vs_prior(sess,save_dir,ys_org_tf,
                                ys_org,dict_tfvar_input_pair,grad_ll_tf,
                                grad_prior_tf,phi_tf,phi,sigma_tf,sigma,
                                mode,tile_shape,im_size,epoch,batch_size,im_batch_size):
    feed_dict = {ys_org_tf:ys_org,phi_tf:phi,sigma_tf:sigma}
    for key,value in dict_tfvar_input_pair.items():
        xs = plotting.split_image(value[0, :, :, 0], im_size)
        xs = np.concatenate([xs, xs[:batch_size-im_batch_size]], 0)
        feed_dict.update({key:xs})
    grad_ll, grad_prior = sess.run([grad_ll_tf,grad_prior_tf], feed_dict)
    save_grad_fid_vs_prior(save_dir, tile_shape, epoch, grad_ll[0][:im_batch_size], grad_prior[0][:im_batch_size],mode)


def get_save_y_pred(sess,save_dir,y_pred_tf,
                                dict_tfvar_input_pair,nb_ch,tile_shape,
                                im_size,epoch,fid_loss,par,batch_size,im_batch_size):
    feed_dict = {}
    for key,value in dict_tfvar_input_pair.items():
        xs = plotting.split_image(value[0, :, :, 0], im_size)
        xs = np.concatenate([xs,xs[:batch_size-im_batch_size]],0)
        feed_dict.update({key:xs})
    ys_pred = sess.run(y_pred_tf, feed_dict)
    if nb_ch==1:
        record_layer(ys_pred[:im_batch_size], save_dir, epoch, False,tile_shape)
    else:
        for ch in range(nb_ch):
            if fid_loss=="mvn":
                record_layer(ys_pred[:im_batch_size,:,:,ch][:,:,:,np.newaxis], os.path.join(save_dir, "band" + str(ch)),
                       epoch, False, tile_shape)
            if fid_loss=="uvn":
                record_layer(ys_pred[str(ch)][:im_batch_size,:,:,np.newaxis], os.path.join(save_dir, "band" + str(ch)),
                       epoch, False, tile_shape)


def record_loss(epoch, sess, expdir,
                         par, m, ior,printout=False):
    #print prior and ll ut loss

    feed_dict = {m.xs_ot_tf: ior.xs_ot}
    if par.with_ut:
        feed_dict.update({m.xs_prior_ut_tf:ior.xs_ut,m.sigma_tf:ior.sigma_ut})
        loss_prior_ut = sess.run(m.loss_prior_ut_tf, feed_dict)
        files.record_loss_and_step(loss_prior_ut, np.expand_dims([ior.sigma_ut], [1]), expdir, "prior_ut",
                                   epoch)
    if par.with_bg:
        feed_dict.update({m.xs_prior_bg_tf:ior.xs_bg,m.sigma_tf:ior.sigma_bg})
        loss_prior_bg = sess.run(m.loss_prior_bg_tf, feed_dict)
        files.record_loss_and_step(loss_prior_bg, np.expand_dims([ior.sigma_bg],
                                                                 [1]), expdir, "prior_bg", epoch)

    ###loss mask for the case of msi where 1 of the bands has Du =0
    if par.nb_ch > 1 and par.fid_loss=="uvn":
        mean_mix_loss = 0
        for ch in range(par.nb_ch):
            if par.with_ut:
                feed_dict[m.phi_ut_tf] = ior.phi_ut[ch]
            if par.with_bg:
                feed_dict[m.phi_bg_tf] = ior.phi_bg[ch]
            feed_dict[m.ys_org_tf] = ior.ys_org
            loss_mix = sess.run(m.loss_ll_ut_tf[str(ch)], feed_dict)
            mix_loss = np.mean(loss_mix)
            mean_mix_loss += mix_loss
        mean_mix_loss = mean_mix_loss/par.nb_ch
    else:
        feed_dict[m.ys_org_tf] = ior.ys_org
        if par.with_ut:
            feed_dict[m.phi_tf] = ior.phi_ut
            loss_mix = sess.run(m.loss_ll_ut_tf, feed_dict)
        elif par.with_bg:
            feed_dict[m.phi_tf] = ior.phi_bg
            loss_mix = sess.run(m.loss_ll_bg_tf, feed_dict)
        mean_mix_loss = np.mean(loss_mix)
    if printout:
        prt_out = 'Iter:{}, Timer:{}, Prior ut: {}, Likelihood: {}, Prior bg: {}'.format(epoch,
                                                        time.time() - ior.start_time,
                                                        loss_prior_ut,
                                                        mean_mix_loss, loss_prior_bg)
        print(prt_out)

    files.record_loss_and_step(mean_mix_loss,np.expand_dims([ior.sigma_ut],[1]),expdir, "mix", epoch)


    if par.pal_name=="art_pal":
        ut_rec_loss = l2_loss(tf.constant(ior.org_img_ut, dtype=tf.float32),
                              tf.constant(ior.init_img_ut, dtype=tf.float32), True)
        loss_rec_org_im = sess.run(ut_rec_loss)
        files.record_loss(loss_rec_org_im, expdir, "loss_org_im_rec", epoch)
