import tensorflow as tf
import numpy as np


def weighted_l2_loss(org,pred,data_range,sum_all):
    """L2 loss function weighted by inverted data range"""
    assert len(org.shape.as_list()) == len(pred.shape.as_list())
    inv_data_range = data_range
    recon_loss = (inv_data_range*(pred - org)) ** 2
    if sum_all:
        recon_loss = tf.reduce_sum(recon_loss)
    return recon_loss

def l2_loss(org,pred,sum_all):
    """L2 loss function"""
    assert len(org.shape.as_list()) == len(pred.shape.as_list())
    recon_loss = (pred - org) ** 2
    if sum_all:
        recon_loss = tf.reduce_sum(recon_loss)
    return recon_loss

def cross_cor_loss(x1,x2):
    """Cross correlation loss"""
    height,width = x2.get_shape()[1],x2.get_shape()[2]
    x2 = tf.reshape(x2,[height,width,1,1])
    out = tf.nn.convolution(x1,x2,padding="SAME")
    return out

def detect_plateau(new_loss,old_loss,plateau_thresh=0.1):
    """Detect plataeu in loss function"""
    percn_from_old_loss = new_loss*100/old_loss
    res = abs(100 - percn_from_old_loss)
    if np.mean(res)<plateau_thresh:
        return True
    else:
        return False

def compute_exclusion_loss(img1, img2, level=1, im_grads=False):
    """
            Loss on the gradient. taken from:
            http://openaccess.thecvf.com/content_cvpr_2018/papers/Zhang_Single_Image_Reflection_CVPR_2018_paper.pdf
            """
    gradx_loss = []
    grady_loss = []

    for l in range(level):
        gradx1, grady1 = compute_gradient(img1)
        gradx2, grady2 = compute_gradient(img2)
        alphax = 2.0 * tf.reduce_mean(tf.abs(gradx1)) / tf.reduce_mean(tf.abs(gradx2))
        alphay = 2.0 * tf.reduce_mean(tf.abs(grady1)) / tf.reduce_mean(tf.abs(grady2))

        #scale between -1 and 1
        gradx1_s = (tf.nn.sigmoid(gradx1) * 2) - 1
        grady1_s = (tf.nn.sigmoid(grady1) * 2) - 1
        gradx2_s = (tf.nn.sigmoid(gradx2 * alphax) * 2) - 1
        grady2_s = (tf.nn.sigmoid(grady2 * alphay) * 2) - 1

        mult_gradx = tf.multiply(tf.square(gradx1_s), tf.square(gradx2_s))
        mult_grady = tf.multiply(tf.square(grady1_s), tf.square(grady2_s))
        gradx_loss.append(
            tf.reduce_mean(mult_gradx, reduction_indices=[1, 2, 3]) ** 0.25)
        grady_loss.append(
            tf.reduce_mean(mult_grady, reduction_indices=[1, 2, 3]) ** 0.25)
        img1 = tf.nn.avg_pool(img1, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        img2 = tf.nn.avg_pool(img2, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
    if im_grads:
        return gradx_loss, grady_loss, mult_gradx, mult_grady, gradx1, gradx2, gradx1_s, gradx2_s
    else:
        return gradx_loss, grady_loss


def compute_gradient(img):
    gradx = img[:, 1:, :, :] - img[:, :-1, :, :]
    grady = img[:, :, 1:, :] - img[:, :, :-1, :]
    return gradx, grady