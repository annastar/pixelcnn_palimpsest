import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np
from pixelcnn_palimpsest.utils.read_images import DataUtility


def read_roi_xml(fpath):
    bs_data = ET.parse(fpath)
    root = bs_data.getroot()
    region = root.find('Region')
    geometry = region.find("GeometryDef")
    polygons = geometry.findall("Polygon")
    coords = []
    for polygon in polygons:
        exterior = polygon.find("Exterior")
        linear_ring = exterior.find("LinearRing")
        coord = linear_ring.find('Coordinates')
        coord = coord.text.split()
        nb_pixels = int(len(coord) / 2)
        cc = np.zeros((nb_pixels,))
        rr = np.zeros((nb_pixels,))
        for i in range(len(coord)):
            if i%2==0:
                cc[i//2] = coord[i]
            else:
                rr[i//2] = coord[i]

        #coord = {"top_left":(coord[0],coord[1]),"top_right":(coord[2],coord[3]),
        #         "bottom_right":(coord[4],coord[5]),"bottom_left":(coord[6],coord[7])}
        #for key,value in coord.items():
        #    coord[key]={"col":int(float(value[0])),"row":int(float(value[1]))}
        coords.append(np.stack([rr,cc],0))
    return coords

def show_roi(data_dir,poly_verts):
    d = DataUtility(data_dir,True)
    ims = d.read_ims_msi(0)
    for poly_vert in poly_verts:
        rr1,rr2 = np.min(poly_vert[0]),np.max(poly_vert[0])
        cc1,cc2 = np.min(poly_vert[1]),np.max(poly_vert[1])
        plt.figure()
        plt.imshow(ims[int(rr1):int(rr2),int(cc1):int(cc2),0])
        plt.show()



