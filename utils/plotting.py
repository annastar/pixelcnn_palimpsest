import numpy as np
import os
from skimage import io
from matplotlib import pyplot as plt
from skimage.util import view_as_blocks

def store_ims(ims,save_dir,epoch):
    nb_bands = ims.shape[-1]
    for i in range(nb_bands):
        band_dir = os.path.join(save_dir,"reconstruct",str(i))
        if not os.path.isdir(band_dir):
            os.makedirs(band_dir)
        io.imsave(os.path.join(band_dir,"band{}_epoch{}.png".format(i,epoch)),ims[0,:,:,i])

def plot_im_with_scale(x,title,epoch,vmin,vmax):
    plot_img(x[0, :, :, 0], title=title + "_" + str(epoch), gray=False, vmin=vmin, vmax=vmax)
    plt.colorbar()

def save_im(x,epoch,title,log_dir):
    plot_img(x,title,epoch,True)
    plt.savefig(os.path.join(log_dir,title+"_"+ str(epoch)+".png"))
    plt.close('all')

def split_image_msi(init_img,block_shape_full):
    # Splitting
    init_imgs = view_as_blocks(init_img, block_shape_full).reshape(-1, block_shape_full[1], block_shape_full[2],block_shape_full[3])
    return init_imgs

def save_ims_with_scale(x,epoch,title,log_dir,vmin=-4, vmax=4):
    fig,ax = plt.subplots(len(x),1)
    i = 0
    for im_name,im in x.items():
        ax[i].set_title(im_name)
        ax[i].imshow(im, cmap=plt.cm.RdBu, vmin=vmin, vmax=vmax)
        ax[i].set_axis_off()
        i += 1
    fig.colorbar(ax[i-1].imshow(im, cmap=plt.cm.RdBu, vmin=vmin, vmax=vmax), ax=ax[i-1],
                 orientation='horizontal')
    plt.savefig(os.path.join(log_dir,title+"_"+ str(epoch)+".png"))
    plt.close('all')

def save_im_with_scale(x,epoch,title,log_dir,vmin=-4, vmax=4):
    plot_im_with_scale(x,title,epoch,vmin, vmax)
    plt.savefig(os.path.join(log_dir,title+"_"+ str(epoch)+".png"))
    plt.close('all')
# Plot image examples.
def plot_img(img, title=None,epoch=None,gray=False, vmin=-4, vmax=4):
    plt.figure()
    if gray:
        plt.imshow(img, interpolation='nearest',cmap="gray")
    else:
        plt.imshow(img, interpolation='nearest', cmap=plt.cm.RdBu, vmin=vmin, vmax=vmax)
    if title is not None:
        title1=title
    if epoch is not None:
        title1 = title1 + "_" + str(epoch)
    if title is not None:
        plt.title(title1)
    plt.axis('off')
    plt.tight_layout()

def img_stretch(img):
    img = img.astype(float)
    img -= np.min(img)
    img /= np.max(img)+1e-12
    return img

def plot_hist(x,name):
    plt.figure()
    plt.title("Hist"+name)
    plt.hist(x.ravel(), density=True,stacked=True)

def save_im_grads(sess,xs_ut,xs_bg,init_img_ut,init_img_bg,grads,log_dir,y_pred_tf,epoch,with_bg):
    if not os.path.isdir(os.path.join(log_dir, "grads_ll")):
        os.makedirs(os.path.join(log_dir, "grads_ll"))
    if not os.path.isdir(os.path.join(log_dir, "pred_ll_im")):
        os.makedirs(os.path.join(log_dir, "pred_ll_im"))
    if not os.path.isdir(os.path.join(log_dir, "input_ll_im")):
        os.makedirs(os.path.join(log_dir, "input_ll_im"))
    if with_bg:
        im_pred = sess.run(y_pred_tf, {xs_ut: init_img_ut[np.newaxis, :, :, :],xs_bg: init_img_bg[np.newaxis, :, :, :]})
    else:
        im_pred = sess.run(y_pred_tf, {xs_ut: init_img_ut[np.newaxis, :, :, :]})
    save_im_with_scale(grads[0], epoch, "grads_ll", os.path.join(log_dir, "grads_ll"))
    save_im_with_scale(im_pred, epoch, "pred_ll_im", os.path.join(log_dir, "pred_ll_im"))
    save_im_with_scale(init_img_ut[np.newaxis, :, :, :], epoch, "input_ll_im",
                                os.path.join(log_dir, "pred_ll_im"))
    plt.show()

def split_image(init_img,patch_size):
    # Splitting
    init_imgs = view_as_blocks(init_img, (patch_size, patch_size)).reshape(-1, patch_size, patch_size,1)
    return init_imgs

def stich_image(init_imgs,tile_shape):
    # Stiching
    init_img = img_tile(init_imgs, border=0, stretch=False, tile_shape=tile_shape)
    return init_img

def img_tile(imgs, aspect_ratio=1.0, tile_shape=None, border=1,
             border_color=0, stretch=False):
    ''' Tile images in a grid.
    If tile_shape is provided only as many images as specified in tile_shape
    will be included in the output.
    '''

    # Prepare images
    if stretch:
        imgs = img_stretch(imgs)
    imgs = np.array(imgs)
    if imgs.ndim != 3 and imgs.ndim != 4:
        raise ValueError('imgs has wrong number of dimensions.')
    n_imgs = imgs.shape[0]

    # Grid shape
    img_shape = np.array(imgs.shape[1:3])
    if tile_shape is None:
        img_aspect_ratio = img_shape[1] / float(img_shape[0])
        aspect_ratio *= img_aspect_ratio
        tile_height = int(np.ceil(np.sqrt(n_imgs * aspect_ratio)))
        tile_width = int(np.ceil(np.sqrt(n_imgs / aspect_ratio)))
        grid_shape = np.array((tile_height, tile_width))
    else:
        assert len(tile_shape) == 2
        grid_shape = np.array(tile_shape)

    # Tile image shape
    tile_img_shape = np.array(imgs.shape[1:])
    tile_img_shape[:2] = (img_shape[:2] + border) * grid_shape[:2] - border

    # Assemble tile image
    tile_img = np.empty(tile_img_shape)
    tile_img[:] = border_color
    for i in range(grid_shape[0]):
        for j in range(grid_shape[1]):
            img_idx = j + i*grid_shape[1]
            if img_idx >= n_imgs:
                # No more images - stop filling out the grid.
                break
            img = imgs[img_idx]
            yoff = (img_shape[0] + border) * i
            xoff = (img_shape[1] + border) * j
            tile_img[yoff:yoff+img_shape[0], xoff:xoff+img_shape[1], ...] = img
    return tile_img

def conv_filter_tile(filters):
    n_filters, n_channels, height, width = filters.shape
    tile_shape = None
    if n_channels == 3:
        # Interpret 3 color channels as RGB
        filters = np.transpose(filters, (0, 2, 3, 1))
    else:
        # Organize tile such that each row corresponds to a filter and the
        # columns are the filter channels
        tile_shape = (n_channels, n_filters)
        filters = np.transpose(filters, (1, 0, 2, 3))
        filters = np.resize(filters, (n_filters*n_channels, height, width))
    filters = img_stretch(filters)
    return img_tile(filters, tile_shape=tile_shape)
    
def scale_to_unit_interval(ndar, eps=1e-8):
  """ Scales all values in the ndarray ndar to be between 0 and 1 """
  ndar = ndar.copy()
  ndar -= ndar.min()
  ndar *= 1.0 / (ndar.max() + eps)
  return ndar


def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                       scale_rows_to_unit_interval=True,
                       output_pixel_vals=True):
  """
  Transform an array with one flattened image per row, into an array in
  which images are reshaped and layed out like tiles on a floor.

  This function is useful for visualizing datasets whose rows are images,
  and also columns of matrices for transforming those rows
  (such as the first layer of a neural net).

  :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
  be 2-D ndarrays or None;
  :param X: a 2-D array in which every row is a flattened image.

  :type img_shape: tuple; (height, width)
  :param img_shape: the original shape of each image

  :type tile_shape: tuple; (rows, cols)
  :param tile_shape: the number of images to tile (rows, cols)

  :param output_pixel_vals: if output should be pixel values (i.e. int8
  values) or floats

  :param scale_rows_to_unit_interval: if the values need to be scaled before
  being plotted to [0,1] or not


  :returns: array suitable for viewing as an image.
  (See:`PIL.Image.fromarray`.)
  :rtype: a 2-d array with same dtype as X.

  """

  assert len(img_shape) == 2
  assert len(tile_shape) == 2
  assert len(tile_spacing) == 2

  # The expression below can be re-written in a more C style as
  # follows :
  #
  # out_shape = [0,0]
  # out_shape[0] = (img_shape[0] + tile_spacing[0]) * tile_shape[0] -
  #                tile_spacing[0]
  # out_shape[1] = (img_shape[1] + tile_spacing[1]) * tile_shape[1] -
  #                tile_spacing[1]
  out_shape = [(ishp + tsp) * tshp - tsp for ishp, tshp, tsp
                      in zip(img_shape, tile_shape, tile_spacing)]

  if isinstance(X, tuple):
      assert len(X) == 4
      # Create an output numpy ndarray to store the image
      if output_pixel_vals:
          out_array = np.zeros((out_shape[0], out_shape[1], 4), dtype='uint8')
      else:
          out_array = np.zeros((out_shape[0], out_shape[1], 4), dtype=X.dtype)

      #colors default to 0, alpha defaults to 1 (opaque)
      if output_pixel_vals:
          channel_defaults = [0, 0, 0, 255]
      else:
          channel_defaults = [0., 0., 0., 1.]

      for i in range(4):
          if X[i] is None:
              # if channel is None, fill it with zeros of the correct
              # dtype
              out_array[:, :, i] = np.zeros(out_shape,
                      dtype='uint8' if output_pixel_vals else out_array.dtype
                      ) + channel_defaults[i]
          else:
              # use a recurrent call to compute the channel and store it
              # in the output
              out_array[:, :, i] = tile_raster_images(X[i], img_shape, tile_shape, tile_spacing, scale_rows_to_unit_interval, output_pixel_vals)
      return out_array

  else:
      # if we are dealing with only one channel
      H, W = img_shape
      Hs, Ws = tile_spacing

      # generate a matrix to store the output
      out_array = np.zeros(out_shape, dtype='uint8' if output_pixel_vals else X.dtype)


      for tile_row in range(tile_shape[0]):
          for tile_col in range(tile_shape[1]):
              if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                  if scale_rows_to_unit_interval:
                      # if we should scale values to be between 0 and 1
                      # do this by calling the `scale_to_unit_interval`
                      # function
                      this_img = scale_to_unit_interval(X[tile_row * tile_shape[1] + tile_col].reshape(img_shape))
                  else:
                      this_img = X[tile_row * tile_shape[1] + tile_col].reshape(img_shape)
                  # add the slice to the corresponding position in the
                  # output array
                  out_array[
                      tile_row * (H+Hs): tile_row * (H + Hs) + H,
                      tile_col * (W+Ws): tile_col * (W + Ws) + W
                      ] \
                      = this_img * (255 if output_pixel_vals else 1)
      return out_array

def ll_vs_prior_grad_map(ll_gd,prior_gd,fpath,mode):
    fig, ax = plt.subplots(3, 1)
    pcm0 = ax[0].pcolormesh(ll_gd[::-1, :], cmap='RdBu', vmin=np.amin(ll_gd),
                            vmax=np.amax(ll_gd))
    pcm1 = ax[1].pcolormesh(prior_gd[::-1, :], cmap='RdBu', vmin=np.amin(prior_gd),
                            vmax=np.amax(prior_gd))
    pcm2 = ax[2].pcolormesh(prior_gd[::-1, :]+ll_gd[::-1, :], cmap='RdBu',
                            vmin=np.amin(prior_gd[::-1, :]+ll_gd[::-1, :]),
                            vmax=np.amax(prior_gd[::-1, :]+ll_gd[::-1, :]))

    ax[0].set_title("LogLL {}".format(mode))
    ax[1].set_title("LogPrior {}".format(mode))
    ax[2].set_title("LogPrior+LogLL {}".format(mode))

    ax[0].axis('off')
    ax[1].axis('off')
    ax[2].axis('off')
    ax[0].set_aspect('equal')
    ax[1].set_aspect('equal')
    ax[2].set_aspect('equal')
    fig.colorbar(pcm0, ax=ax[0])
    fig.colorbar(pcm1, ax=ax[1])
    fig.colorbar(pcm2, ax=ax[2])
    plt.savefig(fpath)

def plot_1_colormesh(im,title):
    fig, ax = plt.subplots(1, 1)
    pcm0 = ax.pcolormesh(im[::-1, :], cmap='RdBu', vmin=np.amin(im),
                            vmax=np.amax(im))
    ax.set_title(title)
    ax.axis('off')
    fig.colorbar(pcm0, ax=ax)