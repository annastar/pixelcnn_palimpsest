import json
import os.path



class Dict2Obj():
    def __init__(self, dict_a):
        """Convert dictionary into an object with keys as attributes, and values as values of attributes"""
        for key, value in dict_a.items():
            setattr(self, key, value)

def dict_to_json(fpath,dict_values,append=True):
    """Save dictionary in json file
    Args:
        fpath  - path of intended json file, should have .json extension at the end
        dict_values - dictionary that you want to save"""
    if append:
        mode = "a"
    else:
        mode = "w"
    with open(fpath, mode) as fp:
        json.dump(dict_values, fp)

def json_to_dict(fpath):
    """Restore dictionary from json file"""
    with open(fpath,'r') as fp:
        dict = json.load(fp)
    return dict

def create_dir(dirpath):
    if not os.path.isdir(dirpath):
        os.makedirs(dirpath)

def record_loss(loss,logdir,name,epoch):
    if epoch==0:
        mode = "w"
    else:
        mode = "a"
    with open(os.path.join(logdir,'loss_{}.csv'.format(name)), mode) as fd:
        fd.write(str(epoch)+","+str(loss)+"\n")

def record_loss_and_step(loss,steps,logdir,name,epoch):
    fpath = os.path.join(logdir, 'loss_{}.csv'.format(name))
    if not os.path.exists(fpath):
        steps_str = ",".join(["step"+str(step)+"," for step in range(len(steps))])
        line_str = "epoch,loss,"+steps_str +"\n"
        with open(fpath, "w") as fd:
            fd.write(line_str)

    steps_str = ",".join([str(step) + "," for step in steps[:,0]])
    line_str = str(epoch)+","+str(loss)+","+steps_str+"\n"
    with open(fpath, "a") as fd:
        fd.write(line_str)