import os
import matplotlib.pyplot as plt
from skimage import io
import numpy as np
from skimage.transform import rescale,rotate, resize
import json

class DataUtility():
    """
    Image dataset wrapper class
    Args:
        modalities - the list of requested modalities, WB - flourecent bank,
                MB - visible spectrum bank, TX - transmission bank,
                 RE,RS - raking east,south bank
    """

    def __init__(self,data_dir,msi,modalities=None):
        self.data_dir = data_dir
        if msi:
            fnames = self.read_band_names_txt()
            if not modalities is None:
                self.fnames = [fname for fname in fnames if self.check_if_belong_modality(modalities, fname)]
            else:
                self.fnames = fnames
            self.band_idx_fname,self.ordered_fnames = self.order_bandnames()
            #list of band indexes actually used for reconstruction
            self.nb_bands = len(self.fnames)
            self.idx_list = sorted(self.band_idx_fname.keys())

    def read_band_names_txt(self):
        fnames = [fname for fname in os.listdir(self.data_dir) if "tif" in fname]
        return sorted(fnames)

    def order_bandnames(self):
        band_idx_fname = {}
        ###dictionary with with band_idx: file_name pairs
        for fname in self.fnames:
            idx_str = fname[:-4].split("_")[-2]
            idx = int(idx_str)
            band_idx_fname[idx-1]=fname

        ordered_fnames = {}
        ###dictionary with with file_name:idx pairs, where idx means index of
        # this bands in final msi image
        for idx, band_idx in enumerate(sorted(band_idx_fname.keys())):
            ordered_fnames[band_idx_fname[band_idx]] = idx
        return band_idx_fname,ordered_fnames

    def scale_vals(self,x):
        if np.amax(x) > 255:
            max_val = 65535.0
        elif np.amax(x) > 1:
            max_val = 255.0
        x = x / max_val
        return x

    def normalize_msi(self,im):
        """Do spectral normalization of the multispectral palimpsest image batches"""
        im_norm = np.sqrt(np.sum(im ** 2, 2))
        im_normalized = im / np.expand_dims(np.maximum(im_norm, 1e-9), axis=2)
        im_normalized = (im_normalized*65535.0).astype(np.uint16)
        return im_normalized

    def save_norm_msi_im_as_tif_im(self,im_msi,path_sufix,partial_msi_idx=None):
        if partial_msi_idx is None:
            im_msi = self.normalize_msi(im_msi)
        else:
            im_msi = self.normalize_msi(im_msi[:,:,partial_msi_idx])
        dir_name = "normalized" if len(path_sufix)==0 else "normalized_"+path_sufix
        dir_path = os.path.join(self.data_dir, dir_name)
        if not os.path.isdir(dir_path):
            os.makedirs(dir_path)
        if partial_msi_idx is None:
            for i in range(im_msi.shape[-1]):
                for key,value in self.ordered_fnames.items():
                    if value == i:
                        fname = key
                io.imsave(os.path.join(dir_path,fname),im_msi[:,:,i])
        else:
            for i,part_idx in enumerate(partial_msi_idx):
                key = sorted(self.band_idx_fname.keys())[part_idx]
                io.imsave(os.path.join(dir_path,self.band_idx_fname[key]),im_msi[:,:,i])

    def read_json_mean_refl_val(self,mode,bitdepth):
        """Read reflectance values from txt file
        Arg: mode - str, [bg,ut,ot] for background, undertext, and overtext"""
        if mode=="bg":
            filename = "background_mean.json"
        elif mode=="ut":
            filename = "undertext_mean.json"
        elif mode=="ot":
            filename = "overtext_mean.json"
        else:
            Exception("No such file for reading refectance values")
        if bitdepth==16:
            max_val = 65535.0
        elif bitdepth==8:
            max_val =255.0
        with open(os.path.join(self.data_dir,filename),"rb") as f:
            d = json.load(f)
        refl = np.zeros(shape=(self.nb_bands))
        for fname,idx in self.ordered_fnames.items():
            refl[idx]=d[fname]
        refl = refl/max_val
        return refl



    def read_cov_bg_opt_dens(self):
        std_bg = np.loadtxt(os.path.join(self.data_dir,"std_bg_opt_dens.npy"))
        std_bg_lim_bands = np.zeros((len(self.idx_list),len(self.idx_list)))
        ii = 0
        for i in self.idx_list:
            jj = 0
            for j in self.idx_list:
                std_bg_lim_bands[ii,jj]=std_bg[i,j]
                jj += 1
            ii += 1
        return std_bg_lim_bands

    def read_cov_bg(self):
        std_bg = np.loadtxt(os.path.join(self.data_dir,"std_bg.npy"))
        std_bg_lim_bands = np.zeros((len(self.idx_list),len(self.idx_list)))
        ii = 0
        for i in self.idx_list:
            jj = 0
            for j in self.idx_list:
                std_bg_lim_bands[ii,jj]=std_bg[i,j]
                jj += 1
            ii += 1
        return std_bg_lim_bands

    def find_min_max_value(self,im, max_val):
        """Find minimum and maximum value the same way they are
        calculated when creating dataset"""
        bin_width = 1 if max_val == 256 else 10
        bins = np.arange(0, max_val, bin_width)
        hist, bins = np.histogram(im, bins=bins, density=True)
        max_val = None
        for idx in range(len(hist)):
            if hist[-idx] < 0.001 and hist[-idx - 1] == 0:
                max_val = bins[-idx - 1]
        return max_val



    def read_scale_ims_msi_fragment(self,row_coord, col_coord, scale_true=False, rot_angle=0):
        """
        Read images of each band of a page to create image cube,
        scale - default False, if True - scale an image size, such that the avarage character size would
                be the same size as in character generator
                (64 pixels in generator correspond to 172 pixels in palimpsest image)
        row_coord - [start,end] of patch rows of scaled im
        col_coord - [start,end] of patch columns of scaled im
        rot_angle - if rotation needed, set a Rotation angle in degrees in counter-clockwise direction.
        """
        scale = 64 / 192
        if scale_true:
            row_coord = list(map(lambda x: int(x * scale), row_coord))
            col_coord = list(map(lambda x: int(x * scale), col_coord))
        ims_msi = np.zeros((row_coord[1] - row_coord[0], col_coord[1] - col_coord[0], self.nb_bands))

        for fname in self.fnames:
            im = io.imread(os.path.join(self.data_dir, fname), as_gray=True)
            if not rot_angle==0:
                im = rotate(im,rot_angle,resize=True,preserve_range=True)

            if scale_true:
                im = rescale(im, (scale, scale), preserve_range=True)
            im = im[row_coord[0]:row_coord[1], col_coord[0]:col_coord[1]]

            if np.amax(im) > 255:
                im = im / 65535.0
            elif np.amax(im) > 1:
                im = im / 255.0
            ims_msi[:, :, self.ordered_fnames[fname]] = im
        #ims_msi = ims_msi / np.amax(ims_msi)
        return ims_msi[np.newaxis,:,:]

    def check_if_belong_modality(self,modalities,fname):
        assert type(modalities) is list, "Modalities should be a list"
        belongs_to_modality = [True for mode in modalities if mode.lower() in fname.lower()]
        if len(belongs_to_modality)==0:
            return False
        else:
            return True

    def adjust_im_scope(self,coord,patch_size):
        res = coord[1] - coord[0]
        while res % patch_size != 0:
            if res % 2 == 0:
                coord[1] += 1
                coord[0] -= 1
            else:
                coord[1] += 1
            res = coord[1] - coord[0]
        return coord

    def read_scale_ims_fragment(self,fname,row_coord, col_coord, scale_true=False, rot_angle=0):
        """
        Read images of each band of a page to create image cube,
        scale - default False, if True - scale an image size, such that the avarage character size would
                be the same size as in character generator
                (64 pixels in generator correspond to 172 pixels in palimpsest image)
        row_coord - [start,end] of patch rows of unscaled im
        col_coord - [start,end] of patch columns of unscaled im
        """
        scale = 64 / 192
        im = io.imread(os.path.join(self.data_dir, fname), as_gray=True)
        if scale_true:
            row_coord = list(map(lambda x: int(x * scale), row_coord))
            col_coord = list(map(lambda x: int(x * scale), col_coord))
            if not rot_angle==0:
                im = rotate(im,rot_angle,resize=True,preserve_range=True)
            im = rescale(im, (scale, scale), preserve_range=True)
        im = im[row_coord[0]:row_coord[1], col_coord[0]:col_coord[1]]
        if np.amax(im) > 255:
            im = im / 65535.0
        elif np.amax(im) > 1:
            im = im / 255.0
        return im[np.newaxis,:,:,np.newaxis]

    def read_scale_overtext_fragment(self, fname, row_coord, col_coord, scale_true=False):
        """
        Read thresholded overtext image
        scale - default False, if True - scale an image size, such that the avarage character size would
                be the same size as in character generator
                (64 pixels in generator correspond to 172 pixels in palimpsest image)
        row_coord - [start,end] of patch rows
        col_coord - [start,end] of patch columns
        """
        im = io.imread(os.path.join(self.data_dir, fname), as_gray=True)
        scale = 64 / 192
        if scale_true:
            row_coord = list(map(lambda x: int(x * scale), row_coord))
            col_coord = list(map(lambda x: int(x * scale), col_coord))
            im = rescale(im, (scale, scale), preserve_range=True)
        im = im[row_coord[0]:row_coord[1], col_coord[0]:col_coord[1]]
        if np.amax(im) > 255:
            im = im / 65535.0
        elif np.amax(im) > 1:
            im = im / 255.0
        im = 1 - im/np.amax(im)

        return im[np.newaxis,:,:,np.newaxis]

def find_max(im,max_val):
    """Find max value without oversaturated pixels.
    We find suspected oversaturated pixel location, and if the next bin is 0,
    we skip through that bin and count the edge of the zero bin as a max value.
    max_val - scalar, bit depth"""
    if max_val == 1:
        bin_width = 1 / 255.0
    elif max_val == 256:
        bin_width = 1
    else:
        bin_width = 10
    bins = np.arange(0, max_val, bin_width)
    hist, bins = np.histogram(im, bins=bins, density=True)
    hist = hist/np.sum(hist)
    idx = 1
    while hist[-idx] < 0.001 and hist[-max(1, idx + 1)] == 0:
        max_val = bins[-idx-1]
        idx += 1
    return max_val

def msi_img_extr_test():
    data_dir = r"C:\Data\PhD\palimpsest\Greek_960\0086_000084"
    ot_fname = "0086_000084+MB625Rd_007_F_thresh.png"
    coord_x = [2075, 2196]#[1000,1000+192]#
    coord_y = [2085, 3698]#[2800,4500]#
    loader = DataUtility()
    im_out = loader.read_scale_ims_msi_fragment(data_dir, col_coord=coord_y, row_coord=coord_x, modalities=["WB"], rot_angle=-90,scale_true=True)
    fig,ax = plt.subplots(1,im_out.shape[-1]+1)
    for band in range(im_out.shape[-1]):
        ax[band].imshow(im_out[0,:,:,band],cmap="gray")
    im_out = loader.read_scale_overtext_fragment(os.path.join(data_dir,ot_fname), col_coord=coord_y, row_coord=coord_x, scale_true=True)
    ax[band+1].imshow(im_out[0, :, :,0], cmap="gray")
    plt.show()

def build_image_1_line(main_dir):
    """
        Built image from 1 line
        :params
         main_dir - folder with lines
        :return:
        4darray [height,width] - image of page
        """
    dataset_dir = os.path.join(main_dir, "datasets", "Greek_960", "full_lines")
    scale = 64 / 192
    length = 192 * 1
    line1 = io.imread(os.path.join(dataset_dir, "0086_000084", "line_0.png"), as_gray=True)[:, 0:length]
    line1 = line1[int((line1.shape[0] - 192) / 2):int((line1.shape[0] - 192) / 2) + 192]
    im = resize(line1,
                             (64, int(length * scale) if int(length * scale) % 64 == 0 else int(length * scale) + 1))
    return im

def build_im_background(dataset_dir):
    """
        Built image from 1 line
        :params
         main_dir - folder with lines
        :return:
        4darray [height,width] - image of page
        """
    dataload = DataUtility(dataset_dir,False)
    coord_x = [900, 900+64*1*192/64]
    coord_y = [2511, 2511+64*4*192/64]
    im = dataload.read_scale_ims_fragment(fname="0086_000084+MB625Rd_007_F.tif",
                                          row_coord=coord_x,col_coord=coord_y,scale_true=True,rot_angle=-90)
    im = im*255.0
    io.imsave(os.path.join(r"c:\Data\PhD\bss_autoreg_palimpsest","datasets",r"Greek_960","exp_art_palimpsest","bg_line.png"),im[0,:,:,0])
    im = io.imread(os.path.join(r"c:\Data\PhD\bss_autoreg_palimpsest","datasets",r"Greek_960","exp_art_palimpsest","bg_line.png"),as_gray=True)
    #im = im - np.amin(im)
    im = im/255.0
    return im[np.newaxis,:,:,np.newaxis]

def strech_contrast(im,max_val):
    """Strech im by max value without oversaturated pixels
    max_val - int, bit depth"""
    max_val = find_max(im,max_val)

    im = np.clip(im,a_max=max_val,a_min=0.)
    im = im - np.amin(im)
    im = im/np.amax(im)
    return im


def strech_contrast_like_in_dataset(im, max_val):
    bin_width = 1 if max_val == 256 else 10
    bins = np.arange(0, max_val, bin_width)
    hist, bins = np.histogram(im, bins=bins, density=True)
    max_val = None
    for idx in range(len(hist)):
        if hist[-idx] < 0.001 and hist[-idx - 1] == 0:
            max_val = bins[-idx - 1]

    im = np.clip(im, a_max=max_val, a_min=0.)
    amin = np.amin(im)
    amax = np.amax(im)
    im = im - amin
    im = im / amax
    return im, amin, amax

def build_im_background_scaled(dataset_dir):
    """
        Built image from 1 line
        :params
         main_dir - folder with lines
        :return:
        4darray [height,width] - image of page
        """
    folioname = r"0086_000084"
    scale = 64/192
    im = io.imread(os.path.join(dataset_dir, folioname + r"+MB625Rd_007_F.tif"), as_gray=True)
    im = rescale(im, (scale, scale), preserve_range=True)
    im = rotate(im, -90, resize=True, preserve_range=True)
    if np.amax(im) > 255:
        max_val = 65535
    elif np.amax(im) > 1 and np.amax(im) < 256:
        max_val = 256
    else:
        raise IOError("Expected format is uint8 or uint16")
    im = strech_contrast(im, max_val)
    row_coord = [900, 900+64*1*192/64]
    col_coord = [2511, 2511+64*4*192/64]
    row_coord = list(map(lambda x: int(x * scale), row_coord))
    col_coord = list(map(lambda x: int(x * scale), col_coord))
    im = im[row_coord[0]:row_coord[1],col_coord[0]:col_coord[1]]
    return im[np.newaxis,:,:,np.newaxis]

def build_image_2_lines(blank_size,main_dir,mode):
    """
    Built image from two lines with varying space between lines
    :params
     blank_size: number of pixels between the lines of scaled image
     noise_level: noise mask ratio
    :return:
    4darray [height,width] - image of page
    """
    dataset_dir = os.path.join(main_dir,"datasets","Greek_960","full_lines")
    scale = 64/192
    length = 192*3
    line1 = io.imread(os.path.join(dataset_dir,"0086_000084","line_0.png"),as_gray=True)[:,0:length]
    line2 = io.imread(os.path.join(dataset_dir,"0086_000084","line_5.png"),as_gray=True)[:,0:length]
    line1 = line1[int((line1.shape[0]-192)/2):int((line1.shape[0]-192)/2)+192]
    line2 = line2[int((line2.shape[0] - 192) / 2):int((line2.shape[0] - 192) / 2) + 192]
    line1 = resize(line1,(64,int(length*scale) if int(length*scale)%64==0 else int(length*scale)+1))
    line2 = resize(line2,(64,int(length*scale) if int(length*scale)%64==0 else int(length*scale)+1))
    if mode=="concat":
        if blank_size>0:
            im = np.row_stack([line1,np.ones((blank_size,line1.shape[1])),line2])
        else:
            im = np.row_stack([line1,line2])
    elif mode=="cut":
        im = np.row_stack([np.ones((32,line1.shape[1])),line1[:32]])
    elif mode=="cut_low":
        im = np.row_stack( [line1[32:], np.ones((32, line1.shape[1]))] )
    return im

if __name__=="__main__":
    #msi_img_extr_test()
    im = build_im_background_scaled(os.path.join(r"C:\Data\PhD\bss_autoreg_palimpsest", "datasets", r"Greek_960", r"0086_000084"))
    io.imsave(os.path.join("C:\Data\PhD\documents\wiml2022\Du_0.12088244182532486with_bg_t_2022-11-30-08-29","bg_org.png"),np.squeeze(im),)