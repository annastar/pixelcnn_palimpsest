from pixelcnn_palimpsest.utils.read_images import DataUtility

if __name__=="__main__":
    data_dir = r"C:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\0086_000084"
    loader = DataUtility(data_dir, True, ["MB","WB"])
    im_out = loader.read_scale_ims_msi_fragment(col_coord=[0,8176], row_coord=[0,6132],
                                                scale_true=False, rot_angle=0)
    band_idxs = [0, 1, 2, 3,12, 18]
    loader.save_norm_msi_im_as_tif_im(im_out[0],"MB_WB_without_bleedthrough",band_idxs)

