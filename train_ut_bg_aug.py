"""
Trains a Pixel-CNN++ generative model on CIFAR-10 or Tiny ImageNet data.
Uses multiple GPUs, indicated by the flag --nr_gpu

Example usage:
CUDA_VISIBLE_DEVICES=0,1,2,3 python train_double_cnn.py --nr_gpu 4
"""
import copy
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
import sys
import json
import argparse
import time
import numpy as np
import tensorflow as tf
from pixelcnn_palimpsest.pixel_cnn_pp import nn
from pixelcnn_palimpsest.pixel_cnn_pp.model import model_spec_gray_log_mix, model_spec_discrete
from pixelcnn_palimpsest.utils import plotting,files

device = "gpu"
# -----------------------------------------------------------------------------
parser = argparse.ArgumentParser()
# data I/O
parser.add_argument('-i', '--data_dir', type=str, default=r'c:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_ut_dataset_50step_192x192', help='Location for the dataset')
parser.add_argument('-i_at', '--aug_bg_data_dir', type=str, default=r'c:\Data\PhD\bss_autoreg_palimpsest\datasets\Greek_960\Greek960_bg_dataset_strech_contr_step_64_size_192',
                    help='Location for the  text augmentation dataset')
parser.add_argument('-o', '--save_dir', type=str, default=r"c:\Data\PhD\bss_autoreg_palimpsest\training\generators\bg\pixelcnn-temp",
                    help='Location for parameter checkpoints and samples')
parser.add_argument('-ro', '--rest_dir', type=str, default=r"c:\Data\PhD\bss_autoreg_palimpsest\training\generators\bg\pixelcnn-temp", help='Location for parameter checkpoints and samples to restore from')
parser.add_argument('-d', '--data_set', type=str, default='greek960_ut', help='Can be either cifar|greek960')
parser.add_argument('-dv', '--device', type=str, default='cpu', help='Device to run model on')
parser.add_argument('-t', '--save_interval', type=int, default=2, help='Every how many epochs to write checkpoint/samples?')
parser.add_argument('-re', '--restore_epoch', default=0 , type=int,help='Epoch to restore training model checkpoint?')
# model
parser.add_argument('-q', '--nr_resnet', type=int, default=2, help='Number of residual blocks per stage of the model')
parser.add_argument('-n', '--nr_filters', type=int, default=32, help='Number of filters to use across the model. Higher = larger model.')
parser.add_argument('-m', '--nr_logistic_mix', type=int, default=5, help='Number of logistic components in the mixture. Higher = more flexible model')
parser.add_argument('-z', '--resnet_nonlinearity', type=str, default='concat_elu', help='Which nonlinearity to use in the ResNet layers. One of "concat_elu", "elu", "relu" ')
parser.add_argument('-c', '--class_conditional', dest='class_conditional', action='store_true', help='Condition generative model on labels?')
parser.add_argument('-ed', '--energy_distance', dest='energy_distance', action='store_true', help='use energy distance in place of likelihood')
parser.add_argument('-ce', '--cross_entropy', dest='cross_entropy', action='store_true', help='use cross_entropy loss in place of likelihood')
parser.add_argument('-sl', '--noise_level', type=float, default=0.0, help='value of sigma or the scaler of standard Gaussian noise')
# optimization
parser.add_argument('-l', '--learning_rate', type=float, default=0.0004, help='Base learning rate')
parser.add_argument('-e', '--lr_decay', type=float, default=0.999995, help='Learning rate decay, applied every step of the optimization')
parser.add_argument('-b', '--batch_size', type=int, default=2, help='Batch size during training per GPU')
parser.add_argument('-u', '--init_batch_size', type=int, default=16, help='How much data to use for data-dependent initialization.')
parser.add_argument('-p', '--dropout_p', type=float, default=0.5, help='Dropout strength (i.e. 1 - keep_prob). 0 = No dropout, higher = more dropout.')
parser.add_argument('-x', '--max_epochs', type=int, default=5000, help='How many epochs to run in total?')
parser.add_argument('-g', '--nr_gpu', type=int, default=1, help='How many GPUs to distribute the training across?')
parser.add_argument('-bn', '--binary_data', dest='binary_data', action='store_true', help='Generate binary image?')
parser.add_argument('-aug', '--augmentation', action='store_true', help='Do you want to do data augmentation?')
parser.add_argument('-perth', '--per_thresh', type=float, default=0.0, help='If you add augmentation with background patterns, what percentage of pixels'
                                                                            ' you want to be occupied by this pattern, e.g. 100 means full image')
parser.add_argument('-pw', '--positive_weight', type=float, default=5, help='Weight that added to false negative pixels loss')
# evaluation
parser.add_argument('--polyak_decay', type=float, default=0.9995, help='Exponential decay rate of the sum of previous model iterates during Polyak averaging')
parser.add_argument('-ns', '--num_samples', type=int, default=1, help='How many batches of samples to output.')

# reproducibility
parser.add_argument('-s', '--seed', type=int, default=1, help='Random seed to use')
args = parser.parse_args()

device = args.device
args.model_scope = "model_ut"
ckpt_file = args.rest_dir + '/params_' + args.data_set + str(args.restore_epoch) + '.ckpt'
if not args.aug_bg_data_dir is None:
    if args.aug_bg_data_dir.lower()=="none":
        args.aug_bg_data_dir = None
files.create_dir(args.save_dir)
if args.rest_dir!=args.save_dir:
    files.dict_to_json(os.path.join(args.save_dir, "model_param.json"), args.__dict__)
else:
    files.dict_to_json(os.path.join(args.save_dir, "model_param_rest_{}.json".format(args.restore_epoch)), args.__dict__)
print('input args:\n', json.dumps(vars(args), indent=4, separators=(',',':'))) # pretty print args
# save parameters in json file
# -----------------------------------------------------------------------------
# fix random seed for reproducibility
rng = np.random.RandomState(args.seed)
tf.set_random_seed(args.seed)

# energy distance or maximum likelihood?
if args.energy_distance:
    loss_fun = nn.energy_distance
else:
    if args.binary_data:
        if args.aug_bg_data_dir is None:
            loss_fun = nn.sigmoid_loss_binary
        else:
            loss_fun = nn.weighted_loss_decorator(args.positive_weight)
    else:
        if args.cross_entropy:
            loss_fun = nn.softmax_loss_gray
        else:
            loss_fun = nn.discretized_mix_logistic_loss_gray

# initialize data loaders for train/test splits
if args.data_set == 'greek960_ut':
    import data.greek960_ut_data as greek960_data_ut
    DataLoader = greek960_data_ut.DataLoaderBgAug
else:
    raise("unsupported dataset")

train_data = DataLoader(args.data_dir, 'train', args.batch_size * args.nr_gpu, rng=rng,
                        shuffle=True, augment=args.augmentation,aug_with_bg = None if args.aug_bg_data_dir is None else {"data_dir":args.aug_bg_data_dir, "per_thresh":args.per_thresh})
test_data = DataLoader(args.data_dir, 'test', args.batch_size * args.nr_gpu,
                       shuffle=False, aug_with_bg= None if args.aug_bg_data_dir is None else {"data_dir":args.aug_bg_data_dir, "per_thresh":args.per_thresh})
obs_shape = train_data.get_observation_size() # e.g. a tuple (32,32,3)
assert len(obs_shape) == 3, 'shape should be 3, but it is {}'.format(len(obs_shape))
print("Loaded the data")
# data place holders
xs_true = [tf.placeholder(tf.float32, shape=(args.batch_size, ) + obs_shape, name="x_"+str(i)) for i in range(args.nr_gpu)]
# noise place holders
ns = [tf.placeholder(tf.float32, shape=(args.batch_size, ) + obs_shape, name="noise_"+str(i)) for i in range(args.nr_gpu)]
# masks place holders
xs_input = [tf.placeholder(tf.float32, shape=(args.batch_size, ) + obs_shape) for i in range(args.nr_gpu)]
# if the model is class-conditional we'll set up label placeholders + one-hot encodings 'h' to condition on
if args.class_conditional:
    num_labels = train_data.get_num_labels()
    y_init = tf.placeholder(tf.int32, shape=(args.init_batch_size,))
    h_init = tf.one_hot(y_init, num_labels)
    y_sample = np.split(np.mod(np.arange(args.batch_size*args.nr_gpu), num_labels), args.nr_gpu)
    h_sample = [tf.one_hot(tf.Variable(y_sample[i], trainable=False), num_labels) for i in range(args.nr_gpu)]
    ys = [tf.placeholder(tf.int32, shape=(args.batch_size,)) for i in range(args.nr_gpu)]
    hs = [tf.one_hot(ys[i], num_labels) for i in range(args.nr_gpu)]
else:
    h_init = None
    h_sample = [None] * args.nr_gpu
    hs = h_sample

# create the model
model_opt = { 'nr_resnet': args.nr_resnet, 'nr_filters': args.nr_filters, 'nr_logistic_mix': args.nr_logistic_mix,
              'resnet_nonlinearity': args.resnet_nonlinearity, 'energy_distance': args.energy_distance}
if args.binary_data:
    model_opt["q_levels"]=1
    model = tf.make_template(args.model_scope, model_spec_discrete)
else:
    if args.cross_entropy:
        model_opt["q_levels"] = 256
        model = tf.make_template(args.model_scope, model_spec_discrete)
    else:
        model = tf.make_template(args.model_scope, model_spec_gray_log_mix)


# compile model
first_pass = model(xs_input[0]+ns[0], hs[0], dropout_p=args.dropout_p, **model_opt)
# keep track of moving average
all_params = tf.trainable_variables()
ema = tf.train.ExponentialMovingAverage(decay=args.polyak_decay)
maintain_averages_op = tf.group(ema.apply(all_params))
ema_params = [ema.average(p) for p in all_params]

# get loss gradients over multiple GPUs + sampling
grads = []
loss_gen = []
loss_gen_test = []
new_x_gen = []
for i in range(args.nr_gpu):
    with tf.device('/{}:{}'.format(device,i)):
        # train
        out = model(xs_input[i]+ns[i], hs[i], ema=None, dropout_p=args.dropout_p, **model_opt)
        loss_gen.append(loss_fun(tf.stop_gradient(xs_true[i]), out))

        # gradients
        grads.append(tf.gradients(loss_gen[i], all_params, colocate_gradients_with_ops=True))

        # test
        out = model(xs_input[i]+ns[i], hs[i], ema=ema, dropout_p=0., **model_opt)
        loss_gen_test.append(loss_fun(xs_true[i], out))

        # sample
        out = model(xs_input[i]+ns[i], h_sample[i], ema=ema, dropout_p=0., **model_opt)
        if args.energy_distance:
            new_x_gen.append(out[0])
        else:
            if args.binary_data:
                new_x_gen.append(nn.sample_from_sigmoid(out))
            else:
                if args.cross_entropy:
                    new_x_gen.append(nn.sample_from_softmax(out))
                else:
                    new_x_gen.append(nn.sample_from_discretized_mix_logistic_gray(out, args.nr_logistic_mix))

print("compile the model graph")
# add losses and gradients together and get training updates
tf_lr = tf.placeholder(tf.float32, shape=[])
with tf.device('/{}:0'.format(device)):
    for i in range(1,args.nr_gpu):
        loss_gen[0] += loss_gen[i]
        loss_gen_test[0] += loss_gen_test[i]
        for j in range(len(grads[0])):
            grads[0][j] += grads[i][j]
    # training op
    optimizer = tf.group(nn.adam_updates(all_params, grads[0], lr=tf_lr, mom1=0.95, mom2=0.9995), maintain_averages_op)
print("added an optimizer")
# convert loss to bits/dim
bits_per_dim = loss_gen[0]/(args.nr_gpu*np.log(2.)*np.prod(obs_shape)*args.batch_size)
bits_per_dim_test = loss_gen_test[0]/(args.nr_gpu*np.log(2.)*np.prod(obs_shape)*args.batch_size)

# sample from the model
def sample_from_model(sess):
    x_gen = [np.zeros((args.batch_size,) + obs_shape, dtype=np.float32) for i in range(args.nr_gpu)]
    fd = {ns[i]: args.noise_level * np.random.normal(0.0, 1.0, x_gen[0].shape) for i in range(args.nr_gpu)}
    for yi in range(obs_shape[0]):
        for xi in range(obs_shape[1]):
            fd.update({xs_input[i]:x_gen[i] for i in range(args.nr_gpu)})
            new_x_gen_np = sess.run(new_x_gen, fd)
            for i in range(args.nr_gpu):
                x_gen[i][:,yi,xi,:] = new_x_gen_np[i][:,yi,xi,:]
    return np.concatenate(x_gen, axis=0)

# init & save
initializer = tf.global_variables_initializer()
saver = tf.train.Saver(max_to_keep=20)

# turn numpy inputs into feed_dict for use with tensorflow
def make_feed_dict(data,argsi):
    if type(data) == tuple:
        if argsi.class_conditional:
            if len(data)>2:
                x_tr, x_aug_text, y = data
            else:
                x_tr,y = data
                x_aug_text = None
        else:
            x_tr,x_aug_text = data
            y = None
    else:
        x_tr = data
        y = None
        x_aug_text = None
    if argsi.binary_data==True or argsi.cross_entropy==True:
        x_tr = np.cast[np.float32](x_tr)/255.0
        if not x_aug_text is None:
            x_aug_text = np.cast[np.float32](x_aug_text)/255.0
    else:
        x_tr = np.cast[np.float32]((x_tr - 127.5) / 127.5) # input to pixelCNN is scaled from uint8 [0,255] to float in range [-1,1]
        if not x_aug_text is None:
            x_aug_text = np.cast[np.float32]((x_aug_text - 127.5) / 127.5)
    noise = argsi.noise_level * np.random.normal(0.0, 1.0, x_tr.shape)
    noise = np.split(noise, argsi.nr_gpu)
    x_tr = np.split(x_tr, argsi.nr_gpu)

    feed_dict = {xs_true[i] : x_tr[i] for i in range(argsi.nr_gpu)}
    feed_dict.update({ns[i]: noise[i] for i in range(argsi.nr_gpu)})
    if not x_aug_text is None:
        x_aug_text = np.split(x_aug_text, argsi.nr_gpu)
        feed_dict.update({xs_input[i]: x_aug_text[i] for i in range(argsi.nr_gpu)})
    else:
        feed_dict.update({xs_input[i]: x_tr[i] for i in range(argsi.nr_gpu)})
    if y is not None:
        y = np.split(y, argsi.nr_gpu)
        feed_dict.update({ys[i]: y[i] for i in range(argsi.nr_gpu)})
    return feed_dict

# //////////// perform training //////////////
if not os.path.exists(args.save_dir):
    os.makedirs(args.save_dir)
test_bpd = []
train_bpd = []
lr = args.learning_rate
with tf.Session() as sess:
    start_epoch = 0
    if args.restore_epoch>0:
        #if we continue for the same noise level then continue the epoch
        #count, otherwise start over
        train_data.reset()  # rewind the iterator back to 0 to do one full epoch
        print('restoring parameters from', ckpt_file)
        saver.restore(sess, ckpt_file)
        print('starting finetuning')
        if args.rest_dir!=args.save_dir:
            start_epoch = 0
        else:
            start_epoch = args.restore_epoch+1
            graph = tf.get_default_graph()
            test_bpd = np.load(args.save_dir + '/test_bpd_' + args.data_set + '.npz')['test_bpd'].tolist()
            train_bpd = np.load(args.save_dir + '/train_bpd_' + args.data_set + '.npz')["train_bpd"].tolist()
            ###check if moving average is restored
            bar2 = graph.get_tensor_by_name('model_ut/conv2d_0/b/ExponentialMovingAverage:0')
            np.savetxt(os.path.join(args.save_dir, "ema_conv2d_0_b_epoch{}".format(args.restore_epoch)), sess.run(bar2))
    else:
        train_data.reset()  # rewind the iterator back to 0 to do one full epoch
        print('initializing the model...')
        sess.run(initializer)
        data = train_data.next(args.batch_size*args.nr_gpu)
        feed_dict = make_feed_dict(data,args)  # manually retrieve exactly init_batch_size examples
        sess.run(first_pass, feed_dict)
        print('starting training')


    for epoch in range(start_epoch,args.max_epochs+1):

        begin = time.time()
        # train for one epoch
        train_losses = []
        if args.binary_data:
            train_losses_nats = []
            test_losses_nats = []

        for d in train_data:
            feed_dict = make_feed_dict(d,args)
            # forward/backward/update model on each gpu
            lr *= args.lr_decay
            feed_dict.update({ tf_lr: lr })
            l,_ = sess.run([bits_per_dim, optimizer], feed_dict)
            if args.binary_data:
                l_nats = sess.run(loss_gen[0], feed_dict)
                train_losses_nats.append(l_nats)
            train_losses.append(l)
        train_loss_gen = np.mean(train_losses)
        # compute likelihood over test data
        test_losses = []
        for d in test_data:
            feed_dict = make_feed_dict(d,args)
            l = sess.run(bits_per_dim_test, feed_dict)
            if args.binary_data:
                l_nats = sess.run(loss_gen_test[0], feed_dict)
                test_losses_nats.append(l_nats)
            test_losses.append(l)
        test_loss_gen = np.mean(test_losses)
        test_bpd.append(test_loss_gen)
        train_bpd.append(train_loss_gen)

        # log progress to console
        if args.binary_data:
            print("Iteration %d, time = %ds, train nats_per_dim = %.4f, test nats_per_dim = %.4f" % (
            epoch, time.time() - begin, np.mean(train_losses_nats), np.mean(test_losses_nats)))
        print("Iteration %d, time = %ds, train bits_per_dim = %.4f, test bits_per_dim = %.4f" % (epoch, time.time()-begin, train_loss_gen, test_loss_gen))
        sys.stdout.flush()

        if epoch % args.save_interval == 0:
            # generate samples from the model
            sample_x = []
            for i in range(args.num_samples):
                sample_x.append(sample_from_model(sess))
            sample_x = np.concatenate(sample_x,axis=0)
            img_tile_test = plotting.img_tile(d[1] if type(d)==tuple else d, aspect_ratio=1.0, border_color=1.0, stretch=True)
            img_test = plotting.plot_img(img_tile_test, title=args.data_set + ' test_inputs', gray=True)
            plotting.plt.savefig(os.path.join(args.save_dir, '%s_test_inputs%d.png' % (args.data_set, epoch)))
            plotting.plt.close('all')
            img_tile = plotting.img_tile(sample_x[:100], aspect_ratio=1.0, border_color=1.0, stretch=True)
            img = plotting.plot_img(img_tile, title=args.data_set + ' samples', gray=True)
            plotting.plt.savefig(os.path.join(args.save_dir,'%s_sample%d.png' % (args.data_set, epoch)))
            plotting.plt.close('all')
            np.savez(os.path.join(args.save_dir,'%s_sample%d.npz' % (args.data_set, epoch)), sample_x)
            # save params
            saver.save(sess, args.save_dir + '/params_' + args.data_set + str(epoch)+'.ckpt')
            np.savez(args.save_dir + '/test_bpd_' + args.data_set + '.npz', test_bpd=test_bpd)
            np.savez(args.save_dir + '/train_bpd_' + args.data_set + '.npz', train_bpd=train_bpd)
