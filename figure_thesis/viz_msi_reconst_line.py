import os
from skimage import io
import numpy as np
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.utils.files import json_to_dict
from pixelcnn_palimpsest.figure_thesis.viz_msi_reconst import read_org_rgb_im,read_pred_rgb_im




if __name__=="__main__":
    logdir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\greek960\msi_aver_bg1bandwith_bg_t_2023-05-24-18-18"
    read_org_rgb_im(logdir)
    read_pred_rgb_im(logdir)