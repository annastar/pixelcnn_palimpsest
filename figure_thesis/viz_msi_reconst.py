import os
from skimage import io
import numpy as np
from matplotlib import pyplot as plt
from pixelcnn_palimpsest.utils.files import json_to_dict


def read_org_rgb_im(logdir):
    fname0 = os.path.join(logdir,"corrupt_im_band_0.png")
    fname1 = os.path.join(logdir, "corrupt_im_band_1.png")
    fname2 = os.path.join(logdir, "corrupt_im_band_2.png")
    rgb = make_rgb_im(fname0,fname1,fname2)
    io.imsave(os.path.join(logdir,"corrupt_pal_rgb.png"),rgb)
    return rgb


def make_rgb_im(fname0,fname1,fname2):
    r = io.imread(fname0, as_gray=True)
    g = io.imread(fname1, as_gray=True)
    b = io.imread(fname2, as_gray=True)
    rgb = np.stack([r, g, b], axis=2)
    return rgb

def read_pred_rgb_im(logdir):
    fname0 = os.path.join(logdir,"restored", "band0","109999.png")
    fname1 = os.path.join(logdir,"restored", "band1","109999.png")
    fname2 = os.path.join(logdir,"restored", "band2","109999.png")
    rgb = make_rgb_im(fname0,fname1,fname2)
    io.imsave(os.path.join(logdir, "pred_pal_rgb.png"), rgb)
    return rgb

def recreate_rgb_image_with_ut_from_1band(logdir,logdir_1band):
    bg = io.imread(os.path.join(logdir,"bg","109999.png"),as_gray=True)[:,:,np.newaxis]/255.0
    ut = io.imread(os.path.join(logdir_1band,"ut","109999.png"),as_gray=True)[:,:,np.newaxis]/255.0
    d = json_to_dict(os.path.join(logdir,"par.json"))
    ot = io.imread(os.path.join(logdir,"mask.png"),as_gray=True)[:,:,np.newaxis]/255.0
    Du = np.array(d["Du"])
    Do = np.array(d["Do"])
    Db = np.array(d["Db"])
    rgb = Do*ot+Du*ut*(1-ot)+Db*bg
    io.imsave(os.path.join(logdir,"temp", "pred_pal_rgb_with_ut_from_1band_pal.png"), rgb)

def pred_vs_org_bg_sig(coord,org,pred):
    print("Org val",org[coord[0],coord[1]])
    print("Pred val", pred[coord[0], coord[1]])


if __name__=="__main__":
    rgb_pred = read_pred_rgb_im()
    rgb_org = read_org_rgb_im()
    coord = (56,36)
    pred_vs_org_bg_sig(coord,rgb_org,rgb_pred)
    recreate_rgb_image_with_ut_from_1band()
