
from pixelcnn_palimpsest.figure_thesis.viz_msi_reconst import read_org_rgb_im,read_pred_rgb_im





if __name__=="__main__":
    ch = "lower_delta\obscured_delta_lower_148x148_rgb_27"
    logdir = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\rc\msi\{}".format(ch)
    logdir_1band = r"C:\Data\PhD\bss_autoreg_palimpsest\training\langevin\archi_chars\rc\1band\{}".format(ch)
    rgb_pred = read_pred_rgb_im(logdir)
    rgb_org = read_org_rgb_im(logdir)
    coord = (56,36)
    #pred_vs_org_bg_sig(coord,rgb_org,rgb_pred)
    #recreate_rgb_image_with_ut_from_1band()
