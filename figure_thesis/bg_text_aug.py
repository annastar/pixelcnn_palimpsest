import tensorflow as tf
import numpy as np
from pixelcnn_palimpsest.pixel_cnn_pp import nn
from pixelcnn_palimpsest.analysis.log_loss_smoothed_pdf import log_loss_smoothed_pdf
from pixelcnn_palimpsest.langevin_dynamics.prior import compile_prior_model,restore_model
from pixelcnn_palimpsest.langevin_dynamics import data
from matplotlib.gridspec import SubplotSpec
from matplotlib.ticker import FormatStrFormatter
import os
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 18})
plt.rcParams["figure.figsize"] = [8, 8]

#plt.rcParams["figure.autolayout"] = True

def compile_restore_bg_model(sess,nb_samples,res_epoch,nb_mixtures,nb_res_blocks,checkpoint_dir):
    x_prior, l_prior = compile_prior_model(nb_samples, "bg", nr_filters=32,
                                               nr_resnet=nb_res_blocks, nr_mix=nb_mixtures, model_discrete=False,
                                               model_scope="model_bg")
    sess.run(tf.initialize_all_variables())
    restore_model(os.path.join(checkpoint_dir,
                               "params_greek960_bg{}.ckpt".format(res_epoch)), sess, "model_bg")
    return x_prior,l_prior

def create_bg_ut_pair(main_dir):
    data_dir = os.path.join(main_dir, "datasets", r"Greek_960", "exp_art_palimpsest")
    data_dir_bg = os.path.join(main_dir, "datasets", r"Greek_960", r"0086_000084")
    fname_ot = r"line_4_moved.png"
    fname_ut = r"line_8_0_3.png"
    row_coord = [0, 192]
    col_coord = [0, 192 * 4]
    pal = data.PrepArtPal(data_dir, data_dir_bg, row_coord, col_coord,fname_ot, fname_ut, True,True)
    return pal.x_bg[:,:64,:64],pal.x_ut

def create_aug_bg(main_dir):
    bg_org, ut = create_bg_ut_pair(main_dir)
    ut = 1 - ut
    contr = -0.3
    bg = bg_org + contr * ut
    bg = np.clip(bg, a_min=0 / 255., a_max=255. / 255.)
    bg = 2.0 * bg - 1.0
    bg_org = 2.0*bg_org - 1.0
    if noise > 0:
        bg = bg + noise * np.random.uniform(0, 1.0, bg.shape)
    return bg_org,bg
def nll_to_prob(x):
    return np.exp(-x)

def smoothll_to_prob(x):
    return np.exp(x)

def extract_pred(res_epoch,nb_mix,nb_res_blocks,checkpoint_dir,noise,bg):
    tf.compat.v1.reset_default_graph()
    sess = tf.Session()
    s_tf = tf.constant(noise, dtype=tf.float32)
    checkpoint_dir = os.path.join(checkpoint_dir,"noise_{}".format(noise))
    x_prior_tf, l_prior_tf = compile_restore_bg_model( sess, 1, res_epoch, nb_mix,nb_res_blocks,checkpoint_dir)
    smooth_loss_tf,disc_prob_tf = log_loss_smoothed_pdf(x_prior_tf, l_prior_tf, s_tf, False)
    nll_tf = nn.discretized_mix_logistic_loss_gray(x_prior_tf, l_prior_tf, False)
    disc_prob,nll,smooth_loss= sess.run([disc_prob_tf,nll_tf,smooth_loss_tf], feed_dict={
        x_prior_tf: bg[:, 0:64, 0:64]})
    sess.close()
    return nll,disc_prob

def figure_pdf_ll(main_dir,res_epoch,nb_mix,nb_res_blocks,ch_dir_aug,ch_dir_noaug,save_path):
    bg,bg_overlaed = create_aug_bg(main_dir)
    nll_aug_text,disc_prob_aug_text = extract_pred(res_epoch,nb_mix,nb_res_blocks,ch_dir_aug,noise,bg_overlaed)
    nll_noaug_text, disc_prob_noaug_text = extract_pred(res_epoch, nb_mix, nb_res_blocks, ch_dir_noaug, noise, bg_overlaed)
    nll_aug_notext, disc_prob_aug_notext = extract_pred(res_epoch, nb_mix, nb_res_blocks, ch_dir_aug, noise, bg)
    nll_noaug_notext, disc_prob_noaug_notext = extract_pred(res_epoch, nb_mix, nb_res_blocks, ch_dir_noaug, noise, bg)
    sc = (64/192)
    coords = [[int(113*sc), int(58*sc)],[int(142*sc),int(81*sc)]]#[[22,4],[24,6],[31, 10]]
    color = ["r","b","y"]
    stem_stile = ["-","--",":"]
    stem_marker = ["x",".","o"]

    fig,ax = plt.subplots()
    #plot org image
    ax.imshow(bg_overlaed[0, 0:64, 0:64],cmap="gray")
    ax.plot(coords[0][1], coords[0][0], marker='v', color=color[0])
    #for i in range(len(coords)):
    #    ax.plot(coords[i][1],coords[i][0], marker='v', color=color[i])
    ax.grid(False)
    #axbig.set_title("Org.")
    ax.set_xticks([])
    ax.set_yticks([])
    plt.savefig(os.path.join(save_path,"org.png"),dpi=300)
    #plot pdf
    label = ["char","no char"]
    fig, ax = plt.subplots()
    prob = {"0":disc_prob_aug_text[0],"1":disc_prob_aug_notext[0]}
    for i in range(len(coords)):
        plot_pixel_pdf(coords[0],prob[str(i)],bg[0,:64,:64],ax,color[i],"*",stem_stile[i],stem_marker[i],label[i])
    ax.legend()
    plt.savefig(os.path.join(save_path, "pdf_with_vs_without_text_aug.png"), dpi=300)
    fig, ax = plt.subplots()
    prob = {"0": disc_prob_noaug_text[0], "1": disc_prob_noaug_notext[0]}
    #plot pdf
    for i in range(len(coords)):
        plot_pixel_pdf(coords[0], prob[str(i)], bg[0, :64, :64], ax, color[i], "*", stem_stile[i],
                       stem_marker[i], label[i])
    ax.legend()
    #fig.tight_layout()
    plt.savefig(os.path.join(save_path,"pdf_with_vs_without_text_noaug.png"),dpi=300)
    #plot prob map
    #ax.set_ylabel('With text aug.')
    #ax.set_ylabel('Without text aug.')
    fig, ax = plt.subplots()
    plt_disc_l(ax, nll_aug_text, coords, color)
    ax.plot(coords[0][1], coords[0][0], marker='v', color=color[0])
    plt.savefig(os.path.join(save_path, "text_aug_likel.png"),dpi=300)
    fig, ax = plt.subplots()
    plt_disc_l(ax, nll_noaug_text, coords, color)
    ax.plot(coords[0][1], coords[0][0], marker='v', color=color[0])
    plt.savefig(os.path.join(save_path, "notext_aug_likel.png"),dpi=300)
    #fig.tight_layout()
    #grid = plt.GridSpec(2, 3)
    #create_subtitle(fig, grid[0, ::], 'With text aug.')
    #create_subtitle(fig, grid[1, ::], 'Without text aug.')

def create_subtitle(fig: plt.Figure, grid: SubplotSpec, title: str):
    "Sign sets of subplots with title"
    row = fig.add_subplot(grid)
    # the '\n' is important
    row.set_title(f'{title}\n', fontweight='semibold')
    # hide subplot
    row.set_frame_on(False)
    row.axis('off')

def plt_disc_l(ax,nll,coords,color):
    ax.set_xticks([])
    ax.set_yticks([])
    ax.imshow(nll_to_prob(nll[0, :64, :64]), cmap="gray", vmax=0.2,
                     vmin=0.0)
    #ax.set_title("Prob. map")
    #for i in range(len(coords)):
    #    ax.plot(coords[i][1], coords[i][0], marker='v', color=color[i])

def plot_smooth_l(smooth_loss,contr,coords,color):
    plt.figure()
    plt.imshow(smoothll_to_prob(smooth_loss[0,:64,:64]), cmap="gray", vmax=np.max(smoothll_to_prob(smooth_loss[0,:64,:64])), vmin=0.5)
    plt.title("Smooth LL, ut={}".format(2.*contr-1))
    for i in range(len(coords)):
        plt.plot(coords[i][1], coords[i][0], marker='v', color=color[i])


def plot_pixel_pdf(coord,pdf,im,ax,color,val_marker,stem_stile,stem_marker,label):
    ax.stem(np.arange(len(pdf)), pdf[:, coord[0], coord[1],0], label=label, linefmt='{}{}'.format(color,stem_stile), markerfmt="{}{}".format(color,stem_marker))
    ax.plot(im[coord[0], coord[1]], 0, marker=val_marker, color=color)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.set_xlabel('intensity, digital counts',loc = "center")
    ax.set_ylabel('Likelihood')
    #ax.set_title("Pdf")



if __name__=="__main__":
    main_dir =  r"C:\Data\PhD\bss_autoreg_palimpsest"
    ch_dir_aug = os.path.join(main_dir,r"training\generators\background\text_aug_exp_Greek960\pixelcnn-gray-2022-12-23-19-08_with_text_aug")
    ch_dir_noaug = os.path.join(main_dir,
                              r"training\generators\background\text_aug_exp_Greek960\pixelcnn-gray-2022-12-21-20-32_without_text_aug")
    ch_step_dict= {0: 40, 0.01: 80, 0.1: 120, 0.2: 160,
     0.3: 200, 0.4: 240, 0.5: 280, 0.6: 320,
     0.7: 360, 0.8: 400, 0.9: 440, 1.0: 480, 1.2: 520, 1.4: 560, 1.6: 600}
    noise = 0.1
    nb_mixtures=3
    nb_res_blocks=3
    save_path = os.path.join(main_dir,r"training\generators\background\text_aug_exp_Greek960\text_aug_figures")
    #ll_vs_ut_contr_on_bg(main_dir, noises, nb_mixtures, nb_res_blocks, checkpoint_dir)
    figure_pdf_ll(main_dir,ch_step_dict[noise],nb_mixtures,nb_res_blocks,ch_dir_aug,ch_dir_noaug,save_path)
